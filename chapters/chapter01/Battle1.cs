using Godot;

public class Battle1 : Battle
{
	string battleDialog = "battleDialog";

	protected override bool hasLost()
	{
		foreach(ActorCore actor in currentActors)
		{
			if(actor.gang.index == 2 && actor.isAlive())
			{
				return false;
			}
		}
		return true;
	}

	protected override bool callDialogues(ActorCore actor, ActorCore target, string action, int enduranceChange = 0)
	{
		if(actor == playerActors["Tara"] && target == playerActors["Alaitz"] && action == "push" && battleDialog != "" && playerActors["Alaitz"].endurance + enduranceChange <= 0)
		{
			Setting.CallDeferred("interpolateBeforeActionDialogues", battleDialog, actor.Controller);
			battleDialog = "";
			return true;
		}
		return false;
	}

	protected override void executeDialoguesCallbacks(string interpolatedDialoguesKey)
	{
		endBattle(false);
	}

	protected override void endBattle(bool hasWon)
	{
		completionState = CompletionState.Completed;
		UI.Call("hide");

		endTimer = new Timer();
		endTimer.OneShot = true;
		endTimer.WaitTime = 2;
		endTimer.Connect("timeout", Setting, "endMode");
		AddChild(endTimer);
		endTimer.Start();
	}
}
