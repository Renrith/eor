public class Battle2_0 : Battle
{
	int gateOpeningThreshold = 5;

	string battleDialog = "battleDialog";

	protected override bool hasLost()
	{
		if(playerActors["Alaitz"].gang.index == (int)Factions.names.NoGangPlayer && playerActors["Alaitz"].isAlive() && playerActors["Tara"].isAlive())
		{
			return false;
		}
		return true;
	}

	protected override bool hasWon()
	{
		int currentFighters = 0;
		foreach(ActorCore actor in currentActors)
		{
			if(actor.isAlive())
			{
				if(actor.gang == Factions.allFactions[(int)Factions.names.OnTheEdge] && actor != playerActors["Tara"])
				{
					return false;
				}
				currentFighters++;
			}
		}
		if(currentFighters < gateOpeningThreshold)
		{
			return true;
		}
		return false;
	}

	protected override bool callDialogues(ActorCore actor, ActorCore target, string action, int enduranceChange = 0)
	{
		if(actor == playerActors["Tara"]
		&& target != null
		&& target.gang == playerActors["Tara"].gang
		&& battleDialog != ""
		&& playerActors["Tara"].turnOrder > playerActors["Tara"].turnRate)
		{
			Setting.CallDeferred("interpolateBeforeActionDialogues", battleDialog, actor.Controller);
			battleDialog = "";
			playerActors["Tara"].Controller.Set("shouldBecomePlayer", true);
			return true;
		}
		return false;
	}
}
