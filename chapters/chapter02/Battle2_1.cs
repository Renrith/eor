using Godot;
public class Battle2_1 : Battle
{
	string recruitDialog = "recruitDialog";
	string pushDialog = "pushDialog";
	string postRecruitDialog = "postRecruitDialog";

	protected override bool hasLost()
	{
		if(playerActors["Tara"].isAlive())
		{
			return false;
		}
		return true;
	}

	protected override bool callDialogues(ActorCore actor, ActorCore target, string action, int enduranceChange = 0)
	{
		if(actor == playerActors["Alaitz"] && target != null && (action == "recruit" && recruitDialog != "") || (action == "push" && pushDialog != ""))
		{
			if(action == "recruit")
			{
				Setting.CallDeferred("interpolateBeforeActionDialogues", recruitDialog, actor.Controller);
				recruitDialog = "";
			}
			else
			{
				Setting.CallDeferred("interpolateBeforeActionDialogues", pushDialog, actor.Controller);
				pushDialog = "";
			}

			return true;
		}
		return false;
	}

	protected override void executeDialoguesCallbacks(string interpolatedDialoguesKey)
	{
		if(interpolatedDialoguesKey == "postRecruitDialog")
		{
			base.endActorTurn();
		}
	}

	protected override void endActorTurn()
	{
		if(recruitDialog == "" && postRecruitDialog != "")
		{
			playerActors["Tara"].gang = Factions.allFactions[(int)Factions.names.NoGangPlayer];
			Setting.CallDeferred("interpolateBetweenTurnsDialogues", postRecruitDialog, currentActor.Controller);
			postRecruitDialog = "";
		}
		else
		{
			base.endActorTurn();
		}
	}

	protected override void endBattle(bool hasWon)
	{
		UI.Call("hide");

		if(hasWon)
		{
			completionState = CompletionState.Completed;

			endTimer = new Timer();
			endTimer.OneShot = true;
			endTimer.WaitTime = 0.1F;
			endTimer.Connect("timeout", Setting, "endMode");
			AddChild(endTimer);
			endTimer.Start();
		}
		else
		{
			UI.Call("onBattleOver", hasWon);
			completionState = CompletionState.Ended;
		}
	}
}
