using System;

public class Battle4 : Battle
{
	private const int minX = 25;
	private const int maxX = 27;
	private const int minY = 6;
	private const int maxY = 8;

	private const int maxShareGangInfoDistance = 10;
	private const int turnsBeforeReinforcements = 4;
	private int turnsSpanned = 0;

	protected override bool hasWon()
	{
		if(playerActors["Alaitz"].tile.x >= minX && playerActors["Alaitz"].tile.x <= maxX && playerActors["Alaitz"].tile.y >= minY && playerActors["Alaitz"].tile.y <= maxY
			&& playerActors["Tara"].tile.x >= minX && playerActors["Tara"].tile.x <= maxX && playerActors["Tara"].tile.y >= minY && playerActors["Tara"].tile.y <= maxY
			&& playerActors["Nalke"].tile.x >= minX && playerActors["Nalke"].tile.x <= maxX && playerActors["Nalke"].tile.y >= minY && playerActors["Nalke"].tile.y <= maxY)
		{
			return true;
		}
		return false;
	}

	protected override void endActorTurn()
	{
		if(turnsSpanned < turnsBeforeReinforcements && (bool)conditionsList["Ch4HaveEnemiesBeenClearedBeforeReinforcements"] == false)
		{
			bool haveEnemiesBeenClearedBeforeReinforcements = true;
			foreach(ActorCore actor in currentActors)
			{
				if(actor.isAlive() && Factions.getAlliance((int)Factions.names.AlaitzsGang, actor.gang.index) == AllianceType.Enemy)
				{
					haveEnemiesBeenClearedBeforeReinforcements = false;
					break;
				}
			}
			if(haveEnemiesBeenClearedBeforeReinforcements)
			{
				conditionsList["Ch4HaveEnemiesBeenClearedBeforeReinforcements"] = true;
			}
		}
		if(currentActor == playerActors["Alaitz"])
		{
			turnsSpanned++;
		}
		if((turnsSpanned == turnsBeforeReinforcements || (bool)conditionsList["Ch4HaveEnemiesBeenClearedBeforeReinforcements"] == true) && (bool)conditionsList["Ch4HaveReinforcementsBeenTriggered"] == false)
		{
			triggerReinforcements();
			conditionsList["Ch4HaveReinforcementsBeenTriggered"] = true;
			int NalkeToAlaitzDistance = Map.getPathBetweenPoints(playerActors["Nalke"].tile, playerActors["Alaitz"].tile, playerActors["Nalke"]).Count;
			int NalkeToTaraDistance = Map.getPathBetweenPoints(playerActors["Nalke"].tile, playerActors["Tara"].tile, playerActors["Nalke"]).Count;
			if(Math.Min(NalkeToAlaitzDistance, NalkeToTaraDistance) < maxShareGangInfoDistance)
			{
				conditionsList["Ch4HasNalkeSharedGangInfo"] = true;
			}
			Setting.CallDeferred("interpolateBetweenTurnsDialogues", "reinforcementsDialog", currentActor.Controller);
			return;
		}
		base.endActorTurn();
	}

	protected override void executeDialoguesCallbacks(string interpolatedDialoguesKey)
	{
		switch(interpolatedDialoguesKey)
		{
			case "meetDialog":
				shouldMeet = false;
				conditionsList["Ch4HasReunionTakenPlace"] = true;
				break;
			case "reinforcementsDialog":
				shouldMeet = false;
				base.endActorTurn();
				break;
		}
	}

	protected override void restartBattle()
	{
		turnsSpanned = 0;
		conditionsList["Ch4HaveEnemiesBeenClearedBeforeReinforcements"] = false;
		conditionsList["Ch4HaveReinforcementsBeenTriggered"] = false;
		conditionsList["Ch4HasNalkeSharedGangInfo"] = false;
		base.restartBattle();
	}

	public override void _Ready()
	{
		base._Ready();
		conditionsList["Ch4HaveReinforcementsBeenTriggered"] = false;
		shouldMeet = true;
	}
}
