using System.Collections.Generic;

public class Battle7 : Battle
{
	protected override void endActorTurn()
	{
		if(currentActor.lastActionOutcome != null)
		{
			foreach(ActionOutcome outcome in currentActor.lastActionOutcome)
			{
				foreach(AITriggerableCore triggerableActor in triggerableActors)
				{
					if(triggerableActor == outcome.target)
					{
						foreach(AITriggerableCore actor in triggerableActors)
						{
							actor.hasBeenTriggered = true;
						}
						break;
					}
				}
			}
		}
		base.endActorTurn();
	}

	protected override void restartBattle()
	{
		foreach(AITriggerableCore triggerableActor in triggerableActors)
		{
			triggerableActor.hasBeenTriggered = false;
		}
		base.restartBattle();
	}

	public Battle7()
	{
		triggerableActors = new List<AITriggerableCore>();
	}

	public override void _Ready()
	{
		base._Ready();
	}
}
