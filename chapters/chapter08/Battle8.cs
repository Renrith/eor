public class Battle8 : Battle
{
	int gateOpeningThreshold = 8;
	ActorCore Naimel;

	protected override bool hasLost()
	{
		int currentFighters = 0;
		foreach(ActorCore actor in currentActors)
		{
			if(!actor.isAlive())
			{
				if(actor.isPlayer)
				{
					return true;
				}
			}
			else
			{
				currentFighters++;
			}
		}
		if(currentFighters < gateOpeningThreshold && Naimel.gang == Factions.allFactions[(int)Factions.names.NePlusUltra])
		{
			return true;
		}
		return false;
	}

	protected override bool hasWon()
	{
		int currentFighters = 0;
		foreach(ActorCore actor in currentActors)
		{
			if(actor.isAlive())
			{
				currentFighters++;
			}
		}
		if(currentFighters < gateOpeningThreshold && Naimel.gang != Factions.allFactions[(int)Factions.names.NePlusUltra])
		{
			return true;
		}
		return false;
	}

	public override void _Ready()
	{
		base._Ready();
	}
}
