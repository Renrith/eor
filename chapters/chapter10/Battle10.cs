using System.Collections.Generic;

public class Battle10 : Battle
{
	static int minX = 25;
	static int maxX = 27;
	static int minY = 6;
	static int maxY = 8;

	protected override bool hasLost()
	{
		if(!kidnapTarget.isAlive())
		{
			return true;
		}
		else if(kidnapTarget.tile.x >= minX && kidnapTarget.tile.x <= maxX && kidnapTarget.tile.y >= minY && kidnapTarget.tile.y <= maxY)
		{
			return true;
		}
		return false;
	}

	protected override bool hasWon()
	{
		return false;
	}

	private Godot.Collections.Array gdGetDefeatData()
	{
		Godot.Collections.Array defeatData = new Godot.Collections.Array();
		defeatData.Add("Naimel");
		defeatData.Add((int)DefeatReason.Other);
		return defeatData;
	}

	public override void _Ready()
	{
		base._Ready();
		kidnapTarget = playerActors["Naimel"];
		CustomVector cell = new CustomVector(26, 7);
		kidnapTile = Map.customMap[cell];
	}
}
