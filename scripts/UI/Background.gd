extends TextureRect

var initialRectScale: Vector2 = Vector2.ZERO

func resize():
	var newResolution = OS.get_window_size()
	rect_size = newResolution / initialRectScale

func _ready():
	var WorldViewport = get_tree().get_root()
	WorldViewport.connect("size_changed", self, "resize")
	initialRectScale = rect_scale
	call_deferred("resize")
