extends "res://scripts/UI/components/Label.gd"

var baseSpeed: float = 0.027
var userSpeedMultiplier: float = 0.65
var userSpeed: float = 0.35

var angrySpeedMultiplier: float = 0.7
var annoyedSpeedMultiplier: float = 0.85
var eagerSpeedMultiplier: float = 0.9
var neutralSpeedMultiplier: float = 1
var sadSpeedMultiplier: float = 1.15
var scaredSpeedMultiplier: float = 0.85
var thoughtSpeedMultiplier: float = 1.2

func getEmotionSpeed(emotion: String) -> float:
	match emotion:
		"angry":
			return angrySpeedMultiplier
		"annoyed":
			return annoyedSpeedMultiplier
		"eager":
			return eagerSpeedMultiplier
		"sad":
			return sadSpeedMultiplier
		"scared":
			return scaredSpeedMultiplier
		"thought":
			return thoughtSpeedMultiplier
		_:
			return neutralSpeedMultiplier

func setText(newText: String, emotion: String = "") -> void:
	text = newText
	percent_visible = 0
	
	var emotionSpeedMultiplier = getEmotionSpeed(emotion)
	var speed: float = (baseSpeed
	* (userSpeedMultiplier + userSpeed)
	* newText.length()
	* emotionSpeedMultiplier)
	$Tween.interpolate_property(self, "percent_visible", 0, 1, speed)
	$Tween.start()
	
func skipAnimation() -> void:
	$Tween.remove_all()
	percent_visible = 1

func _ready() -> void:
	$Tween.connect("tween_all_completed", get_owner(), "onTextAnimationFinished")
