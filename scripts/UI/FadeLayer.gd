extends CanvasLayer

func fadeIn(callback: FuncRef = null) -> void:
	$Fade/Tween.callback = callback
	$Fade/Tween.fadeIn()

func fadeOut(callback: FuncRef = null) -> void:
	$Fade/Tween.callback = callback
	$Fade/Tween.fadeOut()

func _ready():
	$Fade.color = Colors.baseColor
	$Fade.modulate = 0
