extends Tween

var callback: FuncRef

const fadeInTime := 0.75
const fadeOutTime := 0.5

const fadeInModulate := Color(Colors.baseColor.r, Colors.baseColor.g, Colors.baseColor.b, 0)
const fadeOutModulate := Colors.baseColor

var Fade: ColorRect

func fadeIn() -> void:
	interpolate_property(Fade, "modulate", fadeOutModulate, fadeInModulate, fadeInTime)
	start()

func fadeOut() -> void:
	interpolate_property(Fade, "modulate", fadeInModulate, fadeOutModulate, fadeOutTime)
	start()

func endFade() -> void:
	if callback != null && callback.is_valid():
		callback.call_func()
		callback = null

func _ready() -> void:
	Fade = get_parent()
	connect("tween_all_completed", self, "endFade")
