extends Tween

onready var Notification: PanelContainer = get_parent()
onready var timer: Timer = get_node("Timer")

const showTime := 0.5
const waitTime := 5
const hideTime := 0.25

enum phases {
	show,
	wait,
	hide,
}
var currentPhase: int = phases.hide

func show() -> void:
	interpolate_property(Notification, "modulate", Notification.modulate, Color.white, showTime)
	start()
	currentPhase = phases.show

func hide() -> void:
	interpolate_property(Notification, "modulate", Notification.modulate, Colors.transparentColor, hideTime)
	start()
	currentPhase = phases.hide

func endTransition() -> void:
	match currentPhase:
		phases.show:
			timer.set_wait_time(waitTime)
			timer.start()
			currentPhase = phases.wait

func _ready() -> void:
	connect("tween_all_completed", self, "endTransition")
	timer.connect("timeout", self, "hide")
	timer.set_one_shot(true)
