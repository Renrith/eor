extends CanvasLayer

const sideConversationString := "%s conversation unlocked"

func showSideConversation(scene: Dictionary) -> void:
	$MarginContainer/Panel/Label.text = sideConversationString % scene.sideConversationTitle
	$MarginContainer/Panel/Tween.show()
