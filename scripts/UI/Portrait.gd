extends TextureRect

enum portraitMode {
	battle,
	dialog,
}
var currentPortraitMode: int = portraitMode.dialog

const battlePositionVector := Vector2(150, 0)
var initialResolution: Vector2
var _initialPosition: Vector2
var initialPosition: Vector2 setget, getInitialPosition

func getInitialPosition() -> Vector2:
	match currentPortraitMode:
		portraitMode.battle:
			return _initialPosition - battlePositionVector
		_:
			return _initialPosition

func setPortraitMode(newPortraitMode: int) -> void:
	currentPortraitMode = newPortraitMode
	call_deferred("setPosition")

func setTexture(newTexture = null):
	.set_texture(newTexture)

func resize():
	var newResolution = OS.get_window_size()
	var yScale = newResolution.y / initialResolution.y
	
	rect_scale = Vector2(yScale, yScale)
	call_deferred("setPosition")

func setPosition():
	rect_position.x = getInitialPosition().x * rect_scale.x
	rect_position.y = getInitialPosition().y * rect_scale.y

func _ready():
	initialResolution = get_parent().size / rect_scale
	_initialPosition = rect_position / rect_scale
	
	var WorldViewport = get_tree().get_root()
	WorldViewport.connect("size_changed", self, "resize")
	
	call_deferred("resize")
