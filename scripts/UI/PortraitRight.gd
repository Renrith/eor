extends "res://scripts/UI/Portrait.gd"

var initialScale: Vector2
var _initialXVis: int
var initialXVis: int setget, getInitialXVis

func getInitialXVis() -> int:
	match currentPortraitMode:
		portraitMode.battle:
			return _initialXVis - int(battlePositionVector.x * initialScale.x)
		_:
			return _initialXVis

func setTexture(newTexture = null):
	.set_texture(newTexture)

func setPosition():
	var newXVis: int = getInitialXVis() * rect_scale.x / initialScale.x
	rect_position.x = OS.get_window_size().x - newXVis
	rect_position.y = getInitialPosition().y * rect_scale.y

func _ready():
	var initialRectPosition: Vector2 = rect_position
	initialScale = rect_scale
	_initialXVis = get_parent().size.x - initialRectPosition.x
