extends CanvasLayer

enum modes {
	default,
	build,
	thoughts,
	saving,
	settings,
}
var currentMode: int = modes.default

func setDefaultMode() -> void:
	match currentMode:
		modes.build:
			$Menu/Options/Build.grab_focus()
		modes.thoughts:
			$Menu/Options/Thoughts.grab_focus()
		modes.saving:
			$Menu/Options/Saving.grab_focus()
		modes.settings:
			$Menu/Options/Settings.grab_focus()
	currentMode = modes.default
	$Menu.show()

func setBuildMode() -> void:
	currentMode = modes.build
	$Menu.hide()
	$Build.show()

func setThoughtsMode() -> void:
	currentMode = modes.thoughts
	$Menu.hide()
	$Thoughts.show()

func setSavingMode() -> void:
	currentMode = modes.saving
	$Menu.hide()
	$Saving.show()

func setSettingsMode() -> void:
	$Menu.hide()
	$Settings.show()
	currentMode = modes.settings

func returnToTitle() -> void:
	$Menu.hide()
	Game.returnToTitle()

func grab_focus() -> void:
	$Menu/Options.get_children()[0].grab_focus()

func _input(_event) -> void:
	if currentMode == modes.default:
		if Input.is_action_just_pressed("ui_cancel"):
			queue_free()
			get_tree().get_current_scene().endMode()

func _ready() -> void:
	$Menu/Options/Build.callback = funcref(self, "setBuildMode")
	$Menu/Options/Thoughts.callback = funcref(self, "setThoughtsMode")
	$Menu/Options/Saving.callback = funcref(self, "setSavingMode")
	$Menu/Options/Settings.callback = funcref(self, "setSettingsMode")
	$Menu/Options/Title.callback = funcref(self, "returnToTitle")
	grab_focus()
