extends MarginContainer

enum sideList {
	left,
	right,
	both,
}

export(sideList) var side: int = sideList.both

onready var initialMarginLeft: int = margin_left
onready var initialMarginRight: int = margin_right

func resize():
	var newResolution: Vector2 = OS.get_window_size()
	if newResolution.x / newResolution.y > Globals.maxUISizeRatio:
		var extraWidth = newResolution.x - newResolution.y * Globals.maxUISizeRatio
		match side:
			sideList.left:
				margin_left = initialMarginLeft + extraWidth / 2
			sideList.right:
				margin_left = initialMarginLeft - extraWidth / 2
				margin_right = initialMarginRight - extraWidth / 2
			sideList.both:
				margin_left = initialMarginLeft + extraWidth / 2
				margin_right = initialMarginRight - extraWidth / 2
	else:
		margin_left = initialMarginLeft
		margin_right = initialMarginRight

func _ready() -> void:
	var WorldViewport = get_tree().get_root()
	WorldViewport.connect("size_changed", self, "resize")
	resize()
