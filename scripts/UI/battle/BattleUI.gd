extends CanvasLayer

var DialoguesUI: CanvasLayer
var PortraitLeft: TextureRect
var PortraitRight: TextureRect

func hide():
	$ContextMenu/Container.clearMenu()
	$PlayerMenu.clearMenu()
	$TriangleLeft/Container.clearMenu()
	$ActorDetailsLeft/Nametag.clearMenu()
	clearTriangleRightMenu()

func showTriangleLeftMenu(actor: Node, _camera = null):
	$TriangleLeft/Container.showMenu(actor)
	$ActorDetailsLeft/Nametag.showMenu(actor)
	PortraitLeft.setPortraitMode(PortraitLeft.portraitMode.battle)
	PortraitLeft.texture = actor.battlePortrait
	PortraitLeft.set_visible(true)

func showTriangleRightMenu(placeable: Node):
	if placeable.Core.isActor:
		$TriangleRight/Container.showMenu(placeable)
		$ActorDetailsRight/Nametag.showMenu(placeable)
		PortraitRight.setPortraitMode(PortraitRight.portraitMode.battle)
		PortraitRight.texture = placeable.battlePortrait
		PortraitRight.set_visible(true)

func showPlayerMenu(menuOptions):
	$PlayerMenu.showMenu(menuOptions)

func clearTriangleRightMenu():
	$TriangleRight/Container.clearMenu()
	$ActorDetailsRight/Nametag.clearMenu()
	PortraitRight.set_visible(false)
	PortraitRight.texture = null

func clearPlayerMenu():
	$PlayerMenu.clearMenu()
	$TriangleLeft/Container.clearHighlight()

func clearContextMenu(_position):
	$ContextMenu/Container.clearMenu(true)
	clearTriangleRightMenu()

func onVisualTurnStart(actor: Node):
	$ContextMenu/Container.clearMenu()
	$TriangleLeft/Container.clearHighlight()
	showTriangleLeftMenu(actor)
	clearTriangleRightMenu()

func onActionOutcome(actor: Node, targetTile, action, hasAdditionalTargets, actionOutcome):
	$PlayerMenu.clearMenu()
	$TriangleLeft/Container.set_visible(true)
	$ContextMenu/Container.showMenu(actor, targetTile, action, hasAdditionalTargets, actionOutcome)
	if actionOutcome != null:
		showTriangleRightMenu(actionOutcome.target)
	$TriangleLeft/Container.highlightAction(action)
	$TriangleRight/Container.highlightAction(action, false)

func onPlayerTurnEnd():
	$ContextMenu/Container.clearMenu()
	$TriangleLeft/Container.clearHighlight()
	$TriangleRight/Container.clearHighlight()
	$PlayerMenu.clearMenu()

func onBattleOver(hasWon: bool) -> void:
	hide()
	if hasWon:
		DialoguesUI.handleVictory()
		$VictoryMenu.showMenu()
		$WinLoseAudio.win()
	else:
		DialoguesUI.handleDefeat()
		$RetryMenu/Container.showMenu()
		$WinLoseAudio.lose()

func handlePlayerMenu() -> String:
	var action: String = $PlayerMenu.handleMenu()
	$TriangleLeft/Container.highlightAction(action.to_lower())
	return action

func _ready() -> void:
	DialoguesUI = get_node("/root/Viewports/UIViewport/DialoguesUILayer")
	PortraitLeft = get_node("/root/Viewports/GraphicsViewport/PortraitLeft")
	PortraitRight = get_node("/root/Viewports/GraphicsViewport/PortraitRight")
