extends ColorRect

var Header: ColorRect
var Title: Label
var DecoLeft: TextureRect
var DecoRight: TextureRect

var AttackCurrent: Label
var AttackOutcome: Label
var AttackPower: Label
var AttackTotal: Label

var PushCurrent: Label
var PushArrow: TextureRect
var PushOutcome: Label
var PushLabel: Label
var PushEndurance: VBoxContainer
var PushEnduranceStatus: Label
var PushEnduranceChange: Label
var PushEnduranceLabel: Label
var PushEnergy: VBoxContainer
var PushEnergyStatus: Label
var PushEnergyChange: Label
var PushEnergyLabel: Label
var PushHold: VBoxContainer

var ArrowLeft: TextureRect
var ArrowRight: TextureRect

func showMenu(actor, targetTile, action, hasAdditionalTargets, actionOutcome = null):
	match action:
		"attack":
			var targetCore = actionOutcome.target.Core
			var newEndurance = max(targetCore.gdGetEndurance() + actionOutcome.enduranceChange, 0)
			AttackCurrent.text = String(targetCore.gdGetEndurance())
			AttackOutcome.text = String(newEndurance)
			AttackPower.text = "-" + String(targetCore.gdGetEndurance() - newEndurance) + "!"
			AttackTotal.text = "/" + String(targetCore.maxEndurance)
			$Attack.show()
			$Push.hide()
		
		"push":
			var hasFallDamage = actionOutcome.enduranceChange < 0
			var tile = targetTile.occupant.gdGetTile()
			var hasBorderDistanceChanged = tile != null && tile.minBorderDistance != actionOutcome.newBorderDistance
			var hasEnergyChange = actionOutcome.energyChange != 0
			var hasHoldTile = actionOutcome.holdTile != null
			
			if actionOutcome.newBorderDistance != 999:
				PushOutcome.text = String(actionOutcome.newBorderDistance + 1)
			else:
				PushOutcome.text = "-"
			
			if hasBorderDistanceChanged:
				var borderDistance: int = tile.minBorderDistance
				if borderDistance != 999:
					PushCurrent.text = String(borderDistance + 1)
				else:
					PushCurrent.text = "-"
				PushArrow.show()
				PushCurrent.show()
			else:
				PushArrow.hide()
				PushCurrent.hide()
			
			if hasFallDamage:
				PushEnduranceStatus.text = "Fall!"
				PushEnduranceChange.text = String(actionOutcome.enduranceChange) + " End."
				PushEnduranceLabel.text = String(max(targetTile.occupant.endurance + actionOutcome.enduranceChange, 0))
				PushEndurance.show()
			else:
				PushEndurance.hide()
			
			if hasHoldTile:
				PushHold.show()
			else:
				PushHold.hide()
			
			if hasEnergyChange:
				if targetTile.occupant.energy - actionOutcome.energyChange <= 0:
					PushEnergyStatus.text = "Undo grip!"
					PushEnergyLabel.text = "0"
					PushEnergyChange.hide()
				else:
					PushEnergyStatus.text = "Grip"
					PushEnergyChange.text = String(actionOutcome.energyChange) + " Ener."
					PushEnergyLabel.text = String(min(max(targetTile.occupant.energy + actionOutcome.energyChange, 0), targetTile.occupant.maxEnergy))
					PushEnergyChange.show()
				PushEnergy.show()
			else:
				PushEnergy.hide()
			
			$Attack.hide()
			$Push.show()
		
		"capture":
			$Attack.hide()
			$Push.hide()
	
	Title.text = action.capitalize()
	
	if actionOutcome != null:
		set_visible(true)
		setArrowVisibility(hasAdditionalTargets)
	else:
		Header.set_visible(true)

func clearMenu(isPlayerAction = false):
	if isPlayerAction == true:
		$Audio.cancel()
	set_visible(false)

func setArrowVisibility(value):
	ArrowLeft.visible = value
	ArrowRight.visible = value

func set_visible(value: bool) -> void:
	.set_visible(value)
	
	Header.visible = value
	DecoLeft.visible = value
	DecoRight.visible = value
	
	if value == false:
		PushEndurance.hide()
		PushEnergy.hide()
		PushHold.hide()
	
	setArrowVisibility(value)

func _ready():
	Header = get_node("../../ContextMenuTop")
	Title = Header.get_node("Title")
	DecoLeft = get_node("../../ContextMenuDecoLeft")
	DecoRight = get_node("../../ContextMenuDecoRight")
	
	var AttackContainer: HBoxContainer = get_node("Attack/Container")
	AttackCurrent = AttackContainer.get_node("Current")
	AttackOutcome = AttackContainer.get_node("Outcome")
	var AttackArrow: TextureRect = AttackContainer.get_node("Arrow")
	AttackPower = AttackArrow.get_node("Power")
	AttackTotal = AttackContainer.get_node("Total")
	
	var PushContainer: HBoxContainer = get_node("Push/Center/Container")
	PushCurrent = PushContainer.get_node("Current")
	PushArrow = PushContainer.get_node("Arrow")
	PushOutcome = PushContainer.get_node("Outcome")
	PushLabel = get_node("Push/Center/Label")
	
	PushEndurance = get_node("../../PushEndurance")
	PushEnduranceStatus = PushEndurance.get_node("Status")
	PushEnduranceChange = PushEndurance.get_node("Change")
	PushEnduranceLabel = PushEndurance.get_node("Endurance/Label")
	var PushEnduranceIcon = PushEndurance.get_node("Endurance/Icon")
	
	PushEnergy = get_node("../../PushEnergy")
	PushEnergyStatus = PushEnergy.get_node("Status")
	PushEnergyChange = PushEnergy.get_node("Change")
	PushEnergyLabel = PushEnergy.get_node("Energy/Label")
	var PushEnergyIcon = PushEnergy.get_node("Energy/Icon")
	
	PushHold = get_node("../../PushHold")
	var PushHoldChange = PushHold.get_node("Change")
	
	ArrowLeft = get_node("../../ArrowLeft")
	ArrowRight = get_node("../../ArrowRight")
	
	color = Colors.panelColor
	Header.color = Colors.panelColor
	DecoLeft.modulate = Colors.panelColor
	DecoRight.modulate = Colors.panelColor
	
	var secondaryTextColor:= Color(Colors.textColor.r, Colors.textColor.g, Colors.textColor.b, 0.5)
	var arrowColor:= Color(Colors.primaryColor.r, Colors.primaryColor.g, Colors.primaryColor.b, 0.4)
	
	ArrowLeft.modulate = arrowColor
	ArrowRight.modulate = arrowColor
	
	AttackCurrent.modulate = Colors.primaryColor
	AttackOutcome.modulate = Colors.negativeTextColor
	AttackArrow.self_modulate = secondaryTextColor
	AttackPower.modulate = Colors.textColor
	AttackTotal.modulate = secondaryTextColor
	
	PushCurrent.modulate = Colors.primaryColor
	PushOutcome.modulate = Colors.negativeTextColor
	PushArrow.modulate = secondaryTextColor
	PushLabel.modulate = secondaryTextColor
	PushEnduranceStatus.modulate = Colors.textColor
	PushEnduranceChange.modulate = secondaryTextColor
	PushEnduranceLabel.modulate = Colors.negativeTextColor
	PushEnduranceIcon.modulate = secondaryTextColor
	PushEnergyStatus.modulate = Colors.textColor
	PushEnergyChange.modulate = secondaryTextColor
	PushEnergyLabel.modulate = Colors.negativeTextColor
	PushEnergyIcon.modulate = secondaryTextColor
	PushHoldChange.modulate = secondaryTextColor
	
	Title.add_color_override("font_color", Colors.primaryColor)
	
	set_visible(false)
