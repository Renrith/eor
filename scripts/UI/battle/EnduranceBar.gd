extends ColorRect

var Actor: Node2D

func hide():
	set_visible(false)

func show():
	set_visible(true)

func updateBar():
	var newSize: int = float(Actor.Core.gdGetEndurance()) / float(Actor.Core.maxEndurance) * 30
	$Bar.rect_size = Vector2(newSize, $Bar.rect_size.y)

func _ready():
	Actor = get_parent().get_parent()
	color = Colors.panelColor
