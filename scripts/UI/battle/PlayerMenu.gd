extends NinePatchRect

var Container: Control

var colorVisible: Color
var colorInvisible: Color = Color(1, 1, 1, 0)
var colorSelected: Color

var minSize: int = 35
var sizeStep: int = 49

var labelWidths: Array = []

var lineMargin: Vector2 = Vector2(60, 60)

var selectedAction: Label

func hide():
	material.set_shader_param("color", colorInvisible)
	material.set_shader_param("outlineColor", colorInvisible)
	$Line.modulate = colorInvisible

func show():
	material.set_shader_param("color", colorVisible)
	material.set_shader_param("outlineColor", colorSelected)
	$Line.modulate = colorSelected

func showMenu(actions):
	for action in actions:
		var menuOption: Label = Label.new()
		menuOption.text = action
		menuOption.add_font_override("font", Fonts.UIFont32)
		Container.add_child(menuOption)
		labelWidths.push_back(menuOption.rect_size)
	selectedAction = Container.get_children()[0]
	handleMenuCursor()
	rect_size.y = sizeStep * actions.size() + minSize
	show()

func clearMenu():
	hide()
	selectedAction = null
	for action in Container.get_children():
		action.queue_free()
	labelWidths = []

func handleMenu():
	if Input.is_action_just_pressed("ui_up"):
		handleMenuCursor("ui_up")
		$Line/Audio.navigate()
	elif Input.is_action_just_pressed("ui_down"):
		handleMenuCursor("ui_down")
		$Line/Audio.navigate()
	elif Input.is_action_just_pressed("ui_accept"):
		$Line/Audio.select()
		return selectedAction.text
	elif Input.is_action_just_pressed("ui_cancel"):
		$Line/Audio.cancel()
		return "cancel"
	
	return ""

func handleMenuCursor(action = ""):
	selectedAction.add_font_override("font", Fonts.UIFont32)
	var actionsArray = Container.get_children()
	var selectedActionIndex: int
	
	match action:
		"ui_up":
			selectedActionIndex = actionsArray.find(selectedAction) - 1
			selectedAction = Container.get_children()[selectedActionIndex]
		"ui_down":
			selectedActionIndex = actionsArray.find(selectedAction) + 1
			if selectedActionIndex >= actionsArray.size():
				selectedActionIndex = 0
	
	selectedAction = Container.get_children()[selectedActionIndex]
	selectedAction.add_font_override("font", Fonts.UIFont32OutlinePrimary)
	
	$Line.points[0] = Vector2(selectedAction.rect_position.x, selectedAction.rect_position.y) + lineMargin
	$Line.points[1] = Vector2(selectedAction.rect_position.x + labelWidths[selectedActionIndex].x + 2, selectedAction.rect_position.y) + lineMargin

func _ready():
	colorVisible = Colors.panelColor
	colorSelected = Colors.primaryColor
	
	material.shader = material.shader.duplicate()
	material.set_shader_param("width", 7)
	
	Container = get_node("Actions")
	
	hide()
