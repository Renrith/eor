extends VBoxContainer

var DialoguesUI: CanvasLayer

func enableModeChange():
	$RetryButton.disabled = false
	$TitleButton.disabled = false

func showMenu() -> void:
	$RetryButton.grab_focus()
	
	var timer := Timer.new()
	timer.one_shot = true
	timer.connect("timeout", self, "enableModeChange")
	timer.wait_time = 0.5
	add_child(timer)
	timer.start()
	
	show()

func hide() -> void:
	$RetryButton.disabled = true
	$TitleButton.disabled = true
	DialoguesUI.hide()
	.hide()

func restartBattle() -> void:
	get_tree().get_current_scene().Battle.restartBattle()
	get_node("/root/Viewports/SettingViewport/Map/YSort/Cursor").repaintEnemyRange()
	hide()

func returnToTitle() -> void:
	hide()
	Game.returnToTitle()

func _ready() -> void:
	DialoguesUI = get_node("/root/Viewports/UIViewport/DialoguesUILayer")
	
	$RetryButton.callback = funcref(self, "restartBattle")
	$TitleButton.callback = funcref(self, "returnToTitle")
	$RetryButton.disabled = true
	$TitleButton.disabled = true
