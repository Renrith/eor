extends TextureRect

const altColorBase := Color(0.75, 0.75, 0.75, 0.9)
var altColor: Color

var skillOpacity = 0.7

var Highlight: TextureRect
var Defense: TextureRect
var Frame: TextureRect

var EnduranceIcon: TextureRect
var EnergyIcon: TextureRect

var AttackIcon: TextureRect
var PushIcon: TextureRect

var AttackSkillIcon: TextureRect
var PushSkillIcon: TextureRect
var ActionSkillIcon: TextureRect

var Place: Label
var PlaceSuffix: Label
var Endurance: Label
var Energy: Label

var Attack: Label
var Push: Label
var AttackDef: Label
var PushDef: Label
var Bracelets: Label

func highlightAction(action, active = true):
	clearHighlight()
	
	match action:
		"attack":
			AttackIcon.modulate = Color.white
			AttackIcon.material = preload("res://shaders/UIOutline.tres").duplicate()
			AttackIcon.material.set_shader_param("color", Colors.textColor)
			AttackIcon.material.set_shader_param("outlineColor", Colors.outlineColor)
			AttackIcon.material.set_shader_param("width", 1)
			
			AttackSkillIcon.modulate = Color.white
			
			if active:
				Attack.add_font_override("font", Fonts.UIFont32Outline)
			else:
				AttackDef.add_font_override("font", Fonts.UIFont16Outline)
		
		"push":
			PushIcon.modulate = Color.white
			PushIcon.material = preload("res://shaders/UIOutline.tres").duplicate()
			PushIcon.material.set_shader_param("color", Colors.textColor)
			PushIcon.material.set_shader_param("outlineColor", Colors.outlineColor)
			PushIcon.material.set_shader_param("width", 1)
			
			PushSkillIcon.modulate = Color.white
			
			if active:
				Push.add_font_override("font", Fonts.UIFont32Outline)
			else:
				PushDef.add_font_override("font", Fonts.UIFont16Outline)
		
		"grip", "convert", "end turn":
			pass
		_:
			ActionSkillIcon.modulate = Color.white

func clearHighlight():
	AttackIcon.modulate = altColor
	PushIcon.modulate = altColor
	
	AttackSkillIcon.modulate.a = skillOpacity
	PushSkillIcon.modulate.a = skillOpacity
	ActionSkillIcon.modulate.a = skillOpacity
	
	AttackIcon.material = null
	PushIcon.material = null
	
	Attack.add_font_override("font", Fonts.UIFont32)
	Push.add_font_override("font", Fonts.UIFont32)
	
	AttackDef.add_font_override("font", Fonts.UIFont16)
	PushDef.add_font_override("font", Fonts.UIFont16)
	
	Bracelets.add_font_override("font", Fonts.UIFont16)

func showMenu(actor):
	material.set_shader_param("color", actor.color)
	Highlight.material.set_shader_param("color", actor.color)
	Defense.material.set_shader_param("color", actor.color)
	
	var actorTurnPosition = actor.Controller.getTurnPosition() + 1
	if actorTurnPosition > 10 && actorTurnPosition < 14:
		Place.text = String(actorTurnPosition)
		PlaceSuffix.text = "th"
	else:
		Place.text = String(actorTurnPosition)
		match actorTurnPosition % 10:
			1:
				PlaceSuffix.text = "st"
			2:
				PlaceSuffix.text = "nd"
			3:
				PlaceSuffix.text = "rd"
			_:
				PlaceSuffix.text = "th"
	
	Endurance.text = String(actor.Core.gdGetEndurance()) + "/" + String(actor.Core.maxEndurance)
	Energy.text = String(actor.Core.energy) + "/" + String(actor.Core.maxEnergy)
	
	Attack.text = String(actor.Core.attack)
	Push.text = String(actor.Core.push)
	AttackDef.text = String(actor.Core.defense)
	PushDef.text = String(actor.Core.steadiness)
	#Bracelets.text = String(actor.Core.bracelets)
	
	if actor.Core.hasAttackSkill:
		AttackSkillIcon.texture = load(actor.Core.attackSkillIcon)
		AttackSkillIcon.set_visible(true)
	else:
		AttackSkillIcon.set_visible(false)
	
	if actor.Core.hasPushSkill:
		PushSkillIcon.texture = load(actor.Core.pushSkillIcon)
		PushSkillIcon.set_visible(true)
	else:
		PushSkillIcon.set_visible(false)
	
	if actor.Core.hasActionSkill:
		ActionSkillIcon.texture = load(actor.Core.actionSkillIcon)
		ActionSkillIcon.set_visible(true)
	else:
		ActionSkillIcon.set_visible(false)
	
	altColor = altColorBase.linear_interpolate(actor.color, 0.2) * Colors.textColor
	
	Place.modulate = altColor
	PlaceSuffix.modulate = altColor
	
	AttackIcon.modulate = altColor
	PushIcon.modulate = altColor
	
	set_visible(true)

func clearMenu():
	set_visible(false)

func _ready():
	Highlight = get_node("Highlight")
	Defense = get_node("Defense")
	Frame = get_node("Frame")
	
	EnduranceIcon = get_node("EnduranceIcon")
	EnergyIcon = get_node("EnergyIcon")
	
	AttackIcon = get_node("StatIcons/Attack/Icon")
	PushIcon = get_node("StatIcons/Push/Icon")
	
	AttackSkillIcon = get_node("AttackSkill")
	PushSkillIcon = get_node("PushSkill")
	ActionSkillIcon = get_node("ActionSkill")
	
	Place = get_node("Place")
	PlaceSuffix = get_node("PlaceSuffix")
	Endurance = get_node("Endurance")
	Energy = get_node("Energy")
	
	Attack = get_node("Attack")
	Push = get_node("Push")
	AttackDef = get_node("AttackDef")
	PushDef = get_node("PushDef")
	Bracelets = get_node("Bracelets")
	
	material = material.duplicate(true)
	Highlight.material = Highlight.material.duplicate(true)
	Defense.material = Defense.material.duplicate(true)
	
	clearHighlight()
