extends "res://scripts/UI/components/Label.gd"

var Actor: Node2D

func updateTurnOrder() -> void:
	var position: int = Actor.Controller.getTurnPosition() + 1
	if position < 99:
		show()
		text = String(position)
	else:
		hide()


func _ready() -> void:
	Actor = get_parent().get_parent()
