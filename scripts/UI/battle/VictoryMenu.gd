extends "res://scripts/UI/UIMargin.gd"

var canChangeMode := false
var DialoguesUI: CanvasLayer

func enableModeChange() -> void:
	canChangeMode = true

func showMenu() -> void:
	var timer := Timer.new()
	timer.one_shot = true
	timer.connect("timeout", self, "enableModeChange")
	timer.wait_time = 1
	add_child(timer)
	timer.start()
	
	$ThoughtsContainer.showThoughts()
	show()

func endMode() -> void:
	get_node("/root/Setting").endMode()
	DialoguesUI.disconnect("animationEnded", self, "endMode")

func _input(_event: InputEvent) -> void:
	if canChangeMode && Input.is_action_just_pressed("ui_accept"):
		DialoguesUI.connect("animationEnded", self, "endMode")
		DialoguesUI.hide()
		canChangeMode = false

func _ready() -> void:
	DialoguesUI = get_node("/root/Viewports/UIViewport/DialoguesUILayer")
