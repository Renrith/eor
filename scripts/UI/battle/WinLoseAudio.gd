extends AudioStreamPlayer

export(AudioStream) var winSound = null
export(AudioStream) var loseSound = null

func win() -> void:
	stop()
	Globals.MusicController.pause(funcref(self, "play"))
	stream = winSound

func lose() -> void:
	stop()
	Globals.MusicController.pause(funcref(self, "play"))
	stream = loseSound

func play(pos = 0):
	.play(pos)

func _ready() -> void:
	connect("finished", Globals.MusicController, "resume")
