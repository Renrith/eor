extends "res://scripts/UI/components/FlatIcon.gd"

var AttackAct: Label
var AttackDef: Label
var AttackSkill: TextureRect

var PushAct: Label
var PushDef: Label
var PushSkill: TextureRect

var ActionSkill: TextureRect

func updateInfo(actor: Node2D, buildIndex: int = -1):
	var build = actor.getBuild(buildIndex)
	var stats: Dictionary = actor.Core.gdGetStatsForBuild(build)
	
	AttackAct.text = String(stats.attack)
	AttackDef.text = String(stats.defense)
	
	var attackSkillIndex: int = build.gdGetAttackSkillIndex()
	if attackSkillIndex != -1:
		AttackSkill.texture = load(Globals.Skills.gdGetSkillIcon(attackSkillIndex))
	else:
		AttackSkill.texture = preload("res://sprites/skills/placeholderSkill.png")
	
	PushAct.text = String(stats.push)
	PushDef.text = String(stats.steadiness)
	
	var pushSkillIndex: int = build.gdGetPushSkillIndex()
	if pushSkillIndex != -1:
		PushSkill.texture = load(Globals.Skills.gdGetSkillIcon(pushSkillIndex))
	else:
		PushSkill.texture = preload("res://sprites/skills/placeholderSkill.png")
	
	var actionSkillIndex: int = build.gdGetActionSkillIndex()
	if actionSkillIndex != -1:
		ActionSkill.texture = load(Globals.Skills.gdGetSkillIcon(actionSkillIndex))
	else:
		ActionSkill.texture = null

func _ready():
	AttackAct = get_node("Content/Stats/Attack/Act")
	AttackDef = get_node("Content/Stats/Attack/Def")
	AttackSkill = get_node("Content/Skills/Attack")
	
	PushAct = get_node("Content/Stats/Push/Act")
	PushDef = get_node("Content/Stats/Push/Def")
	PushSkill = get_node("Content/Skills/Push")
	
	ActionSkill = get_node("ActionSkill")
