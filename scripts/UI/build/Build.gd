extends Control

var Preparations: CanvasLayer
var Name: Label
var ModeLabel: Label
var LeftPortrait: TextureRect
var RightPortrait: TextureRect
var MainPortrait: TextureRect

var skillTreeScenes: Array = []
var selectedSceneIndex: int = 0

func setScene(increase: int = 0) -> void:
	var index: int = selectedSceneIndex + increase
	if index == skillTreeScenes.size():
		index = 0
	elif index < 0:
		index = skillTreeScenes.size() - 1
	
	var rightPortraitIndex: int = index + 1
	if rightPortraitIndex == skillTreeScenes.size():
		rightPortraitIndex = 0
	
	Name.text = skillTreeScenes[index].actor.actorName
	
	LeftPortrait.texture = skillTreeScenes[index - 1].actor.preparationsPortrait
	RightPortrait.texture = skillTreeScenes[rightPortraitIndex].actor.preparationsPortrait
	MainPortrait.texture = skillTreeScenes[index].actor.preparationsPortrait
	
	skillTreeScenes[selectedSceneIndex].hide()
	
	selectedSceneIndex = index
	$BuildCurrent.updateActor(skillTreeScenes[index].actor)

func onBuildCurrentSelected() -> void:
	$BuildList.hide()
	$PanelContainer.show()
	$BuildCurrent.grab_focus()

func onBuildListSelected() -> void:
	$PanelContainer.hide()
	$BuildCurrent.hide()
	$BuildList.actor = skillTreeScenes[selectedSceneIndex].actor
	$BuildList.showBuildCards()

func onBuildEditSelected(build: Reference) -> void:
	$PanelContainer.hide()
	$BuildEdit.showSkillTree(skillTreeScenes[selectedSceneIndex], build)

func onCancel() -> void:
	hide()
	Preparations.setDefaultMode()

func show() -> void:
	.show()
	$BuildCurrent.updateActor(skillTreeScenes[selectedSceneIndex].actor)
	$BuildCurrent.show()
	$BuildCurrent.grab_focus()

func _ready() -> void:
	Preparations = get_node("/root/Viewports/UIViewport/Preparations")
	Name = get_node("PanelContainer/Top/Name")
	ModeLabel = get_node("PanelContainer/Bottom/Label")
	LeftPortrait = get_node("PanelContainer/LeftPortrait")
	RightPortrait = get_node("PanelContainer/RightPortrait")
	MainPortrait = get_node("PanelContainer/MainPortrait")
	
	var Setting: ViewportContainer = get_node("/root/Setting")
	
	for actor in Setting.actors:
		if Setting.actors[actor] != null:
			var scenePath: String = Game.actorData[actor]["skillTree"]
			var skillTreeScene = load(scenePath).instance()
			
			skillTreeScenes.push_back(skillTreeScene)
			add_child(skillTreeScene)
			move_child(skillTreeScene, 0)
			skillTreeScene.hide()
	
	LeftPortrait.texture = skillTreeScenes[skillTreeScenes.size() - 1].actor.preparationsPortrait
	RightPortrait.texture = skillTreeScenes[1].actor.preparationsPortrait
	MainPortrait.texture = skillTreeScenes[0].actor.preparationsPortrait
	
	$PanelContainer/ArrowLeft.modulate = Colors.secondaryTextColor
	$PanelContainer/ArrowRight.modulate = Colors.secondaryTextColor
	
	setScene()
