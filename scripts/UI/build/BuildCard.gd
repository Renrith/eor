tool
extends Control

var BuildList: CenterContainer

var Portrait: TextureRect
var Name: Label
var BuildName: Label
var BuildContent: VBoxContainer
var EditButton: MarginContainer
var EquipButton: MarginContainer
var DeleteButton: MarginContainer

var outlineShader: ShaderMaterial

var actor: Node2D
var buildIndex: int = -1

func showBuild(newActor: Node2D, newBuildIndex: int = -1) -> void:
	actor = newActor
	buildIndex = newBuildIndex
	var build = actor.getBuild(buildIndex)
	
	Portrait.texture = build.portrait
	Name.text = actor.actorName
	BuildName.text = build.name
	BuildContent.updateActor(actor, buildIndex)
	
	show()

func onBuildEquipped() -> void:
	actor.equipBuild(buildIndex)

func onBuildDeleted() -> void:
	actor.deleteBuild(buildIndex)
	BuildList.onBuildDeleted()

func onFocus():
	$Frame.setOutline()
	$PanelContainer.showButtons()
	EditButton.grab_focus()

func onUnfocus():
	$Frame.removeOutline()
	$PanelContainer.hideButtons()
	
func hide():
	actor = null
	buildIndex = -1
	.hide()

func _ready():
	BuildList = get_owner()
	
	Portrait = get_node("PortraitContainer/Portrait")
	Name = get_node("Center/Name")
	BuildName = get_node("PanelContainer/VBox/Control/Name")
	BuildContent = get_node("PanelContainer/VBox/Center/BuildContent")
	EditButton = get_node("PanelContainer/VBox/Buttons/Edit/Button")
	EquipButton = get_node("PanelContainer/VBox/Buttons/Equip/Button")
	DeleteButton = get_node("PanelContainer/VBox/Buttons/Delete/Button")
	
	EquipButton.callback = funcref(self, "onBuildEquipped")
	DeleteButton.callback = funcref(self, "onBuildDeleted")
