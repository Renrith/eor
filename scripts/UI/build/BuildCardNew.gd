tool
extends Control

var BuildList: CenterContainer
var Name: Label

var outlineShader: ShaderMaterial

var actor: Node2D

func onFocus() -> void:
	grab_focus()
	$Frame.setOutline()

func onUnfocus() -> void:
	$Frame.removeOutline()

func onSelect() -> void:
	actor.createBuild()
	BuildList.onBuildCreated()

func hide() -> void:
	.hide()

func _input(_event: InputEvent) -> void:
	if has_focus():
		if Input.is_action_just_pressed("ui_accept"):
			onSelect()
			accept_event()

func _ready():
	BuildList = get_owner()
	Name = get_node("Center/Name")
	
	onUnfocus()
