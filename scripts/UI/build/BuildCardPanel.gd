tool
extends "res://scripts/UI/components/PanelContainer.gd"

var y: float = 464
var yExpanded: float = 407

func showButtons() -> void:
	$VBox/Buttons.show()
	rect_position.y = yExpanded
	resize()

func hideButtons() -> void:
	$VBox/Buttons.hide()
	rect_position.y = y
	resize()

func resize() -> void:
	$VBox.rect_size.y = 0
	rect_size.y = 0
