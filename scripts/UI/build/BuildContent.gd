extends VBoxContainer

var SummaryStats: HBoxContainer
var ActionSkills: TextureRect

func updateActor(actor: Node2D, buildIndex: int = -1) -> void:
	SummaryStats.updateStats(actor, buildIndex)
	ActionSkills.updateInfo(actor, buildIndex)

func _ready() -> void:
	SummaryStats = get_node("CenterContainer/SummaryStats")
	ActionSkills = get_node("SkillsContainer/ActionSkills")
