tool
extends ColorRect

var Build: Control
var BuildContent: VBoxContainer
var EditButton: MarginContainer
var Name: Label

var actor: Node2D
var isActive: bool = false

func updateActor(_actor: Node2D) -> void:
	actor = _actor
	Name.text = actor.buildList[actor.currentBuildIndex].name
	BuildContent.updateActor(actor)

func onFocusEntered():
	Name.text = actor.buildList[actor.currentBuildIndex].name
	show()
	EditButton.grab_focus()
	isActive = true

func hide():
	.hide()
	isActive = false

func _input(_event) -> void:
	if isActive:
		if Input.is_action_just_pressed("ui_cancel"):
			isActive = false
			Build.onCancel()
			accept_event()
		if Input.is_action_pressed("ui_left"):
			get_parent().setScene(-1)
		elif Input.is_action_pressed("ui_right"):
			get_parent().setScene(+1)
		elif Input.is_action_just_pressed("ui_accept"):
			isActive = false
			get_parent().onBuildListSelected()
			accept_event()

func _ready() -> void:
	color = Colors.panelColor
	
	Build = get_parent()
	BuildContent = get_node("Panel/CenterContainer/BuildContent")
	EditButton = get_node("Panel/Buttons/Edit")
	Name = get_node("Top/Name")
	
	connect("focus_entered", self, "onFocusEntered")
