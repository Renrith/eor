extends Control

var Build: Control
var SkillTreeScene: Control

var build: Reference

var isActive: bool = false

var titleText = "%s - %s"

func showSkillTree(skillTree: Control, newBuild: Reference) -> void:
	build = newBuild
	SkillTreeScene = skillTree
	$Title.text = titleText % [skillTree.actor.actorName, build.name]
	SkillTreeScene.show()
	var buildIndex: int = skillTree.actor.buildList.find(newBuild)
	$PanelContainer/BuildContent.updateActor(skillTree.actor, buildIndex)
	isActive = true
	show()

func hideSkillTree() -> void:
	SkillTreeScene.hide()
	isActive = false
	hide()

func onSkillSelected(skill, actor) -> void:
	$Points/SkillPoints.updateCount(actor, skill)
	if build != null:
		$Points/BuildPoints.updateCount(actor, skill, build)
		$SkillTreePanel.showSkillProperties(skill, actor, build)
		var buildIndex: int = actor.buildList.find(build)
		$PanelContainer/BuildContent.updateActor(actor, buildIndex)

func onFocusSkillPanel() -> void:
	isActive = false
	SkillTreeScene.isActive = false
	$SkillTreePanel.grab_focus()

func onUnfocusSkillPanel() -> void:
	isActive = true
	SkillTreeScene.isActive = true

func _input(_event) -> void:
	if isActive:
		if Input.is_action_just_pressed("ui_cancel"):
			hideSkillTree()
			Build.onBuildListSelected()
			accept_event()

func _ready() -> void:
	Build = get_parent()
