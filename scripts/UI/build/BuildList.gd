extends CenterContainer

var Build: Control
var BuildCardList: Array
var NewCard: Control
var selectedCard: Control

var actor: Node2D
var isActive: bool = false

const selectedCardIndex: int = 3
var selectedBuildIndex: int = 0
var hiddenCardIndex: int = -999

var cardPositions: Array = []
var cardScale: Array = []

func swapLeft():
	if selectedBuildIndex > 0 - selectedCardIndex:
		BuildCardList.push_front(BuildCardList.pop_back())
		selectedBuildIndex = selectedBuildIndex - 1
		hiddenCardIndex = hiddenCardIndex + 1
		orderBuildCards()

func swapRight():
	if selectedBuildIndex < actor.buildList.size() - selectedCardIndex:
		BuildCardList.push_back(BuildCardList.pop_front())
		selectedBuildIndex = selectedBuildIndex + 1
		hiddenCardIndex = hiddenCardIndex - 1
		orderBuildCards()

func showBuildCards():
	show()
	isActive = true
	selectedBuildIndex = actor.currentBuildIndex - selectedCardIndex
	
	hiddenCardIndex = actor.buildList.size() + selectedCardIndex - actor.currentBuildIndex
	
	NewCard.actor = actor
	
	orderBuildCards()

func orderBuildCards():
	for i in range(0, BuildCardList.size()):
		if i + selectedBuildIndex < 0 || i + selectedBuildIndex >= actor.buildList.size():
			BuildCardList[i].hide()
		else:
			BuildCardList[i].showBuild(actor, i + selectedBuildIndex)
	
	selectedCard.onUnfocus()
	selectedCard = BuildCardList[selectedCardIndex]
	if selectedCardIndex < hiddenCardIndex:
		selectedCard.onFocus()
		NewCard.onUnfocus()
	else:
		NewCard.onFocus()
	
	for i in range(0, BuildCardList.size()):
		BuildCardList[i].get_parent().rect_position = cardPositions[i]
		BuildCardList[i].get_parent().rect_scale = cardScale[i]
	
	if hiddenCardIndex < BuildCardList.size():
		NewCard.get_parent().rect_position = cardPositions[hiddenCardIndex]
		NewCard.get_parent().rect_scale = cardScale[hiddenCardIndex]
		NewCard.show()
	else:
		NewCard.hide()

func hideBuildCards():
	for buildCard in BuildCardList:
		buildCard.hide()
	
	isActive = false
	hide()

func onBuildEditSelected() -> void:
	hideBuildCards()
	var build = actor.getBuild(selectedBuildIndex + selectedCardIndex)
	Build.onBuildEditSelected(build)

func onBuildCreated() -> void:
	hiddenCardIndex += 1
	orderBuildCards()

func onBuildDeleted() -> void:
	hiddenCardIndex -= 1
	orderBuildCards()

func _input(_event) -> void:
	if isActive:
		if Input.is_action_pressed("ui_left"):
			swapLeft()
			accept_event()
		if Input.is_action_pressed("ui_right"):
			swapRight()
			accept_event()
		if Input.is_action_just_pressed("ui_cancel"):
			isActive = false
			get_owner().onBuildCurrentSelected()
			accept_event()

func _ready():
	var BuildCard0 = get_node("Cards/0/BuildCard")
	var BuildCard1 = get_node("Cards/1/BuildCard")
	var BuildCard2 = get_node("Cards/2/BuildCard")
	var BuildCard3 = get_node("Cards/3/BuildCard")
	var BuildCard4 = get_node("Cards/4/BuildCard")
	var BuildCard5 = get_node("Cards/5/BuildCard")
	var BuildCard6 = get_node("Cards/6/BuildCard")
	
	Build = get_parent()
	BuildCardList = [BuildCard0, BuildCard1, BuildCard2, BuildCard3, BuildCard4, BuildCard5, BuildCard6]
	NewCard = $Cards/New/BuildCardNew
	selectedCard = BuildCardList[selectedCardIndex]
	selectedCard.onFocus()
	
	for i in range(0, BuildCardList.size()):
		cardPositions.push_back(BuildCardList[i].get_parent().rect_position)
		cardScale.push_back(BuildCardList[i].get_parent().rect_scale)
		BuildCardList[i].EditButton.callback = funcref(self, "onBuildEditSelected")
	
	hideBuildCards()
