tool
extends PanelContainer

var Count: Label
var Text: Label

func updateCount(actor: Node2D, skill: Dictionary, build: Reference):
	var points: int = actor.getRemainingBuildPoints(build)
	Count.text = String(points)
	
	if skill.isAcquired || points < skill.borrowCost:
		Count.add_color_override("font_color", Colors.textColor)
		Text.add_color_override("font_color", Colors.secondaryTextColor)
	else:
		Count.add_color_override("font_color", Colors.primaryColor)
		Text.add_color_override("font_color", Colors.textColor)

func _ready():
	Count = get_node("Margin/HBox/Count")
	Text = get_node("Margin/HBox/Text")
	
	var styleBox: StyleBoxFlat = get_stylebox("panel")
	styleBox.set("bg_color", Colors.panelColor)
