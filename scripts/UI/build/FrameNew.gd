tool
extends TextureRect

func setOutline():
	material.set_shader_param("outlineColor", Colors.primaryColor)

func removeOutline():
	material.set_shader_param("outlineColor", Colors.transparentColor)

func _ready():
	material.set_shader_param("color", Colors.panelColor)
	setOutline()
