extends HBoxContainer

var EnduranceLabel: Label
var EnergyLabel: Label
var MoveLabel: Label

func updateStats(actor: Node2D, buildIndex: int = -1) -> void:
	var build: Reference = actor.getBuild(buildIndex)
	var stats: Dictionary = actor.Core.gdGetStatsForBuild(build)
	
	EnduranceLabel.text = String(stats.endurance)
	EnergyLabel.text = String(stats.energy)
	MoveLabel.text = String(stats.movementRange)

func _ready():
	EnduranceLabel = get_node("Endurance/Label")
	EnergyLabel = get_node("Energy/Label")
	MoveLabel = get_node("Move/Label")
