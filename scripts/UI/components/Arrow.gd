tool
extends TextureRect

func _init():
	material = preload("res://shaders/arrow.tres")
	material.set_shader_param("color", Colors.panelColor)
	material.set_shader_param("innerColor", Colors.primaryColor)
