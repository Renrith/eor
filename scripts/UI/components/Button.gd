tool
extends MarginContainer

enum buttonTypes {
	default,
	positive,
	negative,
	disabled,
}

enum buttonStates {
	default,
	focused,
}

export(bool) var disabled = false
export(buttonTypes) var buttonType: int = buttonTypes.default

var Audio: AudioStreamPlayer2D

var callback: FuncRef

var shouldPlayNavigateSound: bool = true
var isHovered: bool = false
var isPressed: bool = false

func applyStyle(_state: int) -> void:
	pass

func onSelect() -> void:
	if !disabled && callback != null && callback.is_valid():
		Audio.select()
		isPressed = true

func performSelect() -> void:
	if callback != null && callback.is_valid():
		callback.call_func()
		isPressed = false

func onAudioFinished() -> void:
	if isPressed:
		performSelect()

func onMouseEntered() -> void:
	grab_focus()
	isHovered = true

func onMouseExited() -> void:
	isHovered = false

func onFocusEntered() -> void:
	if shouldPlayNavigateSound:
		Audio.navigate()
	else:
		shouldPlayNavigateSound = true
	applyStyle(buttonStates.focused)

func onFocusExited() -> void:
	applyStyle(buttonStates.default)

func grab_focus() -> void:
	shouldPlayNavigateSound = false
	.grab_focus()

func _input(e) -> void:
	if e is InputEventMouseButton && e.button_index == BUTTON_LEFT && e.pressed && isHovered:
		onSelect()
		accept_event()
	
	elif has_focus():
		if Input.is_action_just_pressed("ui_accept"):
			onSelect()
			accept_event()
		elif isPressed:
			accept_event()

func _ready() -> void:
	Audio = $Audio
	Audio.connect("finished", self, "onAudioFinished")
	applyStyle(buttonStates.default)
	
	connect("mouse_entered", self, "onMouseEntered")
	connect("mouse_exited", self, "onMouseExited")
	connect("focus_entered", self, "onFocusEntered")
	connect("focus_exited", self, "onFocusExited")
