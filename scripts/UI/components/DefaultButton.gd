extends "res://scripts/UI/components/Button.gd"

const defaultStyleBoxes: Dictionary = {
	normal = preload("res://sprites/UI/buttons/buttonDefault.png"),
	focus = preload("res://sprites/UI/buttons/buttonDefaultFocused.png"),
	normalColor = Colors.secondaryTextColor,
	focusColor = Colors.textColor,
}

const positiveStyleBoxes: Dictionary = {
	normal = preload("res://sprites/UI/buttons/buttonPositive.png"),
	focus = preload("res://sprites/UI/buttons/buttonPositiveFocused.png"),
	normalColor = Colors.secondaryPositiveTextColor,
	focusColor = Colors.positiveTextColor,
}

const negativeStyleBoxes: Dictionary = {
	normal = preload("res://sprites/UI/buttons/buttonNegative.png"),
	focus = preload("res://sprites/UI/buttons/buttonNegativeFocused.png"),
	normalColor = Colors.secondaryNegativeTextColor,
	focusColor = Colors.negativeTextColor,
}

const disabledStyleBoxes: Dictionary = {
	normal = preload("res://sprites/UI/buttons/buttonDisabled.png"),
	focus = preload("res://sprites/UI/buttons/buttonDisabledFocused.png"),
	normalColor = Colors.secondaryDisabledTextColor,
	focusColor = Colors.disabledTextColor,
}

const styleTable: Array = [
	defaultStyleBoxes,
	positiveStyleBoxes,
	negativeStyleBoxes,
	disabledStyleBoxes,
]

func applyStyle(state: int) -> void:
	match state:
		buttonStates.default:
			$Label/Texture.texture = styleTable[buttonType].normal
			$Label.add_color_override("font_color", styleTable[buttonType].normalColor)
		buttonStates.focused:
			$Label/Texture.texture = styleTable[buttonType].focus
			$Label.add_color_override("font_color", styleTable[buttonType].focusColor)
