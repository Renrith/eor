tool
extends TextureRect

export(String) var _color = "panelColor"

func _ready():
	self_modulate = Globals.get(_color)
