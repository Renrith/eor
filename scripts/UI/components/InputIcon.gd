tool
extends TextureRect

export(InputActions.inputActions) var actionName

func updateIcon() -> void:
	var input = InputActions.actionToInput[actionName]
	if Devices.isGamepad && input.button != null:
			if Devices.isDefaultGamepad:
				texture = InputActions.inputToDefaultButtonIcon[input.button]
			else:
				texture = InputActions.inputToPsButtonIcon[input.button]
	elif input.key != null:
			texture = InputActions.inputToKeyIcon[input.key]

func _ready() -> void:
	Devices.connect("onInputDeviceChanged", self, "updateIcon")
	updateIcon()
