tool
extends Label

export(String) var font
export(String,
"", "primaryColor", "transparentColor",
"textColor", "positiveTextColor", "negativeTextColor", "disabledTextColor", "titleColor",
"secondaryTextColor", "secondaryPositiveTextColor", "secondaryNegativeTextColor", "secondaryDisabledTextColor"
) var color

func _ready():
	if font != "":
		add_font_override("font", Fonts.get(font))
	if color != "":
		add_color_override("font_color", Colors.get(color))
	else:
		add_color_override("font_color", Colors.textColor)
