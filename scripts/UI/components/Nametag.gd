extends HBoxContainer

func showMenu(actor) -> void:
	$Name/Label.text = actor.actorName
	
	if actor.Core.gangName != "":
		$Gang/Label.text = actor.Core.gangName
		var gangColor: Color = Globals.getGangColor(actor.Core.gangIndex)
		$Gang/Label/Texture.material.set_shader_param("color", gangColor)
		$Gang.set_visible(true)
		$ChainContainer.set_visible(true)
	else:
		$Gang.set_visible(false)
		$ChainContainer.set_visible(false)
	
	set_visible(true)

func clearMenu() -> void:
	set_visible(false)

func _ready() -> void:
	$Gang/Label/Texture.material = $Gang/Label/Texture.material.duplicate()
