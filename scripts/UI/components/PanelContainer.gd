tool
extends PanelContainer

func _ready() -> void:
	var styleBox: StyleBoxFlat = get_stylebox("panel")
	styleBox.set("bg_color", Colors.panelColor)
