extends HBoxContainer

func onFocusEntered() -> void:
	$Right.get_child(0).grab_focus()

func _ready() -> void:
	connect("focus_entered", self, "onFocusEntered")
