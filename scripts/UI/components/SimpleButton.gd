extends "res://scripts/UI/components/Button.gd"

const defaultStyleBoxes: Dictionary = {
	normal = preload("res://sprites/UI/buttons/buttonSimpleDefault.png"),
	focus = preload("res://sprites/UI/buttons/buttonSimpleDefaultFocused.png"),
	normalColor = Colors.secondaryTextColor,
	focusColor = Colors.textColor,
}

const positiveStyleBoxes: Dictionary = {
	normal = preload("res://sprites/UI/buttons/buttonSimplePositive.png"),
	focus = preload("res://sprites/UI/buttons/buttonSimplePositiveFocused.png"),
	normalColor = Colors.secondaryPositiveTextColor,
	focusColor = Colors.positiveTextColor,
}

const negativeStyleBoxes: Dictionary = {
	normal = preload("res://sprites/UI/buttons/buttonSimpleNegative.png"),
	focus = preload("res://sprites/UI/buttons/buttonSimpleNegativeFocused.png"),
	normalColor = Colors.secondaryNegativeTextColor,
	focusColor = Colors.negativeTextColor,
}

const disabledStyleBoxes: Dictionary = {
	normal = preload("res://sprites/UI/buttons/buttonSimpleDisabled.png"),
	focus = preload("res://sprites/UI/buttons/buttonSimpleDisabledFocused.png"),
	normalColor = Colors.secondaryDisabledTextColor,
	focusColor = Colors.disabledTextColor,
}

const styleTable: Array = [
	defaultStyleBoxes,
	positiveStyleBoxes,
	negativeStyleBoxes,
	disabledStyleBoxes,
]

func applyStyle(state: int) -> void:
	match state:
		buttonStates.default:
			$Label/Texture.texture = styleTable[buttonType].normal
			$Label.add_color_override("font_color", styleTable[buttonType].normalColor)
		buttonStates.focused:
			$Label/Texture.texture = styleTable[buttonType].focus
			$Label.add_color_override("font_color", styleTable[buttonType].focusColor)
