extends HSlider

var callback: FuncRef

func onValueChanged(_value: float) -> void:
	callback.call_func()

func _ready() -> void:
	connect("value_changed", self, "onValueChanged")
