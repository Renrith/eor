extends Panel

var callback: FuncRef

var isHovered: bool = false
export var isToggled: bool = false

func onMouseEntered() -> void:
	grab_focus()
	isHovered = true

func onMouseExited() -> void:
	isHovered = false

func onToggle() -> void:
	isToggled = !isToggled
	callback.call_func()
	if isToggled:
		$Button.anchor_left = 1
		$Button.anchor_right = 1
		$Button.rect_position.x -= $Button.rect_size.x
	else:
		$Button.anchor_left = 0
		$Button.anchor_right = 0
		$Button.rect_position.x = 0

func _input(e) -> void:
	if e is InputEventMouseButton && e.button_index == BUTTON_LEFT && e.pressed && isHovered:
		onToggle()
		accept_event()
	
	elif has_focus():
		if Input.is_action_just_pressed("ui_accept"):
			onToggle()
			accept_event()

func _ready() -> void:
	connect("mouse_entered", self, "onMouseEntered")
	connect("mouse_exited", self, "onMouseExited")
	
	if isToggled:
		$Button.anchor_left = 1
		$Button.anchor_right = 1
		$Button.rect_position.x -= $Button.rect_size.x
