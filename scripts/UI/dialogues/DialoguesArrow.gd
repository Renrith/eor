extends TextureRect

var shouldAnimate := false
const positionChange := Vector2(0, 2)
const inDuration := 0.35
const outDuration := 0.5
var initialRectPosition: Vector2

func animate() -> void:
	visible = true
	shouldAnimate = true
	rect_position = initialRectPosition
	$Tween.interpolate_property(self, "rect_position", rect_position, rect_position + positionChange, inDuration, Tween.TRANS_SINE, Tween.EASE_IN)
	$Tween.interpolate_property(self, "rect_position", rect_position + positionChange, rect_position, outDuration, Tween.TRANS_SINE, Tween.EASE_OUT, inDuration)
	$Tween.start()

func stopAnimation() -> void:
	visible = false
	shouldAnimate = false

func updateAnimation() -> void:
	if shouldAnimate:
		animate()

func _ready() -> void:
	visible = false
	initialRectPosition = rect_position
	$Tween.connect("tween_all_completed", self, "updateAnimation")
