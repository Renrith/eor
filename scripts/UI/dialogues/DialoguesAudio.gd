extends AudioStreamPlayer2D

var sound: AudioStream = preload("res://sound/effects/dialogSelect.wav")
var defaultVolume: float = -12

func select() -> void:
	stop()
	play()

func _ready() -> void:
	stream = sound
	volume_db = defaultVolume
