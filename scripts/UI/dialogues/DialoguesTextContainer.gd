extends TextureRect

const yHiddenChange: int = 60
const time: float = 0.35
const delayTime: float = 0.45

var yShown: int = 0

func slideIn() -> void:
	var newPosition = Vector2(rect_position.x, rect_position.y - yHiddenChange)
	$Tween.interpolate_property(self, "rect_position", rect_position, newPosition, time, Tween.TRANS_QUAD, Tween.EASE_OUT, delayTime)
	$Tween.interpolate_property(self, "modulate", modulate, Color.white, time, Tween.TRANS_QUAD, Tween.EASE_OUT, delayTime)
	$Tween.start()

func slideOut() -> void:
	var newPosition = Vector2(rect_position.x, rect_position.y + yHiddenChange)
	$Tween.interpolate_property(self, "rect_position", rect_position, newPosition, time, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.interpolate_property(self, "modulate", modulate, Colors.transparentColor, time, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()

func _ready() -> void:
	rect_position.y = rect_position.y + yHiddenChange
	modulate = Colors.transparentColor
	$Tween.connect("tween_all_completed", get_parent(), "endContainerAnimation")
