extends CanvasLayer

signal animationReady
signal animationEnded

const animationReadyDelay: float = 0.1

var Container: TextureRect

var PortraitLeft: TextureRect
var PortraitRight: TextureRect

var NameLeft: HBoxContainer
var NameRight: HBoxContainer
var TextLabel: Label

var Audio: AudioStreamPlayer2D

var isThought: bool = false

var isFenceReady: bool = false
var isContainerReady: bool = false

var isReady: bool setget, getIsReady
var hasEnded: bool setget, getHasEnded
var isAnimating: bool setget, getIsAnimating
var hasFinishedAnimatingText: bool setget, getHasFinishedAnimatingText

func getIsReady() -> bool:
	if isThought:
		return isContainerReady
	else:
		return isFenceReady && isContainerReady

func getHasEnded() -> bool:
	return !isFenceReady && !isContainerReady

func getIsAnimating() -> bool:
	return $Container/Tween.is_active() || $Fence/Tween.is_active()

func getHasFinishedAnimatingText() -> bool:
	return (TextLabel.text == ""
	|| TextLabel.visible_characters == -1
	|| TextLabel.text.length() <= TextLabel.visible_characters + 1)

func endFenceAnimation() -> void:
	isFenceReady = !isFenceReady
	if getIsReady():
		$Timer.start()
	elif getHasEnded():
		onAnimationEnded()

func endContainerAnimation() -> void:
	isContainerReady = !isContainerReady
	if getIsReady():
		$Timer.start()
	elif getHasEnded():
		onAnimationEnded()

func onAnimationReady() -> void:
	emit_signal("animationReady")

func onAnimationEnded() -> void:
	emit_signal("animationEnded")

func onTextAnimationFinished() -> void:
	$Container/Arrow.animate()

func show() -> void:
	PortraitLeft.texture = null
	PortraitRight.texture = null
	PortraitLeft.setPortraitMode(PortraitLeft.portraitMode.dialog)
	PortraitRight.setPortraitMode(PortraitRight.portraitMode.dialog)
	PortraitLeft.set_visible(true)
	PortraitRight.set_visible(true)
	NameLeft.clearMenu()
	NameRight.clearMenu()
	TextLabel.text = ""
	
	if !getIsReady():
		if !isThought:
			$Fence.slideIn()
		if !isContainerReady:
			Container.slideIn()
	else:
		onAnimationReady()

func hide() -> void:
	if !isThought:
		$Fence.slideOut()
	$Container/Arrow.stopAnimation()
	$Container/Text.text = ""
	Container.slideOut()
	PortraitLeft.set_visible(false)
	PortraitRight.set_visible(false)
	PortraitLeft.texture = null
	PortraitRight.texture = null

func handleDefeat() -> void:
	var details = get_node("/root/Setting").getDefeatDetails()
	if !details.empty() && details.text != null:
		show()
		showNextDialog(details.actor, details.text, "")

func handleVictory() -> void:
	var details = get_node("/root/Setting").getVictoryDetails()
	if !details.empty() && details.text != null:
		show()
		showNextDialog(details.actor, details.text, "", true, details.portrait)

func showNextDialog(
	actor: Node2D,
	text: String,
	emotion := "",
	isLeftSide := true,
	portrait: StreamTexture = null
	) -> void:
	$Container/Arrow.stopAnimation()
	TextLabel.setText(text, emotion)
	
	if actor != null:
		if isLeftSide:
			NameLeft.showMenu(actor)
			NameRight.clearMenu()
			if portrait != null:
				PortraitLeft.texture = portrait
		
		else:
			NameRight.showMenu(actor)
			NameLeft.clearMenu()
			if portrait != null:
				PortraitRight.texture = portrait
	
	else:
		NameRight.clearMenu()
		NameLeft.clearMenu()
		
		if isLeftSide:
			PortraitLeft.texture = null
		else:
			PortraitRight.texture = null
	
	if Container.visible:
		Audio.select()

func skipAnimation() -> void:
	TextLabel.skipAnimation()
	$Container/Arrow.animate()

func _ready() -> void:
	Container = get_node("Container")
	
	PortraitLeft = get_node("/root/Viewports/GraphicsViewport/PortraitLeft")
	PortraitRight = get_node("/root/Viewports/GraphicsViewport/PortraitRight")
	
	NameLeft = Container.get_node("NametagLeft")
	NameRight = Container.get_node("NametagRight")
	TextLabel = Container.get_node("Text")
	
	Audio = Container.get_node("Audio")
	
	$Timer.connect("timeout", self, "onAnimationReady")
	$Timer.wait_time = animationReadyDelay
	$Timer.set_one_shot(true)
