extends TextureRect

const yHiddenChange: int = 20
const time: float = 0.8

func slideIn() -> void:
	var newPosition = Vector2(rect_position.x, rect_position.y - yHiddenChange)
	$Tween.interpolate_property(self, "rect_position", rect_position, newPosition, time, Tween.TRANS_EXPO, Tween.EASE_IN_OUT)
	$Tween.interpolate_property(self, "modulate", modulate, Color.white, time, Tween.TRANS_EXPO, Tween.EASE_IN_OUT)
	$Tween.start()

func slideOut() -> void:
	var newPosition = Vector2(rect_position.x, rect_position.y + yHiddenChange)
	$Tween.interpolate_property(self, "rect_position", rect_position, newPosition, time, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.interpolate_property(self, "modulate", modulate, Colors.whiteTransparentColor, time, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()

func _ready() -> void:
	rect_position.y = rect_position.y + yHiddenChange
	modulate = Colors.whiteTransparentColor
	$Tween.connect("tween_all_completed", get_parent(), "endFenceAnimation")
