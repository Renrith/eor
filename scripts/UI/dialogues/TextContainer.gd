tool
extends NinePatchRect

var colorVisible: Color
var colorInvisible: Color = Color(1, 1, 1, 0)
var colorSelected: Color

func hide():
	material.set_shader_param("color", colorInvisible)
	material.set_shader_param("outlineColor", colorInvisible)

func show():
	material.set_shader_param("color", colorVisible)
	material.set_shader_param("outlineColor", colorSelected)

func _ready():
	colorVisible = Colors.panelColor
	colorSelected = Colors.primaryColor
	
	material = material.duplicate()
	material.shader = material.shader.duplicate()
	material.set_shader_param("width", 7)
	
	show()
