extends "res://scripts/UI/saving/SlotButton.gd"

func loadData() -> void:
	Saves.loadData(savePath)

func _ready():
	callback = funcref(self, "loadData")
