extends CenterContainer

const LoadButton: PackedScene = preload("res://scenes/UI/saves/LoadButton.tscn")
onready var Title = get_parent()

var isActive := false

func show() -> void:
	isActive = true
	$Container/Slots/AutoSaveButton.grab_focus()
	.show()

func hide() -> void:
	isActive = false
	.hide()

func _input(_event) -> void:
	if isActive && Input.is_action_just_pressed("ui_cancel"):
		Title.setDefaultMode()
		accept_event()

func _enter_tree() -> void:
	var saveCount := Saves.getManualSaveCount()
	for i in range(0, saveCount):
		var LoadButtonInstance = LoadButton.instance()
		$Container/Slots.add_child(LoadButtonInstance)
		LoadButtonInstance.slotIndex = i + 1
