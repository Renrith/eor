extends "res://scripts/UI/saving/SlotButton.gd"

func saveData() -> void:
	Saves.saveData(savePath)
	SaveInformation.updateSavePath(savePath)
	updateLabel()

func _ready():
	if slotIndex > 0:
		callback = funcref(self, "saveData")
