extends "res://scripts/UI/UIMargin.gd"

var SaveTypeLabel: Label
var PartLabel: Label
var ChapterLabel: Label
var DateLabel: Label
var ModeLabel: Label

const dateLabelText = "%s. %s, %s."
const dateLabelNoSectorText = "%s. %s."

var currentSavePath: String

func updateSavePath(newSavePath: String) -> void:
	currentSavePath = newSavePath
	showSaveData()

func showSaveData() -> void:
	if Saves.hasSavedDataPath(currentSavePath):
		var savedData = Saves.getSavedDataPreview(currentSavePath)
		
		if currentSavePath == Saves.autoSavePath:
			SaveTypeLabel.text = "Autosave"
		else:
			SaveTypeLabel.text = ""
		
		PartLabel.text = "Pt " + String(savedData.partIndex) + " - " + savedData.part
		ChapterLabel.text = String(savedData.chapterIndex) + ". " + savedData.chapter
		
		if savedData.location != "":
			if savedData.sector != "":
				DateLabel.text = dateLabelText % [savedData.date, savedData.location, savedData.sector]
			else:
				DateLabel.text = dateLabelNoSectorText % [savedData.date, savedData.location]
		
		# Couldn't get a match properly working here, no clue why
		if savedData.mode == Globals.modes.battle:
			ModeLabel.text = "Battle"
		elif savedData.mode == Globals.modes.preparations:
			ModeLabel.text = "Battle prep"
		else:
			ModeLabel.text = ""
		
		modulate = Color.white
		
	else:
		modulate = Colors.transparentColor

func _ready() -> void:
	var LabelsContainer = get_node("Panel/Labels")
	SaveTypeLabel = LabelsContainer.get_node("SaveType")
	PartLabel = LabelsContainer.get_node("Part")
	ChapterLabel = LabelsContainer.get_node("Chapter")
	DateLabel = LabelsContainer.get_node("Date")
	ModeLabel = LabelsContainer.get_node("Mode")
	
	updateSavePath(Saves.getMostRecentSavePath())
