extends CenterContainer

const SaveButton: PackedScene = preload("res://scenes/UI/saves/SaveButton.tscn")
var Preparations: CanvasLayer

const maxSlots = 5
var isActive := false

func show() -> void:
	isActive = true
	$Container/Slots/SaveButton.grab_focus()
	.show()

func hide() -> void:
	isActive = false
	Preparations.setDefaultMode()
	.hide()

func _input(_event) -> void:
	if isActive && Input.is_action_just_pressed("ui_cancel"):
		hide()
		accept_event()

func _enter_tree() -> void:
	Preparations = get_node("/root/Viewports/UIViewport/Preparations")
	var saveCount := Saves.getManualSaveCount()
	for i in range(0, min(maxSlots, saveCount + 1)):
		var SaveButtonInstance = SaveButton.instance()
		$Container/Slots.add_child(SaveButtonInstance)
		SaveButtonInstance.slotIndex = i + 1
