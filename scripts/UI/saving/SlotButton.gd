extends "res://scripts/UI/components/DefaultButton.gd"

export(int) var slotIndex
const dataString = "%s. Ch %s. %s mins"
const newSlotString = "New save"

var savePath := Saves.autoSavePath

onready var SaveInformation: MarginContainer = get_node("../../SaveInformation")

func onFocusEntered() -> void:
	SaveInformation.updateSavePath(savePath)
	updateLabel()
	.onFocusEntered()

func updateLabel() -> void:
	if Saves.hasSavedDataPath(savePath):
		var savedData := Saves.getSavedDataPreview(savePath)
		var mins = ceil(savedData.timeElapsed / 60)
		$Label.text = dataString % [slotIndex, savedData.chapterIndex, mins]
	else:
		$Label.text = newSlotString

func _ready() -> void:
	if slotIndex > 0:
		savePath = Saves.savePath % slotIndex
	updateLabel()
