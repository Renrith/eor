extends CenterContainer

onready var Preparations = get_parent()

var isActive := false

func onStoryToggled() -> void:
	Game.showStory = $Labels/Story/Right/Toggle.isToggled

func onSideConversationsToggled() -> void:
	Game.showSideConversations = $Labels/SideConversations/Right/Toggle.isToggled

func onMusicVolumeChanged() -> void:
	var volume = linear2db($Labels/Music/Right/Slider.value)
	AudioServer.set_bus_volume_db(Globals.musicBusIndex, volume)

func onSFXVolumeChanged() -> void:
	var volume = linear2db($Labels/SFX/Right/Slider.value)
	AudioServer.set_bus_volume_db(Globals.SFXBusIndex, volume)

func show() -> void:
	isActive = true
	$Labels/Story.grab_focus()
	.show()

func hide() -> void:
	isActive = false
	Preparations.setDefaultMode()
	Saves.saveSettings()
	.hide()

func _input(_event) -> void:
	if isActive && Input.is_action_just_pressed("ui_cancel"):
		hide()
		accept_event()

func _enter_tree() -> void:
	$Labels/Story/Right/Toggle.isToggled = Game.showStory
	$Labels/Story/Right/Toggle.callback = funcref(self, "onStoryToggled")
	
	$Labels/SideConversations/Right/Toggle.isToggled = Game.showSideConversations
	$Labels/SideConversations/Right/Toggle.callback = funcref(self, "onSideConversationsToggled")
	
	$Labels/Music/Right/Slider.value = db2linear(AudioServer.get_bus_volume_db(Globals.musicBusIndex))
	$Labels/Music/Right/Slider.callback = funcref(self, "onMusicVolumeChanged")

	$Labels/SFX/Right/Slider.value = db2linear(AudioServer.get_bus_volume_db(Globals.SFXBusIndex))
	$Labels/SFX/Right/Slider.callback = funcref(self, "onSFXVolumeChanged")
