extends "res://scripts/UI/components/DefaultButton.gd"

var sideConversation: String

func setSideConversation() -> void:
	Game.setSideConversation(sideConversation)

func _ready() -> void:
	$Label.text = GameData.sideConversations[sideConversation].sideConversationTitle
	callback = funcref(self, "setSideConversation")
	if !GameData.sideConversations[sideConversation].read:
		buttonType = buttonTypes.positive
