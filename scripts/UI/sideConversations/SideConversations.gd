extends CenterContainer

const SideConversationButton := preload("res://scenes/UI/sideConversations/SideConversationButton.tscn")
onready var Title = get_parent()

var isActive := false

func show() -> void:
	isActive = true
	$Container.get_child(0).grab_focus()
	.show()

func hide() -> void:
	isActive = false
	.hide()

func _input(_event) -> void:
	if isActive && Input.is_action_just_pressed("ui_cancel"):
		Title.setDefaultMode()
		accept_event()

func _ready() -> void:
	for sideConversation in GameData.sideConversations:
		if GameData.sideConversations[sideConversation].unlocked:
			var Button = SideConversationButton.instance()
			Button.sideConversation = sideConversation
			$Container.add_child(Button)
