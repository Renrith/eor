extends PanelContainer

var Get: Label
var Cost: Label

var styleBox: StyleBoxFlat

func toggleButton(skillData: Dictionary, skillPointsAvailable: int):
	if skillData.isAcquired:
		hide()
	else:
		show()
		Cost.text = String(skillData.cost)
		if skillPointsAvailable < skillData.cost:
			Get.add_color_override("font_color", Colors.secondaryTextColor)
			Cost.add_color_override("font_color", Colors.negativeTextColor)
			styleBox.set("border_color", Colors.secondaryTextColor)
		else:
			Get.add_color_override("font_color", Colors.primaryColor)
			Cost.add_color_override("font_color", Colors.primaryColor)
			styleBox.set("border_color", Colors.primaryColor)

func _ready():
	Get = get_node("HBox/Get")
	Cost = get_node("HBox/Cost")
	styleBox = get_stylebox("panel")
