extends Control

export(String) var actorData

var BuildEdit: Control

var actor: Node2D
var selectedSkill: TextureRect

var isActive: bool = false

func selectSkill(skill: TextureRect) -> void:
	if selectedSkill != null:
		selectedSkill.deselect()
	selectedSkill = skill
	selectedSkill.select()
	
	BuildEdit.onSkillSelected(
		actor.skillList[selectedSkill.index],
		actor)

func acquireSkill() -> void:
	var skill = actor.skillList[selectedSkill.index]
	if skill.isAcquired == false && skill.cost <= actor.skillPoints:
		actor.acquireSkill(selectedSkill.index)
		selectedSkill.acquireSkill()
		
		BuildEdit.onSkillSelected(skill, actor)

func show() -> void:
	BuildEdit.onSkillSelected(
		actor.skillList[selectedSkill.index],
		actor)
	
	isActive = true
	.show()

func hide() -> void:
	isActive = false
	.hide()

func _input(_event) -> void:
	if isActive:
		var newSkill: TextureRect
		
		if Input.is_action_just_pressed("ui_accept"):
			BuildEdit.onFocusSkillPanel()
		elif Input.is_action_pressed("ui_down"):
			newSkill = get_node(selectedSkill.focus_neighbour_bottom)
		elif Input.is_action_pressed("ui_up"):
			newSkill = get_node(selectedSkill.focus_neighbour_top)
		elif Input.is_action_pressed("ui_left"):
			newSkill = get_node(selectedSkill.focus_neighbour_left)
		elif Input.is_action_pressed("ui_right"):
			newSkill = get_node(selectedSkill.focus_neighbour_right)
		
		if newSkill != null:
			selectedSkill.material = null
			selectSkill(newSkill)

func _enter_tree() -> void:
	actor = get_tree().get_current_scene().actors[actorData]

func _ready() -> void:
	BuildEdit = get_parent().get_node("BuildEdit")
	
	var skill: TextureRect = $Skills.get_child(0)
	selectSkill(skill)
