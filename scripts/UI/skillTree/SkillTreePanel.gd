tool
extends "res://scripts/UI/components/PanelContainer.gd"

var BuildEdit: Control
var SkillName: Label
var SkillDescription: Label
var SkillType: Label

var EquipButton: MarginContainer
var ReturnButton: MarginContainer
var BorrowButton: MarginContainer
var BuyButton: MarginContainer

var activeButton: MarginContainer
var isActive: bool = false

var actor: Node2D
var skillData: Dictionary
var build: Reference

const returnButtonText: String = "Return (+%s build points)"
const buyButtonText: String = "Get (-%s skill points)"
const borrowButtonText: String = "Borrow (-%s build points)"
const equipButtonText: String = "Equip"
const unequipButtonText: String = "Unequip"

func showSkillProperties(
	newSkillData: Dictionary,
	newActor: Node2D,
	newBuild: Reference) -> void:
	actor = newActor
	skillData = newSkillData
	build = newBuild
	SkillName.text = Globals.Skills.gdGetSkillName(skillData.index).capitalize()
	SkillDescription.text = Globals.Skills.gdGetSkillDescription(skillData.index)
	SkillType.text = Globals.Skills.gdGetSkillType(skillData.index)
	
	toggleButtons(skillData, actor.skillPoints, actor.getRemainingBuildPoints(build))
	
	rect_size.y = 0

func toggleButtons(
	skillData: Dictionary,
	skillPoints: int,
	buildPoints: int) -> void:
	if skillData.isAcquired:
		EquipButton.show()
		if build.gdIsSkillEquipped(skillData.index):
			EquipButton.text = unequipButtonText
		else:
			EquipButton.text = equipButtonText
		
		ReturnButton.hide()
		BorrowButton.hide()
		BuyButton.hide()
	else:
		var canBeAcquired: bool = false
		for dependency in skillData.dependencies:
			var dependencyData = actor.skillList[dependency]
			if dependencyData.isAcquired == true:
				canBeAcquired = true
		
		if build.gdIsSkillBorrowed(skillData.index):
			EquipButton.show()
			if build.gdIsSkillEquipped(skillData.index):
				EquipButton.text = unequipButtonText
			else:
				EquipButton.text = equipButtonText
			ReturnButton.show()
			ReturnButton.text = returnButtonText % skillData.borrowCost
		else:
			EquipButton.hide()
			ReturnButton.hide()
			BorrowButton.show()
			BorrowButton.text = borrowButtonText % skillData.borrowCost
			var canBeBorrowed = false
			for index in skillData.dependencies:
				if build.gdIsSkillBorrowed(index):
					canBeBorrowed = true
			if buildPoints > skillData.borrowCost && (canBeAcquired || canBeBorrowed):
				BorrowButton.disabled = false
			else:
				BorrowButton.disabled = true
		
		BuyButton.show()
		BuyButton.text = buyButtonText % skillData.cost
		if skillPoints > skillData.cost && canBeAcquired:
			BuyButton.disabled = false
		else:
			BuyButton.disabled = true

func onSkillEquipToggled() -> void:
	build.gdToggleSkill(skillData.index)
	BuildEdit.onSkillSelected(skillData, actor)

func onSkillBorrowed() -> void:
	build.gdBorrowSkill(skillData.index)
	BuildEdit.onSkillSelected(skillData, actor)
	onFocusEntered()

func onSkillReturned() -> void:
	var dependencyIndexes = [skillData.index]
	while dependencyIndexes.size() > 0:
		var index: int = dependencyIndexes.pop_front()
		if build.gdIsSkillBorrowed(index):
			build.gdReturnSkill(index)
			for skillIndex in actor.skillList:
				for dependencyIndex in actor.skillList[skillIndex].dependencies:
					if index == dependencyIndex:
						dependencyIndexes.push_back(skillIndex)
	BuildEdit.onSkillSelected(skillData, actor)
	onFocusEntered()

func onSkillAcquired() -> void:
	if build.gdIsSkillBorrowed(skillData.index):
		build.gdReturnSkill(skillData.index)
	actor.acquireSkill(skillData.index)
	BuildEdit.onSkillSelected(skillData, actor)
	onFocusEntered()

func onFocusEntered() -> void:
	var buttons: Array = $VBox/ButtonContainer.get_children()
	for button in buttons:
		if button.visible:
			activeButton = button
			break
	activeButton.grab_focus()
	isActive = true

func _input(_event: InputEvent) -> void:
	if isActive:
		if Input.is_action_just_pressed("ui_cancel"):
			isActive = false
			activeButton.release_focus()
			BuildEdit.onUnfocusSkillPanel()
			accept_event()

func _ready() -> void:
	BuildEdit = get_parent()
	SkillName = $Top.get_node("SkillName")
	SkillDescription = $VBox.get_node("SkillDescription")
	SkillType = $VBox.get_node("SkillType")
	
	EquipButton = $VBox/ButtonContainer.get_node("EquipButton")
	EquipButton.callback = funcref(self, "onSkillEquipToggled")
	ReturnButton = $VBox/ButtonContainer.get_node("ReturnButton")
	ReturnButton.callback = funcref(self, "onSkillReturned")
	BorrowButton = $VBox/ButtonContainer.get_node("BorrowButton")
	BorrowButton.callback = funcref(self, "onSkillBorrowed")
	BuyButton = $VBox/ButtonContainer.get_node("BuyButton")
	BuyButton.callback = funcref(self, "onSkillAcquired")
	
	connect("focus_entered", self, "onFocusEntered")
