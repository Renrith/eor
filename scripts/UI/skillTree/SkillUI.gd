tool
extends TextureRect

export(int) var index

var obtainableSkillMaterial := ShaderMaterial.new()
var selectedSkillMaterial := ShaderMaterial.new()
var selectedObtainableSkillMaterial := ShaderMaterial.new()

var skillTree: Control
var skillName: String

func isAcquired() -> bool:
	return skillTree.actor.skillList[index].isAcquired

func deselect() -> void:
	if isAcquired():
		material = null
	else:
		material = obtainableSkillMaterial

func select() -> void:
	if isAcquired():
		material = selectedSkillMaterial
	else:
		material = selectedObtainableSkillMaterial

func acquireSkill() -> void:
	material = selectedSkillMaterial

func _ready() -> void:
	skillTree = get_owner()
	skillName = Globals.Skills.gdGetSkillName(index)
	texture = load(Globals.Skills.gdGetSkillIcon(index))
	
	obtainableSkillMaterial.shader = preload("res://shaders/skillTree/skillObtainable.shader")
	obtainableSkillMaterial.set_shader_param("color", Colors.primaryColor)
	obtainableSkillMaterial.set_shader_param("outlineColor", Colors.textColor)
	
	selectedSkillMaterial.shader = preload("res://shaders/skillTree/skillSelected.shader")
	selectedSkillMaterial.set_shader_param("color", Colors.primaryColor)
	selectedSkillMaterial.set_shader_param("outlineColor", Colors.textColor)
	
	selectedObtainableSkillMaterial.shader = preload("res://shaders/skillTree/skillObtainableSelected.shader")
	selectedObtainableSkillMaterial.set_shader_param("color", Colors.primaryColor)
	selectedObtainableSkillMaterial.set_shader_param("outlineColor", Colors.textColor)
	
	deselect()
