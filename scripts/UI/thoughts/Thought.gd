extends "res://scripts/UI/dialogues/TextContainer.gd"

const skillText: String = "+%s SP"
const buildText: String = "+%s BP"
const skillBuildText: String = "+%s SP | +%s BP"

const tickIcon: StreamTexture = preload("res://sprites/UI/tick.png")
const crossIcon: StreamTexture = preload("res://sprites/UI/cross.png")

func showThought(thought: Reference, sprite: StreamTexture) -> void:
	$Text.text = thought.text
	$Wincon.text = thought.winconText
	
	if thought.skillPoints != 0 && thought.buildPoints != 0:
		$Points.text = skillBuildText % [thought.skillPoints, thought.buildPoints]
	elif thought.skillPoints != 0:
		$Points.text = skillText % thought.skillPoints
	elif thought.buildPoints != 0:
		$Points.text = buildText % thought.buildPoints
	else:
		$Points.text = ""
	
	if thought.isAchieved:
		if thought.shouldBeAchieved:
			$Icon.texture = tickIcon
		else:
			$Icon.texture = null
	else:
		if thought.shouldBeAchieved:
			$Icon.texture = null
		else:
			$Icon.texture = crossIcon
	
	$Sprite.texture = sprite
	
	.show()
