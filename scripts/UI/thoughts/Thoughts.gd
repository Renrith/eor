extends Control

var Preparations: CanvasLayer
var BattleRewards: Node
var SkillPoints: Label
var BuildPoints: Label
var Separator: Label
var Separator2: Label

const skillText = "%s SP"
const buildText = "%s BP"

var isActive: bool = false

func onCancel() -> void:
	hide()
	Preparations.setDefaultMode()

func show() -> void:
	if BattleRewards.skillPoints != 0 && BattleRewards.buildPoints != 0:
		Separator2.show()
	else:
		Separator2.hide()
	
	if BattleRewards.skillPoints != 0 || BattleRewards.buildPoints != 0:
		Separator.show()
	else:
		Separator.hide()
	
	if BattleRewards.skillPoints != 0:
		SkillPoints.text = skillText % BattleRewards.skillPoints
		SkillPoints.show()
	else:
		SkillPoints.hide()
	
	if BattleRewards.buildPoints != 0:
		BuildPoints.text = buildText % BattleRewards.buildPoints
		BuildPoints.show()
	else:
		BuildPoints.hide()
	
	$ThoughtsContainer.showThoughts()
	
	isActive = true
	.show()

func hide() -> void:
	isActive = false
	.hide()

func _input(_event) -> void:
	if isActive && Input.is_action_just_pressed("ui_cancel"):
		onCancel()
		accept_event()

func _ready() -> void:
	Preparations = get_node("/root/Viewports/UIViewport/Preparations")
	BattleRewards = get_node("/root/Setting").BattleRewards
	SkillPoints = $Header/HBox/SkillPoints
	BuildPoints = $Header/HBox/BuildPoints
	Separator = $Header/HBox/Separator
	Separator2 = $Header/HBox/Separator2
