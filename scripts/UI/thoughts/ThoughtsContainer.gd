extends CenterContainer

const Thought: PackedScene = preload("res://scenes/UI/thoughts/Thought.tscn")

func showThoughts() -> void:
	for child in $Thoughts.get_children():
		$Thoughts.remove_child(child)
	
	var allActors: Array = get_tree().get_nodes_in_group("actors")
	for actor in allActors:
		if actor.Core && actor.Core.isPlayer && actor.Core.thought != null:
			var actorThought = Thought.instance()
			$Thoughts.add_child(actorThought)
			var sprite: StreamTexture = actor.Sprite.frames.get_frame("default", 0)
			actorThought.showThought(actor.Core.thought, sprite)
