extends "res://scripts/UI/components/DefaultButton.gd"

onready var SaveInformation: MarginContainer = get_tree().current_scene.get_node("SaveInformation")
var hasSavedData := false

func onFocusEntered() -> void:
	if hasSavedData:
		SaveInformation.show()
	.onFocusEntered()

func onFocusExited() -> void:
	if hasSavedData:
		SaveInformation.hide()
	.onFocusExited()

func _ready() -> void:
	grab_focus()
	hasSavedData = Saves.hasSavedData()
	if hasSavedData:
		callback = funcref(Saves, "loadMostRecentData")
	else:
		callback = funcref(Game, "advanceSetting")
		var Label: Label = get_node("Label")
		Label.text = "Start"
		SaveInformation.hide()
