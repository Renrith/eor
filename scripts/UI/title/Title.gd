extends ViewportContainer

enum titleMenu {
	default,
	loading,
	sideConversations,
}
var currentMenu: int = titleMenu.default

var titleTrack = preload("res://sound/music/winterNightWarmth.mp3")

func setDefaultMode() -> void:
	$Loading.hide()
	$SideConversations.hide()
	$Buttons.show()
	$SaveInformation.show()
	match currentMenu:
		titleMenu.loading:
			$Buttons/Container/Load.grab_focus()
		titleMenu.sideConversations:
			$Buttons/Container/SideConversations.grab_focus()
	currentMenu = titleMenu.default

func setLoadMode() -> void:
	$Buttons.hide()
	$SaveInformation.hide()
	$Loading.show()
	currentMenu = titleMenu.loading

func setSideConversationsMode() -> void:
	$Buttons.hide()
	$SaveInformation.hide()
	$Loading.hide()
	$SideConversations.show()
	currentMenu = titleMenu.sideConversations

func quitGame() -> void:
	get_tree().quit()

func _ready() -> void:
	var NewGameButton: MarginContainer = get_node("Buttons/Container/NewGame")
	if Saves.hasSavedData():
		NewGameButton.callback = funcref(Game, "advanceSetting")
		$Buttons/Container/Load.callback = funcref(self, "setLoadMode")
		$Buttons/Container/SideConversations.callback = funcref(self, "setSideConversationsMode")
	else:
		NewGameButton.hide()
		$Buttons/Container/Load.hide()
	
	$Buttons/Container/Quit.callback = funcref(self, "quitGame")
	
	Viewports.get_node("GraphicsViewport/Background").texture = preload("res://sprites/UI/titleBg.png")
	
	Globals.MusicController.setMusic(titleTrack)

func _exit_tree() -> void:
	Viewports.get_node("GraphicsViewport/Background").texture = null
