using System.Collections.Generic;

public enum AllActions
{
	none = 0,
	attack = 1,
	rangedSkipAttack = 2,
	rangedLineAttack = 4,
	push = 8,
	slashAttack = 16,
	newAction = 32,
}

public enum TargetingType
{
	Neighbour,
	RangedSkip,
	RangedLine,
	Slash,
	Self,
}

public class ActionOutcome
{
	public IPlaceable target;

	public int enduranceChange;
	public int energyChange;

	public int turnOrderChange = 0;

	public List<Tile> path;
	public int heightLevelChange;
	public Tile holdTile;

	public List<Buff> attackBuffsApplied;
	public List<Buff> attackBuffsRemoved;
	public List<Buff> pushBuffsApplied;
	public List<Buff> pushBuffsRemoved;

	public Gang gang;

	public static Godot.Collections.Array getGdData(List<ActionOutcome> outcomeDataList)
	{
		Godot.Collections.Array gdOutcomeDataList = new Godot.Collections.Array();
		foreach(ActionOutcome outcomeData in outcomeDataList)
		{
			Godot.Collections.Dictionary gdOutcomeData = new Godot.Collections.Dictionary();
			Godot.Collections.Array gdPath = new Godot.Collections.Array();
			if(outcomeData.path != null)
			{
				foreach(Tile tile in outcomeData.path)
				{
					gdPath.Add(tile);
				}
			}

			gdOutcomeData.Add("target", outcomeData.target.Base);
			gdOutcomeData.Add("path", gdPath);
			gdOutcomeData.Add("energyChange", outcomeData.energyChange);
			gdOutcomeData.Add("heightLevelChange", outcomeData.heightLevelChange);
			gdOutcomeData.Add("holdTile", outcomeData.holdTile);
			gdOutcomeData.Add("enduranceChange", outcomeData.enduranceChange);

			if(outcomeData.gang != null)
			{
				gdOutcomeData.Add("gang", outcomeData.gang.index);
			}

			if(outcomeData.attackBuffsApplied != null)
			{
				Godot.Collections.Array gdAttackBuffsApplied = new Godot.Collections.Array();
				foreach(Buff attackBuff in gdAttackBuffsApplied)
				{
					Godot.Collections.Dictionary gdAttackBuff = new Godot.Collections.Dictionary();
					gdAttackBuff.Add("turnsRemaining", attackBuff.turnsRemaining);
					gdAttackBuff.Add("change", attackBuff.change);
					gdAttackBuffsApplied.Add(gdAttackBuff);
				}
				gdOutcomeData.Add("attackBuffsApplied", gdAttackBuffsApplied);
			}
			if(outcomeData.attackBuffsRemoved != null)
			{
				Godot.Collections.Array gdAttackBuffsRemoved = new Godot.Collections.Array();
				foreach(Buff attackBuff in gdAttackBuffsRemoved)
				{
					Godot.Collections.Dictionary gdAttackBuff = new Godot.Collections.Dictionary();
					gdAttackBuff.Add("turnsRemaining", attackBuff.turnsRemaining);
					gdAttackBuff.Add("change", attackBuff.change);
					gdAttackBuffsRemoved.Add(gdAttackBuff);
				}
				gdOutcomeData.Add("attackBuffsRemoved", gdAttackBuffsRemoved);
			}
			if(outcomeData.pushBuffsApplied != null)
			{
				Godot.Collections.Array gdPushBuffsApplied = new Godot.Collections.Array();
				foreach(Buff pushBuff in gdPushBuffsApplied)
				{
					Godot.Collections.Dictionary gdPushBuff = new Godot.Collections.Dictionary();
					gdPushBuff.Add("turnsRemaining", pushBuff.turnsRemaining);
					gdPushBuff.Add("change", pushBuff.change);
					gdPushBuffsApplied.Add(gdPushBuff);
				}
				gdOutcomeData.Add("pushBuffsApplied", gdPushBuffsApplied);
			}
			if(outcomeData.pushBuffsRemoved != null)
			{
				Godot.Collections.Array gdPushBuffsRemoved = new Godot.Collections.Array();
				foreach(Buff pushBuff in gdPushBuffsRemoved)
				{
					Godot.Collections.Dictionary gdPushBuff = new Godot.Collections.Dictionary();
					gdPushBuff.Add("turnsRemaining", pushBuff.turnsRemaining);
					gdPushBuff.Add("change", pushBuff.change);
					gdPushBuffsRemoved.Add(gdPushBuff);
				}
				gdOutcomeData.Add("pushBuffsRemoved", gdPushBuffsRemoved);
			}

			gdOutcomeDataList.Add(gdOutcomeData);
		}
		return gdOutcomeDataList;
	}

	public ActionOutcome(ActorCore _target, int _enduranceChange)
	{
		target = _target;
		enduranceChange = _enduranceChange;
	}

	public ActionOutcome(ActorCore _target, Gang _gang)
	{
		target = _target;
		gang = _gang;
	}

	public ActionOutcome(ActorCore _target)
	{
		target = _target;
	}

	public ActionOutcome() { }
}
