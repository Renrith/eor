using Godot;
using System.Collections.Generic;

public class Battle : Node
{
	protected enum CompletionState
	{
		None,
		Ended,
		Completed,
	}

	protected enum DefeatReason
	{
		EnduranceDepleted,
		PushedOff,
		Other,
	}

	[Signal]
	public delegate void actorTurnStart();

	protected Timer endTimer;

	private bool isAITakingAction = false;

	protected CompletionState completionState = CompletionState.None;

	public List<IPlaceable> initialPlaceables = new List<IPlaceable>();
	public List<IPlaceable> reinforcementPlaceables = new List<IPlaceable>();
	public List<OriginalCore> reinforcementActors = new List<OriginalCore>();
	public List<IPlaceable> currentPlaceables = new List<IPlaceable>();
	public List<OriginalCore> currentActors = new List<OriginalCore>();

	public static readonly Dictionary<string, ActorCore> playerActors = new Dictionary<string, ActorCore>
	{
		{"Alaitz", null},
		{"Tara", null},
		{"Nalke", null},
		{"Naimel", null},
	};

	private ActorCore _currentActor;
	public ActorCore currentActor
	{
		get { return _currentActor; }
		set
		{
			previousActor = _currentActor;
			_currentActor = value;
		}
	}
	public ActorCore previousActor; //TODO: Not working properly!

	public ActorCore kidnapTarget;
	public Tile kidnapTile;
	public List<AITriggerableCore> triggerableActors;
	public bool shouldMeet = false;

	protected Node Globals;
	protected Godot.Collections.Dictionary conditionsList;
	protected ViewportContainer Setting;
	protected Node2D Cursor;
	protected Map Map;
	protected CanvasLayer UI;

	protected virtual bool hasLost()
	{
		foreach(ActorCore actor in currentActors)
		{
			if(actor.gang.index == (int)Factions.names.AlaitzsGang && actor.isAlive())
			{
				return false;
			}
		}
		return true;
	}

	protected virtual bool hasWon()
	{
		foreach(ActorCore actor in currentActors)
		{
			if(actor.gang.index != (int)Factions.names.AlaitzsGang && actor.isAlive() && Factions.getAlliance((int)Factions.names.AlaitzsGang, actor.gang.index) == AllianceType.Enemy)
			{
				return false;
			}
		}
		return true;
	}

	private Godot.Collections.Array gdGetDefeatData()
	{
		Godot.Collections.Array defeatData = new Godot.Collections.Array();

		foreach(KeyValuePair<string, ActorCore> entry in playerActors)
		{
			if(entry.Value != null && !entry.Value.isAlive())
			{
				defeatData.Add(entry.Key);
				if(entry.Value.tile == null)
				{
					defeatData.Add((int)DefeatReason.PushedOff);
				}
				else
				{
					defeatData.Add((int)DefeatReason.EnduranceDepleted);
				}
				break;
			}
		}
		return defeatData;
	}

	private void clearSimulatedTurn()
	{
		foreach(ActorCore actor in currentActors)
		{
			actor.simulatedTurnOrder = actor.turnOrder;
		}
	}

	protected virtual bool callDialogues(ActorCore actor, ActorCore target, string action, int enduranceChange = 0)
	{
		return false;
	}

	protected virtual void executeDialoguesCallbacks(string interpolatedDialoguesKey) { }

	public List<ActorCore> getTurnOrder(int n, List<ActorCore> actors = null)
	{
		List<ActorCore> turnOrder = new List<ActorCore>();
		while(turnOrder.Count < n)
		{
			ActorCore actor = getNextActor(actors);
			if(actor.isAlive()) //we could have a character animating but not alive anymore
			{
				turnOrder.Add(actor);
				actor.simulatedTurnOrder += actor.turnRate;
			}
		}
		clearSimulatedTurn();
		return turnOrder;
	}

	public List<ActorCore> getClosestActorsTurnOrder(int n, Tile tile)
	{
		List<ActorCore> closestActors = Map.getClosestActors(n, tile);
		return getTurnOrder(n, closestActors);
	}

	public int getActorTurnPosition(ActorCore actor)
	{
		int position = -1;
		ActorCore nextActor;
		do
		{
			nextActor = getNextActor();
			nextActor.simulatedTurnOrder += nextActor.turnRate;
			position++;
		}
		while(nextActor != actor && position < 100);
		clearSimulatedTurn();
		return position;
	}

	private ActorCore getNextActor(List<ActorCore> actors = null)
	{
		List<ActorCore> turnOrder = new List<ActorCore>();

		List<OriginalCore> defaultOrderActorList = currentActors;
		if(actors != null && actors.Count > 0)
		{
			defaultOrderActorList = new List<OriginalCore>();
			foreach(OriginalCore actor in currentActors)
			{
				if(actors.Contains(actor))
				{
					defaultOrderActorList.Add(actor);
				}
			}
		}

		foreach(ActorCore actor in defaultOrderActorList)
		{
			if(actor.isAlive())
			{
				if(turnOrder.Count > 0)
				{
					int index = 0;
					foreach(ActorCore sortedActor in turnOrder)
					{
						if(actor.simulatedTurnOrder > sortedActor.simulatedTurnOrder)
						{
							index += 1;
						}
						else
						{
							break;
						}
					}
					turnOrder.Insert(index, actor);
				}
				else
				{
					turnOrder.Add(actor);
				}
			}
		}

		return turnOrder[0];
	}

	private void initializeActor(Node2D actorBase)
	{
		actorBase.Call("addController");
		OriginalCore actor = (OriginalCore)actorBase.Get("Core");
		currentActors.Add(actor);

		switch(actorBase.Get("actorName"))
		{
			case "Alaitz":
				playerActors["Alaitz"] = actor;
				break;
			case "Tara":
				playerActors["Tara"] = actor;
				break;
			case "Nalke":
				playerActors["Nalke"] = actor;
				break;
			case "Naimel":
				playerActors["Naimel"] = actor;
				break;
			case "Rinuri":
				playerActors["Rinuri"] = actor;
				break;
			case "Kadori":
				playerActors["Kadori"] = actor;
				break;
			case "Amiya":
				playerActors["Amiya"] = actor;
				break;
		}

		Label TurnOrder = (Label)actorBase.GetNode("AnimatedSprite/TurnOrder");
		Connect("actorTurnStart", TurnOrder, "updateTurnOrder");
	}

	private void removeActor(OriginalCore actor)
	{
		actor.Base.Call("removeController");
		Label TurnOrder = (Label)actor.Base.GetNode("AnimatedSprite/TurnOrder");
		Disconnect("actorTurnStart", TurnOrder, "updateTurnOrder");
	}

	protected void startActorTurn()
	{
		Map.debugActorMapMismatch(currentActors);
		clearSimulatedTurn();
		currentActor = getNextActor();
		Cursor.Set("selectedActor", currentActor.Controller);
		currentActor.Controller.Call("startTurn");
	}

	public virtual void startActorVisualTurn(Node2D _actorBase)
	{
		EmitSignal("actorTurnStart");
	}

	protected virtual void endActorTurn()
	{
		if(!hasLost())
		{
			if(!hasWon())
			{
				startActorTurn();
			}
			else
			{
				Cursor.Set("isActive", "false");
				endBattle(true);
			}
		}
		else
		{
			Cursor.Set("isActive", "false");
			endBattle(false);
		}
	}

	protected virtual void endBattle(bool hasWon)
	{
		UI.Call("onBattleOver", hasWon);

		if(hasWon)
		{
			completionState = CompletionState.Completed;
		}
		else
		{
			completionState = CompletionState.Ended;
		}
	}

	public void setActionTurnEnd()
	{
		isAITakingAction = false;
	}

	protected void triggerReinforcements()
	{
		Godot.Collections.Array gdAllPlaceables = GetTree().GetNodesInGroup("placeables");
		foreach(Node2D placeableBase in gdAllPlaceables)
		{
			bool isReinforcement = (bool)placeableBase.Get("isReinforcement");
			if(isReinforcement)
			{
				Vector2 position = Map.WorldToMap((Vector2)placeableBase.Get("position"));
				CustomVector customPosition = new CustomVector((int)position.x, (int)position.y);
				if(Map.customMap[customPosition].occupant == null)
				{
					placeableBase.Show();
					placeableBase.Call("addCore");
					IPlaceable placeable = (IPlaceable)placeableBase.Get("Core");
					reinforcementPlaceables.Add(placeable);
					currentPlaceables.Add(placeable);
					placeable.setBattle(this);
					Map.initializePlaceablePosition(placeableBase);
					if(placeable is OriginalCore)
					{
						initializeActor(placeableBase);
						placeableBase.Call("updateOutline");
						OriginalCore actor = (OriginalCore)placeable;
						reinforcementActors.Add(actor);
						int turnOrder = actor.turnRate + currentActor.turnOrder;
						actor.turnOrder = turnOrder;
						actor.initialTurnOrder = turnOrder;
						actor.simulatedTurnOrder = turnOrder;
					}
				}
			}
		}
	}

	protected virtual void restartBattle()
	{
		Map.wipeTiles(WipeTilesMode.Restart);
		currentActors = new List<OriginalCore>();
		currentPlaceables = new List<IPlaceable>();
		foreach(IPlaceable placeable in initialPlaceables)
		{
			placeable.setInitialState();
			placeable.tile.occupant = placeable;
			currentPlaceables.Add(placeable);

			if(placeable is OriginalCore)
			{
				OriginalCore actor = (OriginalCore)placeable;
				actor.turnOrder = actor.initialTurnOrder;
				actor.simulatedTurnOrder = actor.initialTurnOrder;
				currentActors.Add(actor);
			}

			placeable.Base.Call("restartSprite");
		}
		foreach(OriginalCore placeable in reinforcementPlaceables)
		{
			placeable.Base.Call("remove");
			placeable.Base.Call("restartSprite");
			placeable.Base.Hide();
		}
		foreach(OriginalCore actor in reinforcementActors)
		{
			removeActor(actor);
		}
		AudioStreamPlayer MusicController = (AudioStreamPlayer)Globals.Get("MusicController");
		MusicController.Call("resume");
		startActorTurn();
	}

	public override void _Ready()
	{
		Globals = (Node)GetNode("/root/Globals");
		Node Conditions = (Node)GetNode("/root/Conditions");
		conditionsList = (Godot.Collections.Dictionary)Conditions.Get("conditionsList");
		Setting = (ViewportContainer)GetParent();
		Map = (Map)Setting.Get("Map");
		Cursor = (Node2D)Map.GetNode("YSort/Cursor");
		UI = (CanvasLayer)GetNode("/root/Viewports/UIViewport/BattleUILayer");

		Godot.Collections.Array gdAllPlaceables = GetTree().GetNodesInGroup("placeables");
		foreach(Node2D placeableBase in gdAllPlaceables)
		{
			bool isReinforcement = (bool)placeableBase.Get("isReinforcement");
			if(!isReinforcement)
			{
				IPlaceable placeable = (IPlaceable)placeableBase.Get("Core");
				initialPlaceables.Add(placeable);
				currentPlaceables.Add(placeable);
				placeable.setBattle(this);
			}
		}

		Godot.Collections.Array gdAllActors = GetTree().GetNodesInGroup("actors");
		foreach(Node2D actorBase in gdAllActors)
		{
			bool isReinforcement = (bool)actorBase.Get("isReinforcement");
			if(!isReinforcement)
			{
				initializeActor(actorBase);
				actorBase.Call("updateOutline");
			}
		}

		Godot.Collections.Array cameras = GetTree().GetNodesInGroup("cameras");
		foreach(Camera2D camera in cameras)
		{
			camera.LimitTop = Map.mapSize[0] - 64;
			camera.LimitLeft = Map.mapSize[1] - 256;
			camera.LimitBottom = Map.mapSize[2] + 64;
			camera.LimitRight = Map.mapSize[3] + 256;
		}

		Node Game = (Node)GetNode("/root/Game");
	}
}
