using Godot;

public class BattleRewards : Node
{
	Node Game;

	[Export]
	public int skillPoints = 0;
	[Export]
	public int buildPoints = 0;

	[Export]
	public ThoughtType AlaitzThoughtType = ThoughtType.None;
	[Export]
	public ThoughtQualifier AlaitzThoughtQualifier = ThoughtQualifier.Amount;
	[Export]
	public int AlaitzThreshold = 0;
	[Export]
	public bool AlaitzShouldBeAchieved = true;
	[Export]
	public TargetAlliance AlaitzTargetAlliance = TargetAlliance.Any;
	[Export]
	public ThoughtEvaluationStage AlaitzStage = ThoughtEvaluationStage.TurnEnd;
	[Export]
	public string AlaitzTarget = "";
	[Export]
	public int AlaitzSkillPoints = 0;
	[Export]
	public int AlaitzBuildPoints = 0;
	[Export]
	public string AlaitzText = "";

	[Export]
	public ThoughtType TaraThoughtType = ThoughtType.None;
	[Export]
	public ThoughtQualifier TaraThoughtQualifier = ThoughtQualifier.Amount;
	[Export]
	public int TaraThreshold = 0;
	[Export]
	public bool TaraShouldBeAchieved = true;
	[Export]
	public TargetAlliance TaraTargetAlliance = TargetAlliance.Any;
	[Export]
	public ThoughtEvaluationStage TaraStage = ThoughtEvaluationStage.TurnEnd;
	[Export]
	public string TaraTarget = "";
	[Export]
	public int TaraSkillPoints = 0;
	[Export]
	public int TaraBuildPoints = 0;
	[Export]
	public string TaraText = "";

	[Export]
	public ThoughtType NalkeThoughtType = ThoughtType.None;
	[Export]
	public ThoughtQualifier NalkeThoughtQualifier = ThoughtQualifier.Amount;
	[Export]
	public int NalkeThreshold = 0;
	[Export]
	public bool NalkeShouldBeAchieved = true;
	[Export]
	public TargetAlliance NalkeTargetAlliance = TargetAlliance.Any;
	[Export]
	public ThoughtEvaluationStage NalkeStage = ThoughtEvaluationStage.TurnEnd;
	[Export]
	public string NalkeTarget = "";
	[Export]
	public int NalkeSkillPoints = 0;
	[Export]
	public int NalkeBuildPoints = 0;
	[Export]
	public string NalkeText = "";

	[Export]
	public ThoughtType NaimelThoughtType = ThoughtType.None;
	[Export]
	public ThoughtQualifier NaimelThoughtQualifier = ThoughtQualifier.Amount;
	[Export]
	public int NaimelThreshold = 0;
	[Export]
	public bool NaimelShouldBeAchieved = true;
	[Export]
	public TargetAlliance NaimelTargetAlliance = TargetAlliance.Any;
	[Export]
	public ThoughtEvaluationStage NaimelStage = ThoughtEvaluationStage.TurnEnd;
	[Export]
	public string NaimelTarget = "";
	[Export]
	public int NaimelSkillPoints = 0;
	[Export]
	public int NaimelBuildPoints = 0;
	[Export]
	public string NaimelText = "";

	[Export]
	public ThoughtType RinuriThoughtType = ThoughtType.None;
	[Export]
	public ThoughtQualifier RinuriThoughtQualifier = ThoughtQualifier.Amount;
	[Export]
	public int RinuriThreshold = 0;
	[Export]
	public bool RinuriShouldBeAchieved = true;
	[Export]
	public TargetAlliance RinuriTargetAlliance = TargetAlliance.Any;
	[Export]
	public ThoughtEvaluationStage RinuriStage = ThoughtEvaluationStage.TurnEnd;
	[Export]
	public string RinuriTarget = "";
	[Export]
	public int RinuriSkillPoints = 0;
	[Export]
	public int RinuriBuildPoints = 0;
	[Export]
	public string RinuriText = "";

	[Export]
	public ThoughtType KadoriThoughtType = ThoughtType.None;
	[Export]
	public ThoughtQualifier KadoriThoughtQualifier = ThoughtQualifier.Amount;
	[Export]
	public int KadoriThreshold = 0;
	[Export]
	public bool KadoriShouldBeAchieved = true;
	[Export]
	public TargetAlliance KadoriTargetAlliance = TargetAlliance.Any;
	[Export]
	public ThoughtEvaluationStage KadoriStage = ThoughtEvaluationStage.TurnEnd;
	[Export]
	public string KadoriTarget = "";
	[Export]
	public int KadoriSkillPoints = 0;
	[Export]
	public int KadoriBuildPoints = 0;
	[Export]
	public string KadoriText = "";

	[Export]
	public ThoughtType AmiyaThoughtType = ThoughtType.None;
	[Export]
	public ThoughtQualifier AmiyaThoughtQualifier = ThoughtQualifier.Amount;
	[Export]
	public int AmiyaThreshold = 0;
	[Export]
	public bool AmiyaShouldBeAchieved = true;
	[Export]
	public TargetAlliance AmiyaTargetAlliance = TargetAlliance.Any;
	[Export]
	public ThoughtEvaluationStage AmiyaStage = ThoughtEvaluationStage.TurnEnd;
	[Export]
	public string AmiyaTarget = "";
	[Export]
	public int AmiyaSkillPoints = 0;
	[Export]
	public int AmiyaBuildPoints = 0;
	[Export]
	public string AmiyaText = "";

	PlayerCore Alaitz = null;
	PlayerCore Tara = null;
	PlayerCore Nalke = null;
	PlayerCore Naimel = null;
	PlayerCore Rinuri = null;
	PlayerCore Kadori = null;
	PlayerCore Amiya = null;

	PlayerCore[] playerActors = new PlayerCore[4];

	public void applyBattleRewards()
	{
		foreach(PlayerCore actor in playerActors)
		{
			if(actor != null)
			{
				int actorSkillPoints = (int)actor.Base.Get("skillPoints");
				actorSkillPoints += skillPoints;

				int actorBuildPoints = (int)actor.Base.Get("buildPoints");
				actorBuildPoints += buildPoints;

				if(actor.thought != null && actor.thought.isAchieved == actor.thought.shouldBeAchieved)
				{
					actorSkillPoints += actor.thought.skillPoints;
					actorBuildPoints += actor.thought.buildPoints;
					Game.Call("setThoughtCompleted", actor.Base);
				}

				actor.Base.Set("skillPoints", skillPoints);
				actor.Base.Set("buildPoints", actorBuildPoints);
			}
		}
	}

	private Thought createThought(PlayerCore actor, ThoughtType type, ThoughtQualifier qualifier, int threshold, bool shouldBeAchieved, bool isCompleted, ThoughtEvaluationStage stage, string target, int skillPoints, int buildPoints, string text, TargetAlliance alliance)
	{
		switch(type)
		{
			case ThoughtType.Defeat:
				ActorCore targetCore = null;
				Godot.Collections.Array gdAllActors = GetTree().GetNodesInGroup("actors");
				foreach(Node2D targetBase in gdAllActors)
				{
					if((string)targetBase.Get("actorName") == target)
					{
						targetCore = (ActorCore)targetBase.Get("Core");
					}
				}
				return new DefeatThought(actor, type, qualifier, threshold, shouldBeAchieved, isCompleted, stage, targetCore, skillPoints, buildPoints, text, alliance);

			case ThoughtType.Endurance:
				return new EnduranceThought(actor, type, qualifier, threshold, shouldBeAchieved, isCompleted, skillPoints, buildPoints, text, alliance);

			case ThoughtType.PushOff:
				return new PushOffThought(actor, type, qualifier, threshold, shouldBeAchieved, isCompleted, skillPoints, buildPoints, text, alliance);

			default:
				return new Thought(actor, type, qualifier, threshold, shouldBeAchieved, isCompleted, skillPoints, buildPoints, text, alliance);
		}
	}

	public override void _Ready()
	{
		Game = GetNode("/root/Game");
		Godot.Collections.Dictionary thoughtsCompleted = (Godot.Collections.Dictionary)Game.Call("getCurrentSceneThoughtsCompleted");

		Godot.Collections.Array gdAllActors = GetTree().GetNodesInGroup("actors");
		foreach(Node2D actor in gdAllActors)
		{
			switch(actor.Get("actorName"))
			{
				case "Alaitz":
					Alaitz = (PlayerCore)actor.Get("Core");
					break;
				case "Tara":
					Tara = (PlayerCore)actor.Get("Core");
					break;
				case "Nalke":
					Nalke = (PlayerCore)actor.Get("Core");
					break;
				case "Naimel":
					Naimel = (PlayerCore)actor.Get("Core");
					break;
				case "Rinuri":
					Rinuri = (PlayerCore)actor.Get("Core");
					break;
				case "Kadori":
					Kadori = (PlayerCore)actor.Get("Core");
					break;
				case "Amiya":
					Amiya = (PlayerCore)actor.Get("Core");
					break;
			}
		}

		playerActors[0] = Alaitz;
		playerActors[1] = Tara;
		playerActors[2] = Nalke;
		playerActors[3] = Naimel;
		playerActors[4] = Rinuri;
		playerActors[5] = Kadori;
		playerActors[6] = Amiya;

		if(AlaitzThoughtType != ThoughtType.None)
		{
			bool AlaitzIsThoughtCompleted = thoughtsCompleted.Contains("Alaitz");
			Alaitz.thought = createThought(Alaitz, AlaitzThoughtType, AlaitzThoughtQualifier, AlaitzThreshold, AlaitzShouldBeAchieved, AlaitzIsThoughtCompleted, AlaitzStage, AlaitzTarget, AlaitzSkillPoints, AlaitzBuildPoints, AlaitzText, AlaitzTargetAlliance);
		}

		if(TaraThoughtType != ThoughtType.None)
		{
			bool TaraIsThoughtCompleted = thoughtsCompleted.Contains("Tara");
			Tara.thought = createThought(Tara, TaraThoughtType, TaraThoughtQualifier, TaraThreshold, TaraShouldBeAchieved, TaraIsThoughtCompleted, TaraStage, TaraTarget, TaraSkillPoints, TaraBuildPoints, TaraText, TaraTargetAlliance);
		}

		if(NalkeThoughtType != ThoughtType.None)
		{
			bool NalkeIsThoughtCompleted = thoughtsCompleted.Contains("Nalke");
			Nalke.thought = createThought(Nalke, NalkeThoughtType, NalkeThoughtQualifier, NalkeThreshold, NalkeShouldBeAchieved, NalkeIsThoughtCompleted, NalkeStage, NalkeTarget, NalkeSkillPoints, NalkeBuildPoints, NalkeText, NalkeTargetAlliance);
		}

		if(NaimelThoughtType != ThoughtType.None)
		{
			bool NaimelIsThoughtCompleted = thoughtsCompleted.Contains("Naimel");
			Naimel.thought = createThought(Naimel, NaimelThoughtType, NaimelThoughtQualifier, NaimelThreshold, NaimelShouldBeAchieved, NaimelIsThoughtCompleted, NaimelStage, NaimelTarget, NaimelSkillPoints, NaimelBuildPoints, NaimelText, NaimelTargetAlliance);
		}
	}
}
