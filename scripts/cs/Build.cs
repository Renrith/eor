using Godot;
using System.Collections.Generic;

public class Build : Reference
{
	public List<AttributeModifier> attributeSkills = new List<AttributeModifier>();

	public IActionModifier attackSkill;
	public IActionModifier pushSkill;

	public INewAction actionSkill;

	public List<Skill> allBorrowedSkills = new List<Skill>();

	public string name;
	public StreamTexture portrait;

	public int gdGetAttackSkillIndex()
	{
		return Skills.allSkills.FindIndex(skill => skill == attackSkill);
	}

	public int gdGetPushSkillIndex()
	{
		return Skills.allSkills.FindIndex(skill => skill == pushSkill);
	}

	public int gdGetActionSkillIndex()
	{
		return Skills.allSkills.FindIndex(skill => skill == actionSkill);
	}

	public void gdBorrowSkill(int skillIndex)
	{
		Skill skill = Skills.allSkills[skillIndex];
		allBorrowedSkills.Add(skill);
	}

	public void gdReturnSkill(int skillIndex)
	{
		Skill skill = Skills.allSkills[skillIndex];
		allBorrowedSkills.Remove(skill);
		if(gdIsSkillEquipped(skillIndex))
		{
			gdRemoveSkill(skillIndex);
		}
	}

	public bool gdIsSkillBorrowed(int skillIndex)
	{
		foreach(Skill borrowedSkill in allBorrowedSkills)
		{
			int index = Skills.allSkills.FindIndex(skill => skill == borrowedSkill);
			if(index == skillIndex)
			{
				return true;
			}
		}
		return false;
	}

	public Godot.Collections.Array gdGetBorrowedSkills()
	{
		Godot.Collections.Array allBorrowedSkillIndexes = new Godot.Collections.Array();
		foreach(Skill skill in allBorrowedSkills)
		{
			allBorrowedSkillIndexes.Add(skill.index);
		}
		return allBorrowedSkillIndexes;
	}

	public void gdSetSkill(int skillIndex)
	{
		Skill skill = Skills.allSkills[skillIndex];
		if(skill is AttributeModifier)
		{
			AttributeModifier attributeSkill = (AttributeModifier)skill;
			attributeSkills.Add(attributeSkill);
		}
		else if(skill is IActionModifier)
		{
			IActionModifier actionModifierSkill = (IActionModifier)skill;
			switch(actionModifierSkill.action)
			{
				case AllActions.attack:
					attackSkill = actionModifierSkill;
					break;
				case AllActions.push:
					pushSkill = actionModifierSkill;
					break;
			}
		}
		else if(skill is INewAction)
		{
			actionSkill = (INewAction)skill;
		}
	}

	public void gdRemoveSkill(int skillIndex)
	{
		Skill skill = Skills.allSkills[skillIndex];
		if(skill is AttributeModifier)
		{
			AttributeModifier attributeSkill = (AttributeModifier)skill;
			attributeSkills.Remove(attributeSkill);
		}
		else if(skill is IActionModifier)
		{
			IActionModifier actionModifierSkill = (IActionModifier)skill;
			switch(actionModifierSkill.action)
			{
				case AllActions.attack:
					attackSkill = null;
					break;
				case AllActions.push:
					pushSkill = null;
					break;
			}
		}
		else if(skill is INewAction)
		{
			actionSkill = null;
		}
	}

	public void gdToggleSkill(int skillIndex)
	{
		if(gdIsSkillEquipped(skillIndex))
		{
			gdRemoveSkill(skillIndex);
		}
		else
		{
			gdSetSkill(skillIndex);
		}
	}

	public bool gdIsSkillEquipped(int skillIndex)
	{
		Skill skill = Skills.allSkills[skillIndex];
		if(skill is AttributeModifier)
		{
			AttributeModifier attributeSkill = (AttributeModifier)skill;
			foreach(AttributeModifier equippedSkill in attributeSkills)
			{
				if(attributeSkill == equippedSkill)
				{
					return true;
				}
			}
		}
		else if(skill is IActionModifier)
		{
			IActionModifier actionModifierSkill = (IActionModifier)skill;
			switch(actionModifierSkill.action)
			{
				case AllActions.attack:
					if(attackSkill == actionModifierSkill)
					{
						return true;
					}
					else
					{
						return false;
					}
				case AllActions.push:
					if(pushSkill == actionModifierSkill)
					{
						return true;
					}
					else
					{
						return false;
					}
			}
		}
		else if(skill is INewAction)
		{
			if(actionSkill == skill)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

	public void gdSetPortrait(StreamTexture _portrait)
	{
		portrait = _portrait;
	}

	Build() { }

	Build(string _name, StreamTexture _portrait)
	{
		name = _name;
		portrait = _portrait;
	}
}
