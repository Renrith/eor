using Godot;

public class CustomTileSet : TileSet
{
	[Export]
	public Godot.Collections.Array<TileTypes> tileTypes = new Godot.Collections.Array<TileTypes>();
}
