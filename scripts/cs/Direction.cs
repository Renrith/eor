using System.Collections.Generic;

public enum Direction
{
	Right,
	Down,
	Left,
	Up,
	Below,
}

public static class DirectionHelper
{
	public static readonly Dictionary<Direction, Direction> oppositeDirection = new Dictionary<Direction, Direction>
	{
		{Direction.Right, Direction.Left},
		{Direction.Down, Direction.Up},
		{Direction.Left, Direction.Right},
		{Direction.Up, Direction.Down},
	};

	public static readonly Dictionary<Direction, Direction[]> sideDirections = new Dictionary<Direction, Direction[]>
	{
		{Direction.Right, new Direction[2]{Direction.Up, Direction.Down}},
		{Direction.Down, new Direction[2]{Direction.Right, Direction.Left}},
		{Direction.Left, new Direction[2]{Direction.Down, Direction.Up}},
		{Direction.Up, new Direction[2]{Direction.Left, Direction.Right}},
	};

	public static readonly Dictionary<string, Direction> gdDirections = new Dictionary<string, Direction>
	{
		{"right", Direction.Right},
		{"down", Direction.Down},
		{"left", Direction.Left},
		{"up", Direction.Up},
		{"below", Direction.Below},
	};

	public static readonly Dictionary<Direction, string> gdDirectionsInverse = new Dictionary<Direction, string>
	{
		{Direction.Right, "right"},
		{Direction.Down,"down"},
		{Direction.Left, "left"},
		{Direction.Up, "up"},
		{Direction.Below, "below"},
	};
}
