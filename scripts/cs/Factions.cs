using Godot;

public enum AllianceType
{
	Enemy,
	Rival,
	Neutral,
	Ally,
	None,
}

public enum TargetAlliance
{
	None = 0,
	Ally = 1,
	NeutralAlly = 2,
	NeutralRival = 4,
	Rival = 8,
	Enemy = 16,
	Self = 32,

	NonEnemy = 3,
	NonAlly = 28,
	Any = 31,
}

public class Factions : Reference
{
	public enum names
	{
		AlaitzsGang = 0,
		NoGang = 1,
		NoGangPlayer = 2,
		NePlusUltra = 3,
		EmeraldFront = 4,
		Aureos = 5,
		Besova = 6,
		Cenobe = 7,
		Princi = 8,
		OnTheEdge = 9,
		Clockwise = 10,
	}

	public static int Count
	{
		get { return names.GetValues(typeof(names)).Length; }
	}

	public static Gang[] allFactions = new Gang[Count];

	public static void setFaction(ActorCore actor, int gangIndex)
	{
		actor.gang = allFactions[gangIndex];
		actor.InitialState.gang = allFactions[gangIndex];
	}

	public static void gdSetTarget(ActorCore actor, int gangIndex)
	{
		allFactions[gangIndex].target = actor;
	}

	public static void createFaction
	(names name, string displayName, bool isGang, names[] enemies, names[] rivals, names[] neutrals, names[] allies)
	{
		Gang gang = new Gang();
		gang.index = (int)name;
		gang.name = displayName;
		gang.isGang = isGang;
		gang.alliances = new AllianceType[Count];

		foreach(names ally in allies)
		{
			gang.alliances[(int)ally] = AllianceType.Ally;
		}
		foreach(names rival in rivals)
		{
			gang.alliances[(int)rival] = AllianceType.Rival;
		}
		foreach(names neutral in neutrals)
		{
			gang.alliances[(int)neutral] = AllianceType.Neutral;
		}
		foreach(names enemy in enemies)
		{
			gang.alliances[(int)enemy] = AllianceType.Enemy;
		}

		allFactions[(int)name] = gang;
	}

	public static AllianceType getAlliance(int gangIndex, int targetGangIndex)
	{
		return allFactions[gangIndex].alliances[targetGangIndex];
	}

	public static AllianceType getAlliance(Gang gang, Gang targetGang)
	{
		return allFactions[gang.index].alliances[targetGang.index];
	}

	public static TargetAlliance getTargetAlliance(ActorCore actor, ActorCore target)
	{
		if(actor == target)
		{
			return TargetAlliance.Self;
		}

		AllianceType personalAlliance = actor.getPersonalAlliance(target);
		switch(personalAlliance)
		{
			case AllianceType.Enemy:
				return TargetAlliance.Enemy;
			case AllianceType.Rival:
				return TargetAlliance.Rival;
			case AllianceType.Neutral:
				if(Factions.getAlliance(actor.gang, target.gang) == AllianceType.Ally)
				{
					return TargetAlliance.NeutralAlly;
				}
				else
				{
					return TargetAlliance.NeutralRival;
				}
			case AllianceType.Ally:
				return TargetAlliance.Ally;
			default:
				return TargetAlliance.None;
		}
	}

	protected Factions()
	{
		createFaction(names.AlaitzsGang, "Alaitz's Gang", true,
			new names[] { names.NePlusUltra },
			new names[] { },
			new names[] { },
			new names[] { names.AlaitzsGang, names.NoGangPlayer, names.EmeraldFront, names.Aureos });

		createFaction(names.NoGang, "", false,
			new names[] { },
			new names[] { },
			new names[] { },
			new names[] { });

		createFaction(names.NoGangPlayer, "", false,
			new names[] { names.NePlusUltra },
			new names[] { },
			new names[] { },
			new names[] { names.AlaitzsGang, names.NoGangPlayer, names.EmeraldFront, names.Aureos });

		createFaction(names.NePlusUltra, "Ne Plus Ultra", true,
			new names[] { names.AlaitzsGang, names.NoGangPlayer, names.EmeraldFront, names.Aureos },
			new names[] { },
			new names[] { },
			new names[] { names.NePlusUltra });

		createFaction(names.EmeraldFront, "Emerald Front", true,
			new names[] { names.NePlusUltra },
			new names[] { },
			new names[] { },
			new names[] { names.EmeraldFront, names.AlaitzsGang, names.NoGangPlayer, names.Aureos });

		createFaction(names.Aureos, "Aureos", true,
			new names[] { names.NePlusUltra },
			new names[] { },
			new names[] { },
			new names[] { names.AlaitzsGang, names.NoGangPlayer, names.EmeraldFront, names.Aureos });

		createFaction(names.Besova, "Besova", true,
			new names[] { },
			new names[] { },
			new names[] { },
			new names[] { names.Besova });

		createFaction(names.Cenobe, "Cenobe", true,
			new names[] { },
			new names[] { },
			new names[] { },
			new names[] { names.Cenobe });

		createFaction(names.Princi, "Princi", true,
			new names[] { },
			new names[] { },
			new names[] { },
			new names[] { names.Princi });

		createFaction(names.OnTheEdge, "On The Edge", true,
			new names[] { },
			new names[] { },
			new names[] { },
			new names[] { names.OnTheEdge });

		createFaction(names.Clockwise, "Clockwise", true,
			new names[] { names.OnTheEdge },
			new names[] { names.NoGangPlayer },
			new names[] { names.AlaitzsGang },
			new names[] { names.Clockwise });
	}
}
