using Godot;
using System;
using System.Collections.Generic;

public class Map : MapPainter
{
	public int[] mapSize = new int[4];

	public void debugActorMapMismatch(List<OriginalCore> currentActors)
	{
		foreach(Tile tile in customMap.Values)
		{
			if(tile.occupant != null)
			{
				IPlaceable occupant = (IPlaceable)tile.occupant;
				if(occupant.OriginalState.tile != tile)
				{
					tintTile(TintType.Error, tile);
					GD.Print("ERROR: tile has occupant that occupies different tile");
				}
			}
			if(tile.holdOccupant != null)
			{
				OriginalCore holdOccupant = (OriginalCore)tile.holdOccupant;
				if(holdOccupant.gdGetHoldTile() != tile)
				{
					tintTile(TintType.Error, tile);
					GD.Print("ERROR: tile has hold occupant that holds different tile");
				}
			}
		}
		for(int i = 0; i < currentActors.Count; i++)
		{
			OriginalCore actor = currentActors[i];
			if(actor.gdIsAlive() && actor.gdGetTile() != null && actor.gdGetTile().occupant != actor)
			{
				tintTile(TintType.Error, actor.gdGetTile());
				GD.Print("ERROR: actor is occupying tile that is unoccupied or occupied by another actor");
			}
		}
		for(int i = 0; i < currentActors.Count; i++)
		{
			OriginalCore actor = currentActors[i];
			if(actor.gdIsAlive() && actor.gdGetHoldTile() != null && actor.gdGetHoldTile().holdOccupant != actor)
			{
				tintTile(TintType.Error, actor.gdGetHoldTile());
				GD.Print("ERROR: actor is occupying hold tile that is not held or held by another actor");
			}
		}
	}

	public Tile gdGetTile(int x, int y)
	{
		CustomVector cell = new CustomVector(x, y);
		Tile tile;
		customMap.TryGetValue(cell, out tile);
		return tile;
	}

	public Vector2 gdSnapToMapBoundaries(Vector2 position)
	{
		position.x = Math.Min(mapSize[3], Math.Max(position.x, mapSize[1]));
		position.y = Math.Min(mapSize[2], Math.Max(position.y, mapSize[0]));
		return position;
	}

	public Tile gdGetCursorNeighbour(Tile origin, string gdDirection)
	{
		Direction direction = DirectionHelper.gdDirections[gdDirection];

		switch(direction)
		{
			case Direction.Right:
				int xEnd = Math.Max(mapBoundaries[3], origin.x);
				for(int x = origin.x + 1; x <= xEnd; x++)
				{
					CustomVector cell = new CustomVector();
					cell.x = x;
					cell.y = origin.y;
					if(customMap.ContainsKey(cell))
					{
						return customMap[cell];
					}
				}
				break;

			case Direction.Down:
				int yEnd = Math.Max(mapBoundaries[2], origin.y);
				for(int y = origin.y + 1; y <= yEnd; y++)
				{
					CustomVector cell = new CustomVector();
					cell.x = origin.x;
					cell.y = y;
					if(customMap.ContainsKey(cell))
					{
						return customMap[cell];
					}
				}
				break;

			case Direction.Left:
				int xStart = Math.Min(mapBoundaries[1], origin.x);
				for(int x = origin.x - 1; x >= xStart; x--)
				{
					CustomVector cell = new CustomVector();
					cell.x = x;
					cell.y = origin.y;
					if(customMap.ContainsKey(cell))
					{
						return customMap[cell];
					}
				}
				break;

			case Direction.Up:
				int yStart = Math.Min(mapBoundaries[0], origin.y);
				for(int y = origin.y - 1; y >= yStart; y--)
				{
					CustomVector cell = new CustomVector();
					cell.x = origin.x;
					cell.y = y;
					if(customMap.ContainsKey(cell))
					{
						return customMap[cell];
					}
				}
				break;
		}

		return null;
	}

	public List<Tile> getUnoccupiedTilesInRange()
	{
		List<Tile> unoccupiedTilesInRange = new List<Tile>();
		foreach(Tile tile in customMap.Values)
		{
			if(tile.actorDistance != -1 && tile.occupant == null)
			{
				unoccupiedTilesInRange.Add(tile);
			}
		}
		return unoccupiedTilesInRange;
	}

	public List<ActorCore> getClosestActors(int n, Tile origin, bool shouldOnlyGetPlayer = false)
	{
		List<ActorCore> actorsInRange = new List<ActorCore>();
		List<Tile> current = new List<Tile>();
		List<Tile> next = new List<Tile>();
		List<Tile> closed = new List<Tile>();

		current.Add(origin);

		while(actorsInRange.Count < n && current.Count > 0)
		{
			foreach(Tile tile in current)
			{
				if(actorsInRange.Count < n && !closed.Contains(tile))
				{
					if(tile.occupant is ActorCore)
					{
						ActorCore actor = (ActorCore)tile.occupant;
						if(!shouldOnlyGetPlayer || actor.isPlayer)
						{
							actorsInRange.Add(actor);
						}
					}
					if(tile.holdOccupant != null && (!shouldOnlyGetPlayer || tile.holdOccupant.isPlayer))
					{
						actorsInRange.Add(tile.holdOccupant);
					}
				}
				foreach(Tile neighbour in tile.neighbours.Values)
				{
					if(neighbour != null && !closed.Contains(neighbour) && !next.Contains(neighbour))
					{
						next.Add(neighbour);
					}
				}
				closed.Add(tile);
			}
			current = next;
			next = new List<Tile>();
		}

		return actorsInRange;
	}

	public List<Tile> getTilesInSlash(Tile origin, Tile neighbour)
	{
		List<Tile> tilesInSlash = new List<Tile>();

		Direction direction = origin.getNeighbourDirection(neighbour);
		CustomVector[] slashCells = getSlashCells(origin, direction);
		foreach(CustomVector cell in slashCells)
		{
			if(customMap.ContainsKey(cell) && customMap[cell] != origin)
			{
				tilesInSlash.Add(customMap[cell]);
			}
		}

		return tilesInSlash;
	}

	public List<ActorCore> getActorsInSlash(Tile origin, Tile neighbour)
	{
		List<ActorCore> actorsInSlash = new List<ActorCore>();

		Direction direction = origin.getNeighbourDirection(neighbour);
		CustomVector[] slashCells = getSlashCells(origin, direction);
		foreach(CustomVector cell in slashCells)
		{
			if(customMap.ContainsKey(cell) && customMap[cell].occupant is ActorCore && customMap[cell] != origin && Tile.isWithinReach(customMap[cell], origin))
			{
				actorsInSlash.Add((ActorCore)customMap[cell].occupant);
			}
		}

		return actorsInSlash;
	}

	public List<ActorCore> getAdjacentRangedSkipActors(Tile origin, int range)
	{
		List<ActorCore> adjacentRangedSkipActors = new List<ActorCore>();
		for(int x = origin.x - range; x <= origin.x + range; x++)
		{
			for(int y = origin.y - range; y <= origin.y + range; y++)
			{
				if(isRangedSkipTargetable(origin, x, y, range))
				{
					CustomVector cell = new CustomVector();
					cell.x = x;
					cell.y = y;
					if(customMap.ContainsKey(cell))
					{
						Tile tile = customMap[cell];
						if(tile.occupant is ActorCore)
						{
							adjacentRangedSkipActors.Add((ActorCore)tile.occupant);
						}
					}
				}
			}
		}
		return adjacentRangedSkipActors;
	}

	public List<Tile> getGripTilesInRange()
	{
		List<Tile> gripTilesInRange = new List<Tile>();
		foreach(Tile tile in customMap.Values)
		{
			if(tile.graspable)
			{
				bool isInRange = false;
				foreach(Tile neighbour in tile.neighbours.Values)
				{
					if(neighbour != null && neighbour.actorDistance != -1)
					{
						isInRange = true;
					}
				}
				if(isInRange)
				{
					gripTilesInRange.Add(tile);
				}
			}
		}
		return gripTilesInRange;
	}

	public List<Tile> getPathBetweenPoints(Tile origin, Tile goal, ActorCore actor)
	{
		List<Tile> path = new List<Tile>();
		wipeTiles(WipeTilesMode.Pathfinding);

		List<Tile> open = new List<Tile>(customMap.Count);
		List<Tile> closed = new List<Tile>();
		origin.gCost = 0;
		open.Add(origin);

		while(open.Count > 0)
		{
			Tile tile = open[0];
			open.RemoveAt(0);
			foreach(Tile neighbour in tile.neighbours.Values)
			{
				if(neighbour != null)
				{
					if(neighbour == goal && tile.isTilePassable(actor))
					{
						while(tile != origin)
						{
							path.Add(tile);
							tile = (Tile)tile.parent;
						};
						if(tile != origin)
						{
							path.Add(origin);
							path.Reverse();
						}
						return path;
					}
					else if(neighbour.isTilePassable(actor))
					{
						int newGCost = tile.gCost + neighbour.movementCost;
						if(!closed.Contains(neighbour) || neighbour.gCost > newGCost)
						{
							neighbour.parent = tile;
							neighbour.gCost = newGCost;
							neighbour.hCost = Tile.getManhattanDistance(neighbour, goal);
							if(open.Contains(neighbour))
							{
								open.Sort();
							}
							else
							{
								open.Add(neighbour);
							}
						}
					}
				}
			}
			closed.Add(tile);
		}
		return path;
	}

	public static List<ActionOutcome> getPushPath(ActorCore actor, IPlaceable target, Direction pushDirection, bool isVisual = false, int tilesToMove = -1)
	{
		Tile origin;

		//TODO: Make separate methods that determine origin to converge in a single one
		if(isVisual)
		{
			origin = target.gdGetTileOrHoldTile();
		}
		else
		{
			origin = target.getTileOrHoldTile();
		}

		ActionOutcome pushData = new ActionOutcome();
		pushData.target = target;
		pushData.path = new List<Tile>();

		pushData.path.Add(origin);

		List<ActionOutcome> pushDataList = new List<ActionOutcome>();
		pushDataList.Add(pushData);

		if(pushDirection == Direction.Below)
		{
			pushData.path.Add(null);
		}
		else
		{
			int pushEnergy;
			if(tilesToMove == -1)
			{
				pushEnergy = actor.getPushStrength(target.steadiness);
			}
			else
			{
				pushEnergy = tilesToMove;
			}

			int pushedTilesTotal = 0;
			int targetEnergy = target.energy;
			int currentLevel = target.tile.heightLevel;

			while(pushEnergy > 0 && pushData.path[pushData.path.Count - 1] != null)
			{
				Tile nextTile = null;
				bool shouldFlyOverTile = false;

				if(target.grasping && targetEnergy > 0)
				{
					targetEnergy -= 5;
					pushData.energyChange = targetEnergy - target.energy;
				}

				if(!target.grasping || targetEnergy <= 0)
				{
					nextTile = pushData.path[pushData.path.Count - 1].neighbours[pushDirection];
					if(nextTile != null)
					{
						if(nextTile.passable && nextTile.heightLevel <= currentLevel)
						{
							shouldFlyOverTile = nextTile.heightLevel != currentLevel && (nextTile.neighbours[pushDirection] == null || nextTile.neighbours[pushDirection].heightLevel < currentLevel);

							if(nextTile.occupant != null && !shouldFlyOverTile)
							{
								pushData.heightLevelChange = target.tile.heightLevel - pushData.path[pushData.path.Count - 1].heightLevel;

								target = nextTile.occupant;
								pushEnergy = actor.getPushStrength(target.steadiness) - pushedTilesTotal;
								pushData = new ActionOutcome();
								pushData.target = target;
								pushData.path = new List<Tile>();
								pushDataList.Add(pushData);
								pushData.path.Add(nextTile);
							}
							else
							{
								pushData.path.Add(nextTile);
								pushedTilesTotal++;
							}
						}
						else
						{
							pushEnergy = 0;
						}
					}
					else
					{
						Tile prevTile = pushData.path[pushData.path.Count - 1];
						if(prevTile.holdable && prevTile.holdOccupant == null)
						{
							pushData.holdTile = prevTile;
						}
						pushData.path.Add(null);
					}
				}

				if(nextTile == null || !nextTile.slippery || shouldFlyOverTile)
				{
					pushEnergy--;
				}

				if(pushEnergy <= 0 && pushData.path.Count > 1 && pushData.path[pushData.path.Count - 1] != null && currentLevel > pushData.path[pushData.path.Count - 1].heightLevel)
				{
					pushEnergy += currentLevel - pushData.path[pushData.path.Count - 1].heightLevel;
					currentLevel--;
				}
			}
			if(pushData.path.Count > 0 && pushData.path[pushData.path.Count - 1] != null)
			{
				pushData.heightLevelChange = pushData.target.tile.heightLevel - pushData.path[pushData.path.Count - 1].heightLevel;
			}
		}
		return pushDataList;
	}

	public static Godot.Collections.Array gdGetPushPath(ActorCore actor, Tile tile)
	{
		Direction pushDirection = actor.getPushDirection(tile);
		IPlaceable target = tile.occupant;
		if(pushDirection == Direction.Below)
		{
			target = tile.holdOccupant;
		}
		List<ActionOutcome> pushOutcome = getPushPath(actor, target, pushDirection, true);
		if(actor.pushSkill != null)
		{
			pushOutcome = actor.pushSkill.calculateModifierOutcome(pushOutcome, tile, actor);
		}
		return ActionOutcome.getGdData(pushOutcome);
	}

	public void calculateInfluenceRange(Tile origin, ActorCore actor)
	{
		origin.score += 6;
		List<Tile> current = new List<Tile>();
		List<Tile> old = new List<Tile>();
		current.Add(origin);
		do
		{
			Tile tile = current[current.Count - 1];
			current.RemoveAt(current.Count - 1);
			old.Add(tile);
			if(tile.score > -1)
			{
				foreach(Tile neighbour in tile.neighbours.Values)
				{
					if(neighbour != null && neighbour.isTilePassable(actor) && neighbour.score == -1)
					{
						current.Add(neighbour);
						old.Add(neighbour);
						neighbour.score = tile.score - 1;
					}
				}
			}
		}
		while(current.Count > 0);

		foreach(Tile tile in old)
		{
			tile.cumulativeScore += tile.score;
			tile.score = -1;
		}
	}

	public bool areNeighbours(Tile tileA, Tile tileB)
	{
		if(tileA.x == tileB.x)
		{
			if(tileA.y + 1 == tileB.y || tileA.y - 1 == tileB.y)
			{
				return true;
			}
		}
		else if(tileA.y == tileB.y)
		{
			if(tileA.x + 1 == tileB.x || tileA.x - 1 == tileB.x)
			{
				return true;
			}
		}
		return false;
	}

	public void initializePlaceablePosition(Node2D placeableBase)
	{
		Vector2 position = WorldToMap((Vector2)placeableBase.Get("position"));
		IPlaceable placeableCore = (IPlaceable)placeableBase.Get("Core");
		CustomVector customPosition = new CustomVector((int)position.x, (int)position.y);
		placeableCore.tile = customMap[customPosition];
		placeableCore.tile.occupant = placeableCore;
		placeableCore.InitialState.tile = customMap[customPosition];
		placeableBase.Call("initializePosition");
	}

	public Map() { }

	public override void _Ready()
	{
		base._Ready();

		Godot.Collections.Array gdCellArray = GetUsedCells();
		List<CustomVector> cellArray = new List<CustomVector>(gdCellArray.Count);
		foreach(Vector2 cell in gdCellArray)
		{
			CustomVector vector = new CustomVector();
			vector.x = (int)cell.x;
			vector.y = (int)cell.y;
			cellArray.Add(vector);
		}

		customMap = new Dictionary<CustomVector, Tile>(cellArray.Count);

		YSort ySort = (YSort)GetNode("YSort");
		YSort grid = new YSort();
		grid.Name = "Grid";
		PackedScene gridScene = (PackedScene)ResourceLoader.Load("res://scenes/GridSprite.tscn");

		int newTileIndex = TileSet.GetTilesIds().Count;
		foreach(CustomVector cell in cellArray)
		{
			Vector2 vector = new Vector2(cell.x, cell.y);
			int tileIndex = GetCellv(vector);
			newTileIndex = newTileIndex + 1;
			CustomTileSet customTileSet = (CustomTileSet)TileSet;
			TileTypes tileType = customTileSet.tileTypes[tileIndex];

			StreamTexture texture = (StreamTexture)TileSet.TileGetTexture(tileIndex);
			Tile tile = new Tile(this, vector, tileIndex, newTileIndex, tileType, (StreamTexture)texture);
			customMap.Add(cell, tile);

			if(Cursor != null && tile.passable)
			{
				Sprite gridSprite = (Sprite)gridScene.Instance();
				gridSprite.Offset = tile.gdOffset;
				gridSprite.Position = new Vector2(tile.position.x, tile.position.y);
				gridSprite.ZIndex = (int)tile.position.y - 16;

				grid.AddChild(gridSprite);
				grid.MoveChild(gridSprite, 0);
			}
		}

		if(Cursor != null)
		{
			ySort.AddChild(grid);
			Cursor.Call("addCursorSprite");
		}

		foreach(CustomVector cell in cellArray)
		{
			Tile tile = customMap[cell];
			CustomVector rightCell = new CustomVector(cell.x + 1, cell.y);
			CustomVector downCell = new CustomVector(cell.x, cell.y + 1);
			CustomVector leftCell = new CustomVector(cell.x - 1, cell.y);
			CustomVector upCell = new CustomVector(cell.x, cell.y - 1);
			if(customMap.ContainsKey(rightCell))
			{
				tile.neighbours[Direction.Right] = customMap[rightCell];
			}
			else if(tile.passable)
			{
				tile.borderDistance[Direction.Right] = 0;
			}
			if(customMap.ContainsKey(downCell))
			{
				tile.neighbours[Direction.Down] = customMap[downCell];
			}
			else if(tile.passable)
			{
				tile.borderDistance[Direction.Down] = 0;
			}
			if(customMap.ContainsKey(leftCell))
			{
				tile.neighbours[Direction.Left] = customMap[leftCell];
			}
			else if(tile.passable)
			{
				tile.borderDistance[Direction.Left] = 0;
			}
			if(customMap.ContainsKey(upCell))
			{
				tile.neighbours[Direction.Up] = customMap[upCell];
			}
			else if(tile.passable)
			{
				tile.borderDistance[Direction.Up] = 0;
			}
		}

		foreach(CustomVector cell in cellArray)
		{
			Tile tile = customMap[cell];
			foreach(KeyValuePair<Direction, int> distance in tile.borderDistance)
			{
				if(distance.Value == 0)
				{
					Direction opposite = DirectionHelper.oppositeDirection[distance.Key];
					Tile neighbour = customMap[cell].neighbours[opposite];
					int borderDistance = 1;
					while(neighbour != null && neighbour.passable == true)
					{
						neighbour.borderDistance[distance.Key] = borderDistance;
						neighbour = (Tile)neighbour.neighbours[opposite];
						borderDistance++;
					}
				}
			}
		}

		foreach(CustomVector cell in cellArray)
		{
			Tile tile = customMap[cell];
			if(!tile.passable)
			{
				foreach(KeyValuePair<Direction, Tile> entry in tile.neighbours)
				{
					Tile neighbour = entry.Value;
					if(neighbour != null)
					{
						neighbour.borderDistance[entry.Key] = Tile.MAX_DISTANCE;
						Direction opposite = DirectionHelper.oppositeDirection[entry.Key];
						while(neighbour != null && neighbour.passable == true)
						{
							neighbour.borderDistance[opposite] = Tile.MAX_DISTANCE;
							neighbour = neighbour.neighbours[entry.Key];
						}
					}
				}
			}
		}

		foreach(CustomVector cell in cellArray)
		{
			Tile tile = customMap[cell];
			foreach(int distance in tile.borderDistance.Values)
			{
				if(distance < tile.minBorderDistance)
				{
					tile.minBorderDistance = distance;
				}
			}
		}

		Godot.Collections.Array gdAllPlaceables = GetTree().GetNodesInGroup("placeables");

		foreach(Node2D placeableBase in gdAllPlaceables)
		{
			bool isReinforcement = (bool)placeableBase.Get("isReinforcement");
			if(!isReinforcement)
			{
				initializePlaceablePosition(placeableBase);
			}
		}

		int topCoordinate = Tile.MAX_DISTANCE;
		int leftCoordinate = Tile.MAX_DISTANCE;
		int bottomCoordinate = -1;
		int rightCoordinate = -1;

		int topPosition = Tile.MAX_DISTANCE;
		int leftPosition = Tile.MAX_DISTANCE;
		int bottomPosition = -Tile.MAX_DISTANCE;
		int rightPosition = -Tile.MAX_DISTANCE;

		foreach(Tile tile in customMap.Values)
		{
			int x = (int)MapToWorld(tile.cell).x;
			int y = (int)MapToWorld(tile.cell).y - tile.heightLevel * 32;
			if(x < leftCoordinate)
			{
				leftCoordinate = x;
			}
			else if(x > rightCoordinate)
			{
				rightCoordinate = x;
			}
			if(y < topCoordinate)
			{
				topCoordinate = y;
			}
			else if(y > bottomCoordinate)
			{
				bottomCoordinate = y;
			}

			if(tile.x < leftPosition)
			{
				leftPosition = tile.x;
			}
			else if(tile.x > rightPosition)
			{
				rightPosition = tile.x;
			}
			if(tile.y < topPosition)
			{
				topPosition = tile.y;
			}
			else if(tile.y > bottomPosition)
			{
				bottomPosition = tile.y;
			}
		}

		mapSize[0] = topCoordinate;
		mapSize[1] = leftCoordinate;
		mapSize[2] = bottomCoordinate;
		mapSize[3] = rightCoordinate;

		mapBoundaries[0] = topPosition;
		mapBoundaries[1] = leftPosition;
		mapBoundaries[2] = bottomPosition;
		mapBoundaries[3] = rightPosition;

		Camera2D SettingCamera = (Camera2D)GetParent().GetNode("SettingCamera");
		ReferenceRect CameraReference = (ReferenceRect)GetNode("CameraReference");
		Vector2 cameraPosition = CameraReference.RectPosition;
		Vector2 cameraZoom = CameraReference.RectScale;
		SettingCamera.Position = cameraPosition;
		SettingCamera.Zoom = cameraZoom;
	}
}
