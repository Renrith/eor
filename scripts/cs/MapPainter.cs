using Godot;
using System.Collections.Generic;

public enum PaintType
{
	MovementRange,
	MovementPath,
	PushPath,
	ActionTile,
	Enemy,
	Ally,
	Clear,
}

public enum TintType
{
	EnemyRange,
	Error,
	Clear,
}

public enum WipeTilesMode
{
	Pathfinding,
	Paint,
	PaintEnemyRange,
	Simulation,
	Restart,
}

public class MapPainter : MapRange
{
	private Battle Battle;
	private YSort YSort;
	protected Node2D Cursor;

	protected List<Tile> enemyRangeTiles = new List<Tile>();

	private Tile cursorTile { get { return (Tile)Cursor.Get("tile"); } }
	private Tile previousCursorTile { get { return (Tile)Cursor.Get("previousTile"); } }

	private ShaderMaterial rangeShader;
	private ShaderMaterial actionShader;
	private ShaderMaterial enemyShader;
	private ShaderMaterial allyShader;
	private ShaderMaterial pathShader;
	private ShaderMaterial pushPathShader;

	private Color clearTint = new Color(1F, 1F, 1F);
	private Color enemyRangeTint = new Color(1F, 0.75F, 0.75F, 1F);
	private Color errorTint = new Color(1F, 0.2F, 0.2F);

	public void paintTile(PaintType paintType, Tile tile)
	{
		switch(paintType)
		{
			case PaintType.MovementRange:
				TileSet.TileSetMaterial(tile.index, rangeShader);
				break;
			case PaintType.MovementPath:
				pathShader.SetShaderParam("outlineHeight", tile.offset.y);
				TileSet.TileSetMaterial(tile.index, pathShader);
				break;
			case PaintType.PushPath:
				TileSet.TileSetMaterial(tile.index, pushPathShader);
				break;
			case PaintType.ActionTile:
				TileSet.TileSetMaterial(tile.index, actionShader);
				break;
			case PaintType.Enemy:
				enemyShader.SetShaderParam("outlineHeight", tile.offset.y);
				TileSet.TileSetMaterial(tile.index, enemyShader);
				break;
			case PaintType.Ally:
				allyShader.SetShaderParam("outlineHeight", tile.offset.y);
				TileSet.TileSetMaterial(tile.index, allyShader);
				break;
			case PaintType.Clear:
				TileSet.TileSetMaterial(tile.index, null);
				break;
			default:
				TileSet.TileSetMaterial(tile.index, null);
				break;
		}
		tile.paintType = paintType;
	}

	protected void tintTile(TintType tintType, Tile tile)
	{
		switch(tintType)
		{
			case TintType.Clear:
				TileSet.Set(tile.index + "/modulate", clearTint);
				tile.isPaintingEnemyRange = false;
				break;
			case TintType.EnemyRange:
				if(!tile.isPaintingEnemyRange)
				{
					TileSet.Set(tile.index + "/modulate", enemyRangeTint);
					tile.isPaintingEnemyRange = true;
				}
				break;
			case TintType.Error:
				TileSet.Set(tile.index + "/modulate", errorTint);
				break;
		}
	}

	protected void paintMovementRange()
	{
		foreach(Tile tile in allyTilesInRange)
		{
			if(tile.paintType == PaintType.Clear || tile.paintType == PaintType.MovementPath)
			{
				paintTile(PaintType.Ally, tile);
			}
		}
		foreach(Tile tile in enemyTilesInRange)
		{
			if(tile.paintType == PaintType.Clear || tile.paintType == PaintType.MovementPath)
			{
				paintTile(PaintType.Enemy, tile);
			}
		}
		foreach(Tile tile in tilesInRange)
		{
			if(tile.paintType == PaintType.Clear || tile.paintType == PaintType.MovementPath)
			{
				paintTile(PaintType.MovementRange, tile);
			}
		}
		foreach(Tile tile in cursorPathTiles)
		{
			if(tile.paintType != PaintType.MovementPath)
			{
				paintTile(PaintType.MovementPath, tile);
			}
		}
	}

	protected void paintTargetRange()
	{
		foreach(Tile tile in enemyTilesInRange)
		{
			if(tile.paintType == PaintType.Clear)
			{
				paintTile(PaintType.Enemy, tile);
			}
		}
		foreach(Tile tile in allyTilesInRange)
		{
			if(tile.paintType == PaintType.Clear)
			{
				paintTile(PaintType.Ally, tile);
			}
		}
		foreach(Tile tile in tilesInRange)
		{
			if(tile.paintType == PaintType.Clear)
			{
				paintTile(PaintType.MovementRange, tile);
			}
		}
		foreach(Tile tile in actionTilesInRange)
		{
			if(tile.paintType == PaintType.Clear)
			{
				paintTile(PaintType.ActionTile, tile);
			}
		}
	}

	protected void paintEnemyRange(bool shouldPaint)
	{
		if(shouldPaint)
		{
			wipeTiles(WipeTilesMode.PaintEnemyRange);
			clearTintedTiles();
			foreach(OriginalCore actor in Battle.currentActors)
			{
				AllianceType actorAlliance = actor.getAlliance(Factions.allFactions[(int)Factions.names.AlaitzsGang]);
				if((actorAlliance == AllianceType.Enemy || actorAlliance == AllianceType.Rival) && actor.gdGetTile() != null)
				{
					base.calculateMovementRange(actor, true, true);
					calculateTargetableTiles(actor);
					tilesInRange.Add(actor.gdGetTile());

					foreach(Tile tile in tilesInRange)
					{
						tintTile(TintType.EnemyRange, tile);
					}
					foreach(Tile tile in enemyTilesInRange)
					{
						tintTile(TintType.EnemyRange, tile);
					}
					foreach(Tile tile in actionTilesInRange)
					{
						tintTile(TintType.EnemyRange, tile);
					}

					wipeTiles(WipeTilesMode.PaintEnemyRange);
				}
			}
		}
		else
		{
			clearTintedTiles();
		}
	}

	protected void paintCursorPath()
	{
		cursorPathTiles = new List<Tile>();
		Tile pathTile = cursorTile;
		if(pathTile.parent != null && pathTile.occupant == null && pathTile.passable)
		{
			while(pathTile != null)
			{
				cursorPathTiles.Add(pathTile);
				pathTile = (Tile)pathTile.parent;
			}
		}

		paintMovementRange();
	}

	public override void calculateMovementRange(ActorCore actor, bool shouldPaint = true, bool isCheckingRange = false)
	{
		base.calculateMovementRange(actor, shouldPaint, isCheckingRange);

		if(shouldPaint && (actor.isPlayer || isCheckingRange))
		{
			OriginalCore originalActor = (OriginalCore)actor;
			tilesInRange.Add(originalActor.gdGetTile());
			calculateTargetableTiles(actor);
			if(isCheckingRange)
			{
				paintTargetRange();
			}
			else
			{
				paintMovementRange();
			}
		}
	}

	public void clearPaintedTiles()
	{
		foreach(Tile tile in customMap.Values)
		{
			if(tile.paintType != PaintType.Clear)
			{
				paintTile(PaintType.Clear, tile);
			}
		}
		cursorPathTiles = new List<Tile>();
	}

	public void clearTintedTiles()
	{
		foreach(Tile tile in customMap.Values)
		{
			if(tile.isPaintingEnemyRange)
			{
				tintTile(TintType.Clear, tile);
				tile.isPaintingEnemyRange = false;
			}
		}
	}

	public void gdWipeTiles()
	{
		wipeTiles(WipeTilesMode.Paint);
	}

	public void wipeTiles(WipeTilesMode mode)
	{
		foreach(Tile tile in customMap.Values)
		{
			tile.actorDistance = -1;
			tile.parent = null;
			if(mode == WipeTilesMode.Simulation || mode == WipeTilesMode.Restart)
			{
				tile.occupant = null;
				tile.holdOccupant = null;
				tile.score = -1;
				tile.cumulativeScore = -1;
			}
		}
		switch(mode)
		{
			case WipeTilesMode.Paint:
				clearPaintedTiles();
				tilesInRange = new List<Tile>();
				actionTilesInRange = new List<Tile>();
				enemyTilesInRange = new List<Tile>();
				allyTilesInRange = new List<Tile>();
				cursorPathTiles = new List<Tile>();
				break;
			case WipeTilesMode.PaintEnemyRange:
				tilesInRange = new List<Tile>();
				actionTilesInRange = new List<Tile>();
				enemyTilesInRange = new List<Tile>();
				allyTilesInRange = new List<Tile>();
				break;
		}
	}

	protected void calculateTargetableTiles(ActorCore actor)
	{
		List<Tile> newActionTiles = getActionTilesInRange(actor.tile);
		foreach(Tile tile in newActionTiles)
		{
			actionTilesInRange.Add(tile);
		}

		List<ActorCore> targets = getActorsInRange(actor.tile);
		foreach(ActorCore target in targets)
		{
			AllActions actions = actor.getActions(target);
			List<Tile> tiles = getAdjacentTilesInRange(target.tile, actor);
			if(tiles.Count > 0)
			{
				if((actions & AllActions.attack) != 0 || (actions & AllActions.push) != 0)
				{
					if(actor.getAlliance(target) == AllianceType.Ally)
					{
						allyTilesInRange.Add(target.tile);
					}
					else
					{
						enemyTilesInRange.Add(target.tile);
					}
				}
			}
		}

		List<ActorCore> holdTargets = getHoldActorsInRange(actor);
		foreach(ActorCore holdTarget in holdTargets)
		{
			if(actor.getAlliance(holdTarget) != AllianceType.Ally)
			{
				enemyTilesInRange.Add(holdTarget.holdTile);
				break;
			}
		}

		switch(actor.attackType)
		{
			case TargetingType.RangedSkip:
				List<Tile> rangedSkipTiles = getRangedSkipTilesInRange(actor.tile, actor.movementRange, actor.attackRange);
				foreach(Tile tile in rangedSkipTiles)
				{
					if(!actionTilesInRange.Contains(tile))
					{
						actionTilesInRange.Add(tile);
						if(tile.occupant is ActorCore && ((ActorCore)tile.occupant).isAlive() && actor.getAlliance((ActorCore)tile.occupant) != AllianceType.Ally)
						{
							enemyTilesInRange.Add(tile);
						}
					}
				}
				break;
			case TargetingType.RangedLine:
				List<Tile> rangedLineTiles = getRangedLineTilesInRange(actor.tile, actor.attackRange);
				foreach(Tile tile in rangedLineTiles)
				{
					if(!actionTilesInRange.Contains(tile))
					{
						actionTilesInRange.Add(tile);
						if(tile.occupant != null && ((ActorCore)tile.occupant).isAlive() && actor.getAlliance((ActorCore)tile.occupant) != AllianceType.Ally)
						{
							enemyTilesInRange.Add(tile);
						}
					}
				}
				break;
			case TargetingType.Slash:
				List<Tile> attackTiles = new List<Tile>();
				foreach(Tile tile in actionTilesInRange)
				{
					foreach(KeyValuePair<Direction, Tile> tilePair in tile.neighbours)
					{
						Tile origin = tilePair.Value;
						if(origin != null && origin.actorDistance != -1)
						{
							Direction direction = origin.getNeighbourDirection(tile);
							CustomVector[] slashCells = getSlashCells(origin, direction);
							foreach(CustomVector cell in slashCells)
							{
								if(customMap.ContainsKey(cell) && customMap[cell] != origin && Tile.isWithinReach(customMap[cell], origin))
								{
									attackTiles.Add(customMap[cell]);
								}
							}
						}
					}
				}
				foreach(Tile tile in attackTiles)
				{
					actionTilesInRange.Add(tile);
				}

				List<KeyValuePair<Tile, Tile>> slashTargetTiles = getSlashTilePairsInRange(actor);
				foreach(KeyValuePair<Tile, Tile> tilePair in slashTargetTiles)
				{
					Tile neighbour = tilePair.Key;
					Tile origin = tilePair.Value;
					Direction direction = origin.getNeighbourDirection(neighbour);
					CustomVector[] slashCells = getSlashCells(origin, direction);
					foreach(CustomVector cell in slashCells)
					{
						if(customMap.ContainsKey(cell) && customMap[cell] != actor.tile && customMap[cell].occupant != null && Tile.isWithinReach(customMap[cell], origin))
						{
							enemyTilesInRange.Add(customMap[cell]);
						}
					}
				}
				break;
		}
	}

	public override void _Ready()
	{
		YSort = (YSort)GetNode("YSort");
		Cursor = (Node2D)YSort.GetNode("Cursor");

		rangeShader = (ShaderMaterial)ResourceLoader.Load("res://shaders/tiles/range.tres");
		actionShader = (ShaderMaterial)ResourceLoader.Load("res://shaders/tiles/action.tres");
		enemyShader = (ShaderMaterial)ResourceLoader.Load("res://shaders/tiles/enemy.tres");
		allyShader = (ShaderMaterial)ResourceLoader.Load("res://shaders/tiles/ally.tres");
		pathShader = (ShaderMaterial)ResourceLoader.Load("res://shaders/tiles/path.tres");
		pushPathShader = (ShaderMaterial)ResourceLoader.Load("res://shaders/tiles/pushPath.tres");
	}
}
