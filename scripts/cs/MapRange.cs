using Godot;
using System;
using System.Collections.Generic;

public class MapRange : TileMap
{
	public Dictionary<CustomVector, Tile> customMap;

	public int[] mapBoundaries = new int[4];

	protected List<Tile> tilesInRange = new List<Tile>();
	protected List<Tile> actionTilesInRange = new List<Tile>();
	protected List<Tile> enemyTilesInRange = new List<Tile>();
	protected List<Tile> allyTilesInRange = new List<Tile>();
	protected List<Tile> cursorPathTiles = new List<Tile>();

	protected static bool isRangedSkipTargetable(Tile origin, int x, int y, int range)
	{
		if(Math.Abs(x - origin.x) + Math.Abs(y - origin.y) <= range)
		{
			return true;
		}
		return false;
	}

	public List<Tile> getRangedSkipTilesInRange(Tile origin, int movementRange, int targetingRange)
	{
		int range = movementRange + targetingRange;
		List<Tile> rangedSkipTiles = new List<Tile>();
		for(int x = origin.x - range; x <= origin.x + range; x++)
		{
			for(int y = origin.y - range; y <= origin.y + range; y++)
			{
				if(isRangedSkipTargetable(origin, x, y, range))
				{
					CustomVector cell = new CustomVector();
					cell.x = x;
					cell.y = y;
					if(customMap.ContainsKey(cell) && Tile.isWithinReach(origin, customMap[cell]))
					{
						rangedSkipTiles.Add(customMap[cell]);
					}
				}
			}
		}
		return rangedSkipTiles;
	}

	public List<ActorCore> getRangedSkipActorsInRange(Tile origin, int movementRange, int targetingRange)
	{
		int range = movementRange + targetingRange;
		List<ActorCore> rangedSkipActors = new List<ActorCore>();
		for(int x = origin.x - range; x <= origin.x + range; x++)
		{
			for(int y = origin.y - range; y <= origin.y + range; y++)
			{
				if(isRangedSkipTargetable(origin, x, y, range))
				{
					CustomVector cell = new CustomVector();
					cell.x = x;
					cell.y = y;
					if(customMap.ContainsKey(cell))
					{
						Tile tile = customMap[cell];
						if(tile.occupant is ActorCore && tile != origin && Tile.isWithinReach(origin, tile))
						{
							rangedSkipActors.Add((ActorCore)tile.occupant);
						}
					}
				}
			}
		}
		return rangedSkipActors;
	}

	public List<Tile> getActionTilesInRange(Tile origin)
	{
		List<Tile> actionTiles = new List<Tile>();
		foreach(Tile tile in customMap.Values)
		{
			if(tile.actorDistance != -1)
			{
				foreach(Tile neighbour in tile.neighbours.Values)
				{
					if(neighbour != null && neighbour != origin)
					{
						if(!actionTiles.Contains(neighbour))
						{
							actionTiles.Add(neighbour);
						}
					}
				}
			}
		}
		return actionTiles;
	}

	public List<ActorCore> getActorsInRange(Tile origin)
	{
		List<ActorCore> actorsInRange = new List<ActorCore>();
		foreach(Tile tile in customMap.Values)
		{
			if(tile.actorDistance != -1)
			{
				foreach(Tile neighbour in tile.neighbours.Values)
				{
					if(neighbour != null && neighbour != origin)
					{
						if(neighbour.occupant is ActorCore && !actorsInRange.Contains((ActorCore)neighbour.occupant))
						{
							actorsInRange.Add((ActorCore)neighbour.occupant);
						}
					}
				}
			}
		}
		return actorsInRange;
	}

	public List<ActorCore> getHoldActorsInRange(ActorCore actor)
	{
		List<ActorCore> holdActorsInRange = new List<ActorCore>();
		foreach(Tile tile in customMap.Values)
		{
			if(tile.actorDistance != -1)
			{
				foreach(Tile neighbour in tile.neighbours.Values)
				{
					if(neighbour != null)
					{
						ActorCore holdOccupant = neighbour.holdOccupant;
						if(holdOccupant != null && !holdActorsInRange.Contains(holdOccupant))
						{
							holdActorsInRange.Add(holdOccupant);
							if(!holdOccupant.isAlive())
							{
								throw new Exception("Actor is not alive, but is occupying hold tile");
							}
						}
					}
				}
			}
		}
		return holdActorsInRange;
	}

	public static List<Tile> getAdjacentTilesInRange(Tile origin, ActorCore actor)
	{
		List<Tile> adjacentTiles = new List<Tile>();
		foreach(Tile neighbour in origin.neighbours.Values)
		{
			if(neighbour != null && neighbour.actorDistance != -1 && (neighbour.occupant == null || neighbour.occupant == actor) && Tile.isWithinReach(neighbour, origin))
			{
				adjacentTiles.Add(neighbour);
			}
		}
		return adjacentTiles;
	}

	public List<Tile> getRangedSkipTiles(Tile origin, int range)
	{
		List<Tile> tilesWithinDistance = new List<Tile>();
		for(int x = origin.x - range; x <= origin.x + range; x++)
		{
			for(int y = origin.y - range; y <= origin.y + range; y++)
			{
				if(isRangedSkipTargetable(origin, x, y, range))
				{
					CustomVector cell = new CustomVector();
					cell.x = x;
					cell.y = y;
					if(customMap.ContainsKey(cell))
					{
						Tile tile = customMap[cell];
						if(tile.occupant == null && tile.actorDistance != -1 && Tile.isWithinReach(origin, tile))
						{
							tilesWithinDistance.Add(tile);
						}
					}
				}
			}
		}
		return tilesWithinDistance;
	}

	public List<Tile> getRangedLineTilesInRange(Tile origin, int range)
	{
		List<Tile> tilesInLine = new List<Tile>();

		int xEnd = Math.Min(mapBoundaries[3], origin.x + range);
		int yEnd = Math.Min(mapBoundaries[2], origin.y + range);
		int xStart = Math.Max(mapBoundaries[1], origin.x - range);
		int yStart = Math.Max(mapBoundaries[0], origin.y - range);

		int xEndMaxHeight = 0;
		int yEndMaxHeight = 0;
		int xStartMaxHeight = 0;
		int yStartMaxHeight = 0;

		for(int x = origin.x + 1; x <= xEnd; x++)
		{
			CustomVector cell = new CustomVector();
			cell.x = x;
			cell.y = origin.y;
			if(customMap.ContainsKey(cell))
			{
				Tile tile = customMap[cell];
				if(tile.heightLevel >= xEndMaxHeight)
				{
					tilesInLine.Add(tile);
					xEndMaxHeight = Math.Max(xEndMaxHeight, tile.heightLevel);
				}
				else
				{
					break;
				}
			}
		}

		for(int y = origin.y + 1; y <= yEnd; y++)
		{
			CustomVector cell = new CustomVector();
			cell.x = origin.x;
			cell.y = y;
			if(customMap.ContainsKey(cell))
			{
				Tile tile = customMap[cell];
				if(tile.heightLevel >= yEndMaxHeight)
				{
					tilesInLine.Add(tile);
					yEndMaxHeight = Math.Max(yEndMaxHeight, tile.heightLevel);
				}
				else
				{
					break;
				}
			}
		}

		for(int x = origin.x - 1; x >= xStart; x--)
		{
			CustomVector cell = new CustomVector();
			cell.x = x;
			cell.y = origin.y;
			if(customMap.ContainsKey(cell))
			{
				Tile tile = customMap[cell];
				if(tile.heightLevel >= xStartMaxHeight)
				{
					tilesInLine.Add(tile);
					xStartMaxHeight = Math.Max(xStartMaxHeight, tile.heightLevel);
				}
				else
				{
					break;
				}
			}
		}

		for(int y = origin.y - 1; y >= yStart; y--)
		{
			CustomVector cell = new CustomVector();
			cell.x = origin.x;
			cell.y = y;
			if(customMap.ContainsKey(cell))
			{
				Tile tile = customMap[cell];
				if(tile.heightLevel >= yStartMaxHeight)
				{
					tilesInLine.Add(tile);
					yStartMaxHeight = Math.Max(yStartMaxHeight, tile.heightLevel);
				}
				else
				{
					break;
				}
			}
		}

		return tilesInLine;
	}

	public List<ActorCore> getRangedLineActorsInRange(Tile origin, int range)
	{
		List<ActorCore> actorsInLine = new List<ActorCore>();

		int xEnd = Math.Min(mapBoundaries[3], origin.x + range);
		int yEnd = Math.Min(mapBoundaries[2], origin.y + range);
		int xStart = Math.Max(mapBoundaries[1], origin.x - range);
		int yStart = Math.Max(mapBoundaries[0], origin.y - range);

		int xEndMaxHeight = 0;
		int yEndMaxHeight = 0;
		int xStartMaxHeight = 0;
		int yStartMaxHeight = 0;

		for(int x = origin.x + 1; x <= xEnd; x++)
		{
			CustomVector cell = new CustomVector();
			cell.x = x;
			cell.y = origin.y;
			if(customMap.ContainsKey(cell))
			{
				Tile tile = customMap[cell];
				if(tile.occupant is ActorCore)
				{
					if(tile.heightLevel >= xEndMaxHeight)
					{
						actorsInLine.Add((ActorCore)tile.occupant);
					}
					break;
				}
				else
				{
					xEndMaxHeight = Math.Max(xEndMaxHeight, tile.heightLevel);
				}
			}
		}

		for(int y = origin.y + 1; y <= yEnd; y++)
		{
			CustomVector cell = new CustomVector();
			cell.x = origin.x;
			cell.y = y;
			if(customMap.ContainsKey(cell))
			{
				Tile tile = customMap[cell];
				if(tile.occupant is ActorCore)
				{
					if(tile.heightLevel >= yEndMaxHeight)
					{
						actorsInLine.Add((ActorCore)tile.occupant);
					}
					break;
				}
				else
				{
					yEndMaxHeight = Math.Max(yEndMaxHeight, tile.heightLevel);
				}
			}
		}

		for(int x = origin.x - 1; x >= xStart; x--)
		{
			CustomVector cell = new CustomVector();
			cell.x = x;
			cell.y = origin.y;
			if(customMap.ContainsKey(cell))
			{
				Tile tile = customMap[cell];
				if(tile.occupant is ActorCore)
				{
					if(tile.heightLevel >= xStartMaxHeight)
					{
						actorsInLine.Add((ActorCore)tile.occupant);
					}
					break;
				}
				else
				{
					xStartMaxHeight = Math.Max(xStartMaxHeight, tile.heightLevel);
				}
			}
		}

		for(int y = origin.y - 1; y >= yStart; y--)
		{
			CustomVector cell = new CustomVector();
			cell.x = origin.x;
			cell.y = y;
			if(customMap.ContainsKey(cell))
			{
				Tile tile = customMap[cell];
				if(tile.occupant is ActorCore)
				{
					if(tile.heightLevel >= yStartMaxHeight)
					{
						actorsInLine.Add((ActorCore)tile.occupant);
					}
					break;
				}
				else
				{
					yStartMaxHeight = Math.Max(yStartMaxHeight, tile.heightLevel);
				}
			}
		}

		return actorsInLine;
	}

	public static CustomVector[] getSlashCells(Tile origin, Direction direction)
	{
		switch(direction)
		{
			case Direction.Right:
				return new CustomVector[3]
				{
					new CustomVector(origin.x + 1, origin.y - 1),
					new CustomVector(origin.x + 1, origin.y),
					new CustomVector(origin.x + 1, origin.y + 1),
				};
			case Direction.Down:
				return new CustomVector[3]
				{
					new CustomVector(origin.x + 1, origin.y + 1),
					new CustomVector(origin.x, origin.y + 1),
					new CustomVector(origin.x - 1, origin.y + 1),
				};
			case Direction.Left:
				return new CustomVector[3]
				{
					new CustomVector(origin.x - 1, origin.y + 1),
					new CustomVector(origin.x - 1, origin.y),
					new CustomVector(origin.x - 1, origin.y - 1),
				};
			default:
				return new CustomVector[3]
				{
					new CustomVector(origin.x - 1, origin.y - 1),
					new CustomVector(origin.x, origin.y - 1),
					new CustomVector(origin.x + 1, origin.y - 1),
				};
		}
	}

	public List<KeyValuePair<Tile, Tile>> getSlashTilePairsInRange(ActorCore actor)
	{
		List<KeyValuePair<Tile, Tile>> slashTilePairs = new List<KeyValuePair<Tile, Tile>>();
		foreach(Tile targetTile in customMap.Values)
		{
			if(targetTile.occupant is ActorCore && ((ActorCore)targetTile.occupant).isAlive() && targetTile.occupant != actor && actor.getAlliance((ActorCore)targetTile.occupant) != AllianceType.Ally)
			{
				for(int x = targetTile.x - 1; x < targetTile.x + 2; x++)
				{
					for(int y = targetTile.y - 1; y < targetTile.y + 2; y++)
					{
						CustomVector cell = new CustomVector();
						cell.x = x;
						cell.y = y;
						if(customMap.ContainsKey(cell))
						{
							Tile moveTile = customMap[cell];
							if(moveTile.actorDistance != -1 && (moveTile.occupant == null || moveTile == actor.tile))
							{
								if(x != targetTile.x && y != targetTile.y)
								{
									CustomVector cellX = new CustomVector();
									cellX.x = x;
									cellX.y = targetTile.y;
									CustomVector cellY = new CustomVector();
									cellY.x = targetTile.x;
									cellY.y = y;
									if(customMap.ContainsKey(cellX))
									{
										Tile targetableTile = customMap[cellX];
										if(targetableTile != actor.tile && Tile.isWithinReach(targetTile, moveTile))
										{
											slashTilePairs.Add(new KeyValuePair<Tile, Tile>(targetableTile, moveTile));
										}

									}
									if(customMap.ContainsKey(cellY))
									{
										Tile targetableTile = customMap[cellY];
										if(targetableTile != actor.tile && Tile.isWithinReach(targetTile, moveTile))
										{
											slashTilePairs.Add(new KeyValuePair<Tile, Tile>(targetableTile, moveTile));
										}
									}
								}
								else if(moveTile != targetTile && Tile.isWithinReach(targetTile, moveTile))
								{
									slashTilePairs.Add(new KeyValuePair<Tile, Tile>(targetTile, moveTile));
								}
							}
						}
					}
				}
			}
		}
		return slashTilePairs;
	}

	public virtual void calculateMovementRange(ActorCore actor, bool shouldPaint = true, bool _isCheckingRange = false)
	{
		Tile origin = actor.tile;

		if(origin != null)
		{
			List<Tile> current = new List<Tile>();
			List<Tile> next = new List<Tile>();
			current.Add(origin);
			origin.actorDistance = 0;

			while(current.Count > 0)
			{
				foreach(Tile tile in current)
				{
					if(tile.actorDistance < actor.movementRange)
					{
						foreach(Tile neighbour in tile.neighbours.Values)
						{
							if(neighbour != null && neighbour.isTilePassable(actor))
							{
								int heightDifference = tile.heightLevel - neighbour.heightLevel;
								if(heightDifference == 0
								|| (heightDifference < 0 && neighbour.climbable)
								|| (heightDifference > 0 && tile.climbable || tile.slidable))
								{
									int newDistance = tile.actorDistance + 1 + Math.Abs(heightDifference);
									if(neighbour.actorDistance > newDistance || (neighbour.actorDistance == -1 && newDistance <= actor.movementRange))
									{
										neighbour.actorDistance = newDistance;
										neighbour.parent = tile;
										next.Add(neighbour);
										if(shouldPaint)
										{
											tilesInRange.Add(neighbour);
										}
									}
								}
							}
						}
					}
				}
				current = next;
				next = new List<Tile>();
			}

			origin.parent = null;
		}
	}
}
