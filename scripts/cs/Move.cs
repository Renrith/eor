public enum MoveAction
{
	None,
	Attack,
	Climb,
	Grip,
	Move,
	NewAction,
	Push,
}

public class Move
{
	public readonly Tile tile;
	public readonly Tile origin;
	public readonly MoveAction action;

	public Move(Tile _tile, Tile _origin, MoveAction _action)
	{
		tile = _tile;
		origin = _origin;
		action = _action;
	}
}
