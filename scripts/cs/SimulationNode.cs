using System.Collections.Generic;

public class SimulationNode
{
	public SimulationNode parent;
	public List<SimulationNode> children = new List<SimulationNode>();
	public SimulationNode selectedChild;

	public int level = -1;

	public const float MIN_SCORE = -9999F;

	public List<Evaluation> evaluation = new List<Evaluation>();
	public float score = MIN_SCORE;
	public float childrenScore = MIN_SCORE;

	public List<ActorState> states { get; private set; }
	public Move move;
	public List<Move> moves = new List<Move>();

	public void setStates(List<ActorState> currentStates)
	{
		List<ActorState> actorStates = new List<ActorState>();
		foreach(ActorState state in currentStates)
		{
			ActorState newState = state.Clone();
			actorStates.Add(newState);
			state.Core.State = newState;
			if(state.endurance > 0)
			{
				if(state.tile != null)
				{
					newState.tile.occupant = state.Core;
				}
				else if(state.holdTile != null)
				{
					newState.holdTile.holdOccupant = (ActorCore)state.Core;
				}
			}
		}
		states = actorStates;
	}

	public void setEvaluation(SimulationNode childNode)
	{
		evaluation = childNode.evaluation;
		childrenScore = childNode.score;
		selectedChild = childNode;
	}

	public SimulationNode(SimulationNode parentNode)
	{
		parent = parentNode;
		if(parentNode != null)
		{
			level = parentNode.level + 1;
		}
	}
}
