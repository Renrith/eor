using Godot;
using System.Collections.Generic;

public enum SkillType
{
	NewAction,
	ActionModifier,
	AttributeModifier,
}

public enum ActionPhase
{
	Act,
	Defend,
}

public enum AttributeType
{
	Attack = 1,
	Defense = 2,
	Push = 4,
	Steadiness = 8,
	Endurance = 16,
	EnduranceRecovery = 32,
	Energy = 64,
	EnergyRecovery = 128,
	Range = 256,
	TurnRate = 512,
}

public class Buff
{
	public int turnsRemaining;
	public int change;

	public Buff(int _change = 0, int _turnsRemaining = -1)
	{
		change = _change;
		turnsRemaining = _turnsRemaining;
	}
}

public class Skill
{
	public string name { get { return _name; } }
	public string description { get { return _description; } }
	public string icon { get { return _icon; } }
	public int index { get { return _index; } }

	protected string _name;
	protected string _description;
	protected string _icon;
	public int _index;

	public SkillType type;

	protected Skill(string skillName)
	{
		_name = skillName;
		_index = Skills.allSkills.Count;
	}

	protected Skill(string skillName, string skillDescription)
	{
		_name = skillName;
		_description = skillDescription;
		_index = Skills.allSkills.Count;
	}

	protected Skill(string skillName, string skillDescription, string skillIcon)
	{
		_name = skillName;
		_description = skillDescription;
		_icon = skillIcon;
		_index = Skills.allSkills.Count;
	}
}

public interface IActionModifier
{
	string name { get; }
	string icon { get; }
	int index { get; }
	AllActions action { get; }
	ActionPhase phase { get; }
	List<ActionOutcome> calculateModifierOutcome(List<ActionOutcome> outcomeList, Tile tile, ActorCore actor);
}

public interface INewAction
{
	string name { get; }
	string icon { get; }
	int index { get; }
	TargetAlliance targetableActorType { get; }
	TargetingType targetingType { get; }
	int range { get; }

	void initialize(ActorCore actor);
	List<ActionOutcome> calculateOutcome(Tile tile, Tile origin);
}

public class AttributeModifier : Skill
{
	AttributeType attributeType;
	Dictionary<AttributeType, int> attributes;

	public void setAttributes(ActorCore actor)
	{
		foreach(KeyValuePair<AttributeType, int> attribute in attributes)
		{
			switch(attribute.Key)
			{
				case AttributeType.Attack:
					actor.attack += attribute.Value;
					break;
				case AttributeType.Defense:
					actor.defense += attribute.Value;
					break;
				case AttributeType.Push:
					actor.push += attribute.Value;
					break;
				case AttributeType.Steadiness:
					actor.steadiness += attribute.Value;
					break;
				case AttributeType.Endurance:
					actor.endurance += attribute.Value;
					actor.maxEndurance += attribute.Value;
					break;
				case AttributeType.EnduranceRecovery:
					break;
				case AttributeType.Energy:
					actor.energy += attribute.Value;
					actor.maxEnergy += attribute.Value;
					break;
				case AttributeType.EnergyRecovery:
					break;
				case AttributeType.Range:
					actor.attackRange += attribute.Value;
					break;
				case AttributeType.TurnRate:
					actor.turnRate += attribute.Value;
					actor.turnOrder += attribute.Value;
					break;
			}
		}
	}

	public void removeAttributes(ActorCore actor)
	{
		foreach(KeyValuePair<AttributeType, int> attribute in attributes)
		{
			switch(attribute.Key)
			{
				case AttributeType.Attack:
					actor.attack -= attribute.Value;
					break;
				case AttributeType.Defense:
					actor.defense -= attribute.Value;
					break;
				case AttributeType.Push:
					actor.push -= attribute.Value;
					break;
				case AttributeType.Steadiness:
					actor.steadiness -= attribute.Value;
					break;
				case AttributeType.Endurance:
					actor.endurance -= attribute.Value;
					actor.maxEndurance -= attribute.Value;
					break;
				case AttributeType.EnduranceRecovery:
					break;
				case AttributeType.Energy:
					actor.energy -= attribute.Value;
					actor.maxEnergy -= attribute.Value;
					break;
				case AttributeType.EnergyRecovery:
					break;
				case AttributeType.Range:
					actor.attackRange -= attribute.Value;
					break;
				case AttributeType.TurnRate:
					actor.turnRate -= attribute.Value;
					actor.turnOrder -= attribute.Value;
					break;
			}
		}
	}

	public AttributeModifier(string _name, Dictionary<AttributeType, int> _attributes) : base(_name)
	{
		type = SkillType.AttributeModifier;
		attributes = _attributes;
		foreach(AttributeType attribute in attributes.Keys)
		{
			attributeType = attributeType | attribute;
		}
	}
}

public class Skills : Reference
{
	public static List<Skill> allSkills;

	public static string gdGetSkillName(int skillIndex)
	{
		string skillName = allSkills[skillIndex].name;
		return skillName;
	}

	public static string gdGetSkillDescription(int skillIndex)
	{
		string skillDescription = allSkills[skillIndex].description;
		return skillDescription;
	}

	public static string gdGetSkillIcon(int skillIndex)
	{
		Skill skill = allSkills[skillIndex];
		return skill.icon;
	}

	public static string gdGetSkillType(int skillIndex)
	{
		Skill skill = allSkills[skillIndex];
		if(skill.type == SkillType.AttributeModifier)
		{
			return "Attribute modifier";
		}
		else if(skill.type == SkillType.ActionModifier)
		{
			IActionModifier actionModifierSkill = (IActionModifier)skill;
			switch(actionModifierSkill.action)
			{
				case AllActions.attack:
					return "Attack skill";
				case AllActions.push:
					return "Push skill";
			}
		}
		else if(skill.type == SkillType.NewAction)
		{
			return "Action skill";
		}
		return "Skill";
	}

	public static void setSkill(ActorCore actor, Skill skill)
	{
		if(skill is AttributeModifier)
		{
			AttributeModifier attributeSkill = (AttributeModifier)skill;
			attributeSkill.setAttributes(actor);
			actor.attributeSkills.Add(attributeSkill);
		}
		else if(skill is IActionModifier)
		{
			IActionModifier actionModifierSkill = (IActionModifier)skill;
			switch(actionModifierSkill.action)
			{
				case AllActions.attack:
					actor.attackSkill = actionModifierSkill;
					break;
				case AllActions.push:
					actor.pushSkill = actionModifierSkill;
					break;
			}
		}
		else if(skill is INewAction)
		{
			actor.actionSkill = (INewAction)skill;
			actor.actionSkill.initialize(actor);
		}
	}

	public static void gdSetSkill(ActorCore actor, int skillIndex)
	{
		Skill skill = allSkills[skillIndex];
		setSkill(actor, skill);
	}

	Skills()
	{
		allSkills = new List<Skill>();

		allSkills.Add(new AttributeModifier("attack +2", new Dictionary<AttributeType, int>() { { AttributeType.Attack, 2 } }));

		allSkills.Add(new SlashSkill());
		allSkills.Add(new ArdousFallSkill());
		allSkills.Add(new MomentumSkill());
		allSkills.Add(new CounterSkill());

		allSkills.Add(new SheathSkill());
		allSkills.Add(new BandAidSkill());
		allSkills.Add(new PositivistDiscourseSkill());
		allSkills.Add(new SprintSkill());
		allSkills.Add(new PassBatonSkill());
		allSkills.Add(new CaptureSkill());
	}
}
