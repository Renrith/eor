using Godot;
using System;
using System.Collections.Generic;

public enum ThoughtType
{
	None,
	Defeat,
	Endurance,
	HeightDifference,
	LowTurnCount,
	Move,
	PushOff,
	Support,
}

public enum ThoughtQualifier
{
	Amount,
	Several,
	Target,
	Custom,
}

public enum ThoughtEvaluationStage
{
	TurnEnd,
	BattleEnd,
}

public class Thought : Reference
{
	public int skillPoints = 0;
	public int buildPoints = 0;
	public bool isAchieved = false;
	public bool shouldBeAchieved = true;
	public string text = "";
	public string winconText { get { return getWinconText(); } }

	protected ActorCore actor = null;

	protected int threshold = 0;
	protected ThoughtType type = ThoughtType.None;
	protected ThoughtQualifier qualifier = ThoughtQualifier.Amount;
	protected ThoughtEvaluationStage stage = ThoughtEvaluationStage.TurnEnd;
	protected TargetAlliance alliance = TargetAlliance.Any;

	protected string getAllianceText()
	{
		switch(alliance)
		{
			case TargetAlliance.Ally:
				return "allies";
			case TargetAlliance.NeutralAlly:
			case TargetAlliance.NeutralRival:
				return "neutrals";
			case TargetAlliance.Enemy:
				return "enemies";
			case TargetAlliance.Self:
				return "self";
			case TargetAlliance.NonEnemy:
				return "allies or neutrals";
			case TargetAlliance.NonAlly:
				return "enemies or neutrals";
			default:
				return "units";
		}
	}

	protected virtual string getWinconText()
	{
		return "";
	}

	public virtual void checkAchievementStatus(List<ActionOutcome> actionOutcomeList)
	{
		if(shouldBeAchieved != isAchieved)
		{
			int achievedThisTurn = 0;
			switch(type)
			{
				case ThoughtType.HeightDifference:
					foreach(ActionOutcome outcome in actionOutcomeList)
					{
						if(outcome.target is ActorCore
							&& outcome.target.getTileOrHoldTile() != null
							&& (Factions.getTargetAlliance(actor, (ActorCore)outcome.target) & alliance) != 0)
						{
							achievedThisTurn = actor.tile.heightLevel - outcome.target.getTileOrHoldTile().heightLevel;
						}
					}
					break;

				case ThoughtType.LowTurnCount:
					achievedThisTurn++;
					break;
			}

			setIsAchieved(achievedThisTurn);
		}
	}

	protected void setIsAchieved(int achievedThisTurn)
	{
		switch(qualifier)
		{
			case ThoughtQualifier.Amount:
				threshold -= achievedThisTurn;
				if(threshold <= 0)
				{
					isAchieved = !isAchieved;
				}
				break;

			case ThoughtQualifier.Several:
				if(achievedThisTurn >= threshold)
				{
					isAchieved = !isAchieved;
				}
				break;
		}
	}

	public Thought(PlayerCore _actor, ThoughtType _type, ThoughtQualifier _qualifier, int _threshold, bool _shouldBeAchieved, bool isCompleted, int _skillPoints, int _buildPoints, string _text, TargetAlliance _alliance)
	{
		actor = _actor;
		type = _type;
		qualifier = _qualifier;
		threshold = _threshold;
		shouldBeAchieved = _shouldBeAchieved;
		if(isCompleted)
		{
			isAchieved = _shouldBeAchieved;
		}
		else
		{
			isAchieved = !_shouldBeAchieved;
		}
		skillPoints = _skillPoints;
		buildPoints = _buildPoints;
		text = _text;
		alliance = _alliance;
	}
}

public class DefeatThought : Thought
{
	private ActorCore target = null;

	protected override string getWinconText()
	{
		string winconText = "";
		string targetName = (string)target.Base.Get("actorName");

		switch(qualifier)
		{
			case ThoughtQualifier.Target:
				if(actor == target)
				{
					winconText = "Prevent self from being KOd";
				}
				else if(shouldBeAchieved)
				{
					winconText = $"KO {targetName}";
				}
				else
				{
					winconText = $"Prevent {targetName} from being KOd";
				}
				break;
		}
		return winconText;
	}

	public override void checkAchievementStatus(List<ActionOutcome> actionOutcomeList)
	{
		if(shouldBeAchieved != isAchieved)
		{
			switch(qualifier)
			{
				case ThoughtQualifier.Target:
					foreach(ActionOutcome outcome in actionOutcomeList)
					{
						if(outcome.target == target && outcome.enduranceChange + ((ActorCore)outcome.target).endurance <= 0)
						{
							isAchieved = !isAchieved;
						}
					}
					break;
			}
		}
	}

	public DefeatThought(PlayerCore _actor, ThoughtType _type, ThoughtQualifier _qualifier, int _threshold, bool _shouldBeAchieved, bool isCompleted, ThoughtEvaluationStage _stage, ActorCore _target, int _skillPoints, int _buildPoints, string _text, TargetAlliance _alliance) : base(_actor, _type, _qualifier, _threshold, _shouldBeAchieved, isCompleted, _skillPoints, _buildPoints, _text, _alliance)
	{
		stage = _stage;
		target = _target;
	}
}

public class EnduranceThought : Thought
{
	protected override string getWinconText()
	{
		string winconText = "";
		string allianceText = getAllianceText();

		switch(qualifier)
		{
			case ThoughtQualifier.Amount:
				if(shouldBeAchieved)
				{
					if(threshold < 0)
					{
						winconText = $"Deal {Math.Abs(threshold)} or more damage to {allianceText}";
					}
					else
					{
						winconText = $"Heal {Math.Abs(threshold)} or more health to {allianceText}";
					}
				}
				else
				{
					winconText = $"Prevent {threshold} or more damage to {allianceText}";
				}
				break;

			case ThoughtQualifier.Several:
				if(shouldBeAchieved)
				{
					if(threshold < 0)
					{
						winconText = $"Deal {Math.Abs(threshold)} or more damage at once to {allianceText}";
					}
					else
					{
						winconText = $"Heal {Math.Abs(threshold)} or more health at once to {allianceText}";
					}
				}
				else
				{
					winconText = $"Prevent {threshold} or more damage at once to {allianceText}";
				}
				break;
		}

		return winconText;
	}

	public override void checkAchievementStatus(List<ActionOutcome> actionOutcomeList)
	{
		if(shouldBeAchieved != isAchieved)
		{
			int achievedThisTurn = 0;
			foreach(ActionOutcome outcome in actionOutcomeList)
			{
				if(outcome.target is ActorCore
				&& outcome.target != actor
				&& (outcome.enduranceChange < 0 && threshold < 0) || (outcome.enduranceChange > 0 && threshold > 0)
				&& (Factions.getTargetAlliance(actor, (ActorCore)outcome.target) & alliance) != 0)
				{
					achievedThisTurn += outcome.enduranceChange;
				}
			}

			setIsAchieved(achievedThisTurn);
		}
	}

	public EnduranceThought(PlayerCore _actor, ThoughtType _type, ThoughtQualifier _qualifier, int _threshold, bool _shouldBeAchieved, bool isCompleted, int _skillPoints, int _buildPoints, string _text, TargetAlliance _alliance) : base(_actor, _type, _qualifier, _threshold, _shouldBeAchieved, isCompleted, _skillPoints, _buildPoints, _text, _alliance) { }
}

public class MoveThought : Thought
{
	protected override string getWinconText()
	{
		string winconText = "";
		string allianceText = getAllianceText();

		switch(qualifier)
		{
			case ThoughtQualifier.Amount:
				if(shouldBeAchieved)
				{
					winconText = $"Move {Math.Abs(threshold)} {allianceText}";
				}
				else
				{
					winconText = $"Prevent {threshold} or more {allianceText} from being moved";
				}
				break;

			case ThoughtQualifier.Several:
				if(shouldBeAchieved)
				{
					winconText = $"Move {Math.Abs(threshold)} {allianceText} at once";
				}
				else
				{
					winconText = $"Prevent {threshold} or more {allianceText} from being moved at once";
				}
				break;
		}

		return winconText;
	}

	public override void checkAchievementStatus(List<ActionOutcome> actionOutcomeList)
	{
		if(shouldBeAchieved != isAchieved)
		{
			int achievedThisTurn = 0;
			foreach(ActionOutcome outcome in actionOutcomeList)
			{
				if(outcome.target != null && outcome.path != null && outcome.path.Count > 0)
				{
					achievedThisTurn++;
				}
			}

			setIsAchieved(achievedThisTurn);
		}
	}

	public MoveThought(PlayerCore _actor, ThoughtType _type, ThoughtQualifier _qualifier, int _threshold, bool _shouldBeAchieved, bool isCompleted, int _skillPoints, int _buildPoints, string _text, TargetAlliance _alliance) : base(_actor, _type, _qualifier, _threshold, _shouldBeAchieved, isCompleted, _skillPoints, _buildPoints, _text, _alliance) { }
}

public class PushOffThought : Thought
{
	protected override string getWinconText()
	{
		string winconText = "";
		string allianceText = getAllianceText();

		switch(qualifier)
		{
			case ThoughtQualifier.Amount:
				if(shouldBeAchieved)
				{
					winconText = $"Push off {Math.Abs(threshold)} {allianceText}";
				}
				else
				{
					winconText = $"Prevent {threshold} or more {allianceText} from being pushed off";
				}
				break;

			case ThoughtQualifier.Several:
				if(shouldBeAchieved)
				{
					winconText = $"Push off {Math.Abs(threshold)} {allianceText} at once";
				}
				else
				{
					winconText = $"Prevent {threshold} or more {allianceText} from being pushed off at once";
				}
				break;
		}

		return winconText;
	}

	public override void checkAchievementStatus(List<ActionOutcome> actionOutcomeList)
	{
		if(shouldBeAchieved != isAchieved)
		{
			int achievedThisTurn = 0;
			foreach(ActionOutcome outcome in actionOutcomeList)
			{
				if(outcome.target is ActorCore
					&& outcome.path != null
					&& outcome.path[outcome.path.Count - 1] == null
					&& outcome.holdTile == null
					&& (Factions.getTargetAlliance(actor, (ActorCore)outcome.target) & TargetAlliance.NonAlly) != 0)
				{
					achievedThisTurn++;
				}
			}

			setIsAchieved(achievedThisTurn);
		}
	}

	public PushOffThought(PlayerCore _actor, ThoughtType _type, ThoughtQualifier _qualifier, int _threshold, bool _shouldBeAchieved, bool isCompleted, int _skillPoints, int _buildPoints, string _text, TargetAlliance _alliance) : base(_actor, _type, _qualifier, _threshold, _shouldBeAchieved, isCompleted, _skillPoints, _buildPoints, _text, _alliance) { }
}
