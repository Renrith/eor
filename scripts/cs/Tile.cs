using Godot;
using System;
using System.Collections.Generic;

public struct CustomVector
{
	public int x;
	public int y;

	public CustomVector(int _x, int _y)
	{
		x = _x;
		y = _y;
	}
}

public enum TileTypes
{
	floorTile,
	wallTile,
	gripTile,
	climbTile,
	slideTile,
	holdTile,
};

public class Tile : Reference, IComparable<Tile>
{
	public const int MAX_DISTANCE = 999;

	public bool passable = false;
	public bool graspable = false;
	public bool climbable = false;
	public bool slidable = false;
	public bool holdable = false;
	public bool slippery = false;

	public IPlaceable occupant = null;
	public ActorCore holdOccupant = null;
	public int actorDistance = -1;
	public int areaDistance = MAX_DISTANCE;
	public StreamTexture sprite;
	public int index;
	public Tile parent;

	public int x;
	public int y;
	public Vector2 cell;
	public Vector2 position;
	public Vector2 offset;
	public Vector2 gdOffset;
	public Vector2 cursorOffset = Vector2.Zero;
	public static int baseYOffset = 16;

	public Dictionary<Direction, Tile> neighbours = new Dictionary<Direction, Tile>();
	public Dictionary<Direction, int> borderDistance = new Dictionary<Direction, int>();
	public int minBorderDistance = MAX_DISTANCE;

	public int score = -1;
	public int cumulativeScore = -1;

	public PaintType paintType = PaintType.Clear;
	public bool isPaintingEnemyRange = false;

	public int heightLevel = 0;
	public int movementCost = 1;
	public int gCost;
	public int hCost;
	public int fCost
	{
		get { return gCost + hCost; }
	}

	int priorityListIndex;
	public int listIndex
	{
		get { return priorityListIndex; }
		set { priorityListIndex = value; }
	}

	public int CompareTo(Tile tile)
	{
		int priority = tile.fCost.CompareTo(fCost);
		if(priority == 0)
		{
			priority = tile.hCost.CompareTo(hCost);
		}
		return priority;
	}

	public bool isTilePassable(ActorCore actor)
	{
		if(passable && (occupant == null || occupant is ActorCore && actor.getAlliance((ActorCore)occupant) == AllianceType.Ally))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static bool isWithinReach(Tile tile, Tile origin)
	{
		return Math.Abs(tile.heightLevel - origin.heightLevel) < 2;
	}

	public static bool hasTargetableOccupant(Tile tile, Tile origin)
	{
		if(tile != null && tile.occupant != null && isWithinReach(tile, origin))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public Tile gdGetNeighbour(string gdDirection)
	{
		Direction direction = DirectionHelper.gdDirections[gdDirection];
		return neighbours[direction];
	}

	public Tile getNeighbour(Direction direction)
	{
		return neighbours[direction];
	}

	public string gdGetNeighbourDirection(Tile neighbour)
	{
		foreach(KeyValuePair<Direction, Tile> entry in neighbours)
		{
			if(entry.Value == neighbour)
			{
				foreach(KeyValuePair<string, Direction> gdDirection in DirectionHelper.gdDirections)
				{
					if(gdDirection.Value == entry.Key)
					{
						return gdDirection.Key;
					}
				}
			}
		}
		throw new ArgumentException("Origin and target tiles are not neighbours.", "Target tile: " + neighbour.cell.ToString() + ", origin tile: " + cell.ToString());
	}

	public Direction getNeighbourDirection(Tile neighbour)
	{
		foreach(KeyValuePair<Direction, Tile> entry in neighbours)
		{
			if(entry.Value == neighbour)
			{
				return entry.Key;
			}
		}
		throw new ArgumentException("Origin and target tiles are not neighbours.", "Target tile: " + neighbour.cell.ToString() + ", origin tile: " + cell.ToString());
	}

	public static int getManhattanDistance(Tile origin, Tile goal)
	{
		int manhattanDistance = Math.Abs(origin.x - goal.x) + Math.Abs(origin.y - goal.y);
		return manhattanDistance;
	}

	public static int getManhattanDistance(Tile origin, List<Tile> goals)
	{
		int manhattanDistance = MAX_DISTANCE;

		foreach(Tile goal in goals)
		{
			manhattanDistance = Math.Min(Math.Abs(origin.x - goal.x) + Math.Abs(origin.y - goal.y), manhattanDistance);
		}
		return manhattanDistance;
	}

	public Tile() { }

	public Tile(TileMap TileMap, Vector2 cellPosition, int tileId, int newTileId, TileTypes tileType, StreamTexture cellSprite)
	{
		neighbours.Add(Direction.Right, null);
		neighbours.Add(Direction.Down, null);
		neighbours.Add(Direction.Left, null);
		neighbours.Add(Direction.Up, null);

		borderDistance.Add(Direction.Right, MAX_DISTANCE);
		borderDistance.Add(Direction.Down, MAX_DISTANCE);
		borderDistance.Add(Direction.Left, MAX_DISTANCE);
		borderDistance.Add(Direction.Up, MAX_DISTANCE);

		cell = cellPosition;
		x = (int)cellPosition.x;
		y = (int)cellPosition.y;

		heightLevel = (cellSprite.GetHeight() - 32) / 32;

		switch(tileType)
		{
			case TileTypes.floorTile:
				passable = true;
				break;

			case TileTypes.gripTile:
				graspable = true;
				break;

			case TileTypes.climbTile:
				passable = true;
				climbable = true;
				break;

			case TileTypes.slideTile:
				passable = true;
				slidable = true;
				break;

			case TileTypes.holdTile:
				passable = true;
				holdable = true;
				break;
		}

		index = newTileId;
		sprite = cellSprite;

		TileSet TileSet = (TileSet)TileMap.TileSet;

		position = TileMap.MapToWorld(cell);

		offset = (Vector2)TileSet.Call("tile_get_texture_offset", tileId);
		gdOffset = new Vector2(offset.x, baseYOffset - offset.y * -1);
		Vector2 navigationOffset = (Vector2)TileSet.Call("tile_get_navigation_polygon_offset", tileId);
		if(navigationOffset != null)
		{
			cursorOffset = new Vector2(navigationOffset.x, baseYOffset - navigationOffset.y * -1);
		}

		TileSet.CreateTile(index);
		TileSet.TileSetTexture(index, sprite);
		TileSet.TileSetTextureOffset(index, offset);
		if(heightLevel > 0)
		{
			TileSet.TileSetZIndex(index, (int)position.y - 14);
		}
		else
		{
			TileSet.TileSetZIndex(index, (int)position.y - 16);
		}
		TileMap.SetCellv(cell, index);
	}
}
