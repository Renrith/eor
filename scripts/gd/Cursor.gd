extends Sprite

enum occupant {
	none,
	actor,
	selectedActor,
}

enum cursorState {
	idle,
	player,
	playerMove,
	playerMenu,
	targetNavigation,
}
var state: int = cursorState.idle

var isActive: bool = true
var timer := Timer.new()
var isCursorTimerActive: bool = false
var isMoving: bool = false
var isMovingLongPressed: bool = false
var lastFocusedOccupant: int = occupant.none

var isPaintingEnemyRange: bool = false
var isPaintingGrid: bool = true

var tile: Reference
var previousTile: Reference
var selectedActor: Node
var tileListIndex: int = 0

const cameraSpeed: float = 0.3
const cursorSpeed: float = 0.2
const longPressCursorSpeed: float = 0.15
const longPressCursorSpeedChange: float = -0.015
const longPressCursorSpeedMin: float = 0.08
var longPressCursorSpeedCurrent: float = longPressCursorSpeed
var moveDirection: String = ""

var Map: TileMap
var UI: CanvasLayer
var Setting: ViewportContainer
var SettingCamera: Camera2D
var Grid: YSort

var Audio: AudioStreamPlayer2D
var Camera: Camera2D
var CursorSprite: AnimatedSprite
var CursorDot: Node2D

var cursorSpriteScene = preload("res://scenes/CursorSprite.tscn")
var cursorDotScene = preload("res://scenes/CursorDot.tscn")

func remove() -> void:
	CursorDot.queue_free()
	queue_free()

func show() -> void:
	CursorSprite.set_visible(true)

func hide() -> void:
	CursorSprite.set_visible(false)

func cursorTimerStart(time) -> void:
	isCursorTimerActive = true
	timer.set_wait_time(time)
	timer.start()

func cursorTimerOver() -> void:
	isCursorTimerActive = false
	var action := ""
	if Input.is_action_pressed("cursor_up"):
		action = "cursor_up"
	if Input.is_action_pressed("cursor_left"):
		action = "cursor_left"
	if Input.is_action_pressed("cursor_down"):
		action = "cursor_down"
	if Input.is_action_pressed("cursor_right"):
		action = "cursor_right"
	
	if action != "":
		var event := InputEventAction.new()
		event.action = action
		event.pressed = true
		Input.parse_input_event(event)

func toggleEnemyRange() -> void:
	isPaintingEnemyRange = !isPaintingEnemyRange
	repaintEnemyRange()
	if !selectedActor.hasMoved && (tile.occupant == null || tile.occupant == selectedActor.Core):
		Map.calculateMovementRange(selectedActor.Core, true, false)
		Map.paintCursorPath()

func toggleGrid() -> void:
	isPaintingGrid = !isPaintingGrid
	Grid.visible = isPaintingGrid

func repaintEnemyRange() -> void:
	Map.paintEnemyRange(isPaintingEnemyRange)

func setTile(newTile, shouldPaintMovementRange):
	previousTile = tile
	tile = newTile
	
	if tile != null:
		setVisualPosition(tile.cursorOffset, tile.position)
		if shouldPaintMovementRange:
			Map.paintCursorPath()

func setVisualPosition(offset, position) -> void:
	CursorSprite.offset = offset
	CursorSprite.position = position
	CursorSprite.z_index = position.y

func setPosition(newTile):
	offset = newTile.gdOffset
	position = Map.map_to_world(newTile.cell)
	setTile(newTile, false)

func movePosition(newTile):
	if !isMoving:
		if lastFocusedOccupant == occupant.actor:
			Map.gdWipeTiles()
		if !Setting.Battle.isAITakingAction:
			UI.clearTriangleRightMenu()
		isMoving = true
	elif !isMovingLongPressed:
		if lastFocusedOccupant == occupant.actor:
			Map.calculateMovementRange(selectedActor.Core, true, false)
		longPressCursorSpeedCurrent = longPressCursorSpeed
		isMovingLongPressed = true
	else:
		longPressCursorSpeedCurrent += longPressCursorSpeedChange
		longPressCursorSpeedCurrent = max(longPressCursorSpeedCurrent, longPressCursorSpeedMin)
	
	var shouldPaintMovementRange = true
	if tile.occupant == selectedActor.Core && tile.occupant.Controller.hasMoved:
		shouldPaintMovementRange = false
	
	setTile(newTile, shouldPaintMovementRange)
	
	$Tween.addPoint(Map.map_to_world(newTile.cell), newTile.gdOffset, cameraSpeed)
	$Tween.interpolateNextPoint()
	Audio.navigate()

func onPositionMoved():
	if tile.occupant == null || !tile.occupant.isActor:
		if lastFocusedOccupant == occupant.actor:
			Map.gdWipeTiles()
			Map.calculateMovementRange(selectedActor.Core, true, false)
			Map.paintCursorPath()
		lastFocusedOccupant = occupant.none
	elif tile.occupant == selectedActor.Core:
		if lastFocusedOccupant == occupant.actor:
			Map.gdWipeTiles()
			Map.calculateMovementRange(selectedActor.Core, true, false)
		lastFocusedOccupant = occupant.selectedActor
	else:
		Map.gdWipeTiles()
		Map.calculateMovementRange(tile.occupant, true, true)
		UI.showTriangleRightMenu(tile.occupant.Base)
		lastFocusedOccupant = occupant.actor

func handleCursorMove(event: InputEvent):
	var newTile: Reference
	var _moveDirection := ""
	
	if event.is_action("cursor_down", true):
		_moveDirection = "down"
	elif event.is_action("cursor_up", true):
		_moveDirection = "up"
	elif event.is_action("cursor_right", true):
		_moveDirection = "right"
	elif event.is_action("cursor_left", true):
		_moveDirection = "left"
	
	if _moveDirection != "":
		newTile = Map.gdGetCursorNeighbour(tile, _moveDirection)
		
		if newTile != null:
			if moveDirection != _moveDirection:
				longPressCursorSpeedCurrent = longPressCursorSpeed
				moveDirection = _moveDirection
			
			movePosition(newTile)
			
			if isMovingLongPressed:
				cursorTimerStart(longPressCursorSpeedCurrent)
			else:
				cursorTimerStart(cursorSpeed)

func handlePlayerMove(_position, _camera):
	state = cursorState.playerMove
	hide()
	Audio.select()

func handleCancelPlayerMove():
	state = cursorState.player
	UI.clearPlayerMenu()
	selectedActor.handleCancelInput()
	repaintEnemyRange()
	movePosition(selectedActor.Core.gdGetTileOrHoldTile())
	Map.calculateMovementRange(selectedActor.Core, true, false)
	show()

func handlePlayerMenu(_menuOptions):
	state = cursorState.playerMenu
	repaintEnemyRange()
	hide()

func handleTargetNavigation(increase = 0):
	show()
	Audio.navigate()
	
	var tiles: Array = selectedActor.targetTiles
	
	if state != cursorState.targetNavigation:
		if tile.occupant != null && tile.occupant != selectedActor.Core:
			tileListIndex = tiles.find(tile)
		else:
			tileListIndex = 0
		state = cursorState.targetNavigation
	
	elif selectedActor.action == "attack" || selectedActor.action == "push":
		handleTargetListNavigation(increase)
		return
	
	elif tiles.size() > 1:
		tileListIndex = tileListIndex + increase
		if tileListIndex == -1:
			tileListIndex = tiles.size() - 1
		elif tiles.size() == tileListIndex:
			tileListIndex = 0
	
	setPosition(tiles[tileListIndex])
	selectedActor.handleActionForTarget(tileListIndex)

func handleTargetListNavigation(increase):
	if selectedActor.shouldNavigateTargetList(increase):
		selectedActor.handleTargetListNavigation(increase)
	
	elif selectedActor.targetTiles.size() > 1:
		tileListIndex = tileListIndex + increase
		if tileListIndex == -1:
			tileListIndex = selectedActor.targetTiles.size() - 1
		elif selectedActor.targetTiles.size() == tileListIndex:
			tileListIndex = 0
		selectedActor.handleActionForTarget(tileListIndex, increase)

func startVisualTurn(actor):
	setPosition(actor.Core.gdGetTileOrHoldTile())
	if actor.Core.isPlayer:
		state = cursorState.player
		tileListIndex = 0
		show()

func addCursorSprite():
	var YSort = Map.get_node("YSort")
	CursorSprite = cursorSpriteScene.instance()
	YSort.call_deferred("add_child", CursorSprite)
	YSort.call_deferred("move_child", CursorSprite, 0)
	
	Grid = YSort.get_node("Grid")

func _input(event: InputEvent):
	if (event.is_pressed()
	&& isActive && !CursorDot.isActive && !isCursorTimerActive
	&& selectedActor && is_instance_valid(selectedActor) && selectedActor.Core.isPlayer && selectedActor.hasActiveVisualTurn):
		match state:
			cursorState.player:
				if Input.is_action_just_pressed("ui_cancel"):
					movePosition(selectedActor.Core.gdGetTileOrHoldTile())
					cursorTimerStart(cursorSpeed)
				elif Input.is_action_just_pressed("ui_accept"):
					selectedActor.handleAcceptInput(tile)
					cursorTimerStart(cursorSpeed)
				else:
					handleCursorMove(event)
				
			cursorState.playerMenu:
				var action: String = UI.handlePlayerMenu()
				if action != "":
					match action:
						"cancel":
							handleCancelPlayerMove()
						_:
							selectedActor.handleSelectedAction(action)
					cursorTimerStart(cursorSpeed)
				
			cursorState.targetNavigation:
				if Input.is_action_just_pressed("ui_left"):
					handleTargetNavigation(-1)
				elif Input.is_action_just_pressed("ui_right"):
					handleTargetNavigation(+1)
				elif Input.is_action_just_pressed("ui_accept"):
					state = cursorState.idle
					hide()
					selectedActor.executeSelectedAction()
				elif Input.is_action_just_pressed("ui_cancel"):
					selectedActor.undoActionPreview()
				cursorTimerStart(cursorSpeed)
		
		if Input.is_action_just_pressed("ui_select"):
			toggleEnemyRange()
		elif Input.is_action_just_pressed("toggle_grid"):
			toggleGrid()

func _ready() -> void:
	if !Engine.editor_hint:
		Audio = $Audio
		Camera = $Camera
		
		Map = get_node("/root/Viewports/SettingViewport/Map")
		UI = get_node("/root/Viewports/UIViewport/BattleUILayer")
		Setting = get_node("/root/Setting")
		SettingCamera = get_node("/root/Viewports/SettingViewport/SettingCamera")
		SettingCamera.CursorCamera = Camera
		
		CursorDot = cursorDotScene.instance()
		Map.call_deferred("add_child", CursorDot)
		
		timer.connect("timeout", self, "cursorTimerOver")
		timer.set_one_shot(true)
		add_child(timer)

func _exit_tree() -> void:
	SettingCamera.CursorCamera = null
