extends "res://scripts/gd/SFXAudio.gd"

export(AudioStream) var selectSound = preload("res://sound/effects/cursorSelect.wav")
export(AudioStream) var navigateSound = preload("res://sound/effects/cursor.wav")
export(AudioStream) var cancelSound = preload("res://sound/effects/cursorCancel.wav")

func navigate():
	stop()
	stream = navigateSound
	play()

func select():
	stop()
	stream = selectSound
	play()

#Might not need?
func cancel():
	stop()
	stream = cancelSound
	play()
