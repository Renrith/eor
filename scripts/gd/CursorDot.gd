extends Sprite

const actionStrengthMultiplier := Vector2(6, 6)
const cursorDotOffset := Vector2(0, 16)

onready var Map: TileMap = get_node("/root/Viewports/SettingViewport/Map")
onready var Cursor: Sprite = get_node("/root/Viewports/SettingViewport/Map/YSort/Cursor")

var isActive: bool = false

var timer := Timer.new()
var isTimerActive: bool = false
const timerTime := 0.01

func timerStart() -> void:
	isTimerActive = true
	timer.set_wait_time(timerTime)
	timer.start()

func timerOver() -> void:
	isTimerActive = false
	handleCursorMove()

func handleCursorMove() -> void:
	if Input.get_joy_axis(0, JOY_AXIS_0) || Input.get_joy_axis(0, JOY_AXIS_1):
		var actionStrength := Input.get_vector("cursor_axis_left", "cursor_axis_right", "cursor_axis_up", "cursor_axis_down")
		
		if actionStrength != Vector2.ZERO:
			if !isActive:
				isActive = true
				position = Cursor.tile.position + cursorDotOffset
			
			var actionStrengthRatio := abs(actionStrength.x) / max(abs(actionStrength.y), 0.001)
			
			if actionStrengthRatio < 0.5:
				actionStrength = Vector2(0, actionStrength.y)
			elif actionStrengthRatio > 0.5 && actionStrengthRatio < 4:
				var newActionY := abs(actionStrength.x) / 2
				if actionStrength.y < 0:
					newActionY = -newActionY
				actionStrength = Vector2(actionStrength.x, newActionY)
			else:
				actionStrength = Vector2(actionStrength.x, 0)
			
			position = Map.gdSnapToMapBoundaries(position + actionStrength * actionStrengthMultiplier)
			
			var cellPosition: Vector2 = Map.world_to_map(position)
			if Cursor.tile.cell != cellPosition:
				var newTile: Reference = Map.gdGetTile(cellPosition.x, cellPosition.y)
				if newTile != null:
					Cursor.movePosition(newTile)
			
			timerStart()
		else:
			isActive = false
		
	else:
		isActive = false

func _input(_event: InputEvent) -> void:
	if !isTimerActive && Cursor.state == Cursor.cursorState.player:
		handleCursorMove()

func _ready() -> void:
	timer.connect("timeout", self, "timerOver")
	timer.set_one_shot(true)
	add_child(timer)

