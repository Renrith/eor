extends "res://scripts/gd/Tween.gd"

func interpolateNextPoint():
	if points.size() > 0:
		var point = points.pop_front()
		interpolate_property(movingNode, "position", movingNode.position, point.position, point.speed)
		interpolate_property(movingNode, "offset", movingNode.offset, point.offset, point.speed)
		interpolate_property(movingNode, "z_index", movingNode.z_index, point.position.y, point.speed)
		start()
	else:
		endPath()

func endPath():
	movingNode.isMoving = false
	movingNode.isMovingLongPressed = false
	movingNode.onPositionMoved()
