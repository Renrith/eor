extends Node

var Setting: ViewportContainer
var Map: TileMap
onready var DialoguesUI: Node = get_node("/root/Viewports/UIViewport/DialoguesUILayer")

signal nextDialog(actor, text, isLeftSide)
signal skippingAnimation
signal dialoguesUnmounted

class Dialog:
	var actor: Node2D
	var text: String
	var emotion: String = ""
	var isLeftSide: bool
	var portrait: StreamTexture
	var path: Array
	var music: Resource
	var shouldStopMusic: bool = false
	var condition: String = ""
	var conditionDirection: bool

var dialogues: Array
var currentDialog: Dialog
var isActive: bool = false

func advanceDialog() -> void:
	if DialoguesUI.hasFinishedAnimatingText:
		while (currentDialog == null || currentDialog.condition != "" && Conditions.conditionsList[currentDialog.condition] != currentDialog.conditionDirection) && dialogues.size() > 0:
			currentDialog = dialogues.pop_front()
		
		if currentDialog && (currentDialog.condition == "" || Conditions.conditionsList[currentDialog.condition] == currentDialog.conditionDirection):
			DialoguesUI.isThought = currentDialog.actor == null
			if DialoguesUI.isReady:
				if !DialoguesUI.isThought:
					Globals.OutlineController.applyOutline(currentDialog.actor)
				elif Setting.actors["Alaitz"] != null:
					Globals.OutlineController.applyOutline(Setting.actors["Alaitz"])
				
				if currentDialog.path != null && currentDialog.path.size() > 0:
					currentDialog.actor.updatePathPosition(currentDialog.path, "step")
				
				if currentDialog.music != null:
					Globals.MusicController.setMusic(currentDialog.music)
				
				if currentDialog.shouldStopMusic == true:
					Globals.MusicController.setMusic(null)
				
				emit_signal("nextDialog",
					currentDialog.actor,
					currentDialog.text,
					currentDialog.emotion,
					currentDialog.isLeftSide,
					currentDialog.portrait)
				
				currentDialog = null
			
			elif !DialoguesUI.isAnimating:
				DialoguesUI.show()
		else:
			isActive = false
			emit_signal("dialoguesUnmounted")
	else:
		emit_signal("skippingAnimation")

func onDialoguesEnded() -> void:
	Setting.endMode()

func enableInput() -> void:
	isActive = true
	advanceDialog()

func _input(_event) -> void:
	if isActive && Input.is_action_just_pressed("ui_accept"):
		advanceDialog()

func _ready() -> void:
	Setting = get_parent()
	Map = Setting.Map
	
	connect("nextDialog", DialoguesUI, "showNextDialog")
	connect("skippingAnimation", DialoguesUI, "skipAnimation")
	connect("dialoguesUnmounted", DialoguesUI, "hide")
	DialoguesUI.connect("animationReady", self, "advanceDialog")
	DialoguesUI.connect("animationEnded", self, "onDialoguesEnded")
