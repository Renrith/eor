extends AudioStreamPlayer

var MusicTween: Tween

var newStream: AudioStreamMP3
var defaultVolume: float = -16
var pausedTime: float = 0

func fade() -> void:
	if stream != null && playing:
		MusicTween.fadeOut()
		MusicTween.callback = funcref(self, "playNew")
	else:
		playNew()

func playNew() -> void:
	stop()
	volume_db = defaultVolume
	stream = newStream
	play()

func pause(callback: FuncRef) -> void:
	pausedTime = get_playback_position()
	MusicTween.fadeOut()
	MusicTween.callback = callback

func resume() -> void:
	play(pausedTime + MusicTween.timeElapsed)
	MusicTween.fadeIn()

func setMusic(music: AudioStreamMP3) -> void:
	if music != stream:
		newStream = music
		fade()

func _ready() -> void:
	MusicTween = preload("res://scripts/gd/MusicTween.gd").new()
	add_child(MusicTween)
	bus = "Music"
