extends Tween

var MusicController: AudioStreamPlayer

const fadeInTime := 1.5
const fadeOutTime := 2
const fadeOutVolume := -40

var timeElapsed := 0
var isFadeOut = false

var callback: FuncRef

func fadeIn() -> void:
	interpolate_property(MusicController, "volume_db", MusicController.volume_db, MusicController.defaultVolume, fadeInTime)
	interpolate_property(self, "timeElapsed", 0, fadeInTime, fadeInTime)
	isFadeOut = false
	start()

func fadeOut() -> void:
	interpolate_property(MusicController, "volume_db", MusicController.volume_db, fadeOutVolume, fadeOutTime)
	interpolate_property(self, "timeElapsed", 0, fadeOutTime, fadeOutTime)
	isFadeOut = true
	start()

func endFade() -> void:
	if isFadeOut:
		MusicController.stop()
	if callback != null && callback.is_valid():
		callback.call_func()
		callback = null

func _ready() -> void:
	MusicController = get_parent()
	connect("tween_all_completed", self, "endFade")
