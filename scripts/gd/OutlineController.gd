extends Reference

var outline: Material
var actor: Node2D

func applyOutline(newActor: Node2D) -> void:
	if actor != null && is_instance_valid(actor):
		actor.Sprite.material = actor.spriteGangColor
	
	actor = newActor
	if actor != null:
		actor.Sprite.material = outline
		outline.set_shader_param("color", actor.color)

func _init() -> void:
	outline = preload("res://shaders/placeable/spriteOutline.tres")
