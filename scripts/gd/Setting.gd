extends ViewportContainer

signal inputReady

const DialoguesScript = preload("res://scripts/gd/Dialogues.gd")

var actors = {
	"Alaitz": null,
	"Tara": null,
	"Nalke": null,
	"Naimel": null,
}

var modeDialogues := {}
var interpolatedDialogues := {}
var actorDefeatDialogues := {}
var actorVictoryDialogues := {}

var dialogActor: Node
var dialogKey: String

var Battle: Node
var Map: TileMap
var BattleRewards: Node
var Cursor: Node2D
var UI: CanvasLayer
var Dialogues: Node
onready var FadeLayer = get_node("/root/Viewports/UIViewport/FadeLayer")

enum interpolatedDialoguesMode {
	none,
	beforeAction,
	betweenTurns,
	meet,
}
var currentInterpolatedDialoguesMode: int = interpolatedDialoguesMode.none

var isInputReady: bool = false

func getActorData(actorName: String) -> Node2D:
	for actor in actors:
		if actor == actorName:
			return actors[actor]
	
	return null

func getDefeatDetails() -> Dictionary:
	var defeatData: Array = Battle.gdGetDefeatData()
	var actor = actors[defeatData[0]]
	
	if (actorDefeatDialogues.get(actor.actorName) != null
	&& actorDefeatDialogues[actor.actorName][defeatData[1]] != ""):
		return {"actor": actor, "text": actorDefeatDialogues[actor.actorName][defeatData[1]], "emotion": ""}
	
	elif actor.get("defeatDialogues") != null:
		return {"actor": actor, "text": actor.defeatDialogues[defeatData[1]], "emotion": ""}
	
	else:
		return {}

func getVictoryDetails() -> Dictionary:
	var actor = actors["Alaitz"]
	
	if actorVictoryDialogues.get(actor.actorName) != null:
		var portrait: StreamTexture = load(actor.portraits[actorVictoryDialogues[actor.actorName].portrait])
		return {
			"actor": actor,
			"text": actorVictoryDialogues[actor.actorName].text,
			"emotion": "",
			"portrait": portrait,
		}
	
	elif actor.get("victoryDialogues") != null:
		#TODO: Proper default data
		return {
			"actor": actor,
			"text": actor.victoryDialogues,
			"emotion": "",
			"portrait": actor.preparationsPortrait,
		}
	
	else:
		return {}

func getDialogues(dialogJSON: String) -> Array:
	var dialogues: Array
	
	var data: JSONParseResult = Globals.openJSON(dialogJSON)
	
	for result in data.result:
		var dialog = DialoguesScript.Dialog.new()
		dialog.actor = getActorData(result.actor)
		dialog.text = result.text
		if result.get("emotion") != null:
			dialog.emotion = result.emotion
		dialog.isLeftSide = result.left
		if result.portrait != "":
			dialog.portrait = load(dialog.actor.portraits[result.portrait])
		
		if result.get("condition") != null && result.condition != "":
			dialog.condition = result.condition
			dialog.conditionDirection = result.conditionDirection
		
		if result.get("path") != null:
			dialog.path = []
			for position in result.path:
				var tile: Reference = Map.gdGetTile(position.x, position.y)
				dialog.path.push_back(tile)
		
		if result.get("music") != null:
			dialog.music = load(result.music)
		
		if result.get("shouldStopMusic") == true:
			dialog.shouldStopMusic = true
		
		dialogues.append(dialog)
	
	return dialogues

func interpolateBeforeActionDialogues(interpolatedDialoguesKey: String, actor: Node) -> void:
	currentInterpolatedDialoguesMode = interpolatedDialoguesMode.beforeAction
	interpolateDialogues(interpolatedDialoguesKey, actor)

func interpolateBetweenTurnsDialogues(interpolatedDialoguesKey: String, actor: Node) -> void:
	currentInterpolatedDialoguesMode = interpolatedDialoguesMode.betweenTurns
	interpolateDialogues(interpolatedDialoguesKey, actor)

func interpolateMeetDialogues() -> void:
	currentInterpolatedDialoguesMode = interpolatedDialoguesMode.meet
	interpolateDialogues("meetDialog", Cursor.selectedActor)

func interpolateDialogues(interpolatedDialoguesKey: String, actor: Node) -> void:
	dialogActor = actor
	dialogKey = interpolatedDialoguesKey
	
	if Game.showStory:
		Cursor.isActive = false
		UI.hide()
		Dialogues = DialoguesScript.new()
		Dialogues.dialogues = interpolatedDialogues[dialogKey]
		add_child(Dialogues)
		Dialogues.enableInput()
	else:
		endMode()

func startMode() -> void:
	if !Game.isShowingSideConversationFromTitle:
		Saves.autoSaveData()
	match Game.currentMode:
		Globals.modes.battle:
			startBattle()
		Globals.modes.dialogue:
			if Game.showStory:
				startDialogues()
			else:
				endMode()
		Globals.modes.sideConversation:
			if Game.showStory && Game.showSideConversations:
				startDialogues()
			else:
				endMode()
		Globals.modes.preparations:
			startPreparations()

func startBattle() -> void:
	Battle = load(Game.currentScene["battleScript"]).new()
	Map.Battle = Battle
	add_child(Battle)
	
	if isInputReady:
		Battle.call_deferred("startActorTurn")
	else:
		connect("inputReady", Battle, "startActorTurn")
	
	if Game.currentScene.has("battleTrack"):
		Globals.MusicController.setMusic(load(Game.currentScene["battleTrack"]))

func startDialogues() -> void:
	Dialogues = DialoguesScript.new()
	Dialogues.dialogues = modeDialogues[Game.currentModeIndex]
	add_child(Dialogues)
	
	if isInputReady:
		Dialogues.enableInput()
	else:
		connect("inputReady", Dialogues, "enableInput")

func startPreparations() -> void:
	var Preparations = preload("res://scenes/UI/PreparationsUI.tscn")
	Viewports.get_node("UIViewport").add_child(Preparations.instance())
	Globals.MusicController.setMusic(null)

func endMode() -> void:
	if currentInterpolatedDialoguesMode == interpolatedDialoguesMode.none:
		match Game.currentMode:
			Globals.modes.battle:
				Battle.queue_free()
				if BattleRewards != null:
					BattleRewards.applyBattleRewards()
			Globals.modes.dialogue:
				Dialogues.queue_free()
			Globals.modes.sideConversation:
				Dialogues.queue_free()
				Game.currentScene["read"] = true
				Saves.saveSideConversationData()
		
		Game.currentModeIndex += 1
		if Game.currentModes.size() > Game.currentModeIndex:
			startMode()
		else:
			Game.currentModeIndex = 0
			FadeLayer.fadeOut(funcref(Game, "endSetting"))
		
	else:
		Globals.OutlineController.applyOutline(dialogActor.Base)
		Battle.executeDialoguesCallbacks(dialogKey)
		dialogKey = ""
		Dialogues.queue_free()

		Cursor.isActive = true
		if currentInterpolatedDialoguesMode != interpolatedDialoguesMode.betweenTurns:
			dialogActor.executeSelectedAction()
		dialogActor = null
		currentInterpolatedDialoguesMode = interpolatedDialoguesMode.none

func loadDefeatDialogues() -> void:
	var data: JSONParseResult = Globals.openJSON(Game.currentScene["defeatDialoguesJSON"])
	
	for result in data.result:
		actorDefeatDialogues[result] = data.result[result]

func loadVictoryDialogues() -> void:
	var data: JSONParseResult = Globals.openJSON(Game.currentScene["victoryDialoguesJSON"])
	
	for result in data.result:
		actorVictoryDialogues[result] = data.result[result]

func setSceneReady() -> void:
	isInputReady = true
	emit_signal("inputReady")

func _enter_tree() -> void:
	if Game.currentScene.has("mapScene"):
		Map = load(Game.currentScene["mapScene"]).instance()
		Viewports.get_node("SettingViewport").add_child(Map)

func _ready() -> void:
	UI = Viewports.get_node("UIViewport/BattleUILayer")
	
	if Map != null:
		Cursor = Map.get_node_or_null("YSort/Cursor")
		BattleRewards = Map.get_node_or_null("BattleRewards")
	
	var allActors: Array = get_tree().get_nodes_in_group("actors")
	for actorName in actors:
		for actor in allActors:
			if actor.actorName == actorName:
				actors[actorName] = actor
	
	if Game.currentScene.has("modeDialoguesJSON"):
		var modeDialoguesJSON = Game.currentScene["modeDialoguesJSON"]
		for dialogJSON in modeDialoguesJSON:
			var dialog = getDialogues(modeDialoguesJSON[dialogJSON])
			modeDialogues[dialogJSON] = dialog
	
	if Game.currentScene.has("interpolatedDialoguesJSON"):
		var interpolatedDialoguesJSON = Game.currentScene["interpolatedDialoguesJSON"]
		for dialogJSON in interpolatedDialoguesJSON:
			var dialog = getDialogues(interpolatedDialoguesJSON[dialogJSON])
			interpolatedDialogues[dialogJSON] = dialog
	
	if Game.currentScene.has("defeatDialoguesJSON"):
		loadDefeatDialogues()
	
	if Game.currentScene.has("victoryDialoguesJSON"):
		loadVictoryDialogues()
	
	FadeLayer.call_deferred("fadeIn", funcref(self, "setSceneReady"))
	startMode()

func _exit_tree() -> void:
	if Map != null:
		Viewports.get_node("SettingViewport").remove_child(Map)
