extends "res://scripts/gd/Camera.gd"

const zoomValues = {
	attack = Vector2(0.375, 0.375),
	default = Vector2(0.5, 0.5),
}

var SettingViewport: Viewport
var CursorCamera: Camera2D #Set and destroyed by Cursor, when Cursor exists

var NextCamera: Camera2D

func moveToActor(actor):
	if actor.Core.isPlayer:
		setActionCamera("move", actor.position, CursorCamera)
	else:
		setActionCamera("move", actor.position, actor.Camera)

func prepareMove(position, camera):
	centerAroundPoint(position)
	setActionCamera("prepare move", position, camera)

func undoMove(tile):
	setActionCamera("undo move", tile.position, CursorCamera)

func showOutcomeCamera(actor, target, action, _hasAdditionalTargets, pushOutcome):
	if actor.Core.isPlayer:
		if !current:
			centerAroundPoint(actor.Base.position)
			make_current()
		match action:
			"attack", "capture":
				setActionCamera(action, target.position)
			"push":
				setActionCamera(action, pushOutcome.newPosition)
	else:
		centerAroundPoint(actor.move.origin.position)
		actor.Camera.make_current()

func undoPreview(position):
	setActionCamera("undo preview", position)

func setTurnEndCamera():
	setActionCamera("end turn")

func setNavigateCamera(actor, camera = null):
	setActionCamera("", actor.Base.position, camera)

func setActionCamera(action, point = null, pointCamera = null):
	match action:
		"attack", "capture":
			$Tween.interpolate_property(self, "zoom", zoom, zoomValues.attack, 0.375, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
			$Tween.interpolate_property(self, "position", position, getCenter(point, zoomValues.attack), 0.375, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		"prepare move":
			$Tween.interpolate_property(self, "position", zoom, getCenter(point, zoomValues.default), 0.1)
		"move", "undo move", "undo preview":
			var finalPosition: Vector2 = getCenter(point, zoomValues.default)
			var speed: float = sqrt(pow((position.x - finalPosition.x), 2) + pow((position.y - finalPosition.y), 2)) / 171
			$Tween.interpolate_property(self, "zoom", zoom, zoomValues.default, speed, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
			$Tween.interpolate_property(self, "position", position, finalPosition, speed, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		"end turn":
			$Tween.interpolate_property(self, "zoom", zoom, zoomValues.default, 0.25)
		_:
			$Tween.interpolate_property(self, "zoom", zoom, zoomValues.default, 0.25)
			$Tween.interpolate_property(self, "position", position, getCenter(point, zoomValues.default), 0.25)
	$Tween.start()
	NextCamera = pointCamera

func getCenter(point: Vector2, newZoom = null):
	var targetZoom: Vector2 = zoom
	if newZoom:
		 targetZoom = newZoom
	return point - SettingViewport.size / 2 * targetZoom

func centerAroundPoint(point: Vector2, newZoom = null):
	var targetZoom: Vector2 = zoom
	if newZoom:
		 targetZoom = newZoom
	set_position(getCenter(point, targetZoom))

func delegateCamera():
	if NextCamera:
		NextCamera.make_current()
		NextCamera = null

func _ready():
	SettingViewport = get_parent()
	$Tween.connect("tween_all_completed", self, "delegateCamera")
