extends Tween

class Point:
	var easeType: int = Tween.EASE_IN_OUT
	var offset: Vector2 = Vector2(0, 16)
	var modulate: Color = Color.black
	var position: Vector2
	var speed: float
	var transitionType = Tween.TRANS_LINEAR
	var zIndex: int = -1

var points: Array = []

var movingNode: Node2D

func getPoint(position, offset, speed) -> Point:
	var point = Point.new()
	point.position = position
	point.offset = offset
	point.speed = speed
	return point

func addPoint(newPosition, newOffset, speed, zIndex = -1) -> void:
	var point = getPoint(newPosition, newOffset, speed)
	point.zIndex = zIndex
	points.push_back(point)

func interpolateNextPoint() -> void:
	if !is_active() && points.size() > 0:
		var point = points.pop_front()
		interpolate_property(movingNode, "position", movingNode.position, point.position, point.speed)
		interpolate_property(movingNode, "offset", movingNode.offset, point.offset, point.speed)
		start()
	else:
		endPath()

func endPath() -> void:
	pass

func _ready() -> void:
	if movingNode == null:
		movingNode = get_parent()
	self.connect("tween_all_completed", self, "interpolateNextPoint")
