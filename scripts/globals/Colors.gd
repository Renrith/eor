tool
extends Node

var baseColor := Color(0.33, 0.32, 0.42, 1)
var primaryColor := Color(0.36, 0.94, 1, 1)
var negativeColor := Color(1, 0.4, 0.54, 1)
var transparentColor := Color(0, 0, 0, 0)
var whiteTransparentColor := Color(1, 1, 1, 0)

var baseColorDark := baseColor * baseColor
var outlineColor := Color(baseColorDark.r, baseColorDark.g, baseColorDark.b, 0.5)
var primaryOutlineColor := Color(primaryColor.r, primaryColor.g, primaryColor.b, 0.35)
var panelColor := Color(0.24, 0.17, 0.28, 0.95)

var textColor := Color(1, 0.97, 0.88, 1)
var positiveTextColor := Color(0.16, 0.9, 0.91, 1)
var negativeTextColor := negativeColor
var disabledTextColor := Color(0.3, 0.14, 0.29, 1)
var titleColor := Color(0.86, 0.65, 0.44, 1)

var secondaryTextColor := Color(textColor.r, textColor.g, textColor.b, 0.52)
var secondaryPositiveTextColor := Color(positiveTextColor.r, positiveTextColor.g, positiveTextColor.b, 0.52)
var secondaryNegativeTextColor := Color(negativeTextColor.r, negativeTextColor.g, negativeTextColor.b, 0.52)
var secondaryDisabledTextColor := Color(disabledTextColor.r, disabledTextColor.g, disabledTextColor.b, 0.85)
