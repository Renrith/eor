extends Node

var conditionsList := {
	"Ch4HaveEnemiesBeenClearedBeforeReinforcements": false,
	"Ch4HaveReinforcementsBeenTriggered": false,
	"Ch4HasReunionTakenPlace": false,
	"Ch4HasNalkeSharedGangInfo": false,
	"Ch8PlayerCapturedNaimel": false,
	"Ch9HasHostageBeenSaved": false,
	"Ch10HasAlaitzCapturedGangLeader": false,
	"Ch17HasNaimelFoughtLoren": false,
	"Ch18HasRinuriBeenPushed": false,
}
