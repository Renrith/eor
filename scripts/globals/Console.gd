extends LineEdit

func load(currentSceneIndex: String, currentModeIndex: String) -> void:
	Game.currentSceneIndex = currentSceneIndex.to_int()
	Game.currentModeIndex = currentModeIndex.to_int()
	Game.advanceSetting()

func onTextEntered(newText: String) -> void:
	var output: Array = newText.split(" ")
	callv(output.pop_front(), output)
	visible = false

func _input(_event: InputEvent):
	if OS.is_debug_build() && Input.is_action_just_pressed("debug_console"):
		if visible:
			visible = false
			release_focus()
		else:
			visible = true
			grab_focus()
		
		accept_event()

func _ready() -> void:
	if OS.is_debug_build():
		connect("text_entered", self, "onTextEntered")
