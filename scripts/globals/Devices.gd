tool
extends Node

var controllerCount := 0
var isGamepad := false
var isDefaultGamepad := false

signal onInputDeviceChanged(isGamepad)

func onControllerChanged(_id: int, _isConnected: bool) -> void:
	var _isGamepad := Input.get_connected_joypads().size() > 0
	var _isDefaultGamepad := Input.get_joy_name(0).to_lower().find("ps") == -1
	if _isGamepad != isGamepad || _isDefaultGamepad != isDefaultGamepad:
		isGamepad = _isGamepad
		isDefaultGamepad = _isDefaultGamepad
		emit_signal("onInputDeviceChanged")

func _ready() -> void:
	Input.connect("joy_connection_changed", self, "onControllerChanged")
	isGamepad = Input.get_connected_joypads().size() > 0
