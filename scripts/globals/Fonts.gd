tool
extends Node

var UIFont16: DynamicFont
var UIFont24: DynamicFont
var UIFont32: DynamicFont
var UIFont48: DynamicFont
var UIFont64: DynamicFont
var UIFont128: DynamicFont

var UIFont16Outline: DynamicFont
var UIFont32Outline: DynamicFont
var UIFont32OutlineBase: DynamicFont
var UIFont32OutlinePrimary: DynamicFont
var UIFont48Outline: DynamicFont
var UIFont64Outline: DynamicFont
var UIFont128Outline: DynamicFont

var DialogFont16: DynamicFont
var DialogFont24: DynamicFont
var DialogFont32: DynamicFont
var DialogFont48: DynamicFont
var DialogFont64: DynamicFont

const UIFont := preload("res://fonts/mago3.ttf")
const DialogFont := preload("res://fonts/Abaddon Bold.ttf")

const UIFontSpacing := -8

func createFont(fontData: DynamicFontData, size: int) -> DynamicFont:
	var font = DynamicFont.new()
	font.font_data = fontData
	font.font_data.antialiased = false
	font.size = size
	if fontData == UIFont:
		font.extra_spacing_top = size / UIFontSpacing
	return font

func _ready() -> void:
	UIFont16 = createFont(UIFont, 16)
	UIFont24 = createFont(UIFont, 24)
	UIFont32 = createFont(UIFont, 32)
	UIFont48 = createFont(UIFont, 48)
	UIFont64 = createFont(UIFont, 64)
	UIFont128 = createFont(UIFont, 128)
	
	UIFont16Outline = createFont(UIFont, 16)
	UIFont16Outline.outline_size = 2
	UIFont16Outline.outline_color = Colors.outlineColor
	
	UIFont32Outline = createFont(UIFont, 32)
	UIFont32Outline.outline_size = 3
	UIFont32Outline.outline_color = Colors.outlineColor
	
	UIFont32OutlineBase = createFont(UIFont, 32)
	UIFont32OutlineBase.extra_spacing_bottom = -7
	UIFont32OutlineBase.outline_size = 8
	UIFont32OutlineBase.outline_color = Colors.baseColor
	
	UIFont32OutlinePrimary = createFont(UIFont, 32)
	UIFont32OutlinePrimary.extra_spacing_bottom = 14
	UIFont32OutlinePrimary.outline_size = 4
	UIFont32OutlinePrimary.outline_color = Colors.primaryOutlineColor
	
	UIFont48Outline = createFont(UIFont, 48)
	UIFont48Outline.outline_size = 8
	UIFont48Outline.outline_color = Colors.baseColor
	
	UIFont64Outline = createFont(UIFont, 64)
	UIFont64Outline.outline_size = 8
	UIFont64Outline.outline_color = Colors.outlineColor
	
	UIFont128Outline = createFont(UIFont, 128)
	UIFont128Outline.outline_size = 10
	UIFont128Outline.extra_spacing_top = -21
	UIFont128Outline.extra_spacing_bottom = -21
	UIFont128Outline.outline_color = Colors.outlineColor
	
	DialogFont16 = createFont(DialogFont, 16)
	DialogFont16.extra_spacing_top = 4
	DialogFont24 = createFont(DialogFont, 24)
	DialogFont24.extra_spacing_top = 6
	DialogFont32 = createFont(DialogFont, 32)
	DialogFont32.extra_spacing_top = 8
	DialogFont48 = createFont(DialogFont, 48)
	DialogFont48.extra_spacing_top = 12
	DialogFont64 = createFont(DialogFont, 64)
	DialogFont64.extra_spacing_top = 16
