extends Node

onready var NotificationsUILayer = get_node("/root/Viewports/UIViewport/NotificationsUILayer")

const titleScene := "res://scenes/UI/title/Title.tscn"
const settingScene = "res://scenes/Setting.tscn"

var isShowingSideConversationFromTitle := false
var currentSceneIndex := 0
var currentModeIndex := 0

var currentScene: Dictionary setget, getScene
func getScene() -> Dictionary:
	return GameData.sceneList[currentSceneIndex]

var currentModes: Dictionary setget, getModes
func getModes() -> Dictionary:
	return getScene()["modes"]

var currentMode: int setget, getMode
func getMode() -> int:
	return getScene()["modes"][currentModeIndex]

var showStory := true
var showSideConversations := true

var thoughtsData := {}

var actorData := {
	"Alaitz": {
		"script": preload("res://scripts/placeable/gd/Alaitz.gd"),
		"skillTree": "res://scenes/UI/skillTree/SkillTreeAlaitz.tscn",
		"skills": [],
		"buildDataList": [
			{
				"borrowedSkills": null,
				"attributeSkills": null,
				"attackSkill": null,
				"pushSkill": null,
				"actionSkill": null,
				"portrait": "neutralExistential",
				"name": "Default",
			},
		],
		"currentBuildIndex": 0,
		"buildPoints": 5,
		"skillPoints": 5,
	},
	"Naimel": {
		"script": preload("res://scripts/placeable/gd/Naimel.gd"),
		"skillTree": "res://scenes/UI/skillTree/SkillTreeNaimel.tscn",
		"skills": [],
		"buildDataList": [
			{
				"borrowedSkills": null,
				"attributeSkills": null,
				"attackSkill": null,
				"pushSkill": null,
				"actionSkill": null,
				"portrait": "neutralSmiling",
				"name": "The Mary Sue",
			},
		],
		"currentBuildIndex": 0,
		"buildPoints": 0,
		"skillPoints": 4,
	},
	"Nalke": {
		"script": preload("res://scripts/placeable/gd/Nalke.gd"),
		"skillTree": "res://scenes/UI/skillTree/SkillTreeNalke.tscn",
		"skills": [],
		"buildDataList": [
			{
				"borrowedSkills": [],
				"attributeSkills": null,
				"attackSkill": null,
				"pushSkill": null,
				"actionSkill": 6,
				"current": true,
				"portrait": "neutralGoodBoy",
				"name": "The Normal Person",
			},
		],
		"currentBuildIndex": 0,
		"buildPoints": 4,
		"skillPoints": 5,
	},
	"Tara": {
		"script": preload("res://scripts/placeable/gd/Tara.gd"),
		"skillTree": "res://scenes/UI/skillTree/SkillTreeTara.tscn",
		"skills": [],
		"buildDataList": [
			{
				"borrowedSkills": [],
				"attributeSkills": null,
				"attackSkill": 1,
				"pushSkill": null,
				"actionSkill": 5,
				"current": true,
				"portrait": "edgyGrin",
				"name": "The Edgelord",
			},
		],
		"currentBuildIndex": 0,
		"buildPoints": 6,
		"skillPoints": 2,
	},
}

func getCurrentSceneThoughtsCompleted() -> Dictionary:
	var sceneName = GameData.sceneList[currentSceneIndex].name
	if !thoughtsData.has(sceneName):
		thoughtsData[sceneName] = {}
	return thoughtsData[sceneName]

func setThoughtCompleted(actor: Node2D) -> void:
	var sceneName = GameData.sceneList[currentSceneIndex].name
	if !thoughtsData.has(sceneName):
		thoughtsData[sceneName] = {}
	thoughtsData[sceneName][actor.actorName] = true

func endSetting() -> void:
	if !isShowingSideConversationFromTitle:
		currentSceneIndex += 1
		var Setting := get_tree().get_current_scene()
		if Setting.get("actors"):
			for actorName in Setting.actors:
				var actor = Setting.actors[actorName]
				if actor != null && actor.get("buildDataList"):
					Game.actorData[actorName].buildDataList = actor.buildDataList
					Game.actorData[actorName].currentBuildIndex = actor.currentBuildIndex
					Game.actorData[actorName].buildPoints = actor.buildPoints
					Game.actorData[actorName].skillPoints = actor.skillPoints
		advanceSetting()
	else:
		isShowingSideConversationFromTitle = false
		returnToTitle()

func advanceSetting() -> void:
	if currentSceneIndex == 0:
		Saves.saveModifiedTime = Time.get_unix_time_from_system()
	
	if GameData.sceneList.size() > currentSceneIndex:
		handleSideConversation()
	
	while (GameData.sceneList.size() > currentSceneIndex && (
		!showStory && getScene().mainMode != Globals.modes.battle
		|| !showSideConversations && getScene().mainMode == Globals.modes.sideConversation)):
		handleSideConversation()
		currentSceneIndex += 1
	
	if GameData.sceneList.size() > currentSceneIndex:
		Globals.OutlineController.actor = null
		get_tree().change_scene(settingScene)
	else:
		currentSceneIndex = -1
		returnToTitle()

func setCurrentSceneIndex(scene: Dictionary) -> void:
	for sceneIndex in range(0, GameData.sceneList.size()):
		if GameData.sceneList[sceneIndex] == scene:
			currentSceneIndex = sceneIndex
			break

func handleSideConversation() -> void:
	if getScene().mainMode == Globals.modes.sideConversation:
		if showStory && !showSideConversations:
			NotificationsUILayer.showSideConversation(getScene())
		getScene()["unlocked"] = true
		Saves.saveSideConversationData()

func setSideConversation(key: String) -> void:
	isShowingSideConversationFromTitle = true
	var currentScene = GameData.sideConversations[key]
	setCurrentSceneIndex(currentScene)
	get_tree().change_scene(settingScene)

func returnToTitle() -> void:
	get_tree().change_scene(titleScene)

func resize() -> void:
	var newResolution = OS.get_window_size()
	Viewports.rect_size = newResolution

func _ready() -> void:
	randomize()
	
	Saves.loadSideConversationData()
	
	var screen_size: Vector2 = OS.get_screen_size()
	var window_size: Vector2 = OS.get_window_size()
	OS.set_window_position(screen_size * 0.5 - window_size * 0.5)
	
	var WorldViewport = get_tree().get_root()
	WorldViewport.connect("size_changed", self, "resize")
	resize()
