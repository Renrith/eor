tool

extends Node

enum gdFactions {
	AlaitzsGang = 0,
	NoGang = 1,
	NoGangPlayer = 2,
	NePlusUltra = 3,
	EmeraldFront = 4,
	Aureos = 5,
	Besova = 6,
	Cenobe = 7,
	Princi = 8,
	OnTheEdge = 9,
	Clockwise = 10,
}

enum gdDefeatReason {
	EnduranceDepleted = 0,
	PushedOff = 1,
	Other = 2,
}

enum gdSkills {
	AttackPlus2 = 0,
	SlashSkill = 1,
	ArdousFallSkill = 2,
	MomentumSkill = 3,
	CounterSkill = 4,
	SheathSkill = 5,
	BandAidSkill = 6,
	PositivistDiscourseSkill = 7,
	SprintSkill = 8,
	PassBatonSkill = 9,
	CaptureSkill = 10,
}

enum gdTargetingType {
	Neighbour = 0,
	RangedSkip = 1,
	RangedLine = 2,
	Slash = 3,
	Self = 4,
}

enum modes {
	battle,
	dialogue,
	preparations,
	sideConversation,
}

const maxUISizeRatio: float = 1.87

var Factions: Reference
var OutlineController: Reference
var MusicController: AudioStreamPlayer
var Skills: Reference

onready var musicBusIndex := AudioServer.get_bus_index("Music")
onready var SFXBusIndex := AudioServer.get_bus_index("SFX")

func getDirectionVector(direction):
	match direction:
		"right":
			return Vector2(1, 0)
		"down":
			return Vector2(0, 1)
		"left":
			return Vector2(-1, 0)
		"up":
			return Vector2(0, -1)
		_:
			return Vector2.ZERO

func getClockwiseDirectionVector(direction):
	match direction:
		"right":
			return Vector2(0, 1)
		"down":
			return Vector2(-1, 0)
		"left":
			return Vector2(0, -1)
		"up":
			return Vector2(1, 0)
		_:
			return Vector2.ZERO

func getExtendedZIndex(originalZIndex, direction):
	match direction:
		"up", "left":
			return originalZIndex - 32
		_:
			return originalZIndex + 32

func getGangColor(index: int) -> Color:
	match index:
		gdFactions.AlaitzsGang, gdFactions.NoGangPlayer:
			return Color(0.07, 0.78, 0.89, 1)
		gdFactions.NoGang:
			return Color(0.6, 0.6, 0.6, 1)
		gdFactions.EmeraldFront:
			return Color(0.21, 0.76, 0.52, 1)
		gdFactions.Aureos, gdFactions.Clockwise:
			return Color(0.94, 0.76, 0.4, 1)
		_:
			return Color(0.94, 0.29, 0.38, 1)

func openJSON(fileJSON: String) -> JSONParseResult:
	var data: JSONParseResult
	var file = File.new()
	
	if file.open(fileJSON, File.READ) == OK:
		var rawJSON = file.get_as_text()
		file.close()
		data = JSON.parse(rawJSON)
		if data.error != OK:
			print("error parsing JSON: ", data.error)
			return null
	else:
		print("error opening file")
		return null
	
	return data

func _init() -> void:
	if !Engine.editor_hint:
		var factionsScript = preload("res://scripts/cs/Factions.cs")
		Factions = factionsScript.new()
		var outlineControllerScript = preload("res://scripts/gd/OutlineController.gd")
		OutlineController = outlineControllerScript.new()
		MusicController = preload("res://scripts/gd/MusicController.gd").new()
		add_child(MusicController)
		var skillsScript = preload("res://scripts/cs/Skills.cs")
		Skills = skillsScript.new()
