tool
extends Node

enum inputActions {
	ui_accept,
	ui_select,
	ui_cancel,
	ui_focus_next,
	ui_focus_prev,
	ui_home,
	toggle_grid,
	zoom_in,
	zoom_out,
	debug_console,
}

const actionToInput := {
	inputActions.ui_accept: {
		"button": JOY_BUTTON_0,
		"key": KEY_Z,
	},
	inputActions.ui_select: {
		"button": JOY_BUTTON_3,
		"key": KEY_C,
	},
	inputActions.ui_cancel: {
		"button": JOY_BUTTON_1,
		"key": KEY_X,
	},
	inputActions.ui_focus_next: {
		"button": JOY_BUTTON_5,
		"key": KEY_TAB,
	},
	inputActions.ui_focus_prev: {
		"button": JOY_BUTTON_4,
		"key": KEY_BACKTAB,
	},
	inputActions.ui_home: {
		"button": JOY_BUTTON_11,
		"key": KEY_SPACE,
	},
	inputActions.toggle_grid: {
		"button": JOY_BUTTON_2,
		"key": KEY_H,
	},
	inputActions.zoom_in: {
		"button": JOY_BUTTON_7,
		"key": null,
	},
	inputActions.debug_console: {
		"button": null,
		"key": KEY_BACKSLASH,
	},
}

const inputToKeyIcon := {
	KEY_Z: null,
	KEY_H: preload("res://sprites/UI/inputIcons/keys/key_h.png"),
	KEY_C: preload("res://sprites/UI/inputIcons/keys/key_c.png"),
	KEY_X: null,
	KEY_TAB: preload("res://sprites/UI/inputIcons/keys/key_tab.png"),
	KEY_BACKTAB: preload("res://sprites/UI/inputIcons/keys/key_backtab.png"),
	KEY_SPACE: preload("res://sprites/UI/inputIcons/keys/key_space.png"),
	KEY_Q: null,
}

const inputToDefaultButtonIcon := {
	JOY_BUTTON_0: preload("res://sprites/UI/inputIcons/default/button_0_default.png"),
	JOY_BUTTON_1: preload("res://sprites/UI/inputIcons/default/button_1_default.png"),
	JOY_BUTTON_2: preload("res://sprites/UI/inputIcons/default/button_2_default.png"),
	JOY_BUTTON_3: preload("res://sprites/UI/inputIcons/default/button_3_default.png"),
	JOY_BUTTON_4: preload("res://sprites/UI/inputIcons/default/button_4_default.png"),
	JOY_BUTTON_5: preload("res://sprites/UI/inputIcons/default/button_5_default.png"),
	JOY_BUTTON_6: preload("res://sprites/UI/inputIcons/default/button_6_default.png"),
	JOY_BUTTON_7: preload("res://sprites/UI/inputIcons/default/button_7_default.png"),
	JOY_BUTTON_11: preload("res://sprites/UI/inputIcons/default/button_11_default.png"),
}

const inputToPsButtonIcon := {
	JOY_BUTTON_0: preload("res://sprites/UI/inputIcons/ps/button_0_ps.png"),
	JOY_BUTTON_1: preload("res://sprites/UI/inputIcons/ps/button_1_ps.png"),
	JOY_BUTTON_2: preload("res://sprites/UI/inputIcons/ps/button_2_ps.png"),
	JOY_BUTTON_3: preload("res://sprites/UI/inputIcons/ps/button_3_ps.png"),
	JOY_BUTTON_4: preload("res://sprites/UI/inputIcons/ps/button_4_ps.png"),
	JOY_BUTTON_5: preload("res://sprites/UI/inputIcons/ps/button_5_ps.png"),
	JOY_BUTTON_6: preload("res://sprites/UI/inputIcons/ps/button_6_ps.png"),
	JOY_BUTTON_7: preload("res://sprites/UI/inputIcons/ps/button_7_ps.png"),
	JOY_BUTTON_11: preload("res://sprites/UI/inputIcons/ps/button_11_ps.png"),
}
