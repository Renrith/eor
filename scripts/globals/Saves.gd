extends Node

const savePath: String = "user://save%s.json"
const autoSavePath = "user://autoSave.json"
const defaultSettingsPath = "user://defaultSettings.json"
const sideConversationsPath = "user://sideConversations.json"

var saveModifiedTime: float = 0

func getMostRecentSavePath() -> String:
	var path := ""
	var time := -1
	var file := File.new()
	
	if hasAutoSavedData():
		path = autoSavePath
		time = file.get_modified_time(autoSavePath)
	
	var manualSaveCount := getManualSaveCount()
	if manualSaveCount > 0:
		for manualSaveIndex in range(0, manualSaveCount):
			var manualSavePath := savePath % manualSaveIndex
			var manualSaveTime := file.get_modified_time(manualSavePath)
			if time < manualSaveTime:
				path = manualSavePath
				time = manualSaveTime
	
	file.close()
	return path

func getManualSaveCount() -> int:
	var manualSaveCount := 0
	var file := File.new()
	while file.file_exists(savePath % (manualSaveCount + 1)):
		manualSaveCount += 1
	file.close()
	return manualSaveCount

func hasAutoSavedData() -> bool:
	return hasSavedDataPath(autoSavePath)

func hasSavedData() -> bool:
	var hasSaved := hasAutoSavedData() || getManualSaveCount() > 0
	return hasSaved

func hasSavedDataPath(saveDataPath) -> bool:
	var file := File.new()
	var hasSaved = file.file_exists(saveDataPath)
	file.close()
	return hasSaved

func getSavedDataPreview(path: String) -> Dictionary:
	var data := Globals.openJSON(path)
	var currentSceneIndex = data.result["currentSceneIndex"]
	var sceneData: Dictionary = GameData.sceneList[currentSceneIndex]
	
	var partIndex: int = sceneData.part
	var chapterIndex: int = sceneData.chapter
	
	var date = ""
	if sceneData.has("date"):
		date = sceneData.date
	else:
		for sceneIndex in range(currentSceneIndex, 0):
			if GameData.sceneList[sceneIndex].has("date"):
				date = GameData.sceneList[sceneIndex].date
	
	var location = ""
	if sceneData.has("location"):
		location = sceneData.location
	else:
		for sceneIndex in range(currentSceneIndex, 0):
			if GameData.sceneList[sceneIndex].has("location"):
				location = GameData.sceneList[sceneIndex].location
	
	var sector = ""
	if sceneData.has("sector"):
		sector = sceneData.sector
	else:
		for sceneIndex in range(currentSceneIndex, 0):
			if GameData.sceneList[sceneIndex].has("sector"):
				sector = GameData.sceneList[sceneIndex].sector
	
	return {
		"part": GameData.partTitles[partIndex],
		"partIndex": partIndex,
		"chapter": GameData.chapterTitles[chapterIndex],
		"chapterIndex": chapterIndex,
		"date": date,
		"location": location,
		"sector": sector,
		"mode": data.result.currentMode,
		"timeElapsed": data.result.timeElapsed,
	}

func loadMostRecentData() -> void:
	var mostRecentPath = Saves.getMostRecentSavePath()
	loadData(mostRecentPath)

func loadData(path: String) -> void:
	var data := Globals.openJSON(path)
	saveModifiedTime = Time.get_unix_time_from_system()
	Game.currentSceneIndex = data.result["currentSceneIndex"]
	Game.currentModeIndex = data.result["currentModeIndex"]
	Game.thoughtsData = data.result["thoughtsData"]
	for actorName in data.result.actorData:
		var actor = data.result.actorData[actorName]
		if actor != null:
			Game.actorData[actorName].buildDataList = actor.buildDataList
			Game.actorData[actorName].currentBuildIndex = actor.currentBuildIndex
			Game.actorData[actorName].buildPoints = actor.buildPoints
			Game.actorData[actorName].skillPoints = actor.skillPoints
			if actor.get("skills"):
				Game.actorData[actorName].skills = actor.skills
	if data.result.has("conditions"):
		Conditions.conditionsList = data.result["conditions"]
	if data.result.has("musicVolume"):
		AudioServer.set_bus_volume_db(Globals.musicBusIndex, linear2db(data.result["musicVolume"]))
		AudioServer.set_bus_volume_db(Globals.SFXBusIndex, linear2db(data.result["SFXVolume"]))
		Game.showStory = data.result["showStory"]
		Game.showSideConversations = data.result["showSideConversations"]
	Game.advanceSetting()

func saveData(path: String) -> void:
	var file := File.new()
	var savedData: Dictionary
	var loadedPath = getMostRecentSavePath()
	if loadedPath != autoSavePath && file.file_exists(loadedPath):
		file.open(loadedPath, File.READ)
		savedData = parse_json(file.get_line())
	file.close()
	file.open(path, File.WRITE)
	
	var Setting := get_tree().get_current_scene()
	var actorData := {}
	if Setting.get("actors"):
		for actor in Setting.actors:
			if Setting.actors[actor] && Setting.actors[actor].get("buildDataList"):
				var skills = []
				for skill in Setting.actors[actor].skillList:
					if Setting.actors[actor].skillList[skill].isAcquired:
						skills.append(skill)
				actorData[actor] = {
					skills = skills,
					buildDataList = Setting.actors[actor].buildDataList,
					currentBuildIndex = Setting.actors[actor].currentBuildIndex,
					buildPoints = Setting.actors[actor].buildPoints,
					skillPoints = Setting.actors[actor].skillPoints,
				}
			else:
				if savedData && savedData.actorData.has(actor):
					actorData[actor] = savedData.actorData[actor]
				else:
					actorData[actor] = null
	
	var currentTime = Time.get_unix_time_from_system()
	var timeElapsed = 0
	if savedData:
		timeElapsed = savedData.timeElapsed
	
	var data := {
		"currentSceneIndex": Game.currentSceneIndex,
		"currentModeIndex": Game.currentModeIndex,
		"currentMode": Game.currentMode,
		"thoughtsData": Game.thoughtsData,
		"actorData": actorData,
		"timeElapsed": timeElapsed + currentTime - saveModifiedTime,
		"conditions": Conditions.conditionsList,
		"showStory": Game.showStory,
		"showSideConversations": Game.showSideConversations,
		"musicVolume": db2linear(AudioServer.get_bus_volume_db(Globals.musicBusIndex)),
		"SFXVolume": db2linear(AudioServer.get_bus_volume_db(Globals.SFXBusIndex)),
	}
	
	saveModifiedTime = currentTime
	
	file.store_line(to_json(data))
	file.close()

func autoSaveData() -> void:
	saveData(autoSavePath)

func loadDefaultSettings() -> void:
	if hasSavedDataPath(defaultSettingsPath):
		var data := Globals.openJSON(defaultSettingsPath)
		AudioServer.set_bus_volume_db(Globals.musicBusIndex, linear2db(data.result["musicVolume"]))
		AudioServer.set_bus_volume_db(Globals.SFXBusIndex, linear2db(data.result["SFXVolume"]))

func saveSettings() -> void:
	autoSaveData()
	saveDefaultSettings()

func saveDefaultSettings() -> void:
	var file := File.new()
	var data := {
		"musicVolume": db2linear(AudioServer.get_bus_volume_db(Globals.musicBusIndex)),
		"SFXVolume": db2linear(AudioServer.get_bus_volume_db(Globals.SFXBusIndex)),
	}
	file.open(defaultSettingsPath, File.WRITE)
	file.store_line(to_json(data))
	file.close()

func loadSideConversationData() -> void:
	if hasSavedDataPath(sideConversationsPath):
		var file := File.new()
		file.open(sideConversationsPath, File.READ)
		var data = parse_json(file.get_line())
		file.close()
		for conversation in GameData.sideConversations:
			GameData.sideConversations[conversation]["unlocked"] = data[conversation]["unlocked"]
			GameData.sideConversations[conversation]["read"] = data[conversation]["read"]

func saveSideConversationData() -> void:
	var file := File.new()
	
	var data := {}
	for conversation in GameData.sideConversations:
		data[conversation] = {
			"unlocked": GameData.sideConversations[conversation]["unlocked"],
			"read": GameData.sideConversations[conversation]["read"],
		}
	
	file.open(sideConversationsPath, File.WRITE)
	file.store_line(to_json(data))
	file.close()

func _ready() -> void:
	loadDefaultSettings()
