extends ViewportContainer

func resize() -> void:
	var newResolution = OS.get_window_size()
	rect_size = newResolution

func _ready() -> void:
	var WorldViewport = get_tree().get_root()
	WorldViewport.connect("size_changed", self, "resize")
	resize()
