using System.Collections.Generic;

public abstract class AICore : OriginalCore, IAI
{
	private Move finalMove;

	//TODO: If closest player is over 10 tiles away, action won't be executed even if it makes sense
	//to do so (such as a rival AI gang)
	public int getDistanceToClosestPlayer()
	{
		Tile tileOrHoldTile = gdGetTileOrHoldTile();
		List<ActorCore> closestPlayerList = Map.getClosestActors(1, tileOrHoldTile, true);

		if(closestPlayerList.Count > 0)
		{
			OriginalCore closestPlayer = (OriginalCore)closestPlayerList[0];
			return Tile.getManhattanDistance(tileOrHoldTile, closestPlayer.gdGetTileOrHoldTile());
		}

		return -1;
	}

	public ActorCore becomePlayer()
	{
		PlayerCore newCore = new PlayerCore();
		newCore.initializeStats(
		_attack, defense, attackType, attackRange,
		_push, steadiness,
		isGangLeader,
		maxEndurance, endurance, maxEnergy, energy,
		movementRange, turnRate, turnOrder,
		gangIndex
		);

		newCore.State = State;
		newCore.OriginalState = OriginalState;
		newCore.InitialState = InitialState;
		newCore.State.Core = newCore;
		newCore.InitialState.Core = newCore;
		newCore.Base = Base;
		newCore.Controller = Controller;

		newCore.attackSkill = attackSkill;
		newCore.pushSkill = pushSkill;
		newCore.actionSkill = actionSkill;

		newCore.actorAlliances = actorAlliances;
		newCore.isGangLeader = isGangLeader;

		newCore.tile = tile;
		newCore.holdTile = holdTile;

		newCore.initialTurnOrder = initialTurnOrder;

		State = null;
		OriginalState = null;
		InitialState = null;

		int initialPlaceablesIndex = Battle.initialPlaceables.FindIndex((placeable) => this == placeable);
		int currentPlaceablesIndex = Battle.currentPlaceables.FindIndex((placeable) => this == placeable);
		int currentActorsIndex = Battle.currentActors.FindIndex((actor) => this == actor);
		Battle.initialPlaceables[initialPlaceablesIndex] = newCore;
		Battle.currentPlaceables[currentPlaceablesIndex] = newCore;
		Battle.currentActors[currentActorsIndex] = newCore;

		string actorName = (string)Base.Get("actorName");
		Battle.playerActors[actorName] = newCore;

		return newCore;
	}

	public Godot.Collections.Dictionary gdGetMove()
	{
		Godot.Collections.Dictionary gdMove = new Godot.Collections.Dictionary();
		if(finalMove.tile != null)
		{
			gdMove.Add("tile", finalMove.tile);
		}
		else
		{
			gdMove.Add("tile", null);
		}
		gdMove.Add("origin", finalMove.origin);

		string action = "";
		switch(finalMove.action)
		{
			case MoveAction.Attack:
				action = "attack";
				break;
			case MoveAction.Climb:
				action = "climb";
				break;
			case MoveAction.Grip:
				action = "grip";
				break;
			case MoveAction.Move:
				action = "move";
				break;
			case MoveAction.NewAction:
				action = "actionSkill";
				break;
			case MoveAction.Push:
				action = "push";
				break;
		}
		gdMove.Add("action", action);

		return gdMove;
	}

	protected abstract float getActorScore(ActorCore actor, List<Evaluation> evaluation);

	protected abstract List<Evaluation> evaluateState(List<ActorCore> currentActors);

	private static void simulateAction(ActorCore actor, Move move)
	{
		MoveAction action = move.action;
		actor.tile = move.origin;
		switch(action)
		{
			case MoveAction.Attack:
				actor.attackTarget(move.tile);
				break;
			case MoveAction.Climb:
				actor.climb();
				break;
			case MoveAction.Grip:
				actor.grip();
				break;
			case MoveAction.Move:
				break;
			case MoveAction.NewAction:
				actor.performAction(actor.actionSkill.calculateOutcome(move.tile, move.origin));
				break;
			case MoveAction.Push:
				actor.pushTarget(move.tile, move.origin);
				break;
		}
	}

	protected virtual void simulateActions(List<ActorCore> turnOrder, List<IPlaceable> currentPlaceables, List<ActorCore> currentActors)
	{
		SimulationNode currentNode = new SimulationNode(null);
		List<ActorState> allActorStates = new List<ActorState>();
		foreach(IPlaceable placeable in currentPlaceables)
		{
			if(placeable.isAlive())
			{
				ActorState actorState = placeable.State;
				allActorStates.Add(actorState);
			}
		}
		currentNode.setStates(allActorStates);
		do
		{
			Map.wipeTiles(WipeTilesMode.Simulation);
			currentNode = new SimulationNode(currentNode);
			currentNode.setStates(currentNode.parent.states);
			ActorCore actor = turnOrder[currentNode.level];
			if(actor.isActive())
			{
				/* Generate possible moves */
				if(currentNode.parent.children.Count == 0)
				{
					actor.startTurn();
					Map.calculateMovementRange(actor, false);
					List<ActorCore>[] alliesAndTargetsLists = actor.getAlliesAndTargetsInRange();
					List<ActorCore> allies = alliesAndTargetsLists[0];
					List<ActorCore> targets = alliesAndTargetsLists[1];
					foreach(ActorCore target in targets)
					{
						AllActions actions = actor.getActions(target);
						List<Tile> moveTiles = Map.getAdjacentTilesInRange(target.tile, actor);
						foreach(Tile tile in moveTiles)
						{
							if((actions & AllActions.attack) != 0)
							{
								Move move = new Move(target.tile, tile, MoveAction.Attack);
								currentNode.parent.moves.Add(move);
							}
							if((actions & AllActions.push) != 0)
							{
								Move move = new Move(target.tile, tile, MoveAction.Push);
								currentNode.parent.moves.Add(move);
							}
							if(actor.actionSkill != null && (actor.actionSkill.targetableActorType & Factions.getTargetAlliance(actor, target)) != 0 && actor.actionSkill.targetingType == TargetingType.Neighbour)
							{
								Move move = new Move(target.tile, tile, MoveAction.NewAction);
								currentNode.parent.moves.Add(move);
							}
						}
					}
					switch(actor.attackType)
					{
						case TargetingType.RangedSkip:
							List<ActorCore> potentialAttackTargets = actor.getRangedSkipTargetsInRange();
							foreach(ActorCore target in potentialAttackTargets)
							{
								List<Tile> rangedSkipTiles = Map.getRangedSkipTiles(target.tile, actor.attackRange);
								foreach(Tile tile in rangedSkipTiles)
								{
									Move move = new Move(target.tile, tile, MoveAction.Attack);
									currentNode.parent.moves.Add(move);
								}
							}
							break;
						case TargetingType.RangedLine:
							List<ActorCore> rangedLineTargets = actor.getTargetsInLine();
							foreach(ActorCore target in rangedLineTargets)
							{
								Move move = new Move(target.tile, actor.tile, MoveAction.Attack);
								currentNode.parent.moves.Add(move);
							}
							break;
						case TargetingType.Slash:
							List<KeyValuePair<Tile, Tile>> slashAttackTilePairs = actor.getSlashTilePairsInRange();
							foreach(KeyValuePair<Tile, Tile> entry in slashAttackTilePairs)
							{
								Move move = new Move(entry.Key, entry.Value, MoveAction.Attack);
								currentNode.parent.moves.Add(move);
							}
							break;
					}
					if(actor.actionSkill != null)
					{
						if((actor.actionSkill.targetableActorType & TargetAlliance.NonEnemy) != 0)
						{
							switch(actor.actionSkill.targetingType)
							{
								case TargetingType.Neighbour:
									foreach(ActorCore ally in allies)
									{
										List<Tile> moveTiles = Map.getAdjacentTilesInRange(ally.tile, actor);
										foreach(Tile tile in moveTiles)
										{
											Move move = new Move(ally.tile, tile, MoveAction.NewAction);
											currentNode.parent.moves.Add(move);
										}
									}
									break;
								case TargetingType.RangedSkip:
									List<ActorCore> rangedSkipAllies = actor.getRangedSkipAlliesInRange();
									foreach(ActorCore ally in rangedSkipAllies)
									{
										List<Tile> rangedSkipMoveTiles = Map.getRangedSkipTiles(ally.tile, actionSkill.range);
										foreach(Tile moveTile in rangedSkipMoveTiles)
										{
											Move move = new Move(ally.tile, moveTile, MoveAction.NewAction);
											currentNode.parent.moves.Add(move);
										}
									}
									break;
							}
						}
					}
					List<ActorCore> holdTargets = actor.getHoldTargetsInRange();
					foreach(ActorCore holdTarget in holdTargets)
					{
						Move move = new Move(holdTarget.holdTile, holdTarget.holdTile, MoveAction.Push);
						currentNode.parent.moves.Add(move);
					}
					/* No targets nor hold tiles in range, or action skill targets self */
					if(currentNode.parent.moves.Count == 0 || (actor.actionSkill != null && actor.actionSkill.targetableActorType == TargetAlliance.Self))
					{
						targets = actor.getTargets(currentActors);
						foreach(ActorCore target in targets)
						{
							if(target.isActive())
							{
								Map.calculateInfluenceRange(target.tile, actor);
							}
							else
							{
								Map.calculateInfluenceRange(target.holdTile, actor);
							}
						}
						List<Tile> unoccupiedTilesInRange = Map.getUnoccupiedTilesInRange();
						Tile destination = actor.tile;
						foreach(Tile tile in unoccupiedTilesInRange)
						{
							if(tile.cumulativeScore != -1)
							{
								int distanceScore = tile.minBorderDistance;
								if(distanceScore > 4)
								{
									distanceScore = 4;
								}
								tile.cumulativeScore += distanceScore;
								if(tile.cumulativeScore > destination.cumulativeScore)
								{
									destination = tile;
								}
							}
						}
						if(destination != actor.tile)
						{
							if(currentNode.parent.moves.Count == 0)
							{
								Move move = new Move(actor.tile, destination, MoveAction.Move);
								currentNode.parent.moves.Add(move);
							}
							if(actor.actionSkill != null && actor.actionSkill.targetableActorType == TargetAlliance.Self)
							{
								switch(actor.actionSkill.targetingType)
								{
									case TargetingType.Neighbour:
										foreach(Tile targetableTile in destination.neighbours.Values)
										{
											Move neighbourMove = new Move(targetableTile, destination, MoveAction.NewAction);
											currentNode.parent.moves.Add(neighbourMove);
										}
										break;
									case TargetingType.Self:
										Move selfMove = new Move(destination, destination, MoveAction.NewAction);
										currentNode.parent.moves.Add(selfMove);
										break;
								}
							}
						}
					}
					if(actor.energy > 0)
					{
						List<Tile> gripTiles = Map.getGripTilesInRange();
						foreach(Tile gripTile in gripTiles)
						{
							List<Tile> tiles = Map.getAdjacentTilesInRange(gripTile, actor);
							foreach(Tile tile in tiles)
							{
								Move move = new Move(actor.tile, tile, MoveAction.Grip);
								currentNode.parent.moves.Add(move);
							}
						}
					}
				}
			}
			/* Holding actor move */
			else if(actor.isAlive())
			{
				actor.startTurn();
				if(actor.isAlive() && actor.holdTile.occupant == null)
				{
					Move move = new Move(actor.holdTile, actor.holdTile, MoveAction.Climb);
					currentNode.parent.moves.Add(move);
				}
			}
			/* No move generated */
			if(currentNode.parent.moves.Count == 0)
			{
				if(currentNode.level != 0)
				{
					Move move = new Move(actor.getTileOrHoldTile(), actor.getTileOrHoldTile(), MoveAction.None);
					currentNode.parent.moves.Add(move);
				}
				else
				{
					return;
				}

			}
			/* Action move */
			while(currentNode.parent.moves.Count > 0)
			{
				Move currentMove = currentNode.parent.moves[currentNode.parent.moves.Count - 1];
				currentNode.parent.moves.RemoveAt(currentNode.parent.moves.Count - 1);
				simulateAction(actor, currentMove);
				currentNode.move = currentMove;
				if(currentNode.level >= turnOrder.Count - 1)
				{
					currentNode.evaluation = evaluateState(currentActors);
					currentNode.score = getActorScore(actor, currentNode.evaluation);
				}
				currentNode.parent.children.Add(currentNode);
				if(currentNode.parent.moves.Count > 0)
				{
					currentNode = new SimulationNode(currentNode.parent);
					currentNode.setStates(currentNode.parent.states);
				}
			}
			/* Evaluation */
			if(currentNode.level >= turnOrder.Count - 1)
			{
				ActorCore currentActor = turnOrder[currentNode.level];
				while(currentNode.parent.children.Count > 0)
				{
					currentNode = currentNode.parent.children[currentNode.parent.children.Count - 1];
					currentNode.score = getActorScore(currentActor, currentNode.evaluation);
					if(currentNode.score > currentNode.parent.childrenScore)
					{
						currentNode.parent.setEvaluation(currentNode);
						if(currentNode.level == 0)
						{
							finalMove = currentNode.move;
						}
					}
					currentNode.parent.children.RemoveAt(currentNode.parent.children.Count - 1);
				}

				SimulationNode bubblingNode = currentNode.parent;

				while(bubblingNode.level > -1)
				{
					ActorCore bubblingActor = turnOrder[bubblingNode.level];
					bubblingNode.score = getActorScore(bubblingActor, bubblingNode.evaluation);
					if(bubblingNode.score > bubblingNode.parent.childrenScore)
					{
						bubblingNode.parent.setEvaluation(bubblingNode);
						if(bubblingNode.level == 0)
						{
							finalMove = bubblingNode.move;
						}
					}
					else if(bubblingNode.level == 0 && bubblingNode.score == bubblingNode.parent.childrenScore && bubblingNode.score > SimulationNode.MIN_SCORE)
					{
						bubblingNode.setStates(bubblingNode.states);
						List<Evaluation> newEvaluation = evaluateState(currentActors);
						float newScore = getActorScore(bubblingActor, newEvaluation);
						bubblingNode.parent.setStates(bubblingNode.selectedChild.states);
						List<Evaluation> oldEvaluation = evaluateState(currentActors);
						float oldScore = getActorScore(bubblingActor, oldEvaluation);
						if(newScore > oldScore)
						{
							bubblingNode.parent.setEvaluation(bubblingNode);
							finalMove = bubblingNode.move;
						}
					}
					else
					{
						break;
					}
					bubblingNode = bubblingNode.parent;
				}

				while(currentNode.children.Count == 0 && currentNode.level > -1)
				{
					currentNode.parent.children.Remove(currentNode);
					currentNode = currentNode.parent;
				}
				if(currentNode.children.Count > 0)
				{
					currentNode = currentNode.children[0];
				}
			}
		}
		while(currentNode.level > -1);
	}

	public virtual void gdCalculateMove()
	{
		Tile tileOrHoldTile = getTileOrHoldTile();
		finalMove = new Move(tileOrHoldTile, tileOrHoldTile, MoveAction.None);

		List<ActorCore> turnOrder = Battle.getClosestActorsTurnOrder(4, tileOrHoldTile);

		ActorCore.simulation = true;
		System.Threading.Thread simulationThread = new System.Threading.Thread(new System.Threading.ThreadStart(() => startSimulation(turnOrder)));
		simulationThread.Priority = System.Threading.ThreadPriority.AboveNormal;
		simulationThread.Start();
	}

	protected void startSimulation(List<ActorCore> turnOrder)
	{
		List<IPlaceable> currentPlaceables = new List<IPlaceable>(Battle.currentPlaceables);
		List<ActorCore> currentActors = new List<ActorCore>(Battle.currentActors);

		simulateActions(turnOrder, currentPlaceables, currentActors);
		clearSimulation();

		ActorCore.simulation = false;
	}

	private void clearSimulation()
	{
		Map.wipeTiles(WipeTilesMode.Simulation);

		foreach(IPlaceable placeable in Battle.currentPlaceables)
		{
			placeable.State = placeable.OriginalState;
			if(placeable.tile != null)
			{
				placeable.tile.occupant = placeable;
			}
			else
			{
				if(placeable is ActorCore)
				{
					ActorCore actor = (ActorCore)placeable;
					if(actor.holdTile != null)
					{
						actor.holdTile.holdOccupant = actor;
					}
				}
			}
		}
	}

	public override void initializeStats
	(
		int _attack, int _defense, TargetingType _attackType, int _attackRange,
		int _push, int _steadiness,
		bool _isGangLeader,
		int _maxEndurance, int _endurance, int _maxEnergy, int _energy,
		int _movementRange, int _turnRate, int _turnOrder,
		int _gangIndex
	)
	{
		base.initializeStats(_attack, _defense, _attackType, _attackRange, _push, _steadiness,
			_isGangLeader, _maxEndurance, _endurance, _maxEnergy, _energy,
			_movementRange, _turnRate, _turnOrder, _gangIndex);

		finalMove = new Move(getTileOrHoldTile(), getTileOrHoldTile(), MoveAction.None);
	}

	protected AICore()
	{
		isPlayer = false;
	}
}
