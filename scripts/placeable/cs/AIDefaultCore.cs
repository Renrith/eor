using System.Collections.Generic;

public class AIDefaultCore : AICore
{
	private const int ALLY_MULTIPLIER = 1;
	private const int GANG_TARGET_MULTIPLIER = -20;
	private const int ENEMY_MULTIPLIER = -1;
	private const float RIVAL_MULTIPLIER = -0.3F;

	protected override float getActorScore(ActorCore actor, List<Evaluation> evaluation)
	{
		float score = 0F;
		foreach(Evaluation actorEvaluation in evaluation)
		{
			TargetAlliance targetAlliance = Factions.getTargetAlliance(actor, actorEvaluation.actor);
			if((targetAlliance & TargetAlliance.NonEnemy) != 0)
			{
				score += actorEvaluation.score * ALLY_MULTIPLIER;
			}
			else if(actor.gang.target == actorEvaluation.actor)
			{
				score += actorEvaluation.score * GANG_TARGET_MULTIPLIER;
			}
			else if(targetAlliance == TargetAlliance.Enemy)
			{
				score += actorEvaluation.score * ENEMY_MULTIPLIER;
			}
			else if((targetAlliance & TargetAlliance.NonAlly) != 0)
			{
				score += actorEvaluation.score * RIVAL_MULTIPLIER;
			}
		}
		return score;
	}

	protected override List<Evaluation> evaluateState(List<ActorCore> currentActors)
	{
		List<Evaluation> actorScores = new List<Evaluation>();
		foreach(ActorCore actor in currentActors)
		{
			if(actor.isActive())
			{
				Evaluation evaluation = new Evaluation();
				float enduranceScore = actor.endurance / 2;
				float tileScore = (float)System.Math.Pow(actor.tile.minBorderDistance + 1, 1.4);
				if(enduranceScore < tileScore)
				{
					evaluation.score = enduranceScore;
				}
				else
				{
					evaluation.score = tileScore + (float)(enduranceScore * 0.2);
				}
				evaluation.score += 10;
				evaluation.actor = actor;
				actorScores.Add(evaluation);
			}
			else if(actor.isAlive())
			{
				Evaluation evaluation = new Evaluation();
				evaluation.score += 5;
				evaluation.actor = actor;
				actorScores.Add(evaluation);
			}
		}
		return actorScores;
	}
}
