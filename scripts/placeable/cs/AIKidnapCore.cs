using System;
using System.Collections.Generic;

public class AIKidnapCore : AICore, IAI
{
	private const int ALLY_MULTIPLIER = 1;
	private const int GANG_TARGET_MULTIPLIER = -20;
	private const int ENEMY_MULTIPLIER = -1;
	private const float RIVAL_MULTIPLIER = -0.3F;

	protected override float getActorScore(ActorCore actor, List<Evaluation> evaluation)
	{
		// No evaluations are generated if kidnap target is not alive.
		// Kidnap target not being alive is a negative for all factions. So we'll always return a very low score.
		float score = 0F;

		if(evaluation.Count > 0)
		{
			foreach(Evaluation actorEvaluation in evaluation)
			{
				AllianceType allianceType = actor.getAlliance(actorEvaluation.actor);
				switch(allianceType)
				{
					case AllianceType.Ally:
						score += actorEvaluation.score * ALLY_MULTIPLIER;
						break;
					case AllianceType.Enemy:
						score += actorEvaluation.score * ENEMY_MULTIPLIER;
						break;
					case AllianceType.Rival:
						score += actorEvaluation.score * RIVAL_MULTIPLIER;
						break;
				}
			}
		}
		else
		{
			return SimulationNode.MIN_SCORE;
		}

		return score;
	}

	protected override List<Evaluation> evaluateState(List<ActorCore> currentActors)
	{
		List<Evaluation> actorScores = new List<Evaluation>();
		if(Battle.kidnapTarget.isAlive())
		{
			foreach(ActorCore actor in currentActors)
			{
				if(actor.isAlive())
				{
					if(actor == Battle.kidnapTarget)
					{
						Evaluation evaluation = new Evaluation();
						int newAreaDistance = Map.getPathBetweenPoints(actor.tile, Battle.kidnapTile, Battle.kidnapTarget).Count;
						int areaDistanceChange = newAreaDistance - actor.gdGetTileOrHoldTile().areaDistance;
						evaluation.score = areaDistanceChange * 100;
						evaluation.actor = actor;
						actorScores.Add(evaluation);
					}
					else
					{
						if(actor.isActive())
						{
							Evaluation evaluation = new Evaluation();
							float enduranceScore = actor.endurance / 2;
							float tileScore = (float)System.Math.Pow(actor.tile.minBorderDistance + 1, 1.4);
							if(enduranceScore < tileScore)
							{
								evaluation.score = enduranceScore;
							}
							else
							{
								evaluation.score = tileScore + (float)(enduranceScore * 0.2);
							}
							if(Battle.kidnapTarget.isAlive())
							{
								int distanceToKidnapTarget = Map.getPathBetweenPoints(actor.tile, Battle.kidnapTarget.tile, actor).Count;
								evaluation.score += Math.Max(10 - distanceToKidnapTarget, 0);
							}
							evaluation.score += 10;
							evaluation.actor = actor;
							actorScores.Add(evaluation);
						}
						else if(actor.isAlive())
						{
							Evaluation evaluation = new Evaluation();
							evaluation.score += 5;
							evaluation.actor = actor;
							actorScores.Add(evaluation);
						}
					}
				}
			}
		}
		return actorScores;
	}

	protected override void simulateActions(List<ActorCore> turnOrder, List<IPlaceable> currentPlaceables, List<ActorCore> currentActors)
	{
		foreach(Tile tile in Map.customMap.Values)
		{
			tile.areaDistance = Map.getPathBetweenPoints(tile, Battle.kidnapTile, Battle.kidnapTarget).Count;
		}
		base.simulateActions(turnOrder, currentPlaceables, currentActors);
		foreach(Tile tile in Map.customMap.Values)
		{
			tile.areaDistance = Tile.MAX_DISTANCE;
		}
	}
}
