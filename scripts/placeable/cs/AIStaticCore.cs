public class AIStaticCore : OriginalCore, IAI
{
	public int getDistanceToClosestPlayer()
	{
		return -1;
	}

	public Godot.Collections.Dictionary gdGetMove()
	{
		Godot.Collections.Dictionary gdMove = new Godot.Collections.Dictionary();
		gdMove.Add("tile", tile);
		gdMove.Add("origin", tile);
		gdMove.Add("action", null);
		return gdMove;
	}

	public void gdCalculateMove() { }

	private AIStaticCore()
	{
		isPlayer = false;
	}
}
