using System.Collections.Generic;

public class AITriggerableCore : AIDefaultCore
{
	public bool hasBeenTriggered = false;

	public override void gdCalculateMove()
	{
		if(hasBeenTriggered)
		{
			base.gdCalculateMove();
		}
	}

	public override void setBattle(Battle _Battle)
	{
		base.setBattle(_Battle);
		Battle.triggerableActors.Add(this);
	}

	public override void _Ready()
	{
		base._Ready();
	}
}
