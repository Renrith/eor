using Godot;
using System;
using System.Collections.Generic;

public class ActorCore : PlaceableCore
{
	public static Map Map;

	protected const int fallDamageMultiplier = 2;

	static Random rng = new Random();

	public Node Controller;

	public int movementRange = 4;
	public bool isPlayer;

	public static bool simulation = false;

	/* Push */
	public int _push = 0;
	public int push
	{
		get
		{
			int cumulativePush = _push;
			foreach(Buff pushBuff in State.pushBuffs)
			{
				cumulativePush += pushBuff.change;
			}
			return cumulativePush;
		}
		set { _push = value; }
	}

	/* Attack */
	public int _attack = 0;
	public int attack
	{
		get
		{
			int cumulativeAttack = _attack;
			foreach(Buff attackBuff in State.attackBuffs)
			{
				cumulativeAttack += attackBuff.change;
			}
			return cumulativeAttack;
		}
		set { _attack = value; }
	}

	public int defense = 0;
	public TargetingType attackType = TargetingType.Neighbour;
	public int attackRange = 1;

	public Tile holdTile
	{
		get { return State.holdTile; }
		set
		{
			if(State.holdTile != null)
			{
				State.holdTile.holdOccupant = null;
			}
			State.holdTile = value;
			if(value != null)
			{
				value.holdOccupant = this;
				if(tile != null)
				{
					tile.occupant = null;
				}
			}
		}
	}
	public new bool isActor = true;

	/* Endurance */
	public int maxEndurance = 10;
	public int endurance
	{
		get { return State.endurance; }
		set { State.endurance = Math.Min(value, maxEndurance); }
	}
	public string qualitativeEndurance
	{
		get
		{
			float enduranceThreshold = (float)State.endurance / (float)maxEndurance;
			if(enduranceThreshold >= 0.5)
			{
				return "great";
			}
			else if(enduranceThreshold > 0)
			{
				return "medium";
			}
			else
			{
				return "KO";
			}
		}
	}

	public int maxEnergy = 15;
	public new int energy
	{
		get { return State.energy; }
		set
		{
			if(value > maxEnergy)
			{
				State.energy = maxEnergy;
			}
			else if(value <= 0)
			{
				State.energy = 0;
				grasping = false;
			}
			else
			{
				State.energy = value;
			}
		}
	}

	public Gang gang
	{
		get { return State.gang; }
		set { State.gang = value; }
	}
	public bool isGangLeader
	{
		get { return State.isGangLeader; }
		set { State.isGangLeader = value; }
	}
	public Dictionary<Gang, AllianceType> actorAlliances;

	public int turnOrder = 3;
	public int initialTurnOrder = 3;
	public int simulatedTurnOrder = 3;
	public int turnRate = 3;

	public List<AttributeModifier> attributeSkills = new List<AttributeModifier>();

	public IActionModifier attackSkill;
	public IActionModifier pushSkill;

	public bool hasAttackSkill
	{
		get { return attackSkill != null; }
	}
	public bool hasPushSkill
	{
		get { return pushSkill != null; }
	}

	public string attackSkillIcon
	{
		get { return attackSkill.icon; }
	}
	public string pushSkillIcon
	{
		get { return pushSkill.icon; }
	}

	public INewAction actionSkill;

	public List<ActionOutcome> lastActionOutcome = null;

	public void startTurn()
	{
		grasping = false;
		if(holdTile != null)
		{
			energy -= 5;
			if(energy == 0)
			{
				holdTile.holdOccupant = null;
				holdTile = null;
			}
		}
	}

	public new bool isAlive()
	{
		if(State != null && (tile != null || holdTile != null) && endurance > 0)
		{
			return true;
		}

		return false;
	}

	public bool isActive()
	{
		if(State != null && tile != null && endurance > 0)
		{
			return true;
		}

		return false;
	}

	public AllianceType getAlliance(Gang targetGang)
	{
		AllianceType alliance = AllianceType.None;
		if(actorAlliances.TryGetValue(targetGang, out alliance))
		{
			return alliance;
		}

		return Factions.getAlliance(gang, targetGang);
	}

	public AllianceType getPersonalAlliance(ActorCore actor)
	{
		if(actor == this)
		{
			return AllianceType.Ally;
		}

		AllianceType selfAlliance = AllianceType.None;
		AllianceType actorAlliance = AllianceType.None;
		if(actorAlliances.TryGetValue(actor.gang, out selfAlliance))
		{
			if(actor.actorAlliances.TryGetValue(gang, out actorAlliance))
			{
				if(selfAlliance < actorAlliance)
				{
					return selfAlliance;
				}
				else
				{
					return actorAlliance;
				}
			}
			else
			{
				return selfAlliance;
			}
		}
		else if(actor.actorAlliances.TryGetValue(gang, out actorAlliance))
		{
			return actorAlliance;
		}

		return AllianceType.Neutral;
	}

	public AllianceType getAlliance(ActorCore actor)
	{
		AllianceType alliance = getPersonalAlliance(actor);
		if(alliance == AllianceType.Neutral)
		{
			alliance = Factions.getAlliance(gang, actor.gang);
		}
		return alliance;
	}

	public new Tile getTileOrHoldTile()
	{
		if(tile == null)
		{
			return holdTile;
		}
		return tile;
	}

	public new Tile gdGetTileOrHoldTile()
	{
		if(OriginalState.tile == null)
		{
			return OriginalState.holdTile;
		}
		return OriginalState.tile;
	}

	public virtual void climb()
	{
		tile = holdTile;
		tile.occupant = this;
		holdTile.holdOccupant = null;
		holdTile = null;
		lastActionOutcome = null;
	}

	public virtual void grip()
	{
		grasping = true;
		lastActionOutcome = null;
	}

	public Direction getPushDirection(Tile targetTile)
	{
		Direction pushDirection = Direction.Below;
		if(tile != targetTile)
		{
			pushDirection = tile.getNeighbourDirection(targetTile);
		}
		return pushDirection;
	}

	public string gdGetPushDirection(Tile targetTile)
	{
		Direction pushDirection = getPushDirection(targetTile);
		return DirectionHelper.gdDirectionsInverse[pushDirection];
	}

	public int getPushStrength(int targetSteadiness)
	{
		int pushStrength = push - targetSteadiness;
		if(pushStrength < 0)
		{
			pushStrength = 0;
		}
		return pushStrength;
	}

	public void pushTarget(Tile tile, Tile origin)
	{
		Direction pushDirection = getPushDirection(tile);
		IPlaceable target = tile.occupant;
		if(pushDirection == Direction.Below)
		{
			target = tile.holdOccupant;
		}
		List<ActionOutcome> pushOutcome = Map.getPushPath(this, target, pushDirection);
		if(pushSkill != null && pushSkill.phase == ActionPhase.Act)
		{
			pushOutcome = pushSkill.calculateModifierOutcome(pushOutcome, tile, this);
		}
		performAction(pushOutcome);
	}

	public int getAttackStrength(ActorCore target)
	{
		return Math.Max(attack - target.defense, 0);
	}

	public List<ActionOutcome> calculateAttackOutcome(Tile targetTile)
	{
		List<ActionOutcome> outcomeList = new List<ActionOutcome>();

		if(targetTile != null && targetTile.occupant is ActorCore)
		{
			ActorCore target = (ActorCore)targetTile.occupant;
			int attackStrength = getAttackStrength(target);
			ActionOutcome outcome = new ActionOutcome(target, -attackStrength);
			outcomeList.Add(outcome);
		}

		if(attackSkill != null && attackSkill.phase == ActionPhase.Act)
		{
			outcomeList = attackSkill.calculateModifierOutcome(outcomeList, targetTile, this);
		}

		List<ActorCore> targets = new List<ActorCore>();
		foreach(ActionOutcome outcome in outcomeList)
		{
			if(outcome.target != null)
			{
				targets.Add((ActorCore)outcome.target);
			}
		}
		foreach(ActorCore target in targets)
		{
			if(target.attackSkill != null && target.attackSkill.phase == ActionPhase.Defend)
			{
				outcomeList = target.attackSkill.calculateModifierOutcome(outcomeList, tile, target);
			}
		}

		return outcomeList;
	}

	public void attackTarget(Tile tile)
	{
		performAction(calculateAttackOutcome(tile));
	}

	public virtual void performAction(List<ActionOutcome> outcomeDataList)
	{
		lastActionOutcome = outcomeDataList;
		foreach(ActionOutcome outcome in outcomeDataList)
		{
			Tile prevTile = outcome.target.tile;

			if(outcome.path != null && outcome.path.Count > 0)
			{
				outcome.target.tile = outcome.path[outcome.path.Count - 1];
			}

			if(outcome.target is ActorCore)
			{
				ActorCore target = (ActorCore)outcome.target;
				AllianceType allianceType = getAlliance(target);

				target.endurance += outcome.enduranceChange;
				target.endurance -= outcome.heightLevelChange * fallDamageMultiplier;
				target.energy += outcome.energyChange;

				if(!simulation)
				{
					target.turnOrder += outcome.turnOrderChange;
				}

				target.holdTile = outcome.holdTile;

				if(outcome.gang != null)
				{
					target.gang = outcome.gang;

					if(target.isGangLeader)
					{
						if(!simulation)
						{
							AnimatedSprite sprite = (AnimatedSprite)target.Base.Get("Sprite");
							Sprite GangLeaderIndicator = (Sprite)sprite.Get("GangLeaderIndicator");
							GangLeaderIndicator.Visible = false;
						}
						foreach(ActorCore actor in Battle.currentActors)
						{
							if(actor.gang == target.gang)
							{
								// Spend a bracelet
							}
						}
					}
				}

				if(outcome.attackBuffsApplied != null)
				{
					target.State.attackBuffs.AddRange(outcome.attackBuffsApplied);
				}
				if(outcome.attackBuffsRemoved != null)
				{
					foreach(Buff attackBuff in outcome.attackBuffsRemoved)
					{
						target.State.attackBuffs.Remove(attackBuff);
					}
				}
				if(outcome.pushBuffsApplied != null)
				{
					target.State.pushBuffs.AddRange(outcome.pushBuffsApplied);
				}
				if(outcome.pushBuffsRemoved != null)
				{
					foreach(Buff pushBuff in outcome.pushBuffsRemoved)
					{
						target.State.pushBuffs.Remove(pushBuff);
					}
				}
			}
		}
	}

	public AllActions getActions(ActorCore target)
	{
		AllActions action = AllActions.none;
		if(getAlliance(target) != AllianceType.Ally)
		{
			switch(attackType)
			{
				case TargetingType.Neighbour:
					action = AllActions.attack;
					break;
				case TargetingType.RangedSkip:
					action = AllActions.rangedSkipAttack;
					break;
				case TargetingType.RangedLine:
					action = AllActions.rangedLineAttack;
					break;
				case TargetingType.Slash:
					action = AllActions.slashAttack;
					break;
			}
		}
		action = action | AllActions.push;
		return action;
	}

	private List<ActorCore>[] getAlliesAndTargets(List<ActorCore> actors)
	{
		List<ActorCore> allies = new List<ActorCore>();
		List<ActorCore> targets = new List<ActorCore>();
		List<ActorCore>[] alliesAndTargets = new List<ActorCore>[] { allies, targets };
		foreach(ActorCore actor in actors)
		{
			if(actor.isAlive() && getAlliance(actor) != AllianceType.Ally)
			{
				targets.Add(actor);
			}
			else
			{
				allies.Add(actor);
			}
		}
		return alliesAndTargets;
	}

	public List<ActorCore> getAllies(List<ActorCore> actors)
	{
		List<ActorCore> allies = new List<ActorCore>();
		foreach(ActorCore actor in actors)
		{
			if(actor.isAlive() && getAlliance(actor) == AllianceType.Ally)
			{
				allies.Add(actor);
			}
		}
		return allies;
	}

	public List<ActorCore> getTargets(List<ActorCore> actors)
	{
		List<ActorCore> targets = new List<ActorCore>();
		foreach(ActorCore target in actors)
		{
			if(target.isAlive() && getAlliance(target) != AllianceType.Ally)
			{
				targets.Add(target);
			}
		}
		return targets;
	}

	private List<KeyValuePair<Tile, Tile>> getSlashTargets(List<KeyValuePair<Tile, Tile>> tilePairsInRange)
	{
		List<KeyValuePair<Tile, Tile>> tilePairsWithTargets = new List<KeyValuePair<Tile, Tile>>();
		foreach(KeyValuePair<Tile, Tile> tilePair in tilePairsInRange)
		{
			List<ActorCore> targets = Map.getActorsInSlash(tilePair.Value, tilePair.Key);
			if(targets.Count > 0)
			{
				tilePairsWithTargets.Add(tilePair);
			}
		}
		return tilePairsWithTargets;
	}

	public List<ActorCore>[] getAlliesAndTargetsInRange()
	{
		List<ActorCore> actorsInRange = Map.getActorsInRange(tile);
		return getAlliesAndTargets(actorsInRange);
	}

	public List<ActorCore> getHoldTargetsInRange()
	{
		List<ActorCore> holdActorsInRange = Map.getHoldActorsInRange(this);
		return getTargets(holdActorsInRange);
	}

	public List<KeyValuePair<Tile, Tile>> getSlashTilePairsInRange()
	{
		List<KeyValuePair<Tile, Tile>> tilePairsInRange = Map.getSlashTilePairsInRange(this);

		return getSlashTargets(tilePairsInRange);
	}

	public List<ActorCore> getRangedSkipAlliesInRange()
	{
		List<ActorCore> rangedSkipActors = Map.getRangedSkipActorsInRange(tile, movementRange, attackRange);
		return getAllies(rangedSkipActors);
	}

	public List<ActorCore> getRangedSkipTargetsInRange()
	{
		List<ActorCore> rangedSkipActors = Map.getRangedSkipActorsInRange(tile, movementRange, attackRange);
		return getTargets(rangedSkipActors);
	}

	public List<ActorCore> getTargetsInLine()
	{
		List<ActorCore> rangedLineTargets = Map.getRangedLineActorsInRange(tile, attackRange);
		return getTargets(rangedLineTargets);
	}

	public bool isActorAttackAdjacent(ActorCore actor)
	{
		switch(actor.attackType)
		{
			case TargetingType.Neighbour:
				if(Tile.hasTargetableOccupant(tile, actor.tile) && Map.areNeighbours(tile, actor.tile))
				{
					return true;
				}
				return false;
			case TargetingType.RangedSkip:
				if(Tile.isWithinReach(tile, actor.tile) && Math.Abs(tile.x - actor.tile.x) <= attackRange && Math.Abs(tile.y - actor.tile.y) <= attackRange)
				{
					return true;
				}
				return false;
			case TargetingType.RangedLine:
				if(tile.x == actor.tile.x || tile.y == actor.tile.y)
				{
					List<ActorCore> actorsInLine = Map.getRangedLineActorsInRange(tile, attackRange);
					foreach(ActorCore target in actorsInLine)
					{
						if(target == actor)
						{
							return true;
						}
					}
				}
				return false;
			case TargetingType.Slash:
				foreach(Tile neighbour in tile.neighbours.Values)
				{
					if(neighbour != null)
					{
						List<ActorCore> directionalActorsInSlash = Map.getActorsInSlash(tile, neighbour);
						foreach(ActorCore directionalActor in directionalActorsInSlash)
						{
							if(actor == directionalActor)
							{
								return true;
							}
						}
					}
				}
				return false;
		}
		return false;
	}

	public virtual void initializeStats
	(
		int _attack, int _defense, TargetingType _attackType, int _attackRange,
		int _push, int _steadiness, bool _isGangLeader,
		int _maxEndurance, int _endurance, int _maxEnergy, int _energy,
		int _movementRange, int _turnRate, int _turnOrder,
		int _gangIndex
	)
	{
		maxEndurance = _maxEndurance;
		maxEnergy = _maxEnergy;
		attack = _attack;
		defense = _defense;
		attackType = _attackType;
		attackRange = _attackRange;
		push = _push;
		steadiness = _steadiness;

		InitialState = new ActorState(this, _maxEndurance, _maxEnergy);
		setInitialState();

		isGangLeader = _isGangLeader;
		movementRange = _movementRange;
		turnOrder = _turnOrder;
		initialTurnOrder = _turnOrder;
		simulatedTurnOrder = _turnOrder;
		turnRate = _turnRate;

		Factions.setFaction(this, _gangIndex);

		actorAlliances = new Dictionary<Gang, AllianceType>();
	}

	public override void _Ready()
	{
		base._Ready();
		Map = (Map)GetNode("/root/Viewports/SettingViewport/Map");
	}
}
