using System.Collections.Generic;

public class ActorState
{
	public IPlaceable Core;

	public Tile tile = null;
	public Tile holdTile = null;

	public int endurance = 0;
	public int energy = 0;
	public bool grasping = false;

	public Gang gang;
	public bool isGangLeader = false;

	public List<Buff> attackBuffs;
	public List<Buff> pushBuffs;

	public ActorState Clone()
	{
		ActorState copy = new ActorState();
		copy.Core = Core;
		copy.tile = tile;
		copy.holdTile = holdTile;
		copy.endurance = endurance;
		copy.energy = energy;
		copy.grasping = grasping;
		copy.gang = gang;
		copy.isGangLeader = isGangLeader;
		copy.attackBuffs = new List<Buff>(attackBuffs);
		copy.pushBuffs = new List<Buff>(pushBuffs);
		return copy;
	}

	ActorState() { }

	public ActorState(IPlaceable actorCore, int actorEndurance, int actorEnergy)
	{
		Core = actorCore;

		endurance = actorEndurance;
		energy = actorEnergy;

		attackBuffs = new List<Buff>();
		pushBuffs = new List<Buff>();
	}
}
