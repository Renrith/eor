public class BoxCore : PlaceableCore
{
	public Tile gdGetTile()
	{
		return OriginalState.tile;
	}

	public void initializeStats(int _steadiness)
	{
		steadiness = _steadiness;
		InitialState = new ActorState(this, 0, 0);
		setInitialState();
	}
}
