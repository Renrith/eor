// Interface which abstracts different types of AI behaviour
public interface IAI
{
	int getDistanceToClosestPlayer();
	void gdCalculateMove();
	Godot.Collections.Dictionary gdGetMove();
}