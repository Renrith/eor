using Godot;

// Base for objects placed in the map which can be pushed
public interface IPlaceable
{
	Node2D Base { get; set; }
	ActorState State { get; set; }
	ActorState OriginalState { get; set; }
	ActorState InitialState { get; set; }

	int steadiness { get; set; }
	bool grasping { get; set; }
	Tile tile { get; set; }
	int energy { get; }

	Tile getTileOrHoldTile();
	Tile gdGetTileOrHoldTile();
	bool gdIsAlive();

	bool isAlive();
	void setInitialState();
	void setBattle(Battle battle);
	void remove();
}
