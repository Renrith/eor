using System.Collections.Generic;

public class OriginalCore : ActorCore
{
	public string gangName
	{
		get { return OriginalState.gang.name; }
	}
	public int gangIndex
	{
		get { return OriginalState.gang.index; }
	}

	public bool hasActionSkill
	{
		get { return actionSkill != null; }
	}
	public string actionSkillName
	{
		get
		{
			if(hasActionSkill)
			{
				return actionSkill.name;
			}
			return "no action skill";
		}
	}
	public string actionSkillIcon
	{
		get { return actionSkill.icon; }
	}

	public bool gdIsGangLeader()
	{
		return OriginalState.isGangLeader;
	}

	public Tile gdGetTile()
	{
		return OriginalState.tile;
	}

	public Tile gdGetHoldTile()
	{
		return OriginalState.holdTile;
	}

	public int gdGetEndurance()
	{
		return OriginalState.endurance;
	}

	public new bool gdIsAlive()
	{
		if(OriginalState != null && gdGetTileOrHoldTile() != null && OriginalState.endurance > 0)
		{
			return true;
		}

		return false;
	}

	public bool gdIsActive()
	{
		if(State != null && tile != null && endurance > 0)
		{
			return true;
		}

		return false;
	}

	public string gdGetActorAlliance(int gangIndex)
	{
		Gang gang = Factions.allFactions[gangIndex];
		if(actorAlliances.ContainsKey(gang))
		{
			if(actorAlliances[gang] == AllianceType.Ally)
			{
				return "ally";
			}
			else
			{
				return "enemy";
			}
		}
		else
		{
			return "neutral";
		}
	}

	public void gdSetActorAlliance(int gangIndex, AllianceType allianceType)
	{
		Gang gang = Factions.allFactions[gangIndex];
		actorAlliances[gang] = allianceType;
	}

	public void gdStartTurn()
	{
		OriginalState.grasping = false;
		if(OriginalState.holdTile != null)
		{
			OriginalState.energy -= 5;
			if(OriginalState.energy == 0)
			{
				OriginalState.holdTile.holdOccupant = null;
				OriginalState.holdTile = null;
			}
		}
	}

	public virtual void endTurn()
	{
		turnOrder = turnOrder + turnRate;
		simulatedTurnOrder = turnOrder;
		Map.wipeTiles(WipeTilesMode.Paint);
	}

	public new void remove()
	{
		base.remove();
		Battle.currentActors.Remove(this);
	}

	public void updatePosition(Tile newTile)
	{
		OriginalState.tile.occupant = null;
		OriginalState.tile = newTile;
		OriginalState.tile.occupant = this;
	}

	public Godot.Collections.Array gdGetAttackOutcome(Tile tile)
	{
		List<ActionOutcome> outcomeDataList = calculateAttackOutcome(tile);
		return ActionOutcome.getGdData(outcomeDataList);
	}

	public Godot.Collections.Array gdGetActionSkillOutcome(Tile targetTile)
	{
		List<ActionOutcome> outcomeDataList = actionSkill.calculateOutcome(targetTile, tile);
		return ActionOutcome.getGdData(outcomeDataList);
	}

	public void performActionSkill(Tile targetTile)
	{
		performAction(actionSkill.calculateOutcome(targetTile, tile));
	}
}
