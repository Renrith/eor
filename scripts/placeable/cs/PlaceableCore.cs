using Godot;

public class PlaceableCore : Node, IPlaceable
{
	protected static Battle Battle;

	public Node2D _Base;
	public Node2D Base
	{
		get { return _Base; }
		set { _Base = value; }
	}
	public ActorState _State;
	public ActorState State
	{
		get { return _State; }
		set { _State = value; }
	}
	public ActorState _OriginalState;
	public ActorState OriginalState
	{
		get { return _OriginalState; }
		set { _OriginalState = value; }
	}
	public ActorState _InitialState;
	public ActorState InitialState
	{
		get { return _InitialState; }
		set { _InitialState = value; }
	}

	public int _steadiness = 0;
	public int steadiness
	{
		get { return _steadiness; }
		set { _steadiness = value; }
	}
	public bool grasping
	{
		get { return State.grasping; }
		set { State.grasping = value; }
	}
	public Tile tile
	{
		get { return State.tile; }
		set
		{
			if(State.tile != null)
			{
				State.tile.occupant = null;
			}
			State.tile = value;
			if(value != null)
			{
				value.occupant = this;
			}
		}
	}
	public int energy { get { return 0; } }
	public bool isActor = false;

	public Tile getTileOrHoldTile()
	{
		return tile;
	}

	public Tile gdGetTileOrHoldTile()
	{
		return OriginalState.tile;
	}

	public bool gdIsAlive()
	{
		if(OriginalState != null && gdGetTileOrHoldTile() != null)
		{
			return true;
		}

		return false;
	}

	public bool isAlive()
	{
		if(State != null && tile != null)
		{
			return true;
		}

		return false;
	}

	public virtual void setBattle(Battle _Battle)
	{
		Battle = _Battle;
	}

	public void setInitialState()
	{
		State = InitialState.Clone();
		OriginalState = State;
	}

	public void remove()
	{
		if(!ActorCore.simulation)
		{
			if(OriginalState.tile != null)
			{
				OriginalState.tile.occupant = null;
			}
			if(OriginalState.holdTile != null)
			{
				OriginalState.holdTile.holdOccupant = null;
			}
		}
		OriginalState.tile = null;
		OriginalState.holdTile = null;
		Battle.currentPlaceables.Remove(this);
	}

	public override void _Ready()
	{
		Base = (Node2D)GetParent();
	}
}
