using System.Collections.Generic;

public class PlayerCore : OriginalCore
{
	public Thought thought = null;

	public Godot.Collections.Dictionary gdGetStatsForBuild(Build build)
	{
		Godot.Collections.Dictionary stats = new Godot.Collections.Dictionary();

		ActorState currentState = State;
		State = State.Clone();
		State.attackBuffs = new List<Buff>();
		State.pushBuffs = new List<Buff>();

		List<AttributeModifier> currentAttributeSkills = attributeSkills;
		attributeSkills = build.attributeSkills;
		foreach(AttributeModifier attributeSkill in currentAttributeSkills)
		{
			attributeSkill.removeAttributes(this);
		}
		foreach(AttributeModifier attributeSkill in attributeSkills)
		{
			attributeSkill.setAttributes(this);
		}

		IActionModifier currentAttackSkill = attackSkill;
		IActionModifier currentPushSkill = pushSkill;
		INewAction currentActionSkill = actionSkill;

		attackSkill = build.attackSkill;
		pushSkill = build.pushSkill;
		actionSkill = build.actionSkill;
		if(actionSkill != null)
		{
			actionSkill.initialize(this);
		}

		stats.Add("endurance", this.endurance);
		stats.Add("energy", this.energy);
		stats.Add("movementRange", this.movementRange);

		stats.Add("attack", this.attack);
		stats.Add("defense", this.defense);
		stats.Add("push", this.push);
		stats.Add("steadiness", this.steadiness);

		State = currentState;

		attackSkill = currentAttackSkill;
		pushSkill = currentPushSkill;
		actionSkill = currentActionSkill;

		foreach(AttributeModifier attributeSkill in attributeSkills)
		{
			attributeSkill.removeAttributes(this);
		}
		foreach(AttributeModifier attributeSkill in currentAttributeSkills)
		{
			attributeSkill.setAttributes(this);
		}
		attributeSkills = currentAttributeSkills;

		return stats;
	}

	public Godot.Collections.Array getMeetTiles(string actorName)
	{
		foreach(Tile neighbour in tile.neighbours.Values)
		{
			if(neighbour != null && neighbour.occupant != null && (string)neighbour.occupant.Base.Get("actorName") == actorName)
			{
				return new Godot.Collections.Array(neighbour);
			}
		}
		return new Godot.Collections.Array();
	}

	public Godot.Collections.Array getAdjacentAttackTiles()
	{
		Godot.Collections.Array adjacentAttackTiles = new Godot.Collections.Array();
		switch(attackType)
		{
			case TargetingType.Neighbour:
				foreach(Tile neighbour in tile.neighbours.Values)
				{
					if(Tile.hasTargetableOccupant(neighbour, tile) && neighbour.occupant is ActorCore && getAlliance((ActorCore)neighbour.occupant) != AllianceType.Ally)
					{
						adjacentAttackTiles.Add(neighbour);
						Map.paintTile(PaintType.Enemy, neighbour);
					}
				}
				break;
			case TargetingType.RangedSkip:
				List<ActorCore> adjacentActors = Map.getAdjacentRangedSkipActors(tile, attackRange);
				foreach(ActorCore actor in adjacentActors)
				{
					if(getAlliance(actor) != AllianceType.Ally && Tile.isWithinReach(tile, actor.tile))
					{
						adjacentAttackTiles.Add(actor.tile);
						Map.paintTile(PaintType.Enemy, actor.tile);
					}
				}
				break;
			case TargetingType.RangedLine:
				List<ActorCore> actorsInLine = Map.getRangedLineActorsInRange(tile, attackRange);
				foreach(ActorCore actor in actorsInLine)
				{
					if(getAlliance(actor) != AllianceType.Ally)
					{
						adjacentAttackTiles.Add(actor.tile);
						Map.paintTile(PaintType.Enemy, actor.tile);
					}
				}
				break;
			case TargetingType.Slash:
				foreach(Tile neighbour in tile.neighbours.Values)
				{
					if(neighbour != null)
					{
						List<ActorCore> directionalActorsInSlash = Map.getActorsInSlash(tile, neighbour);
						bool hasTargetInDirection = false;
						foreach(ActorCore actor in directionalActorsInSlash)
						{
							if(getAlliance(actor) != AllianceType.Ally)
							{
								hasTargetInDirection = true;
							}
						}
						if(hasTargetInDirection)
						{
							foreach(ActorCore actor in directionalActorsInSlash)
							{
								Map.paintTile(PaintType.Enemy, actor.tile);
							}
							adjacentAttackTiles.Add(neighbour);
						}
					}
				}
				break;
		}
		return adjacentAttackTiles;
	}

	public Godot.Collections.Array getAdjacentPushTiles()
	{
		Godot.Collections.Array adjacentPushTiles = new Godot.Collections.Array();
		foreach(Tile neighbour in tile.neighbours.Values)
		{
			if(Tile.hasTargetableOccupant(neighbour, tile))
			{
				adjacentPushTiles.Add(neighbour);
				Map.paintTile(PaintType.Ally, neighbour);
			}
		}
		if(tile.holdOccupant != null)
		{
			adjacentPushTiles.Add(tile);
			Map.paintTile(PaintType.Ally, tile);
		}
		return adjacentPushTiles;
	}

	public Godot.Collections.Array gdGetAdjacentSkillTiles()
	{
		Godot.Collections.Array tiles = new Godot.Collections.Array();
		if(hasActionSkill)
		{
			switch(actionSkill.targetingType)
			{
				case TargetingType.Neighbour:
					foreach(Tile neighbour in tile.neighbours.Values)
					{
						if(neighbour != null && neighbour.occupant is ActorCore)
						{
							if(Tile.hasTargetableOccupant(neighbour, tile) && (Factions.getTargetAlliance(this, (ActorCore)neighbour.occupant) & actionSkill.targetableActorType) != 0)
							{
								tiles.Add(neighbour);
								Map.paintTile(PaintType.Ally, neighbour);
							}
							else if(actionSkill.targetableActorType == TargetAlliance.Self && neighbour.occupant == null)
							{
								tiles.Add(neighbour);
								Map.paintTile(PaintType.Ally, neighbour);
							}
						}
					}
					break;
				case TargetingType.RangedSkip:
					List<ActorCore> adjacentActors = Map.getAdjacentRangedSkipActors(tile, actionSkill.range);
					foreach(ActorCore actor in adjacentActors)
					{
						if(getAlliance(actor) != AllianceType.Ally && Tile.isWithinReach(actor.tile, tile))
						{
							tiles.Add(actor.tile);
							Map.paintTile(PaintType.Enemy, actor.tile);
						}
					}
					break;
				case TargetingType.Self:
					tiles.Add(getTileOrHoldTile());
					break;
			}
		}
		return tiles;
	}

	public Godot.Collections.Array getAdjacentGripTiles()
	{
		Godot.Collections.Array adjacentGripTiles = new Godot.Collections.Array();
		foreach(Tile neighbour in tile.neighbours.Values)
		{
			if(neighbour != null && neighbour.graspable == true && Tile.isWithinReach(neighbour, tile))
			{
				adjacentGripTiles.Add(neighbour);
				Map.paintTile(PaintType.Enemy, neighbour);
			}
		}
		return adjacentGripTiles;
	}

	public void paintActionPreview(Tile targetTile, string action)
	{
		Map.clearPaintedTiles();

		List<Tile> targetableTiles = new List<Tile>();
		List<Tile> pathTiles = new List<Tile>();

		TargetingType targetingType = TargetingType.Neighbour;
		if(action == "attack")
		{
			targetingType = attackType;
		}
		else if(action == "push")
		{
			Direction pushDirection = getPushDirection(targetTile);
			IPlaceable target = targetTile.occupant;
			if(pushDirection == Direction.Below)
			{
				target = targetTile.holdOccupant;
			}
			List<ActionOutcome> pushOutcomes = Map.getPushPath(this, target, pushDirection);

			foreach(ActionOutcome pushOutcome in pushOutcomes)
			{
				foreach(Tile pathTile in pushOutcome.path)
				{
					if(pathTile != null)
					{
						pathTiles.Add(pathTile);
					}
				}
			}
		}
		else if(action == actionSkillName)
		{
			targetingType = actionSkill.targetingType;
		}

		switch(targetingType)
		{
			case TargetingType.Slash:
				targetableTiles = Map.getTilesInSlash(tile, targetTile);
				break;
		}

		foreach(Tile pathTile in pathTiles)
		{
			Map.paintTile(PaintType.PushPath, pathTile);
		}

		foreach(Tile targetableTile in targetableTiles)
		{
			Map.paintTile(PaintType.PushPath, targetableTile);
		}
	}

	protected void initializeBuild(Build build)
	{
		Skills.setSkill(this, (Skill)build.attackSkill);
		Skills.setSkill(this, (Skill)build.pushSkill);
		Skills.setSkill(this, (Skill)build.actionSkill);

		attributeSkills.Clear();
		foreach(AttributeModifier attributeSkill in build.attributeSkills)
		{
			Skills.setSkill(this, (Skill)attributeSkill);
		}
	}

	public override void endTurn()
	{
		base.endTurn();
		if(thought != null && lastActionOutcome != null)
		{
			thought.checkAchievementStatus(lastActionOutcome);
		}
	}

	public PlayerCore()
	{
		isPlayer = true;
	}
}
