tool
extends "res://scripts/placeable/gd/ActorBase.gd"

enum AIType {
	Default = 0,
	Static = 1,
	Kidnap = 2,
	Triggerable = 3,
}

export(AIType) var AIScript = AIType.Default

func _enter_tree() -> void:
	match AIScript:
		AIType.Default:
			coreScript = preload("res://scripts/placeable/cs/AIDefaultCore.cs")
		AIType.Static:
			coreScript = preload("res://scripts/placeable/cs/AIStaticCore.cs")
		AIType.Kidnap:
			coreScript = preload("res://scripts/placeable/cs/AIKidnapCore.cs")
		AIType.Triggerable:
			coreScript = preload("res://scripts/placeable/cs/AITriggerableCore.cs")
