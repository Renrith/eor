tool
extends "res://scripts/placeable/gd/ActorController.gd"

var simulationTimer: Timer = Timer.new()
var cameraTimer: Timer = Timer.new()
var visualTimer: Timer = Timer.new()

var hasCameraWaited: bool = false

var shouldBecomePlayer: bool = false

func endTurn() -> void:
	if shouldBecomePlayer:
		Core.endTurn()
		becomePlayer()
		Battle.endActorTurn()
	else:
		.endTurn()

func becomePlayer() -> void:
	Base.isPlayer = true
	Base.Core = Core.becomePlayer()
	Base.addController()
	Core = null
	connect("visualTurnEnd", self, "queue_free")

func startTurn() -> void:
	if Core.gdIsAlive():
		Core.gdCalculateMove()
		canPerformAction()
	else:
		Base.remove()
		Battle.endActorTurn()

func canPerformAction() -> void:
	if !Battle.isAITakingAction && !Core.simulation:
		if Core.gdIsAlive():
			if Core.gdGetMove().action != "" || Core.getDistanceToClosestPlayer() < 10:
				.startTurn()
				startVisualTurn()
				executeSelectedAction()
			else:
				endTurn()
		else:
			Base.remove()
			Battle.endActorTurn()
	else:
		simulationTimer.set_wait_time(0.05)
		simulationTimer.start()

func executeSelectedAction() -> void:
	if Camera.current && hasCameraWaited:
		move = Core.gdGetMove()
		hasMoved = false
		Battle.isAITakingAction = true
		var shouldShowDialogues := false
		
		if move.tile != null && move.origin != null && move.action != null:
			visualTimer.set_wait_time(2)
			action = move.action
			
			if Core.gdIsActive() && move.origin != Core.gdGetTile():
				Map.calculateMovementRange(Core, true, false)
				Base.updatePosition(move.origin)
				Tween.endPathCallbacks.push_back(funcref(Cursor, "repaintEnemyRange"))
				hasMoved = true
			
			targetTile = move.tile
			
			var outcomeList := []
			match action:
				"attack":
					outcomeList = Core.gdGetAttackOutcome(targetTile)
				"push":
					outcomeList = getPushOutcome()
				"actionSkill", Core.actionSkillName:
					outcomeList = Core.gdGetActionSkillOutcome(targetTile)
			
			if outcomeList.size() > 0:
				var visualOutcome
				
				for outcome in outcomeList:
					if outcome.target != null:
						visualOutcome = outcome
						break
				
				shouldShowDialogues = Battle.callDialogues(Core, visualOutcome.target.Core, action, visualOutcome.enduranceChange)
				
				if !shouldShowDialogues:
					emit_signal("actionOutcome", self, targetTile, action, false, visualOutcome)
		
		else:
			if move.action == null:
				action = "end turn"
			visualTimer.set_wait_time(0.4)
		
		
		if !shouldShowDialogues:
			.executeSelectedAction()
			visualTimer.start()
			endTurn()
	
	elif hasCameraWaited:
		cameraTimer.set_wait_time(0.05)
		cameraTimer.start()
	else:
		hasCameraWaited = true
		cameraTimer.set_wait_time(1)
		cameraTimer.start()

func _ready() -> void:
	if !Engine.editor_hint:
		Core.isPlayer = false
	
	simulationTimer.connect("timeout", self, "canPerformAction")
	simulationTimer.set_one_shot(true)
	add_child(simulationTimer)
	
	cameraTimer.connect("timeout", self, "executeSelectedAction")
	cameraTimer.set_one_shot(true)
	add_child(cameraTimer)
	
	visualTimer.connect("timeout", self, "endVisualTurn")
	visualTimer.set_one_shot(true)
	add_child(visualTimer)
