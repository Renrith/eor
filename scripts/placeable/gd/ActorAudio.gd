extends "res://scripts/placeable/gd/PlaceableAudio.gd"

export(AudioStream) var hitSound = preload("res://sound/effects/hit.wav")
export(AudioStream) var takeHitSound = null
export(AudioStream) var pushSound = preload("res://sound/effects/push.wav")
export(AudioStream) var getZeroEnduranceSound = null
export(Array, AudioStream) var moveToPointSounds = [
	preload("res://sound/effects/step1.wav"),
	preload("res://sound/effects/step2.wav"),
	preload("res://sound/effects/step3.wav"),
	preload("res://sound/effects/step4.wav"),
]

func hit() -> void:
	stop()
	stream = hitSound
	play()

func takeHit() -> void:
	stop()
	stream = takeHitSound
	play()

func push() -> void:
	stop()
	stream = pushSound
	play()

#Not called yet
func getZeroEndurance() -> void:
	stop()
	stream = getZeroEnduranceSound
	play()

func moveToPoint() -> void:
	stop()
	var index: int = randi() % moveToPointSounds.size()
	stream = moveToPointSounds[index]
	callback = funcref(self, "moveToPoint")
	play()
