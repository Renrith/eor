tool
extends "res://scripts/placeable/gd/PlaceableBase.gd"

enum GangLeaderType {
	Undefined = -1,
	False = 0,
	True = 1,
}

var Controller: Node
var Camera: Camera2D

export(String) var actorData
export(String) var actorName
export(String) var sprite = "generic"
export(bool) var isPlayer = false

export(int) var attack = -1
export(int) var defense = -1
export(Globals.gdTargetingType) var attackType = -1
export(int) var attackRange = -1

export(int) var push = -1

export(GangLeaderType) var isGangLeader = GangLeaderType.Undefined

export(int) var endurance = -1
export(int) var maxEndurance = -1
export(int) var energy = -1
export(int) var maxEnergy = -1

export(int) var movementRange = -1
export(int) var turnRate = -1
export(int) var turnOrder = -1

export(Globals.gdFactions) var gang = Globals.gdFactions.NoGang
export(Array, Globals.gdFactions) var enemies = []
export(Array, Globals.gdFactions) var rivals = []
export(Array, Globals.gdFactions) var neutrals = []
export(Array, Globals.gdFactions) var allies = []
export(Array, Globals.gdFactions) var targetOf = []

export(Array, Globals.gdSkills) var skills = []

var portraits: Dictionary
export(StreamTexture) var battlePortrait

var color: Color
var spriteGangColor: Material

func updateOutline():
	color = Globals.getGangColor(Core.gangIndex)
	$AnimatedSprite.material.set_shader_param("color", color)
	$AnimatedSprite.GangLeaderIndicator.material.set_shader_param("color", color)

func previewGetPushed(pathTiles, extendedPushDirection = "", hasHoldTile = false):
	.previewGetPushed(pathTiles, extendedPushDirection, hasHoldTile)
	if AltSprite:
		AltSprite.material.set_shader_param("color", color)

func undoPreviewGetPushed():
	.undoPreviewGetPushed()
	if Globals.OutlineController.actor == self:
		Globals.OutlineController.applyOutline(self)
	else:
		$AnimatedSprite.material = spriteGangColor

func moveSprite(pathTiles, movementType = ""):
	var speed: float
	match movementType:
		"step":
			speed = 0.2
		"undo":
			speed = 0.0000001
		_:
			speed = 0.2
	
	Tween.pathTiles = pathTiles
	Tween.moveToPoint(speed, pathTiles)

func undoMoveSprite():
	Tween.moveToPreviousPosition()

func updatePosition(newTile, action = "step"):
	var pathTiles: Array = []
	var pathTile = newTile
	
	if action == "undo":
		pathTiles = [pathTile]
	elif action == "step":
		while pathTile.parent != null:
			pathTiles.push_front(pathTile)
			pathTile = pathTile.parent
		
	moveSprite(pathTiles, action)
	Core.updatePosition(newTile)

func updatePathPosition(path: Array, action: String) -> void:
	Core.updatePosition(path[path.size() - 1])
	moveSprite(path, action)

func restartSprite():
	Sprite.EnduranceBar.updateBar()
	.restartSprite()

func initializeSprite():
	var frames: SpriteFrames = SpriteFrames.new()
	$AnimatedSprite.frames = frames
	$AnimatedSprite.frames.add_frame("default", load("res://sprites/map/spr_" + sprite + ".png"), 0)
	$AnimatedSprite.frames.add_frame("default", load("res://sprites/map/spr_" + sprite + "_1.png"), 1)
	$AnimatedSprite.play("default")

func setInstanceAlliances():
	for enemy in enemies:
		Core.gdSetActorAlliance(enemy, 0)
	for rival in rivals:
		Core.gdSetActorAlliance(rival, 1)
	for neutral in neutrals:
		Core.gdSetActorAlliance(neutral, 2)
	for ally in allies:
		Core.gdSetActorAlliance(ally, 3)
	
	for enemy in targetOf:
		Globals.Factions.gdSetTarget(Core, enemy)

func setInstanceSkills():
	for skill in skills:
		Globals.Skills.gdSetSkill(Core, skill)

func initializeFromFile() -> void:
	var data: JSONParseResult = Globals.openJSON("res://json/" + actorData + ".json")
	
	for key in data.result.gd.keys():
		var value = get(key)
		match value:
			null, "", -1:
				self.set(key, data.result.gd[key])
	
	if data.result.has("cs"):
		for key in data.result.cs.keys():
			if typeof(self[key]) == TYPE_INT && self[key] == -1:
				set(key, data.result.cs[key])
	
	if !Engine.editor_hint:
		if data.result.has("portraits"):
			for key in data.result.portraits.keys():
				portraits[key] = data.result.portraits[key]
		
		if data.result.has("alliances"):
			if data.result.alliances.has("gang"):
				gang = data.result.alliances.gang

func readyFromFile() -> void:
	var data: JSONParseResult = Globals.openJSON("res://json/" + actorData + ".json")
	
	if  data.result.has("sprite"):
		sprite = data.result.sprite
	
	if !Engine.editor_hint:
		if data.result.has("alliances"):
			for enemy in data.result.alliances.enemies:
				Core.gdSetActorAlliance(enemy, 0)
			for rival in data.result.alliances.rivals:
				Core.gdSetActorAlliance(rival, 1)
			for neutral in data.result.alliances.neutrals:
				Core.gdSetActorAlliance(neutral, 2)
			for ally in data.result.alliances.allies:
				Core.gdSetActorAlliance(ally, 3)
		
		if data.result.has("skills"):
			if data.result.skills.attackSkill != null:
				Globals.Skills.gdSetSkill(Core, data.result.skills.attackSkill)
			if data.result.skills.pushSkill != null:
				Globals.Skills.gdSetSkill(Core, data.result.skills.pushSkill)
			if data.result.skills.actionSkill != null:
				Globals.Skills.gdSetSkill(Core, data.result.skills.actionSkill)
			if data.result.skills.attributeSkills != null:
				for attributeSkill in data.result.skills.attributeSkills:
					Globals.Skills.gdSetSkill(Core, attributeSkill)

func addCore():
	.addCore()
	
	Core.initializeStats(
		attack, defense, attackType, attackRange, push, steadiness,
		isGangLeader,
		maxEndurance, endurance, maxEnergy, energy,
		movementRange, turnRate, turnOrder,
		gang)
	
	if Core.gdIsGangLeader():
		$AnimatedSprite.GangLeaderIndicator.set_visible(true)

func addController():
	var controllerScript: Resource
	if isPlayer:
		controllerScript = preload("res://scripts/placeable/gd/PlayerController.gd")
	else:
		controllerScript = preload("res://scripts/placeable/gd/AIController.gd")
	Controller = controllerScript.new()
	add_child(Controller)
	
	Sprite.EnduranceBar.show()
	Sprite.TurnOrder.show()

func removeController():
	if Controller != null:
		Controller.queue_free()
		Controller = null
		Sprite.EnduranceBar.hide()
		Sprite.TurnOrder.hide()

func remove():
	.remove()
	if !Controller.hasActiveVisualTurn:
		hide()

func _ready():
	if actorData != "":
		initializeFromFile()
	
	if !isReinforcement:
		addCore()
	
	Camera = $Camera
	
	spriteGangColor = preload("res://shaders/placeable/spriteGangColor.tres").duplicate()
	
	$AnimatedSprite.GangLeaderIndicator.material = $AnimatedSprite.GangLeaderIndicator.material.duplicate()
	$AnimatedSprite.GangLeaderIndicator.set_visible(false)
	
	initializeSprite()
	
	add_to_group("actors")
	
	if actorData != "":
		readyFromFile()
	
	setInstanceAlliances()
	setInstanceSkills()
	
	$AnimatedSprite.material = spriteGangColor
	
	if Engine.editor_hint:
		color = Globals.getGangColor(gang)
		$AnimatedSprite.material.set_shader_param("color", color)
		$AnimatedSprite.GangLeaderIndicator.material.set_shader_param("color", color)
	elif !isReinforcement:
		updateOutline()
