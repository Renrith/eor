tool
extends Node

var Base: Node2D
var Camera: Camera2D
var Core: Node
var Tween: Tween

class PushOutcome:
	var target: Node
	var path: Array
	var holdTile: Reference
	var newBorderDistance: int
	var newPosition: Vector2
	var enduranceChange: int
	var pushStrength: int
	var extendedPushDirection: String = ""
	var energyChange: int

signal visualTurnStart(actor)
signal actionOutcome(actor, targetTile, action, hasAdditionalTargets, pushOutcome)
signal visualTurnEnd

const allyActions = ["band aid", "positivist discourse"]
const neutralActions = []
const enemyActions = []
const selfActions = ["sheath"]

var targetTile: Reference
var move: Dictionary
var action: String = ""
var hasActiveVisualTurn: bool = false
var hasMoved: bool = false

var Map: TileMap
var Battle: Node
var Cursor: Sprite

var pushDirection: String

var attackOutcomeList: Array
var pushOutcomeList: Array
var actionSkillOutcomeList: Array

# Getters and setters ----------------------------------------------------------
var outcomeList: Array setget, getOutcomeList
var shouldDelayMovingSprite: bool setget, shouldDelayMovingSprite

func getOutcomeList() -> Array:
	match action:
		"attack":
			return attackOutcomeList
		"push":
			return pushOutcomeList
		"actionSkill", Core.actionSkillName:
			return actionSkillOutcomeList
		_:
			return []

func shouldDelayMovingSprite() -> bool:
	return !Core.isPlayer && hasMoved

# End of getters and setters ---------------------------------------------------

func getTurnPosition() -> int:
	return Battle.getActorTurnPosition(Core)

func startTurn() -> void:
	Core.gdStartTurn()
	action = ""
	attackOutcomeList = []
	pushOutcomeList = []
	actionSkillOutcomeList = []
	pushDirection = ""

func startVisualTurn() -> void:
	hasActiveVisualTurn = true
	emit_signal("visualTurnStart", Base)
	Globals.OutlineController.applyOutline(Base)

func endTurn() -> void:
	Core.endTurn()
	Battle.endActorTurn()

func endVisualTurn() -> void:
	hasActiveVisualTurn = false
	emit_signal("visualTurnEnd")
	if Core && !Core.gdIsAlive():
		Base.remove()

func pushTargetSprite() -> void:
	Tween.push(pushDirection)
	for pushOutcome in pushOutcomeList:
		if pushOutcome.holdTile != null:
			pushOutcome.target.getSpritePushed(pushOutcome.path, pushOutcome.extendedPushDirection, pushOutcome.pushStrength, true)
		else:
			pushOutcome.target.getSpritePushed(pushOutcome.path, pushOutcome.extendedPushDirection, pushOutcome.pushStrength)
		if pushOutcome.target.Core.isActor:
			pushOutcome.target.Sprite.EnduranceBar.updateBar()
	pushOutcomeList = []

func moveTargetSprite():
	match action:
		"attack":
			Tween.hit()
			for attackOutcome in attackOutcomeList:
				attackOutcome.target.Tween.takeHit()
				attackOutcome.target.Sprite.EnduranceBar.updateBar()
		_:
			for actionSkillOutcome in actionSkillOutcomeList:
				actionSkillOutcome.target.Sprite.EnduranceBar.updateBar()
				if actionSkillOutcome.has("gang"):
					actionSkillOutcome.target.updateOutline()
					actionSkillOutcome.target.Tween.getCaptured()

func executeSelectedAction():
	match action:
		"attack":
			attackTarget()
		"push":
			pushTarget()
		"actionSkill", Core.actionSkillName:
			performActionSkill(targetTile)
		"climb":
			climb()
		"grip":
			grip()
		"end turn":
			if Core.gdIsActive():
				Tween.wait()

func getPushOutcome():
	var pushOutcomeList := []
	
	var pushDataList: Array = Map.gdGetPushPath(Core, targetTile)
	var pushDirection = Core.gdGetPushDirection(targetTile)
	
	for pushData in pushDataList:
		var pushOutcome := PushOutcome.new()
		var finalTile = pushData["path"][pushData.path.size() - 1]
		
		pushOutcome.path = pushData["path"]
		pushOutcome.holdTile = pushData["holdTile"]
		pushOutcome.target = pushData["target"]
		pushOutcome.pushStrength = Core.getPushStrength(pushOutcome.target.Core.steadiness)
		pushOutcome.energyChange = pushData["energyChange"]
		
		if pushOutcome.target.Core.isActor:
			if finalTile != null || pushOutcome.holdTile != null:
				pushOutcome.enduranceChange = pushData["heightLevelChange"] * Core.fallDamageMultiplier
			else:
				pushOutcome.enduranceChange = -pushOutcome.target.Core.gdGetEndurance()
		
		if finalTile != null:
			pushOutcome.newBorderDistance = finalTile.minBorderDistance
			pushOutcome.newPosition = Map.map_to_world(finalTile.cell)
		else:
			pushOutcome.path.pop_back()
			pushOutcome.newBorderDistance = -1
			pushOutcome.extendedPushDirection = pushDirection
			if pushOutcome.path.size() > 0:
				pushOutcome.newPosition = Map.map_to_world(pushOutcome.path[pushOutcome.path.size() - 1].cell)
			else:
				pushOutcome.newPosition = pushOutcome.target.position
		
		pushOutcomeList.push_back(pushOutcome)
	
	pushOutcomeList.invert()
	return pushOutcomeList

func attackTarget():
	attackOutcomeList = Core.gdGetAttackOutcome(targetTile)
	Core.attackTarget(targetTile)
	
	if shouldDelayMovingSprite():
		Tween.endPathCallbacks.push_back(funcref(self, "moveTargetSprite"))
	else:
		moveTargetSprite()
	
	for attackOutcome in attackOutcomeList:
		var target: Node2D = attackOutcome.target
		if !target.Core.gdIsAlive():
			if shouldDelayMovingSprite():
				target.Tween.endPathCallbacks.push_back(funcref(target, "remove"))
			else:
				target.remove()

func pushTarget():
	pushOutcomeList = getPushOutcome()
	
	Core.pushTarget(targetTile, Core.gdGetTile())
	
	for i in range(0, pushOutcomeList.size()):
		var pushOutcome: PushOutcome = pushOutcomeList[i]
		if !pushOutcome.target.Core.gdIsAlive():
			if pushOutcome.holdTile == null:
					pushOutcome.target.Tween.endPathCallbacks.push_back(funcref(pushOutcome.target, "remove"))
	
	if shouldDelayMovingSprite():
		Tween.endPathCallbacks.push_back(funcref(self, "pushTargetSprite"))
	else:
		pushTargetSprite()

func performActionSkill(targetTile):
	actionSkillOutcomeList = Core.gdGetActionSkillOutcome(targetTile)
	Core.performActionSkill(targetTile)
	
	if shouldDelayMovingSprite():
		Tween.endPathCallbacks.push_back(funcref(self, "moveTargetSprite"))
	else:
		moveTargetSprite()
	
	for actionSkillOutcome in actionSkillOutcomeList:
		if actionSkillOutcome.target != null:
			if actionSkillOutcome.path != null && actionSkillOutcome.path.size() > 0:
				Base.getSpritePushed(actionSkillOutcome.path)

func grip():
	Core.grip()

func climb():
	Tween.climb()
	Core.climb()

func _ready():
	if !Engine.editor_hint:
		Cursor = get_node("/root/Viewports/SettingViewport/Map/YSort/Cursor")
		Battle = get_node("/root/Setting").Battle
		Map = get_node("/root/Setting").Map
		
		Base = get_parent()
		Camera = Base.Camera
		Core = Base.Core
		Tween = Base.Tween
		
		Core.Controller = self
		
		var SettingCamera: Camera2D = get_node("/root/Viewports/SettingViewport/SettingCamera")
		var UI: Node = get_node("/root/Viewports/UIViewport/BattleUILayer")
		
		connect("visualTurnStart", Battle, "startActorVisualTurn")
		connect("visualTurnStart", SettingCamera, "moveToActor")
		connect("visualTurnStart", UI, "onVisualTurnStart")
		
		connect("actionOutcome", SettingCamera, "showOutcomeCamera")
		connect("actionOutcome", UI, "onActionOutcome")
		
		connect("visualTurnEnd", Battle, "setActionTurnEnd")
		connect("visualTurnEnd", SettingCamera, "make_current")
