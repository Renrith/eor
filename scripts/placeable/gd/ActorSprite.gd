tool
extends AnimatedSprite

var EnduranceBar: ColorRect
var GangLeaderIndicator: Sprite
var TurnOrder: Label

func _ready() -> void:
	EnduranceBar = $Endurance
	GangLeaderIndicator = $GangLeaderIndicator
	TurnOrder = $TurnOrder
