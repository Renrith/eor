tool
extends "res://scripts/placeable/gd/PlaceableTween.gd"

const pushJumpVector = Vector2(0, -8)

var SettingCamera: Camera2D

var timer: Timer

var pathTiles: Array = []

func interpolateNextPoint():
	if points.size() > 0:
		var point = points.pop_front()
		interpolate_property(movingNode, "position", movingNode.position, point.position, point.speed, point.transitionType, point.easeType)
		
		if point.modulate != Color.black:
			interpolate_property(movingNode.Sprite, "modulate", movingNode.Sprite.modulate, point.modulate, point.speed, TRANS_LINEAR, point.easeType)
		
		if point.zIndex == -1:
			interpolate_property(movingNode, "z_index", movingNode.z_index, point.position.y, point.speed, TRANS_LINEAR, point.easeType)
		else:
			interpolate_property(movingNode, "z_index", movingNode.z_index, point.zIndex, point.speed, TRANS_LINEAR, point.easeType)
		
		interpolate_property(movingNode.Sprite, "offset", movingNode.Sprite.offset, point.offset, point.speed, TRANS_CIRC, EASE_IN)
		interpolate_property(movingNode.Sprite.GangLeaderIndicator, "offset", movingNode.Sprite.GangLeaderIndicator.offset, point.offset - Vector2(0, 36), point.speed, TRANS_CIRC, EASE_IN)
		interpolate_property(movingNode.Sprite.EnduranceBar, "rect_position", movingNode.Sprite.EnduranceBar.rect_position, point.offset + Vector2(-16, 34), point.speed, TRANS_CIRC, EASE_IN)
		interpolate_property(movingNode.Sprite.TurnOrder, "rect_position", movingNode.Sprite.TurnOrder.rect_position, point.offset + Vector2(0, 27), point.speed, TRANS_CIRC, EASE_IN)
		start()
	else:
		endPath()

func addReturnPoint(speed = 0.3):
	var point = Point.new()
	point.position = movingNode.position
	point.offset = movingNode.Sprite.offset
	point.speed = speed
	point.zIndex = movingNode.z_index
	points.push_back(point)

func avoid():
	points.clear()
	
	var point = Point.new()
	point.position = movingNode.position + Vector2(25, 0)
	point.offset = movingNode.Sprite.offset
	point.speed = 0.04
	point.zIndex = movingNode.z_index
	points.push_back(point)
	
	addReturnPoint(0.08)
	interpolateNextPoint()

func getCaptured():
	points.clear()
	addPoint(movingNode.position + Vector2(0, 8), movingNode.Sprite.offset, 0.2, movingNode.z_index)
	addReturnPoint(0.2)
	interpolateNextPoint()

func hit():
	Audio.hit()

func takeHit(repetitions = 4):
	points.clear()
	var shakeVector = Vector2(10, 0)
	
	for repetition in repetitions:
		var point = Point.new()
		shakeVector = shakeVector * -1
		point.position = movingNode.position + shakeVector
		point.offset = movingNode.Sprite.offset
		point.speed = 0.07
		point.zIndex = movingNode.z_index
		points.push_back(point)
	
	addReturnPoint()
	interpolateNextPoint()
	Audio.takeHit()

func push(direction = ""):
	points.clear()
	if direction != "":
		var directionVector: Vector2 = Globals.getDirectionVector(direction)
		addPoint(movingNode.position + movingNode.Map.map_to_world(directionVector) * 0.75, movingNode.Sprite.offset + pushJumpVector, 0.05)
	else:
		addPoint(movingNode.position + Vector2(0, 16), movingNode.Sprite.offset + pushJumpVector, 0.05)
	
	addReturnPoint(0.1)
	interpolateNextPoint()
	Audio.push()

func climb():
	var point = Point.new()
	var tile = movingNode.Core.gdGetHoldTile()
	point.position = tile.position
	point.offset = tile.gdOffset
	point.speed = 0.2
	points.push_back(point)
	interpolateNextPoint()

func moveAside():
	if movingNode.Core.gdIsAlive():
		points.clear()
		var directionVector: Vector2 = Globals.getClockwiseDirectionVector(direction)
		addPoint(movingNode.position + movingNode.Map.map_to_world(directionVector) * Vector2(0.25, 0.25), movingNode.Sprite.offset, 0.2, movingNode.z_index)
		addReturnPoint(0.2)
		interpolateNextPoint()

func wait():
	points.clear()
	addPoint(movingNode.position + Vector2(0, 4), movingNode.Sprite.offset, 0.2, movingNode.z_index)
	addReturnPoint(0.1)
	interpolateNextPoint()

func moveToPoint(speed, pathTiles):
	var pathTilesTotal: int = 0
	var previousTile: Reference = movingNode.Core.gdGetTile()
	while pathTiles.size() > 0:
		var pathTile = pathTiles.pop_front()
		var point = Point.new()
		point.position = movingNode.Map.map_to_world(pathTile.cell)
		point.offset = pathTile.gdOffset
		point.speed = speed
		if pathTile.occupant != null && pathTile.occupant != movingNode.Core:
			pathTile.occupant.Base.Tween.direction = pathTile.gdGetNeighbourDirection(previousTile)
			pathTile.occupant.Base.Tween.timer.set_wait_time(pathTilesTotal * speed + 0.01)
			pathTile.occupant.Base.Tween.timer.start()
		pathTilesTotal += 1
		previousTile = pathTile
		points.push_back(point)
	
	endPathCallbacks.push_back(funcref(Audio, "stop"))
	Audio.moveToPoint()
	
	interpolateNextPoint()

func moveToPreviousPosition():
	points.clear()
	addPoint(previousPosition, previousOffset, 0.0000001, previousZIndex)
	previousPosition = Vector2.ZERO
	previousOffset = Vector2.ZERO
	previousZIndex = 0
	
	interpolateNextPoint()

func endPath():
	direction = ""
	directionVector = Vector2.ZERO
	if movingNode.Core.isPlayer && movingNode.Controller != null && movingNode.Controller.hasMoved && !movingNode.Controller.isInMenu:
		movingNode.Controller.handlePlayerMenu()
	for callback in endPathCallbacks:
		if callback.is_valid():
			callback.call_func()
	endPathCallbacks = []

func _ready():
	timer = Timer.new()
	add_child(timer)
	timer.connect("timeout", self, "moveAside")
	timer.set_one_shot(true)
	SettingCamera = get_node("/root/Viewports/SettingViewport/SettingCamera")
