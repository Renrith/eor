tool
extends "res://scripts/placeable/gd/PlayerBase.gd"

func _init():
	actorName = "Alaitz"
	
	portraits = {
		"concernedPouting": "res://sprites/UI/portraits/Alaitz_concerned_pouting.png",
		"concernedWorried": "res://sprites/UI/portraits/Alaitz_concerned_worried.png",
		"confidentGrin": "res://sprites/UI/portraits/Alaitz_confident_grin.png",
		"confidentTalking": "res://sprites/UI/portraits/Alaitz_confident_talking.png",
		"fightmeRaging": "res://sprites/UI/portraits/Alaitz_fightme_raging.png",
		"neutralClaiming": "res://sprites/UI/portraits/Alaitz_neutral_claiming.png",
		"neutralDiscussing": "res://sprites/UI/portraits/Alaitz_neutral_discussing.png",
		"neutralExistential": "res://sprites/UI/portraits/Alaitz_neutral_existential.png",
		"neutralSerious": "res://sprites/UI/portraits/Alaitz_neutral_serious.png",
		"neutralSincereSmile": "res://sprites/UI/portraits/Alaitz_neutral_sincereSmile.png",
		"righteousRaging": "res://sprites/UI/portraits/Alaitz_righteous_raging.png",
		"whatAsking": "res://sprites/UI/portraits/Alaitz_what_asking.png",
		"whatCurious": "res://sprites/UI/portraits/Alaitz_what_curious.png",
	}
	preparationsPortrait = preload("res://sprites/UI/portraits/Alaitz_neutral_sincereSmile.png")
	
	skillList = {
		3: {
			"index": 3,
			"isAcquired": true,
			"cost": 0,
			"borrowCost": 0,
			"dependencies": [],
		},
		4: {
			"index": 4,
			"isAcquired": false,
			"cost": 4,
			"borrowCost": 3,
			"dependencies": [3],
		},
		8: {
			"index": 8,
			"isAcquired": false,
			"cost": 5,
			"borrowCost": 4,
			"dependencies": [3],
		},
		9: {
			"index": 9,
			"isAcquired": false,
			"cost": 5,
			"borrowCost": 4,
			"dependencies": [3],
		},
		10: {
			"index": 10,
			"isAcquired": true,
			"cost": 3,
			"borrowCost": 2,
			"dependencies": [3],
		},
	}
	
	defeatDialogues[Globals.gdDefeatReason.EnduranceDepleted] = "Ugh... I can't stand up anymore...!"
	defeatDialogues[Globals.gdDefeatReason.PushedOff] = "Damnit! I got too close to the edge!"

func _enter_tree():
	attack = 4
	attackRange = 1
	attackType = Globals.gdTargetingType.Neighbour
	defense = 2
	if isGangLeader == -1:
		isGangLeader = 1
	maxEndurance = 12
	endurance = 12
	maxEnergy = 6
	energy = 6
	movementRange = 4
	push = 4
	steadiness = 2
	turnRate = 2
	if turnOrder == -1:
		turnOrder = 2
	
	if gang == Globals.gdFactions.NoGang:
		gang = Globals.gdFactions.AlaitzsGang
	
	if sprite == "generic":
		sprite = "Alaitz"
	if battlePortrait == null:
		battlePortrait = preload("res://sprites/UI/portraits/Alaitz_neutral_serious.png")

func _ready():
	if !Engine.editor_hint:
		Core.gdSetActorAlliance(Globals.gdFactions.NoGangPlayer, 3)
