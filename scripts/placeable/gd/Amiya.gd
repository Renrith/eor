tool
extends "res://scripts/placeable/gd/PlayerBase.gd"

func _init():
	actorName = "Amiya"
	
	portraits = {
		"neutralAngry": "res://sprites/UI/portraits/Amiya_neutral_angry.png",
		"neutralBothered": "res://sprites/UI/portraits/Amiya_neutral_bothered.png",
		"neutralContemplative": "res://sprites/UI/portraits/Amiya_neutral_contemplative.png",
		"neutralCurious": "res://sprites/UI/portraits/Amiya_neutral_curious.png",
		"neutralSmiling": "res://sprites/UI/portraits/Amiya_neutral_smiling.png",
	}
	preparationsPortrait = preload("res://sprites/UI/portraits/Amiya_neutral_contemplative.png")
	
	skillList = {}

func _enter_tree():
	attack = 8
	attackRange = 5
	attackType = Globals.gdTargetingType.RangedSkip
	defense = 2
	isGangLeader = 0
	maxEndurance = 10
	endurance = 10
	maxEnergy = 20
	energy = 20
	movementRange = 3
	push = 1
	steadiness = 3
	turnRate = 3
	if turnOrder == -1:
		turnOrder = 3
	
	if gang == Globals.gdFactions.NoGang:
		gang = Globals.gdFactions.NoGangPlayer
	
	if sprite == "generic":
		sprite = "Amiya"
	if battlePortrait == null:
		battlePortrait = preload("res://sprites/UI/portraits/Amiya_neutral_contemplative.png")
		
	defeatDialogues[Globals.gdDefeatReason.EnduranceDepleted] = "Even to this day, I..."
	defeatDialogues[Globals.gdDefeatReason.PushedOff] = "Please, forget about me..."

func _ready():
	if !Engine.editor_hint:
		Core.gdSetActorAlliance(Globals.gdFactions.AlaitzsGang, 3)
		Core.gdSetActorAlliance(Globals.gdFactions.NoGangPlayer, 3)
