tool
extends "res://scripts/placeable/gd/PlaceableBase.gd"

func remove():
	.remove()
	hide()

func addCore():
	.addCore()
	Core.initializeStats(steadiness)

func _init() -> void:
	coreScript = preload("res://scripts/placeable/cs/BoxCore.cs")

func _ready():
	if !isReinforcement:
		addCore()
