tool
extends "res://scripts/placeable/gd/PlayerBase.gd"

func _init():
	actorName = "Kadori"
	
	portraits = {
		"concernedContemplative": "res://sprites/UI/portraits/Kadori_concerned_contemplative.png",
		"concernedSad": "res://sprites/UI/portraits/Kadori_concerned_sad.png",
		"concernedTalking": "res://sprites/UI/portraits/Kadori_concerned_talking.png",
		"neutralSad": "res://sprites/UI/portraits/Kadori_neutral_sad.png",
		"neutralSmiling": "res://sprites/UI/portraits/Kadori_neutral_smiling.png",
		"neutralWondering": "res://sprites/UI/portraits/Kadori_neutral_wondering.png",
	}
	preparationsPortrait = preload("res://sprites/UI/portraits/Kadori_neutral_wondering.png")
	
	skillList = {
		9: {
			"index": 9,
			"isAcquired": true,
			"cost": 0,
			"borrowCost": 0,
			"dependencies": [],
		},
	}

func _enter_tree():
	attack = 3
	attackRange = 1
	attackType = Globals.gdTargetingType.Neighbour
	defense = 1
	isGangLeader = 0
	maxEndurance = 8
	endurance = 8
	maxEnergy = 10
	energy = 10
	movementRange = 4
	push = 2
	steadiness = 1
	turnRate = 1
	if turnOrder == -1:
		turnOrder = 1
	
	if gang == Globals.gdFactions.NoGang:
		gang = Globals.gdFactions.NoGangPlayer
	
	if sprite == "generic":
		sprite = "Kadori"
	if battlePortrait == null:
		battlePortrait = preload("res://sprites/UI/portraits/Kadori_neutral_wondering.png")
		
	defeatDialogues[Globals.gdDefeatReason.EnduranceDepleted] = "Sorry, I... can't..."
	defeatDialogues[Globals.gdDefeatReason.PushedOff] = "Ugh!"

func _ready():
	if !Engine.editor_hint:
		Core.gdSetActorAlliance(Globals.gdFactions.AlaitzsGang, 3)
		Core.gdSetActorAlliance(Globals.gdFactions.NoGangPlayer, 3)
