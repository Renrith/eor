tool
extends "res://scripts/placeable/gd/PlayerBase.gd"

func _init():
	actorName = "Naimel"
	
	portraits = {
		"angryClosedMouth": "res://sprites/UI/portraits/Naimel_angry_closedMouth.png",
		"angryOpenMouth": "res://sprites/UI/portraits/Naimel_angry_openMouth.png",
		"neutralFocused": "res://sprites/UI/portraits/Naimel_neutral_focused.png",
		"neutralInsecure": "res://sprites/UI/portraits/Naimel_neutral_insecure.png",
		"neutralSmiling": "res://sprites/UI/portraits/Naimel_neutral_smiling.png",
		"neutralTalking": "res://sprites/UI/portraits/Naimel_neutral_talking.png",
	}
	preparationsPortrait = preload("res://sprites/UI/portraits/Naimel_neutral_smiling.png")
	
	skillList = {
		3: {
			"index": 3,
			"isAcquired": true,
			"cost": 0,
			"borrowCost": 0,
			"dependencies": [],
		},
		4: {
			"index": 4,
			"isAcquired": false,
			"cost": 5,
			"borrowCost": 4,
			"dependencies": [3],
		},
		8: {
			"index": 8,
			"isAcquired": false,
			"cost": 4,
			"borrowCost": 3,
			"dependencies": [3],
		},
	}

func _enter_tree():
	attack = 3
	attackRange = 1
	attackType = Globals.gdTargetingType.Neighbour
	defense = 2
	isGangLeader = 0
	maxEndurance = 10
	endurance = 10
	maxEnergy = 8
	energy = 8
	movementRange = 4
	push = 6
	steadiness = 3
	turnRate = 3
	if turnOrder == -1:
		turnOrder = 3
	
	if gang == Globals.gdFactions.NoGang:
		gang = Globals.gdFactions.NoGangPlayer
	
	if sprite == "generic":
		sprite = "Naimel"
	if battlePortrait == null:
		battlePortrait = preload("res://sprites/UI/portraits/Naimel_neutral_focused.png")

func _ready():
	if !Engine.editor_hint:
		Core.gdSetActorAlliance(Globals.gdFactions.NePlusUltra, 0)
		Core.gdSetActorAlliance(Globals.gdFactions.AlaitzsGang, 3)
		Core.gdSetActorAlliance(Globals.gdFactions.NoGangPlayer, 3)
		Core.gdSetActorAlliance(Globals.gdFactions.EmeraldFront, 3)
		Core.gdSetActorAlliance(Globals.gdFactions.Aureos, 3)
