tool
extends "res://scripts/placeable/gd/PlayerBase.gd"

func _init():
	actorName = "Nalke"
	
	portraits = {
		"concernedHelpless": "res://sprites/UI/portraits/Nalke_concerned_helpless.png",
		"concernedSad": "res://sprites/UI/portraits/Nalke_concerned_sad.png",
		"neutralContemplative": "res://sprites/UI/portraits/Nalke_neutral_contemplative.png",
		"neutralExplaining": "res://sprites/UI/portraits/Nalke_neutral_explaining.png",
		"neutralGoodBoy": "res://sprites/UI/portraits/Nalke_neutral_goodBoy.png",
		"neutralReliable": "res://sprites/UI/portraits/Nalke_neutral_reliable.png",
	}
	preparationsPortrait = preload("res://sprites/UI/portraits/Nalke_neutral_reliable.png")
	
	skillList = {
		2: {
			"index": 2,
			"isAcquired": true,
			"cost": 0,
			"borrowCost": 0,
			"dependencies": [],
		},
		6: {
			"index": 6,
			"isAcquired": true,
			"cost": 5,
			"borrowCost": 4,
			"dependencies": [4],
		},
	}

func _enter_tree():
	attack = 4
	attackRange = 1
	attackType = Globals.gdTargetingType.Neighbour
	defense = 3
	isGangLeader = 0
	maxEndurance = 20
	endurance = 20
	maxEnergy = 10
	energy = 10
	movementRange = 5
	push = 3
	steadiness = 3
	turnRate = 3
	if turnOrder == -1:
		turnOrder = 3
	
	if gang == Globals.gdFactions.NoGang:
		gang = Globals.gdFactions.NoGangPlayer

	if sprite == "generic":
		sprite = "Nalke"
	if battlePortrait == null:
		battlePortrait = preload("res://sprites/UI/portraits/Nalke_neutral_reliable.png")
