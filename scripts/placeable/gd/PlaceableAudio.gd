extends "res://scripts/gd/SFXAudio.gd"

export(AudioStream) var getPushedSound = null
export(AudioStream) var fallSound = null

var callback: FuncRef

func getPushed() -> void:
	stop()
	stream = getPushedSound
	play()

func fall() -> void:
	stop()
	stream = fallSound
	play()

func onSoundFinished() -> void:
	if callback != null && callback.is_valid():
		callback.call_func()

func stop():
	callback = null
	.stop()

func _ready() -> void:
	connect("finished", self, "onSoundFinished")
