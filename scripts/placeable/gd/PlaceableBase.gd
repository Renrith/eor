tool
extends Node2D

var coreScript: Resource = preload("res://scripts/placeable/cs/PlaceableCore.cs")

var Core: Node
var Tween: Tween
var Sprite: AnimatedSprite
var AltSprite: AnimatedSprite

var outlineSpriteMaterial = preload("res://shaders/placeable/spriteOutlineOnly.tres")
var altSpriteMaterial = preload("res://shaders/placeable/spriteSemiTransparent.tres").duplicate()

export(bool) var isReinforcement = false
export(int) var steadiness = -1

var Cursor: Sprite
var Map: TileMap

func previewGetPushed(pathTiles, extendedPushDirection = "", hasHoldTile = false):
	if pathTiles.size() > 1 || extendedPushDirection != "":
		AltSprite = $AnimatedSprite.duplicate()
		for child in AltSprite.get_children():
			AltSprite.remove_child(child)
		
		Tween.previewGetPushed(pathTiles, extendedPushDirection, hasHoldTile)
		
		AltSprite.material = altSpriteMaterial
		AltSprite.playing = false
		
		$AnimatedSprite.material = outlineSpriteMaterial
		
		Map.YSort.add_child(AltSprite)
		AltSprite.z_index = AltSprite.position.y + 33

func undoPreviewGetPushed():
	Tween.remove_all()
	$AnimatedSprite.material = null
	if AltSprite != null:
		Map.YSort.remove_child(AltSprite)
		AltSprite = null

func getSpritePushed(pathTiles, direction = "", pushStrength = 0, hasHoldTile = false):
	Tween.getPushed(pathTiles, direction, pushStrength, hasHoldTile)

func restartSprite():
	$Tween.moveToInitialPosition()
	show()

func initializePosition():
	$Tween.altSpritePositionVector = $AnimatedSprite.position
	$Tween.initialPosition = position
	$Tween.initialOffset = Core.gdGetTile().gdOffset
	$Tween.initialZIndex = Map.map_to_world(Core.tile.cell).y
	$Tween.moveToInitialPosition()

func addCore():
	Core = coreScript.new()
	add_child(Core)

func addController():
	pass

func remove():
	Core.remove()
	Cursor.repaintEnemyRange()

func _ready():
	Sprite = $AnimatedSprite
	Tween = $Tween
	
	$AnimatedSprite.speed_scale = 0.33
	
	add_to_group("placeables")
	
	if !Engine.editor_hint:
		Cursor = get_node_or_null("/root/Viewports/SettingViewport/Map/YSort/Cursor")
		Map = get_node("/root/Viewports/SettingViewport/Map")
	else:
		$AnimatedSprite.position = $AnimatedSprite.position + Vector2(0, 16)
