tool
extends "res://scripts/gd/Tween.gd"

var direction: String = ""
var directionVector: Vector2
var altSpritePositionVector: Vector2

var previousPosition := Vector2.ZERO
var previousOffset := Vector2.ZERO
var previousZIndex := 0

var initialPosition: Vector2 = Vector2.ZERO
var initialOffset: Vector2 = Vector2.ZERO
var initialZIndex: int = 0

var endPathCallbacks = []

var Audio: AudioStreamPlayer2D

func interpolateNextPoint():
	if points.size() > 0:
		var point = points.pop_front()
		interpolate_property(movingNode, "position", movingNode.position, point.position, point.speed, point.transitionType, point.easeType)
		
		if point.modulate != Color.black:
			interpolate_property(movingNode.Sprite, "modulate", movingNode.Sprite.modulate, point.modulate, point.speed, TRANS_LINEAR, point.easeType)
		
		if point.zIndex == -1:
			interpolate_property(movingNode, "z_index", movingNode.z_index, point.position.y, point.speed, TRANS_LINEAR, point.easeType)
		else:
			interpolate_property(movingNode, "z_index", movingNode.z_index, point.zIndex, point.speed, TRANS_LINEAR, point.easeType)
		
		interpolate_property(movingNode.Sprite, "offset", movingNode.Sprite.offset, point.offset, point.speed, TRANS_CIRC, EASE_IN)
		start()
	else:
		endPath()

func addFallPoints(finalTile, speed) -> void:
	var secondPointSpeedMultiplier = 5
	var firstPoint = Point.new()
	var secondPoint = Point.new()
	
	var directionVector: Vector2 = Globals.getDirectionVector(direction)
	var firstDirectionVector: Vector2 = directionVector / 2
	var secondDirectionVector: Vector2 = directionVector * secondPointSpeedMultiplier / 3
	
	firstPoint.position = movingNode.Map.map_to_world(firstDirectionVector + finalTile.cell)
	firstPoint.offset = finalTile.gdOffset
	firstPoint.speed = speed / 2
	firstPoint.easeType = EASE_OUT
	
	secondPoint.position = movingNode.Map.map_to_world(secondDirectionVector + finalTile.cell)
	secondPoint.zIndex = Globals.getExtendedZIndex(secondPoint.position.y, direction)
	secondPoint.offset = firstPoint.offset + Vector2(0, 85 * secondPointSpeedMultiplier)
	secondPoint.modulate = Color(movingNode.Sprite.modulate.r, movingNode.Sprite.modulate.g, movingNode.Sprite.modulate.b, 0)
	secondPoint.speed = speed / secondPointSpeedMultiplier * 36
	secondPoint.easeType = EASE_OUT_IN
	secondPoint.transitionType = TRANS_QUART
	
	points.push_back(firstPoint)
	points.push_back(secondPoint)

func getHoldPoint(finalTile, speed):
	var directionVector: Vector2 = Globals.getDirectionVector(direction)
	var point = Point.new()
	
	point.position = Vector2(movingNode.Map.map_to_world(directionVector * 0.75 + finalTile.cell))
	point.offset = Vector2(0, 66 - movingNode.Core.gdGetTileOrHoldTile().gdOffset.y)
	point.speed = speed * 2
	
	point.zIndex = Globals.getExtendedZIndex(point.position.y, direction)
	
	return point

func previewGetPushed(fullPathTiles, pushDirection, hasHoldTile):
	previousPosition = movingNode.position
	previousOffset = movingNode.Sprite.offset
	previousZIndex = movingNode.z_index
	
	var speed: float = 0.0000001
	direction = pushDirection
	var finalTile: Reference = fullPathTiles[fullPathTiles.size() - 1]
	
	if direction == "below":
		movingNode.AltSprite.hide()
	elif hasHoldTile:
		var holdPoint = getHoldPoint(finalTile, speed)
		movingNode.AltSprite.position = holdPoint.position
		movingNode.AltSprite.offset = holdPoint.offset
		movingNode.AltSprite.z_index = holdPoint.zIndex
	elif direction != "":
		directionVector = Globals.getDirectionVector(direction)
		movingNode.AltSprite.position = Vector2(movingNode.Map.map_to_world(directionVector + finalTile.cell)) + altSpritePositionVector
		movingNode.AltSprite.offset = finalTile.gdOffset
		movingNode.AltSprite.z_index = Globals.getExtendedZIndex(finalTile.position.y, direction)
	else:
		movingNode.AltSprite.position = Vector2(movingNode.Map.map_to_world(finalTile.cell) + altSpritePositionVector)
		movingNode.AltSprite.offset = finalTile.gdOffset
		movingNode.AltSprite.z_index = finalTile.position.y
	
	direction = ""
	
	interpolateNextPoint()

func getPushed(fullPathTiles, pushDirection, pushStrength, hasHoldTile):
	var passedTiles: int = 1
	var levelDifference: int = 0
	var speed: float = 0.04
	var prevTile: Reference = null
	direction = pushDirection
	
	for tile in fullPathTiles:
		if pushStrength <= 0:
			if levelDifference > 0:
				levelDifference -= 1
				if levelDifference == 0:
					points.push_back(getPoint(tile.position, tile.gdOffset, speed * passedTiles * 3))
					passedTiles = 0
			elif prevTile != null && prevTile.heightLevel > tile.heightLevel:
				levelDifference = prevTile.heightLevel - tile.heightLevel - 1
				points.push_back(getPoint(prevTile.position, prevTile.gdOffset, speed * passedTiles))
				passedTiles = 0
			prevTile = tile
		passedTiles += 1
		pushStrength -= 1
	
	var finalTile: Reference = fullPathTiles[fullPathTiles.size() - 1]
	points.push_back(getPoint(finalTile.position, finalTile.gdOffset, speed * passedTiles))
	
	if hasHoldTile:
		points.push_back(getHoldPoint(finalTile, speed))
	elif direction != "":
		addFallPoints(finalTile, speed)
	
	if !is_active():
		interpolateNextPoint()
	
	if direction == "":
		Audio.fall()
	else:
		Audio.getPushed()
	
	direction = ""

func moveToInitialPosition():
	points.clear()
	addPoint(initialPosition, initialOffset, 0.0000001, initialZIndex)
	points[0].modulate = Color.white
	interpolateNextPoint()

func _ready():
	Audio = movingNode.get_node("Audio")
