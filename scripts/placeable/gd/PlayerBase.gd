tool
extends "res://scripts/placeable/gd/ActorBase.gd"

export(String) var meetTarget = ""

var buildScript: Resource = preload("res://scripts/cs/Build.cs")

var preparationsPortrait: StreamTexture

var skillList: Dictionary
var skillPoints: int = -1

var buildDataList: Array = []
var buildList: Array = []
var buildPoints: int = -1
var currentBuildIndex: int = 0

var defeatDialogues: Dictionary = {
	Globals.gdDefeatReason.EnduranceDepleted: "",
	Globals.gdDefeatReason.PushedOff: "",
	Globals.gdDefeatReason.Other: "",
}
var victoryDialogues: String = ""

func acquireSkill(skillIndex: int) -> void:
	skillList[skillIndex].isAcquired = true
	skillPoints -= skillList[skillIndex].cost

func createBuild() -> Reference:
	var build: Reference = buildScript.new("Build", preparationsPortrait)
	buildList.push_back(build)
	return build

func getBuild(buildIndex: int = -1) -> Reference:
	var build: Reference
	if buildIndex == -1:
		build = buildList[currentBuildIndex]
	else:
		build = buildList[buildIndex]
	return build

func equipBuild(buildIndex: int) -> void:
	currentBuildIndex = buildIndex
	Core.initializeBuild(buildList[currentBuildIndex])

func deleteBuild(buildIndex: int) -> void:
	buildList.remove(buildIndex)

func getRemainingBuildPoints(build: Reference) -> int:
	var remainingBuildPoints: int = buildPoints
	var borrowedSkillList: Array = build.gdGetBorrowedSkills()
	for skillIndex in borrowedSkillList:
		remainingBuildPoints -= skillList[skillIndex].borrowCost
	return remainingBuildPoints

func initializeBuilds() -> void:
	for i in range(0, buildDataList.size()):
		var build: Reference = createBuild()
		
		if buildDataList[i].attackSkill != null:
			build.gdSetSkill(buildDataList[i].attackSkill)
		if buildDataList[i].pushSkill != null:
			build.gdSetSkill(buildDataList[i].pushSkill)
		if buildDataList[i].actionSkill != null:
			build.gdSetSkill(buildDataList[i].actionSkill)
		if buildDataList[i].attributeSkills != null:
			for attributeSkill in buildDataList[i].attributeSkills:
				build.gdSetSkill(attributeSkill)
		if buildDataList[i].portrait != null:
			var portrait: StreamTexture = load(portraits[buildDataList[i].portrait])
			build.gdSetPortrait(portrait)
		if buildDataList[i].name != null:
			build.name = buildDataList[i].name

func addController() -> void:
	.addController()

func _init() -> void:
	coreScript = preload("res://scripts/placeable/cs/PlayerCore.cs")

func _enter_tree() -> void:
	buildDataList = Game.actorData[actorName].buildDataList
	buildPoints = Game.actorData[actorName].buildPoints
	skillPoints = Game.actorData[actorName].skillPoints
	for skillIndex in Game.actorData[actorName].skills:
		skillList[int(skillIndex)].isAcquired = true

func _ready() -> void:
	initializeBuilds()
	equipBuild(Game.actorData[actorName].currentBuildIndex)
	if !Engine.editor_hint:
		Core.gdSetActorAlliance(Globals.gdFactions.AlaitzsGang, 3)
