tool
extends "res://scripts/placeable/gd/ActorController.gd"

signal playerMenu(menuOptions)
signal playerMove(position, camera)
signal playerUndoMove(previousTile)
signal playerTargets
signal playerUndoActionOutcome(position)
signal meet

var startTurnTimer: Timer = Timer.new()

var isInMenu: bool = false

var previousTile: Reference

var attackTiles: Array = []
var pushTiles: Array = []
var skillTiles: Array = []
var gripTiles: Array = []
var meetTiles: Array = []

var targetTiles: Array setget, getTargetTiles

var targetListIndex: int = 0

# Getters and setters ----------------------------------------------------------

func getTargetTiles() -> Array:
	match action:
		"attack":
			return attackTiles
		"push":
			return pushTiles
		"meet":
			return meetTiles
		_:
			return skillTiles

# End of getters and setters ---------------------------------------------------

func startTurn() -> void:
	if !Battle.isAITakingAction:
		.startTurn()
		if Core.gdIsAlive():
			startVisualTurn()
			Map.calculateMovementRange(Core, true, false)
		else:
			Base.remove()
			Battle.endActorTurn()
	else:
		startTurnTimer.set_wait_time(0.05)
		startTurnTimer.start()

func endTurn() -> void:
	hasMoved = false
	isInMenu = false
	attackTiles = []
	pushTiles = []
	skillTiles = []
	attackOutcomeList = []
	pushOutcomeList = []
	gripTiles = []
	previousTile = null
	endVisualTurn()
	.endTurn()

func handleAcceptInput(cursorTile):
	if cursorTile.passable && cursorTile.occupant == null && cursorTile.actorDistance != -1:
		hasMoved = true
		emit_signal("playerMove", Base.position, Camera)
		previousTile = Core.tile
		Base.updatePosition(cursorTile)
		Map.clearPaintedTiles()
	elif Core.gdIsActive() && (cursorTile == Core.gdGetTile() || Map.areNeighbours(cursorTile, Core.gdGetTile())):
		handlePlayerMenu(cursorTile)
	elif !Core.gdIsActive() && cursorTile == Core.gdGetHoldTile():
		handleHoldMenu()

func handleCancelInput():
	if isInMenu:
		isInMenu = false
	
	if hasMoved:
		Base.Tween.remove_all()
		Base.updatePosition(previousTile, "undo")
		emit_signal("playerUndoMove", previousTile)
		hasMoved = false

func handlePlayerMenu(_cursorTile = null):
	Map.clearPaintedTiles()
	isInMenu = true
	
	var actions: Array = []
	pushTiles = Core.getAdjacentPushTiles()
	attackTiles = Core.getAdjacentAttackTiles()
	skillTiles = Core.gdGetAdjacentSkillTiles()
	gripTiles = Core.getAdjacentGripTiles()
	if Base.get("meetTarget") && Base.meetTarget != "":
		meetTiles = Core.getMeetTiles(Base.meetTarget)
	
	if attackTiles.size() > 0 && (Core.attackType != 2 || !hasMoved):
		actions.push_back("attack")
	
	if pushTiles.size() > 0:
		actions.push_back("push")
	
	if gripTiles.size() > 0 && Core.energy > 0:
		actions.push_back("grip")
	
	if Core.hasActionSkill && skillTiles.size() > 0:
		actions.push_back(Core.actionSkillName)
	
	if Base.get("meetTarget") && Base.meetTarget != "" && Battle.shouldMeet && meetTiles.size() > 0:
		actions.push_back("meet")
	
	actions.push_back("end turn")
	emit_signal("playerMenu", actions)

func handleHoldMenu():
	var actions: Array = []
	
	if Core.holdTile.occupant == null:
		actions.push_back("climb")
	
	actions.push_back("end turn")
	emit_signal("playerMenu", actions)

func handleSelectedAction(menuAction):
	action = menuAction
	match action:
		"climb", "grip", "convert", "end turn":
			executeSelectedAction()
		_:
			emit_signal("playerTargets")

func hasAdditionalTargets():
	return getTargetTiles().size() > 1 || getOutcomeList().size() > 1

func getTargetUnitTile() -> Reference:
	return getOutcomeList()[targetListIndex].target.Core.gdGetTileOrHoldTile()

func getCursorTile() -> Reference:
	if action == "push":
		var path: Array = pushOutcomeList[targetListIndex].path
		for i in range(1, path.size()):
			if path[path.size() - i] != null:
				return path[path.size() - i]
	
	return getTargetUnitTile()

func handleActionForTarget(index = 0, increase = 0):
	targetTile = getTargetTiles()[index]
	var cursorTile: Reference = targetTile
	var directionVector := Vector2.ZERO
	
	match action:
		"attack":
			attackOutcomeList = Core.gdGetAttackOutcome(targetTile)
			if increase == 1:
				targetListIndex = 0
			else:
				targetListIndex = attackOutcomeList.size() - 1
			cursorTile = getCursorTile()
			emit_signal("actionOutcome", self, cursorTile, action, hasAdditionalTargets(), attackOutcomeList[targetListIndex])
		"push":
			handlePushPreview(increase)
			cursorTile = getCursorTile()
			if pushOutcomeList[targetListIndex].holdTile == null:
				directionVector = Globals.getDirectionVector(pushOutcomeList[targetListIndex].extendedPushDirection)
			var targetUnitTile: Reference = getTargetUnitTile()
			emit_signal("actionOutcome", self, targetUnitTile, action, hasAdditionalTargets(), pushOutcomeList[targetListIndex])
		_:
			emit_signal("actionOutcome", self, cursorTile, action, hasAdditionalTargets(), null)
	Cursor.setVisualPosition(cursorTile.cursorOffset, Map.map_to_world(cursorTile.cell + directionVector))
	Core.paintActionPreview(targetTile, action)

func shouldNavigateTargetList(increase = 0):
	if getOutcomeList().size() > 1 && (targetListIndex + increase > -1 && getOutcomeList().size() > targetListIndex + increase || getTargetTiles().size() == 1):
		return true
	return false

func handleTargetListNavigation(increase = 0):
	targetListIndex += increase
	if targetListIndex == -1:
		targetListIndex = getOutcomeList().size() - 1
	elif getOutcomeList().size() == targetListIndex:
		targetListIndex = 0
	
	var targetUnitTile: Reference = getTargetUnitTile()
	var cursorTile: Reference = getCursorTile()
	var directionVector := Vector2.ZERO
	if action == "push" && pushOutcomeList[targetListIndex].holdTile == null:
		directionVector = Globals.getDirectionVector(pushOutcomeList[targetListIndex].extendedPushDirection)
	
	emit_signal("actionOutcome", self, targetUnitTile, action, hasAdditionalTargets(), getOutcomeList()[targetListIndex])
	Cursor.setVisualPosition(cursorTile.cursorOffset, Map.map_to_world(cursorTile.cell + directionVector))

func executeSelectedAction():
	var targetCore: Node = null
	if targetTile && targetTile.occupant != null:
		targetCore = targetTile.occupant
	var shouldShowBattleDialogues = Battle.callDialogues(Core, targetCore, action, 0)
	var shouldShowMeetDialogues = action == "meet" && Battle.shouldMeet
	
	if action == "push":
		undoPushPreview()
	
	if shouldShowMeetDialogues:
		emit_signal("meet")
	else:
		if !shouldShowBattleDialogues:
			.executeSelectedAction()
			endTurn()

func handlePushPreview(increase: int):
	undoPushPreview()
	pushOutcomeList = getPushOutcome()
	if increase == 1:
		targetListIndex = 0
	else:
		targetListIndex = pushOutcomeList.size() - 1
	
	for pushOutcome in pushOutcomeList:
		var hasHoldTile: bool = pushOutcome.holdTile != null
		pushOutcome.target.previewGetPushed(pushOutcome.path, pushOutcome.extendedPushDirection, hasHoldTile)

func undoPushPreview():
	Base.position = Core.gdGetTile().position
	for pushOutcome in pushOutcomeList:
		pushOutcome.target.undoPreviewGetPushed()
	Map.clearPaintedTiles()
	pushOutcomeList = []
	targetListIndex = 0
	pushDirection = ""

func undoActionPreview():
	if action == "push":
		undoPushPreview()
	else:
		Map.clearPaintedTiles()
	handlePlayerMenu()
	emit_signal("playerUndoActionOutcome", Base.position)

func _ready():
	if !Engine.editor_hint:
		Core.isPlayer = true
	
	startTurnTimer.connect("timeout", self, "startTurn")
	startTurnTimer.set_one_shot(true)
	add_child(startTurnTimer)
	
	var Setting = get_node("/root/Setting")
	var SettingCamera: Camera2D = get_node("/root/Viewports/SettingViewport/SettingCamera")
	var UI: Node = get_node("/root/Viewports/UIViewport/BattleUILayer")
	
	connect("visualTurnStart", Cursor, "startVisualTurn")
	
	connect("playerMenu", Cursor, "handlePlayerMenu")
	connect("playerMenu", UI, "showPlayerMenu")
	
	connect("playerMove", SettingCamera, "prepareMove")
	connect("playerMove", Cursor, "handlePlayerMove")
	
	connect("playerUndoMove", Cursor, "movePosition")
	connect("playerUndoMove", SettingCamera, "undoMove")
	
	connect("playerTargets", Cursor, "handleTargetNavigation")
	
	connect("playerUndoActionOutcome", SettingCamera, "undoPreview")
	connect("playerUndoActionOutcome", UI, "clearContextMenu")
	
	connect("visualTurnEnd", SettingCamera, "setTurnEndCamera")
	connect("visualTurnEnd", Cursor, "hide")
	connect("visualTurnEnd", UI, "onPlayerTurnEnd")
	
	connect("meet", Setting, "interpolateMeetDialogues")
