tool
extends "res://scripts/placeable/gd/PlayerBase.gd"

func _init():
	actorName = "Rinuri"
	
	portraits = {
		"neutralExplaining": "res://sprites/UI/portraits/Rinuri_neutral_explaining.png",
		"neutralMischievous": "res://sprites/UI/portraits/Rinuri_neutral_mischievous.png",
		"neutralSerious": "res://sprites/UI/portraits/Rinuri_neutral_serious.png",
		"neutralSmile": "res://sprites/UI/portraits/Rinuri_neutral_smile.png",
	}
	preparationsPortrait = preload("res://sprites/UI/portraits/Rinuri_neutral_mischievous.png")
	
	skillList = {}

func _enter_tree():
	attack = 12
	attackRange = 1
	attackType = Globals.gdTargetingType.Neighbour
	defense = 5
	isGangLeader = 0
	maxEndurance = 30
	endurance = 30
	maxEnergy = 25
	energy = 25
	movementRange = 5
	push = 3
	steadiness = 5
	turnRate = 3
	if turnOrder == -1:
		turnOrder = 3
	
	if gang == Globals.gdFactions.NoGang:
		gang = Globals.gdFactions.NoGangPlayer
	
	if sprite == "generic":
		sprite = "Rinuri"
	if battlePortrait == null:
		battlePortrait = preload("res://sprites/UI/portraits/Rinuri_neutral_mischievous.png")
		
	defeatDialogues[Globals.gdDefeatReason.EnduranceDepleted] = "What the hell was that!?"
	defeatDialogues[Globals.gdDefeatReason.PushedOff] = "Ah well...!"

func _ready():
	if !Engine.editor_hint:
		Core.gdSetActorAlliance(Globals.gdFactions.AlaitzsGang, 3)
		Core.gdSetActorAlliance(Globals.gdFactions.NoGangPlayer, 3)
