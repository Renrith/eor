tool
extends "res://scripts/placeable/gd/PlayerBase.gd"

func _init():
	actorName = "Tara"
	
	portraits = {
		"coolConfident": "res://sprites/UI/portraits/Tara_cool_confident.png",
		"coolSmiling": "res://sprites/UI/portraits/Tara_cool_smiling.png",
		"dismissiveGrin": "res://sprites/UI/portraits/Tara_dismissive_grin.png",
		"dismissiveSerious": "res://sprites/UI/portraits/Tara_dismissive_serious.png",
		"edgyGrin": "res://sprites/UI/portraits/Tara_edgy_grin.png",
		"edgySerious": "res://sprites/UI/portraits/Tara_edgy_serious.png",
		"edgyTalking": "res://sprites/UI/portraits/Tara_edgy_talking.png",
	}
	preparationsPortrait = preload("res://sprites/UI/portraits/Tara_edgy_grin.png")
	
	skillList = {
		5: {
			"index": 5,
			"isAcquired": true,
			"cost": 0,
			"borrowCost": 0,
			"dependencies": [],
		},
		1: {
			"index": 1,
			"isAcquired": true,
			"cost": 5,
			"borrowCost": 4,
			"dependencies": [5],
		},
		3: {
			"index": 3,
			"isAcquired": false,
			"cost": 3,
			"borrowCost": 3,
			"dependencies": [5],
		},
	}
	
	defeatDialogues[Globals.gdDefeatReason.EnduranceDepleted] = "That... hurts..."
	defeatDialogues[Globals.gdDefeatReason.PushedOff] = "Got too careless..."

func _enter_tree():
	attack = 4
	attackRange = 1
	attackType = Globals.gdTargetingType.Slash
	defense = 2
	isGangLeader = 0
	maxEndurance = 15
	endurance = 15
	maxEnergy = 9
	energy = 9
	movementRange = 4
	push = 3
	steadiness = 2
	turnRate = 2
	if turnOrder == -1:
		turnOrder = 2
	
	if gang == Globals.gdFactions.NoGang:
		gang = Globals.gdFactions.NoGangPlayer
	
	if sprite == "generic":
		sprite = "Tara"
	if battlePortrait == null:
		battlePortrait = preload("res://sprites/UI/portraits/Tara_edgy_grin.png")
