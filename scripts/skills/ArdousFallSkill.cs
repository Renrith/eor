using System.Collections.Generic;

public sealed class ArdousFallSkill : Skill, IActionModifier
{
	public AllActions action
	{
		get { return AllActions.push; }
	}

	public ActionPhase phase
	{
		get { return ActionPhase.Act; }
	}

	public List<ActionOutcome> calculateModifierOutcome(List<ActionOutcome> outcomeList, Tile tile, ActorCore actor)
	{
		foreach(ActionOutcome outcome in outcomeList)
		{
			outcome.heightLevelChange = outcome.heightLevelChange * 2;
		}
		return outcomeList;
	}

	public ArdousFallSkill() : base("heavy fall", "Doubles fall damage.", "res://sprites/skills/ardousFallSkill.png")
	{
		type = SkillType.ActionModifier;
	}
}
