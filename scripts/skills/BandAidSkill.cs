using System.Collections.Generic;

public sealed class BandAidSkill : Skill, INewAction
{
	public TargetAlliance targetableActorType
	{
		get { return TargetAlliance.Ally; }
	}

	public TargetingType targetingType
	{
		get { return TargetingType.Neighbour; }
	}

	public int range
	{
		get { return -1; }
	}

	private int enduranceRecovered = 5;
	private int energySpent = 3;

	public void initialize(ActorCore actor) { }

	public List<ActionOutcome> calculateOutcome(Tile tile, Tile origin)
	{
		List<ActionOutcome> outcomeList = new List<ActionOutcome>();
		ActionOutcome outcome = new ActionOutcome((ActorCore)tile.occupant, enduranceRecovered);

		ActionOutcome actorOutcome = new ActionOutcome((ActorCore)origin.occupant);
		actorOutcome.energyChange = -energySpent;
		outcomeList.Add(actorOutcome);

		outcomeList.Add(outcome);
		return outcomeList;
	}

	public BandAidSkill() : base("band aid", "Recovers 5 endurance to an adjacent ally.", "res://sprites/skills/bandAidSkill.png")
	{
		type = SkillType.NewAction;
	}
}
