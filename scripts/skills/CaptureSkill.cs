using System.Collections.Generic;

public sealed class CaptureSkill : Skill, INewAction
{
	public TargetAlliance targetableActorType
	{
		get { return TargetAlliance.NeutralRival; }
	}

	public TargetingType targetingType
	{
		get { return TargetingType.Neighbour; }
	}

	public int range
	{
		get { return 1; }
	}

	public void initialize(ActorCore actor) { }

	public List<ActionOutcome> calculateOutcome(Tile tile, Tile origin)
	{
		List<ActionOutcome> outcomeList = new List<ActionOutcome>();

		ActionOutcome outcome = new ActionOutcome((ActorCore)tile.occupant);
		if(origin.occupant is PlayerCore)
		{
			outcome.gang = Factions.allFactions[(int)Factions.names.AlaitzsGang];
		}
		else
		{
			outcome.gang = ((ActorCore)origin.occupant).gang;
		}
		outcomeList.Add(outcome);

		return outcomeList;
	}

	public CaptureSkill() : base("recruit", "Turns a neutral unit into an ally.", "res://sprites/skills/captureSkill.png")
	{
		type = SkillType.NewAction;
	}
}
