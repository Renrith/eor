using System.Collections.Generic;

public sealed class CounterSkill : Skill, IActionModifier
{
	public AllActions action
	{
		get { return AllActions.attack; }
	}

	public ActionPhase phase
	{
		get { return ActionPhase.Defend; }
	}

	public List<ActionOutcome> calculateModifierOutcome(List<ActionOutcome> outcomeList, Tile tile, ActorCore actor)
	{
		if(tile.occupant is ActorCore target && target.isActorAttackAdjacent(actor))
		{
			int attackStrength = actor.getAttackStrength(target);
			ActionOutcome outcome = new ActionOutcome(target, -attackStrength);
			outcomeList.Add(outcome);
		}
		return outcomeList;
	}

	public CounterSkill() : base("counter", "Retaliates after enemy attack.", "res://sprites/skills/counterSkill.png")
	{
		type = SkillType.ActionModifier;
	}
}
