using System.Collections.Generic;

public sealed class MomentumSkill : Skill, IActionModifier
{
	public AllActions action
	{
		get { return AllActions.push; }
	}

	public ActionPhase phase
	{
		get { return ActionPhase.Act; }
	}

	public List<ActionOutcome> calculateModifierOutcome(List<ActionOutcome> outcomeList, Tile tile, ActorCore actor)
	{
		if(tile != actor.tile)
		{
			Direction direction = tile.getNeighbourDirection(actor.tile);
			List<ActionOutcome> moveBackList = Map.getPushPath(actor, actor, direction, false, 1);
			foreach(ActionOutcome outcome in moveBackList)
			{
				outcomeList.Add(outcome);
			}
		}
		return outcomeList;
	}

	public MomentumSkill() : base("momentum", "Propells self one tile back when pushing.", "res://sprites/skills/momentumSkill.png")
	{
		type = SkillType.ActionModifier;
	}
}
