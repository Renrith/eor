using System.Collections.Generic;

// This does not work during simulation.
// Simulation always simulates actor turns in the same order!
public sealed class PassBatonSkill : Skill, INewAction
{
	public TargetAlliance targetableActorType
	{
		get { return TargetAlliance.Ally; }
	}

	public TargetingType targetingType
	{
		get { return TargetingType.Neighbour; }
	}

	public int range
	{
		get { return -1; }
	}

	private int energySpent = 2;

	public void initialize(ActorCore actor) { }

	public List<ActionOutcome> calculateOutcome(Tile tile, Tile origin)
	{
		List<ActionOutcome> outcomeList = new List<ActionOutcome>();

		ActionOutcome outcome = new ActionOutcome((ActorCore)tile.occupant);
		outcome.turnOrderChange -= ((ActorCore)origin.occupant).turnOrder + 1;
		outcomeList.Add(outcome);

		ActionOutcome actorOutcome = new ActionOutcome((ActorCore)origin.occupant);
		actorOutcome.energyChange = -energySpent;
		outcomeList.Add(actorOutcome);

		return outcomeList;
	}

	public PassBatonSkill() : base("pass baton", "Allows an ally to act next turn.", "res://sprites/skills/passBatonSkill.png")
	{
		type = SkillType.NewAction;
	}
}
