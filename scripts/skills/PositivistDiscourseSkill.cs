using System.Collections.Generic;

public sealed class PositivistDiscourseSkill : Skill, INewAction
{
	public TargetAlliance targetableActorType
	{
		get { return TargetAlliance.Ally; }
	}

	public TargetingType targetingType
	{
		get { return TargetingType.RangedSkip; }
	}

	public int range
	{
		get { return 4; }
	}

	private int enduranceRecovered = 3;
	private int energySpent = 4;

	public void initialize(ActorCore actor) { }

	public List<ActionOutcome> calculateOutcome(Tile tile, Tile origin)
	{
		List<ActionOutcome> outcomeList = new List<ActionOutcome>();
		ActionOutcome outcome = new ActionOutcome((ActorCore)tile.occupant, enduranceRecovered);
		outcomeList.Add(outcome);

		ActionOutcome actorOutcome = new ActionOutcome((ActorCore)origin.occupant);
		actorOutcome.energyChange = -energySpent;
		outcomeList.Add(actorOutcome);

		return outcomeList;
	}

	public PositivistDiscourseSkill() : base("positivist discourse", "Recovers 3 endurance to an ally within 4 tiles.", "res://sprites/skills/positivistDiscourseSkill.png")
	{
		type = SkillType.NewAction;
	}
}
