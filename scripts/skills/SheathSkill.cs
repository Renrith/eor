using System.Collections.Generic;

public sealed class SheathSkill : Skill, INewAction
{
	public TargetAlliance targetableActorType
	{
		get { return TargetAlliance.Self; }
	}

	public TargetingType targetingType
	{
		get { return TargetingType.Self; }
	}

	public int range
	{
		get { return -1; }
	}

	private Buff sheathAttackBuff = new Buff(3);
	private Buff sheathPushBuff = new Buff(-2);

	public void initialize(ActorCore actor)
	{
		actor.State.attackBuffs.Add(sheathAttackBuff);
		actor.State.pushBuffs.Add(sheathPushBuff);
	}

	public List<ActionOutcome> calculateOutcome(Tile tile, Tile origin)
	{
		List<ActionOutcome> outcomeList = new List<ActionOutcome>();
		ActionOutcome outcome = new ActionOutcome((ActorCore)origin.occupant);

		if(origin.occupant.State.attackBuffs.Count == 0 || origin.occupant.State.pushBuffs.Count == 0 || origin.occupant.State.attackBuffs.IndexOf(sheathAttackBuff) == -1)
		{
			outcome.attackBuffsApplied = new List<Buff> { sheathAttackBuff };
			outcome.pushBuffsApplied = new List<Buff>() { sheathPushBuff };
		}
		else
		{
			outcome.attackBuffsRemoved = new List<Buff> { sheathAttackBuff };
			outcome.pushBuffsRemoved = new List<Buff> { sheathPushBuff };
		}

		outcomeList.Add(outcome);
		return outcomeList;
	}

	public SheathSkill() : base("sheath", "-3 attack, +2 push until unsheathed.", "res://sprites/skills/sheathSkill.png")
	{
		type = SkillType.NewAction;
	}
}
