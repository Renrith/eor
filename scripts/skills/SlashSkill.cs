using System.Collections.Generic;

public sealed class SlashSkill : Skill, IActionModifier
{
	public AllActions action
	{
		get { return AllActions.attack; }
	}

	public ActionPhase phase
	{
		get { return ActionPhase.Act; }
	}

	public List<ActionOutcome> calculateModifierOutcome(List<ActionOutcome> outcomeList, Tile tile, ActorCore actor)
	{
		outcomeList.Clear();

		List<ActorCore> targets = ActorCore.Map.getActorsInSlash(actor.tile, tile);
		foreach(ActorCore target in targets)
		{
			int attackStrength = actor.getAttackStrength(target);
			ActionOutcome outcome = new ActionOutcome(target, -attackStrength);
			outcomeList.Add(outcome);
		}

		return outcomeList;
	}

	public SlashSkill() : base("slash", "Attacks the three tiles in front of self.", "res://sprites/skills/slashSkill.png")
	{
		type = SkillType.ActionModifier;
	}
}
