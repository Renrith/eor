using System.Collections.Generic;

public sealed class SprintSkill : Skill, INewAction
{
	public TargetAlliance targetableActorType
	{
		get { return TargetAlliance.Self; }
	}

	public TargetingType targetingType
	{
		get { return TargetingType.Neighbour; }
	}

	public int range
	{
		get { return -1; }
	}

	private int energySpent = 4;
	private int tileNumber = 4;

	public void initialize(ActorCore actor) { }

	public List<ActionOutcome> calculateOutcome(Tile tile, Tile origin)
	{
		List<ActionOutcome> outcomeList = new List<ActionOutcome>();
		ActionOutcome outcome = new ActionOutcome((ActorCore)origin.occupant);

		outcome.target = origin.occupant;
		outcome.energyChange = -energySpent;
		outcome.path = new List<Tile>();
		outcome.path.Add(origin);

		Direction direction = origin.getNeighbourDirection(tile);
		int tilesToMove = tileNumber;

		Tile nextTile = tile;

		while(tilesToMove > 0 && nextTile != null && nextTile.isTilePassable((ActorCore)origin.occupant) && Tile.isWithinReach(outcome.path[outcome.path.Count - 1], nextTile))
		{
			outcome.path.Add(nextTile);
			nextTile = nextTile.neighbours[direction];
			tilesToMove--;
		}

		outcomeList.Add(outcome);
		return outcomeList;
	}

	public SprintSkill() : base("sprint", "Moves up to 4 tiles in a straight line.", "res://sprites/skills/sprintSkill.png")
	{
		type = SkillType.NewAction;
	}
}
