shader_type canvas_item;

uniform vec4 color;
uniform vec4 outlineColor;
uniform sampler2D portraitMask;

void fragment()
{
	float sizeX = 1.0 / float(textureSize(TEXTURE, 0).x);
	float sizeY = 1.0 / float(textureSize(TEXTURE, 0).y);
	vec4 frameColor = texture(TEXTURE, UV);
	float alpha = -4.0 * texture(TEXTURE, UV).a;

	alpha += texture(TEXTURE, UV + vec2(sizeX, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(-sizeX, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(0, sizeY)).a;
	alpha += texture(TEXTURE, UV + vec2(0, -sizeY)).a;
	alpha *= texture(portraitMask, UV).a;
	
	vec4 finalColor = mix(outlineColor, color, frameColor.a);
	COLOR = vec4(finalColor.rgb, min(max(alpha, frameColor.a), max(outlineColor.a, frameColor.a)));
}
