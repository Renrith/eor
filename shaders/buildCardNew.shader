shader_type canvas_item;

uniform vec4 color;
uniform vec4 outlineColor;
uniform sampler2D frameMask;
uniform float centerOpacity = 0.2;

void fragment()
{
	float sizeX = 1.0 / float(textureSize(TEXTURE, 0).x);
	float sizeY = 1.0 / float(textureSize(TEXTURE, 0).y);
	vec4 frameColor = texture(TEXTURE, UV);
	float alpha = -4.0 * texture(TEXTURE, UV).a;

	alpha += texture(TEXTURE, UV + vec2(sizeX, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(-sizeX, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(0, sizeY)).a;
	alpha += texture(TEXTURE, UV + vec2(0, -sizeY)).a;
	alpha *= texture(frameMask, UV).a;
	
	vec4 finalColor = mix(outlineColor, color, frameColor.a);
	float finalOpacity = min(max(alpha, frameColor.a), max(outlineColor.a, frameColor.a));
	finalOpacity += (1.0 - texture(frameMask, UV).a) * centerOpacity;
	
	finalColor = vec4(finalColor.rgb, finalOpacity);
	COLOR = finalColor;
}
