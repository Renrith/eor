shader_type canvas_item;
render_mode unshaded;

const float width = 1.0;
uniform vec4 color: hint_color;

vec4 overlay(vec4 base, vec4 blend)
{
	vec4 limit = step(0.5, base);
	return mix(2.0 * base * blend, 1.0 - 2.0 * (1.0 - base) * (1.0 - blend), limit);
}

void fragment()
{
	float xSize = width / float(textureSize(TEXTURE, 0).x);
	float ySize = width / float(textureSize(TEXTURE, 0).y);
	
	vec4 screenColor = texture(SCREEN_TEXTURE, SCREEN_UV);
	vec4 spriteColor = texture(TEXTURE, UV);
	vec4 overlayColor = color;
	vec4 outlineColor = color;
	vec4 finalColor = spriteColor;
	
	float alpha = 0.0;
	
	alpha += texture(TEXTURE, UV + vec2(xSize, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(-xSize, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(0, ySize)).a;
	alpha += texture(TEXTURE, UV + vec2(0, -ySize)).a;
	alpha += texture(TEXTURE, UV + vec2(xSize / width, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(-xSize / width, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(0, ySize / width)).a;
	alpha += texture(TEXTURE, UV + vec2(0, -ySize / width)).a;
	
	alpha = clamp(alpha, 0, 1);
	
	overlayColor = overlay(color, screenColor);
	outlineColor = mix(overlayColor, color, 0.2);
	finalColor = mix(outlineColor, spriteColor, spriteColor.a);
	
	COLOR = vec4(finalColor.rgb, max(spriteColor.a, alpha));
}
