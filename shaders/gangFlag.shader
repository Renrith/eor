shader_type canvas_item;
render_mode unshaded;

const float lightRatio = 0.75;

uniform sampler2D shadow;
uniform vec4 color: hint_color = vec4(0.5, 0.8, 0.9, 1.0);
uniform vec4 lightColor: hint_color = vec4(0.3, 0.14, 0.29, 1.0);

vec4 hardLight(vec4 base, vec4 blend)
{
	vec4 limit = step(0.5, blend);
	return mix(2.0 * base * blend, 1.0 - 2.0 * (1.0 - base) * (1.0 - blend), limit);
}

void fragment()
{
	vec4 spriteColor = texture(TEXTURE, UV);
	vec4 gangColor = mix(color, hardLight(color, lightColor), lightRatio);
	vec4 unshadedColor;
	vec4 finalColor;
	
	float gangColorRatio = floor(spriteColor.a);
	unshadedColor = mix(gangColor, spriteColor, gangColorRatio);
	finalColor = unshadedColor * texture(shadow, UV);
	
	COLOR = vec4(finalColor.rgb, ceil(spriteColor.a));
}
