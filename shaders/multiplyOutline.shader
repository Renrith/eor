shader_type canvas_item;
render_mode unshaded;

uniform float width: hint_range(0.0, 5.0);
uniform vec4 outlineColor: hint_color;

void fragment()
{
	float xSize = width / float(textureSize(TEXTURE, 0).x);
	float ySize = width / float(textureSize(TEXTURE, 0).y);
	vec4 spriteColor = texture(TEXTURE, UV);
	vec4 finalColor = spriteColor;
	
	if(spriteColor.a < 0.2)
	{
		float alpha = 0.0;
		
		alpha += texture(TEXTURE, UV + vec2(xSize, 0)).a;
		alpha += texture(TEXTURE, UV + vec2(-xSize, 0)).a;
		alpha += texture(TEXTURE, UV + vec2(0, ySize)).a;
		alpha += texture(TEXTURE, UV + vec2(0, -ySize)).a;
		alpha += texture(TEXTURE, UV + vec2(xSize, ySize)).a;
		alpha += texture(TEXTURE, UV + vec2(-xSize, -ySize)).a;
		alpha += texture(TEXTURE, UV + vec2(-xSize, ySize)).a;
		alpha += texture(TEXTURE, UV + vec2(xSize, -ySize)).a;
		
		finalColor = outlineColor * texture(SCREEN_TEXTURE, UV);
		finalColor.a = min(alpha, outlineColor.a);
	}
	
	COLOR = finalColor;
}
