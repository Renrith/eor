shader_type canvas_item;

uniform vec4 color;

void fragment()
{
	float size = 1.0 / float(textureSize(TEXTURE, 0).x);
	vec4 spriteColor = texture(TEXTURE, UV);
	float alpha = -8.0 * spriteColor.a;

	alpha += texture(TEXTURE, UV + vec2(size, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(-size, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(0, size)).a;
	alpha += texture(TEXTURE, UV + vec2(0, -size)).a;
	alpha += texture(TEXTURE, UV + vec2(size, size)).a;
	alpha += texture(TEXTURE, UV + vec2(-size, -size)).a;
	alpha += texture(TEXTURE, UV + vec2(-size, size)).a;
	alpha += texture(TEXTURE, UV + vec2(size, -size)).a;
	
	vec4 finalColor = mix(spriteColor, color, spriteColor.a);
	
	COLOR = vec4(finalColor.rgb, max(alpha, finalColor.a * 0.8) * 0.8);
}