shader_type canvas_item;

uniform vec4 color;

void fragment()
{
	vec4 spriteColor = texture(TEXTURE, UV);
	vec4 gangColor;
	vec4 finalColor;
	
	float tone = spriteColor.r + spriteColor.g + spriteColor.b / 3.0;
	gangColor.r = 0.5 * color.r * tone;
	gangColor.g = 0.5 * color.g * tone;
	gangColor.b = 0.5 * color.b * tone;
	gangColor.a = 1.0;
	
	float gangColorRatio = floor(spriteColor.a);
	finalColor = mix(gangColor, spriteColor, gangColorRatio);
	
	COLOR = vec4(finalColor.rgb, ceil(spriteColor.a));
}