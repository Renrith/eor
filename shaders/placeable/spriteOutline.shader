shader_type canvas_item;
render_mode unshaded;

const float width = 2.0;
uniform vec4 color;
uniform vec4 outlineColor: hint_color;

void fragment()
{
	float xSize = width / float(textureSize(TEXTURE, 0).x);
	float ySize = width / float(textureSize(TEXTURE, 0).y);
	
	vec4 spriteColor = texture(TEXTURE, UV);
	vec4 gangColor;
	vec4 finalColor;
	
	float tone = spriteColor.r + spriteColor.g + spriteColor.b / 3.0;
	gangColor.r = 0.5 * color.r * tone;
	gangColor.g = 0.5 * color.g * tone;
	gangColor.b = 0.5 * color.b * tone;
	gangColor.a = 1.0;
	
	float gangColorRatio = floor(spriteColor.a);
	finalColor = mix(gangColor, spriteColor, 0.1);
	finalColor = mix(gangColor, spriteColor, gangColorRatio);
	finalColor = vec4(finalColor.rgb, ceil(spriteColor.a));
	
	float alpha = 0.0;
	
	alpha += texture(TEXTURE, UV + vec2(xSize, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(-xSize, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(0, ySize)).a;
	alpha += texture(TEXTURE, UV + vec2(0, -ySize)).a;
	alpha += texture(TEXTURE, UV + vec2(xSize / width, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(-xSize / width, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(0, ySize / width)).a;
	alpha += texture(TEXTURE, UV + vec2(0, -ySize / width)).a;
	alpha += texture(TEXTURE, UV + vec2(xSize / width, ySize / width)).a;
	alpha += texture(TEXTURE, UV + vec2(-xSize / width, -ySize / width)).a;
	alpha += texture(TEXTURE, UV + vec2(-xSize / width, ySize / width)).a;
	alpha += texture(TEXTURE, UV + vec2(xSize / width, -ySize / width)).a;
	
	alpha = clamp(ceil(alpha), 0, 1);
	
	finalColor = mix(outlineColor, finalColor, finalColor.a);
	
	COLOR = vec4(finalColor.rgb, max(ceil(spriteColor.a), alpha));
}
