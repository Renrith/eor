shader_type canvas_item;
render_mode unshaded;

uniform vec4 color = vec4(1.0, 1.0, 1.0, 1.0);
const float outlineSearchSize = 0.0;

float getAlpha(sampler2D baseTexture, vec2 currentUV, float xSize, float ySize)
{
	return floor(texture(baseTexture, currentUV + vec2(xSize, ySize)).a + 0.5);
}

void fragment()
{
	float size = 1.0 / float(textureSize(TEXTURE, 0).x);
	vec4 spriteColor = texture(TEXTURE, UV);
	float alpha = 0.0;

	alpha -= getAlpha(TEXTURE, UV, size, 0);
	alpha -= getAlpha(TEXTURE, UV, -size, 0);
	alpha -= getAlpha(TEXTURE, UV, 0, size);
	alpha -= getAlpha(TEXTURE, UV, 0, -size);
	alpha -= getAlpha(TEXTURE, UV, size, size);
	alpha -= getAlpha(TEXTURE, UV, -size, -size);
	alpha -= getAlpha(TEXTURE, UV, -size, size);
	alpha -= getAlpha(TEXTURE, UV, size, -size);
	
	alpha += getAlpha(TEXTURE, UV, size * outlineSearchSize, 0);
	alpha += getAlpha(TEXTURE, UV, -size * outlineSearchSize, 0);
	alpha += getAlpha(TEXTURE, UV, 0, size * outlineSearchSize);
	alpha += getAlpha(TEXTURE, UV, 0, -size * outlineSearchSize);
	alpha += getAlpha(TEXTURE, UV, size * outlineSearchSize, size * outlineSearchSize);
	alpha += getAlpha(TEXTURE, UV, -size * outlineSearchSize, -size * outlineSearchSize);
	alpha += getAlpha(TEXTURE, UV, -size * outlineSearchSize, size * outlineSearchSize);
	alpha += getAlpha(TEXTURE, UV, size * outlineSearchSize, -size * outlineSearchSize);
	
	COLOR = vec4(color.rgb, floor(clamp(alpha, 0, 1) + 0.5));
}