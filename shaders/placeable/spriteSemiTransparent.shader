shader_type canvas_item;

uniform vec4 color;
const float maxAlpha = 0.2;
const float minAlpha = 0.6;

void fragment()
{
	vec4 spriteColor = texture(TEXTURE, UV);
	vec4 gangColor;
	vec4 finalColor;
	
	float tone = spriteColor.r + spriteColor.g + spriteColor.b / 3.0;
	gangColor.r = 0.5 * color.r * tone;
	gangColor.g = 0.5 * color.g * tone;
	gangColor.b = 0.5 * color.b * tone;
	gangColor.a = 1.0;
	
	float gangColorRatio = floor(spriteColor.a);
	finalColor = mix(gangColor, spriteColor, 0.1);
	finalColor = mix(gangColor, spriteColor, gangColorRatio);
	finalColor = vec4(finalColor.rgb, ceil(spriteColor.a));
	
	COLOR = vec4(finalColor.rgb, (abs((cos(TIME) * maxAlpha)) + minAlpha) * finalColor.a);
}
