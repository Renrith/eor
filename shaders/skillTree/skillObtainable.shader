shader_type canvas_item;

uniform vec4 obtainableColor = vec4(0.7, 0.69, 0.68, 1);

void fragment()
{
	vec4 currentColor = texture(TEXTURE, UV);
	vec4 finalColor = mix(currentColor, obtainableColor, 0.47);
	COLOR = vec4(finalColor.rgb, currentColor.a);
}
