shader_type canvas_item;

uniform float width = 3.0;
uniform float borderSize = 1.0;
uniform vec4 color;
uniform vec4 outlineColor;
uniform vec4 white = vec4(1, 1, 1, 1);

void fragment()
{
	vec4 currentColor = texture(TEXTURE, UV);
	vec4 finalColor = currentColor;
	
	float currentSize = width;
	if(currentColor.a > 0.1)
	{
		while(currentSize > 0.0)
		{
			float xSize = currentSize / float(textureSize(TEXTURE, 0).x);
			float ySize = currentSize / float(textureSize(TEXTURE, 0).y);
			
			if(texture(TEXTURE, UV + vec2(xSize, 0)).a < 0.2
			|| texture(TEXTURE, UV + vec2(0, ySize)).a < 0.2
			|| texture(TEXTURE, UV + vec2(-xSize, 0)).a < 0.2
			|| texture(TEXTURE, UV + vec2(0, -ySize)).a < 0.2)
			{
				vec4 glowColor = white - (white - finalColor) * (white - color);
				finalColor = mix(finalColor, glowColor, 0.25);
				finalColor = mix(finalColor, color, 0.1);
				currentSize -= 1.0;
			}
			else
			{
				currentSize -= width;
			}
		}
	}
	else
	{
		float alpha = 0.0;
		
		float xSize = borderSize / float(textureSize(TEXTURE, 0).x);
		float ySize = borderSize / float(textureSize(TEXTURE, 0).y);
		
		alpha += texture(TEXTURE, UV + vec2(xSize, 0)).a;
		alpha += texture(TEXTURE, UV + vec2(-xSize, 0)).a;
		alpha += texture(TEXTURE, UV + vec2(0, ySize)).a;
		alpha += texture(TEXTURE, UV + vec2(0, -ySize)).a;
		
		finalColor = vec4(outlineColor.rgb, min(alpha, outlineColor.a));
	}
	
	COLOR = finalColor;
}
