shader_type canvas_item;
render_mode unshaded;

uniform vec4 color: hint_color = vec4(0.5, 0.8, 0.9, 1.0);

vec4 overlay(vec4 base, vec4 blend)
{
	vec4 limit = step(0.5, base);
	return mix(2.0 * base * blend, 1.0 - 2.0 * (1.0 - base) * (1.0 - blend), limit);
}

void fragment()
{
	vec4 spriteColor = texture(TEXTURE, UV);
	COLOR = color * spriteColor;
}
