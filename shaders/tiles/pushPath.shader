shader_type canvas_item;
render_mode unshaded;

uniform sampler2D stripes;
uniform float outlineHeight;
uniform vec4 color: hint_color = vec4(0.5, 0.8, 0.9, 1.0);
uniform vec4 backgroundColor: hint_color = vec4(0.2, 0.6, 0.7, 1.0);
const float textureRatio = 64.0/32.0;
const float timeRatio = 0.05;
const float lineColorRatio = 0.2;
const float backgroundRatio = 0.5;

vec4 overlay(vec4 base, vec4 blend)
{
	vec4 limit = step(0.5, base);
	return mix(2.0 * base * blend, 1.0 - 2.0 * (1.0 - base) * (1.0 - blend), limit);
}

void fragment()
{
	vec4 spriteColor = texture(TEXTURE, UV);
	
	float xSize = float(textureSize(TEXTURE, 0).x);
	float ySize = float(textureSize(TEXTURE, 0).y);
	float heightRatio = outlineHeight * TEXTURE_PIXEL_SIZE.y;
	float maskRatio = (ySize / xSize * textureRatio);
	float yMask = (UV.y + heightRatio) * maskRatio;
	float xMask = floor(UV.x * 64.0) / 64.0 + (TIME * timeRatio);
	
	vec4 screenColor = overlay(spriteColor, color);
	vec4 baseColor = mix(overlay(spriteColor, spriteColor), backgroundColor, backgroundRatio);
	float lineMixRatio = texture(stripes, vec2(xMask, yMask)).a * lineColorRatio;
	vec4 finalColor = mix(baseColor, screenColor, lineMixRatio);
	
	COLOR = vec4(finalColor.rgb, spriteColor.a);
}
