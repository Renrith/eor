shader_type canvas_item;
render_mode unshaded;

uniform sampler2D outlineMask;
uniform float outlineHeight;
uniform vec4 color: hint_color = vec4(0.1, 0.65, 0.85, 1.0);
const vec4 whiteColor = vec4(1.0, 1.0, 1.0, 1.0);
const float cursorRatio = 64.0/34.0;

void fragment()
{
	float timeRatio = abs(cos(TIME * 1.5));
	float brightness = (timeRatio / 3.0) + 0.9;
	vec4 spriteColor = texture(TEXTURE, UV) * COLOR;
	float midColorMixRatio = (timeRatio / 3.0) + 0.3;
	vec4 midColor = mix(spriteColor * brightness, (color / brightness), midColorMixRatio);
	
	float xSize = float(textureSize(TEXTURE, 0).x);
	float ySize = float(textureSize(TEXTURE, 0).y);
	float heightRatio = (outlineHeight + 1.0) * TEXTURE_PIXEL_SIZE.y;
	float maskRatio = (ySize / xSize * cursorRatio);
	float yMask = (UV.y + heightRatio) * maskRatio;
	float outlineMixRatio = texture(outlineMask, vec2(UV.x, yMask)).a;
	float overlapMixRatio = yMask < 0.5 ? abs(1.0 - (texture(TEXTURE, UV + vec2(0, -2.0 / ySize)).a)) : 1.0;
	vec4 modulateColor = COLOR == whiteColor ? COLOR : (COLOR / 1.10);
	vec4 finalColor = mix(midColor * modulateColor, color, clamp(outlineMixRatio * overlapMixRatio, 0, 1));
	
	COLOR = vec4(finalColor.rgb, spriteColor.a);
}