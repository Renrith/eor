shader_type canvas_item;

uniform vec4 color;

void fragment()
{
	vec4 finalColor = color * 0.5 * texture(TEXTURE, UV);
	COLOR = vec4(finalColor.rgb, finalColor.a);
}
