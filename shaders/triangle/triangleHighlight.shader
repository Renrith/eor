shader_type canvas_item;

uniform vec4 color;

void fragment()
{
	vec4 finalColor = color * texture(TEXTURE, UV);
	COLOR = vec4(finalColor.rgb, finalColor.a * 0.79);
}
